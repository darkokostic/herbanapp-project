@extends('layouts.non-admin')
@section('content')
    @include('layouts.header', ['slide' => false])

    @push('stylesheets')
    <link rel="stylesheet" href="{{ asset('css/clinic-list.css') }}">
    @endpush

    <div class="container has-herb-header" ng-controller="ClinicListCtrl">
        <div class="row">
            <div class="col-xs-12">
                <h4 class="page-title">Clinics</h4>
                <div class="clearfix"></div>
            </div>
            <div ng-cloak class="col-xs-12 col-sm-6 col-md-6 col-lg-3" ng-repeat="clinic in nearByClinics">
                <a class="card-link" href="/clinic/@{{ clinic.id }}">
                    <div class="card center-block">
                        <div class="overlay">
                            <div class="overlay-image"></div>
                            <img ng-if="clinic.cover" class="card-img-top" ng-src="@{{ clinic.cover.url }}">
                            <img ng-if="!clinic.cover" class="card-img-top" src="/assets/images/clinic/cover-image.png">
                        </div>
                        <div class="card-block">
                            <figure class="profile">
                                <img ng-if="clinic.logo" ng-src="@{{ clinic.logo.url }}" class="profile-avatar"
                                     alt="Clinic logo">
                                <img ng-if="!clinic.logo" src="assets/images/clinic/clinic-avatar.png"
                                     class="profile-avatar"
                                     alt="Clinic logo">
                            </figure>
                            <div class="profile-info">
                                <h1 class="clinic-name ng-binding">@{{clinic.name | limitTo: 14}}@{{(clinic.name && clinic.name.length > 16) ? '...' : ''}}</h1>
                                <star-rating-comp rating="clinic.average_rating" show-half-stars="true" read-only="true"
                                                  size="'medium'" label-position="'right'"
                                                  label-text="clinic.average_rating" class="ng-isolate-scope">
                                    <div id="314.4341908769421"
                                         class="rating value-2  hover-0 half label-visible label-right color-ok star-svg noticeable medium read-only"
                                         ng-class="$ctrl.getComponentClassNames()">
                                        <div ng-show="$ctrl.labelText" class="label-value ng-binding">2.5</div>
                                        <div class="star-container" ng-mouseleave="$ctrl.onStarHover()">
                                            <!-- ngRepeat: star in $ctrl.stars track by $index -->
                                            <div class="star ng-scope" ng-repeat="star in $ctrl.stars track by $index"
                                                 ng-click="$ctrl.onStarClicked(star)"
                                                 ng-mouseover="$ctrl.onStarHover(star)">
                                                <!-- ngIf: !$ctrl.svgVisible() --> <!-- ngIf: !$ctrl.svgVisible() -->
                                                <!-- ngIf: !$ctrl.svgVisible() --> <!-- ngIf: $ctrl.svgVisible() -->
                                                <svg class="star-empty default-star-empty-icon"
                                                     ng-if="$ctrl.svgVisible()">
                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink"
                                                         xlink:href="assets/images/star-rating.icons.svg#star-empty"></use>
                                                </svg><!-- end ngIf: $ctrl.svgVisible() -->
                                                <!-- ngIf: $ctrl.svgVisible() -->
                                                <svg class="star-half default-star-half-icon"
                                                     ng-if="$ctrl.svgVisible()">
                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink"
                                                         xlink:href="assets/images/star-rating.icons.svg#star-half"></use>
                                                </svg><!-- end ngIf: $ctrl.svgVisible() -->
                                                <!-- ngIf: $ctrl.svgVisible() -->
                                                <svg class="star-filled default-star-filled-icon"
                                                     ng-if="$ctrl.svgVisible()">
                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink"
                                                         xlink:href="assets/images/star-rating.icons.svg#star-filled"></use>
                                                </svg><!-- end ngIf: $ctrl.svgVisible() --> </div>
                                            <!-- end ngRepeat: star in $ctrl.stars track by $index -->
                                            <div class="star ng-scope" ng-repeat="star in $ctrl.stars track by $index"
                                                 ng-click="$ctrl.onStarClicked(star)"
                                                 ng-mouseover="$ctrl.onStarHover(star)">
                                                <!-- ngIf: !$ctrl.svgVisible() --> <!-- ngIf: !$ctrl.svgVisible() -->
                                                <!-- ngIf: !$ctrl.svgVisible() --> <!-- ngIf: $ctrl.svgVisible() -->
                                                <svg class="star-empty default-star-empty-icon"
                                                     ng-if="$ctrl.svgVisible()">
                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink"
                                                         xlink:href="assets/images/star-rating.icons.svg#star-empty"></use>
                                                </svg><!-- end ngIf: $ctrl.svgVisible() -->
                                                <!-- ngIf: $ctrl.svgVisible() -->
                                                <svg class="star-half default-star-half-icon"
                                                     ng-if="$ctrl.svgVisible()">
                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink"
                                                         xlink:href="assets/images/star-rating.icons.svg#star-half"></use>
                                                </svg><!-- end ngIf: $ctrl.svgVisible() -->
                                                <!-- ngIf: $ctrl.svgVisible() -->
                                                <svg class="star-filled default-star-filled-icon"
                                                     ng-if="$ctrl.svgVisible()">
                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink"
                                                         xlink:href="assets/images/star-rating.icons.svg#star-filled"></use>
                                                </svg><!-- end ngIf: $ctrl.svgVisible() --> </div>
                                            <!-- end ngRepeat: star in $ctrl.stars track by $index -->
                                            <div class="star ng-scope" ng-repeat="star in $ctrl.stars track by $index"
                                                 ng-click="$ctrl.onStarClicked(star)"
                                                 ng-mouseover="$ctrl.onStarHover(star)">
                                                <!-- ngIf: !$ctrl.svgVisible() --> <!-- ngIf: !$ctrl.svgVisible() -->
                                                <!-- ngIf: !$ctrl.svgVisible() --> <!-- ngIf: $ctrl.svgVisible() -->
                                                <svg class="star-empty default-star-empty-icon"
                                                     ng-if="$ctrl.svgVisible()">
                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink"
                                                         xlink:href="assets/images/star-rating.icons.svg#star-empty"></use>
                                                </svg><!-- end ngIf: $ctrl.svgVisible() -->
                                                <!-- ngIf: $ctrl.svgVisible() -->
                                                <svg class="star-half default-star-half-icon"
                                                     ng-if="$ctrl.svgVisible()">
                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink"
                                                         xlink:href="assets/images/star-rating.icons.svg#star-half"></use>
                                                </svg><!-- end ngIf: $ctrl.svgVisible() -->
                                                <!-- ngIf: $ctrl.svgVisible() -->
                                                <svg class="star-filled default-star-filled-icon"
                                                     ng-if="$ctrl.svgVisible()">
                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink"
                                                         xlink:href="assets/images/star-rating.icons.svg#star-filled"></use>
                                                </svg><!-- end ngIf: $ctrl.svgVisible() --> </div>
                                            <!-- end ngRepeat: star in $ctrl.stars track by $index -->
                                            <div class="star ng-scope" ng-repeat="star in $ctrl.stars track by $index"
                                                 ng-click="$ctrl.onStarClicked(star)"
                                                 ng-mouseover="$ctrl.onStarHover(star)">
                                                <!-- ngIf: !$ctrl.svgVisible() --> <!-- ngIf: !$ctrl.svgVisible() -->
                                                <!-- ngIf: !$ctrl.svgVisible() --> <!-- ngIf: $ctrl.svgVisible() -->
                                                <svg class="star-empty default-star-empty-icon"
                                                     ng-if="$ctrl.svgVisible()">
                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink"
                                                         xlink:href="assets/images/star-rating.icons.svg#star-empty"></use>
                                                </svg><!-- end ngIf: $ctrl.svgVisible() -->
                                                <!-- ngIf: $ctrl.svgVisible() -->
                                                <svg class="star-half default-star-half-icon"
                                                     ng-if="$ctrl.svgVisible()">
                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink"
                                                         xlink:href="assets/images/star-rating.icons.svg#star-half"></use>
                                                </svg><!-- end ngIf: $ctrl.svgVisible() -->
                                                <!-- ngIf: $ctrl.svgVisible() -->
                                                <svg class="star-filled default-star-filled-icon"
                                                     ng-if="$ctrl.svgVisible()">
                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink"
                                                         xlink:href="assets/images/star-rating.icons.svg#star-filled"></use>
                                                </svg><!-- end ngIf: $ctrl.svgVisible() --> </div>
                                            <!-- end ngRepeat: star in $ctrl.stars track by $index -->
                                            <div class="star ng-scope" ng-repeat="star in $ctrl.stars track by $index"
                                                 ng-click="$ctrl.onStarClicked(star)"
                                                 ng-mouseover="$ctrl.onStarHover(star)">
                                                <!-- ngIf: !$ctrl.svgVisible() --> <!-- ngIf: !$ctrl.svgVisible() -->
                                                <!-- ngIf: !$ctrl.svgVisible() --> <!-- ngIf: $ctrl.svgVisible() -->
                                                <svg class="star-empty default-star-empty-icon"
                                                     ng-if="$ctrl.svgVisible()">
                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink"
                                                         xlink:href="assets/images/star-rating.icons.svg#star-empty"></use>
                                                </svg><!-- end ngIf: $ctrl.svgVisible() -->
                                                <!-- ngIf: $ctrl.svgVisible() -->
                                                <svg class="star-half default-star-half-icon"
                                                     ng-if="$ctrl.svgVisible()">
                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink"
                                                         xlink:href="assets/images/star-rating.icons.svg#star-half"></use>
                                                </svg><!-- end ngIf: $ctrl.svgVisible() -->
                                                <!-- ngIf: $ctrl.svgVisible() -->
                                                <svg class="star-filled default-star-filled-icon"
                                                     ng-if="$ctrl.svgVisible()">
                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink"
                                                         xlink:href="assets/images/star-rating.icons.svg#star-filled"></use>
                                                </svg><!-- end ngIf: $ctrl.svgVisible() --> </div>
                                            <!-- end ngRepeat: star in $ctrl.stars track by $index --> </div>
                                    </div>
                                </star-rating-comp>
                            </div>
                            <div class="card-info">
                                <h4 class="card-title mt-3 ng-binding"><span><i class="fa fa-map-marker"
                                                                                aria-hidden="true"></i></span>
                                    @{{ clinic.address }}
                                </h4>
                                <h4 class="card-title mt-3"><span><i class="fa fa-clock-o"
                                                                     aria-hidden="true"></i></span>
                                    @{{clinic.working_hours.monday}}</h4>
                                <h4 class="card-title mt-3 ng-binding"><span><i class="fa fa-map-marker"
                                                                                aria-hidden="true"></i></span>

                                    @{{ clinic.distance }} miles away</h4>
                            </div>
                            <div class="card-text ng-binding">
                                Nihil quo voluptatem repellat officia. Aut ut et quo aspernatur odio. Doloremque...
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div ng-show="showLoader" class="herban-loader">
                <div id="loader">
                    <div class="anim">
                        <img src="{{ asset('assets/images/loader/1.svg') }}"/>
                        <img src="{{ asset('assets/images/loader/2.svg') }}"/>
                        <img src="{{ asset('assets/images/loader/3.svg') }}"/>
                        <img src="{{ asset('assets/images/loader/1.svg') }}"/>
                        <img src="{{ asset('assets/images/loader/2.svg') }}"/>
                        <img src="{{ asset('assets/images/loader/3.svg') }}"/>
                        <img src="{{ asset('assets/images/loader/1.svg') }}"/>
                    </div>
                </div>
                <h3 class="text-center herban-loading">Loading <span>.</span><span>.</span><span>.</span></h3>
            </div>

            <div class="col-xs-12">
                <button ng-if="clinicPages" class="btn btn-default load-more-btn center-block has-spinner"
                        ng-click="getClinics()">Load more
                </button>
            </div>
        </div>
    </div>

    @include('layouts.footer', ['mobile'=> false])
    @push('scripts')
    @endpush
@endsection
