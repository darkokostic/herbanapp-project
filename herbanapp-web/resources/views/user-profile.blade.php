@extends('layouts.non-admin')

@section('content')
    @include('layouts.header', ['slide' => false])
    @push('stylesheets')
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-star-rating/css/star-rating.css') }}">
    <link rel="stylesheet" href="{{ asset('css/create-clinic.css') }}">
    <link rel="stylesheet" href="{{ asset('css/user-profile.css') }}">
    @endpush
    <div ng-controller="UserCtrl">
        <div class="container-fluid">
            <div class="row cover-area">
                <div class="container">
                    <div class="row user-profile-header">
                        <div class="col-xs-4 col-sm-2">
                            <img src="{{asset('assets/images/user-profile/user-avatar.png')}}" alt="User avatar"
                                 class="img-responsive user-profile-avatar">
                        </div>
                        <div class="col-xs-8 col-sm-4">
                            <div class="row">
                                <div class="col-xs-12">
                                    <h4 class="user-profile-name" ng-cloak>@{{ user.name + " "+ user.lastname }}</h4>
                                </div>
                                <div class="col-xs-12">
                                    <span class="user-profile-location" ng-cloak><i class="fa fa-map-marker" aria-hidden="true"></i> @{{ user.city + ", " + user.state }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 hidden-xs hidden-sm">
                            <ul class="nav nav-tabs user-profile-tabs pull-right" role="tablist">
                                <li role="presentation" class="active">
                                    <a class="btn btn-default" href="#about" aria-controls="about" role="tab"
                                       data-toggle="tab">
                                        About
                                    </a>
                                </li>
                                <!-- <li role="presentation">
                                    <a class="btn btn-default" href="#favorites" aria-controls="favorites" role="tab"
                                       data-toggle="tab">Favorites
                                    </a>
                                </li> -->
                                <li role="presentation">
                                    <a class="btn btn-default" href="#friends" aria-controls="friends" role="tab"
                                       data-toggle="tab">Friends
                                    </a>
                                </li>
                                <li role="presentation">
                                    <a class="btn btn-default" href="#reviews" aria-controls="reviews" role="tab"
                                       data-toggle="tab">Reviews
                                    </a>
                                </li>
                                <li role="presentation">
                                    <a class="btn btn-default" href="#edit" aria-controls="edit" role="tab"
                                       data-toggle="tab">Edit
                                    </a>
                                </li>
                                <li>
                                    <a class="btn btn-default upgrade-btn" href="#">Upgrade to Premium
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-xs-12 col-sm-6  hidden-md hidden-lg">
                            <div class="dropdown">
                                <button class="btn btn-default dropdown-toggle user-menu-btn center-block" type="button" data-toggle="dropdown">User Menu
                                    <span class="caret"></span></button>
                                <ul class="dropdown-menu clinic-nav-drop">
                                    <li role="presentation" class="active"><a href="#about" role="tab"
                                           data-toggle="tab">About</a></li>
                                    <li role="presentation"><a href="#friends" role="tab"
                                           data-toggle="tab">Friends</a></li>
                                    <li role="presentation"><a href="#reviews" role="tab"
                                           data-toggle="tab">Reviews</a></li>
                                    <li role="presentation"><a href="#edit" role="tab"
                                           data-toggle="tab">Edit</a></li>
                                    <li role="presentation"><a href="#">Upgrade to Premium</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="container user-profile-container">
            <div class="row">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active fade in" id="about">
                        <div class="col-xs-12 col-sm-9">
                            <h4 class="user-profile-tab-title">About Us</h4>

                            <div class="card-block user-profile-card">
                                <h6 class="user-profile-card-title">About me</h6>
                                <p>
                                    @{{ user.about_me }}
                                </p>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-3">
                            <div class="panel panel-default user-profile-collapse">
                                <a data-toggle="collapse" href="#user-info">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            User informations
                                            <i class="fa fa-caret-down pull-right" aria-hidden="true"></i>
                                        </h4>
                                    </div>
                                </a>
                                <div id="user-info" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <h6>Full Name</h6>
                                        <p>@{{ user.name  + " " + user.lastname}}</p>
                                        <h6>Phone</h6>
                                        <p>@{{ user.phone }}</p>
                                        <h6>Email</h6>
                                        <p>@{{ user.email }}</p>
                                        <p></p>
                                        <h6>Member Since</h6>
                                        <p>@{{ user.created_at | date: 'medium'}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--<div role="tabpanel" class="tab-pane fade" id="favorites">
                        <h1>favorites</h1>
                    </div> -->
                    <div role="tabpanel" class="tab-pane fade in" id="friends">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-xs-6 col-md-9">
                                    <h4 class="user-profile-tab-title">Friends list (@{{ friends.length }})</h4>
                                </div>
                                <div class="col-xs-6 col-md-3">
                                    <div class="friend-search">
                                        <div class="input-group input-group-md clinic-input-group">
                                            <span class="input-group-addon" id="sizing-addon3"><i class="fa fa-search" aria-hidden="true"></i></span>
                                            <input type="text" class="form-control" placeholder="Search..." aria-describedby="sizing-addon3" ng-model="searchFriends">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6" ng-repeat="friend in friends | filter:searchFriends">
                                    <div class="panel panel-default friend-card">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-xs-2">
                                                    <img src="{{asset('assets/images/user-profile/user-avatar.png')}}" alt="User avatar" class="user-comment-avatar">
                                                </div>
                                                <div class="col-xs-7">
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <h4 class="friend-name">@{{ friend.name + " " + friend.lastname }}</h4>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <p class="friend-date">@{{ friend.created_at | date: 'medium'}}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-3">
                                                    <button class="btn btn-default btn-toggle-friend"><i class="fa fa-minus-square" aria-hidden="true"></i> Unfriend</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="reviews">

                        <div class="row">
                            <div class="col-xs-9">
                                <h4 class="user-profile-tab-title">Reviews list (@{{ reviews.length }})</h4>
                            </div>
                            <div class="col-xs-3">
                                <div class="friend-search">
                                    <div class="input-group input-group-md clinic-input-group">
                                        <span class="input-group-addon" id="sizing-addon3"><i class="fa fa-search" aria-hidden="true"></i></span>
                                        <input type="text" class="form-control" placeholder="Search reviews..." aria-describedby="sizing-addon3" ng-model="searchReviews">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12" ng-repeat="review in reviews | filter:searchReviews ">
                                <div class="card-block clinic-card">
                                    <div class="review-header">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        <div class="row">
                                                            <div class="col-xs-3">
                                                                <img src="https://image.freepik.com/free-icon/male-user-shadow_318-34042.jpg"
                                                                     alt="user avatar" class="img-circle comment-avatar">
                                                            </div>
                                                            <div class="col-xs-9">
                                                                <div class="row">
                                                                    <div class="col-xs-12">
                                                                        <h4 class="comment-username">@{{ review.user.name + " " + review.user.lastname }}</h4>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-9">
                                                                    <div class="row">
                                                                        <div class="col-xs-12">
                                                                            <h4 class="comment-username">@{{ review.user.name + " " + review.user.lastname }}</h4>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-xs-12 comment-rating">
                                                                            <input name="input-name" type="number"
                                                                                   class="rating rating-clinic" min=1 max=5
                                                                                   showClear="false" step="2" data-size="xs"
                                                                                   data-rtl="false"
                                                                                   ng-value="review.rating" displayOnly ng-cloak>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-6">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-6 text-right">
                                                    <span class="comment-date">@{{ review.created_at | date: 'medium' }}</span>
                                                    <button class="btn btn-default btn-clinic-fav-invert">Change</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <p class="review-description">@{{ review.message }}</p>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control comment-input"
                                                               placeholder="Write a comment...">
                                                        <span class="input-group-btn">
                                                    <button class="btn btn-default comment-submit-btn"
                                                            type="button">Post</button>
                                                </span>
                                                    </div><!-- /input-group -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- EDIT PANEL -->
                    <div role="tabpanel" class="tab-pane fade" id="edit">
                        <div class="col-xs-12">
                            <h4 class="user-profile-tab-title">Update User Informations</h4>
                            <div class="row">
                                <div class="col-xs-12">
                                    <p class="create-clinic-label">GENERAL INFORMATIONS</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-3 col-lg-3 create-clinic-input">
                                    <p>Name</p>
                                    <input type="text" class="form-control" placeholder="Name">
                                </div>
                                <div class="col-xs-12 col-sm-3 col-lg-3 create-clinic-input">
                                    <p>Last name</p>
                                    <input type="text" class="form-control" placeholder="Last name">
                                </div>
                                <div class="col-xs-12 col-sm-3 col-lg-3 create-clinic-input">
                                    <p>Username</p>
                                    <input type="text" class="form-control" placeholder="Username" disabled>
                                </div>
                                <div class="col-xs-12 col-sm-3 col-lg-3 create-clinic-input">
                                    <p>Email</p>
                                    <input type="text" class="form-control" placeholder="Email" disabled>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-3 col-lg-3 create-clinic-input">
                                    <p>Phone</p>
                                    <input type="text" class="form-control" placeholder="Phone">
                                </div>
                                <div class="col-xs-12 col-sm-3 col-lg-3 create-clinic-input">
                                    <p>State</p>
                                    <input type="text" class="form-control" placeholder="State">
                                </div>
                                <div class="col-xs-12 col-sm-3 col-lg-3 create-clinic-input">
                                    <p>City</p>
                                    <input type="text" class="form-control" placeholder="City">
                                </div>
                                <div class="col-xs-12 col-sm-3 col-lg-3 create-clinic-input">
                                    <p>Birthday</p>
                                    <input type="text" class="form-control" placeholder="Birthday">
                                </div>
                                <div class="col-xs-12 col-sm-3 col-lg-3 create-clinic-input">
                                    <p>Gender</p>
                                    <input type="text" class="form-control" placeholder="Gender">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('layouts.footer', ['mobile' => false])
    @push('scripts')
    <script src="{{ asset('bower_components/bootstrap-star-rating/js/star-rating.js') }}"></script>
    <script>
        $(".rating").rating({
            min: 1,
            max: 5,
            step: 2,
            size: 'xs',
            showCaption: false,
            hoverEnabled: false,
            displayOnly: true,
            emptyStar: '<i class="fa fa-star-o" aria-hidden="true"></i>',
            filledStar: '<i class="fa fa-star" aria-hidden="true"></i>'
        });

        $(document).ready(function(){
            $('.user-nav-drop a[data-toggle="tab"]').on('click', function (e) {
                console.log('test');
                $('.user-nav-drop li.active').removeClass('active');
//                $(this).parent('li').addClass('active');
            })
        });
    </script>
    @endpush
@endsection