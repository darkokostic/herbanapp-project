@extends('layouts.non-admin')
@section('content')
    @push('stylesheets')
    <link rel="stylesheet" href="{{ asset('css/login.css') }}">
    @endpush
<div class="container-fluid">
    <div class="row center-block login-panel">
        <div class="col-xs-12">
            <a href="/">

            </a>
        </div>
        <form name="userForm" class="login-form center-block css-form" novalidate action="{{ URL::to('reset/new') }}" method="POST">

            <div class="col-xs-12">
                <div class="form-group">
                    <input type="password" name="password" placeholder="Password"
                           class="center-block text-input" required>
                </div>
            </div>

            <div class="col-xs-12">
                <button type="submit" class="btn center-block login-btn">Log in</button>
            </div>
              <input type="password" name="email" value="{{$email}}" hidden required>
             {{ csrf_field() }}
        </form>
    </div>
    <div class="row">
        <div class="col-xs-12 center-block">
            <p class="text-center dont-have-text">
                <a href="/sign-up">Don't have an account? <b>Sign up</b> now</a>
            </p>
        </div>
    </div>
</div>
@endsection