@extends('layouts.non-admin')
@section('content')
    @push('stylesheets')
    <link rel="stylesheet" href="{{ asset('css/login.css') }}">
    @endpush

    <div class="container-fluid">
        <div class="jumbotron center-block sign-panel">
            <div class="container">
                <a href="/">
                    <img src="{{asset('assets/images/signup/sign-logo.png')}}" alt="Herbanapp logo" class="img-responsive center-block">
                </a>
                <form class="center-block sign-form" method="POST" action="{{ url('/login') }}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <input type="text" class="form-control" id="email" placeholder="Email or Username" name="username" required>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <input type="password" class="form-control" id="password" placeholder="Password" name="password" required>
                        @if ($errors->has('password'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                        @endif

                    </div>
                    <div class="remember-me">
                        <input type="checkbox" id="option" name="remember" {{ old('remember') ? 'checked' : '' }}>
                        <label for="option">Remember me
                            <svg width="100" height="100" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg"><path d="M13 50.986L37.334 75 88 25" stroke-width="16" stroke="#fff" fill="none" fill-rule="evenodd" stroke-dasharray="150" stroke-dashoffset="150"/></svg>
                        </label>
                        <a class="pull-right txt-forgot-password" href="#">Forgot Your Password?</a>
                    </div>
                    <button type="submit" class="btn btn-default btn-sign">Sign In</button>

                    <div class="row">
                        <div class="col-xs-12 text-center">
                            <h4 class="text-center">Or login using social media</h4>
                            <ul class="list-inline sign-social-networks">
                                <li><a href="login/facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>

                            </ul>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection