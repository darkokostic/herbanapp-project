@extends('layouts.non-admin')
@section('content')
    @push('stylesheets')
    <link rel="stylesheet"
          href="{{ asset('bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css')}}"/>
    <link rel="stylesheet" href="{{ asset('css/signup.css') }}">
    @endpush
    <div class="container-fluid">
        <div class="jumbotron center-block sign-panel">
            <div class="container">
                <a href="/">
                    <img src="{{asset('assets/images/signup/sign-logo.png')}}" alt="Herbanapp logo"
                         class="img-responsive center-block">
                </a>
                <form class="center-block sign-form" method="POST" action="{{ url('register') }}">
                    {{ csrf_field() }}
                    @isset($user)
                        <input type="text" name="social_name" value="{{$user->social_name}}" hidden>
                        <input type="text" name="social_id" value="{{$user->id}}" hidden>
                    @endisset
                    <div class="form-group">
                        <input type="email" class="form-control" id="email" placeholder="Email" name="email" required value="@isset($user->user['email']) {{$user->user['email']}}@endisset">
                        @if ($errors->has('email'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="username" placeholder="Username" name="username" value="@isset($user->username) {{$user->username}}@endisset"
                               required>
                        @if ($errors->has('username'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="name" placeholder="Name" name="name" value="@isset($user->name) {{$user->name}}@endisset"
                               required>
                        @if ($errors->has('name'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="lastname" placeholder="Last name" name="lastname" value="@isset($user->lastname) {{$user->lastname}}@endisset"
                               required>
                        @if ($errors->has('lastname'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('lastName') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <div class="row sign-drop-group">
                            <div class="col-sm-4">
                                <select class="form-control" id="state" name="state" required>
                                    <option stlye="color: #90a4ae;" value="" disabled selected>State</option>
                                    <option class="other">USA</option>
                                    <option class="other">USA</option>
                                    <option class="other">USA</option>
                                </select>
                                @if ($errors->has('state'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('state') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-sm-4">
                                <select class="form-control" id="city" name="city" required>
                                    <option stlye="color: #90a4ae;" value="" disabled selected>City</option>
                                    <option class="other">New York</option>
                                    <option class="other">New York</option>
                                    <option class="other">New York</option>
                                </select>
                                @if ($errors->has('city'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="birthdate" name="birth_date" value="@isset($user->user['birthday']) {{$user->user['birthday']}}@endisset"
                                       placeholder="Date of birth" readonly required>
                                @if ($errors->has('birthdate'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('birthdate') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" id="password" name="password" placeholder="Password"
                               required>
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" id="confirm-password" name="confirmPassword"
                               placeholder="Confirm Password" required>
                        @if ($errors->has('confirm-password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('confirm-password') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="phone" placeholder="Phone Number" name="phone"
                               required>
                        @if ($errors->has('phone'))
                            <span class="help-block">
                                <strong>{{ $errors->first('phone') }}</strong>
                            </span>
                        @endif
                    </div>
                    <button type="submit" class="btn btn-default btn-sign">Sign Up</button>
                </form>
            </div>
        </div>
    </div>

    @push('scripts')
    <script type="text/javascript" src="{{asset('bower_components/moment/min/moment.min.js')}}"></script>
    <script type="text/javascript"
            src="{{asset('bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js')}}"></script>
    <script>
        $(document).ready(function () {
            $('#birthdate').datetimepicker({
                viewMode: 'days',
                format: 'DD/MM/YYYY',
                stepping: 60,
                toolbarPlacement: 'bottom',
                focusOnShow: false,
                ignoreReadonly: true,
                widgetPositioning: {
                    horizontal: 'left',
                    vertical: 'bottom'
                },
                debug: true
            })

            $('#state').css('color', '#90a4ae');
            $('#state').change(function () {
                var current = $('#state').val();
                if (current != 'null') {
                    $('#state').css('color', 'black');
                } else {
                    $('#state').css('color', '#90a4ae');
                }
            });

            $('#city').css('color', '#90a4ae');
            $('#city').change(function () {
                var current = $('#city').val();
                if (current != 'null') {
                    $('#city').css('color', 'black');
                } else {
                    $('#city').css('color', '#90a4ae');
                }
            });
        });
    </script>
    @endpush
@endsection