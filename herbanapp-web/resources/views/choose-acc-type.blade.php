@extends('layouts.non-admin')
@section('content')
    @push('stylesheets')
    <link rel="stylesheet" href="{{ asset('css/acc-type.css') }}">
    @endpush

    <div class="container-fluid">
        <div class="jumbotron center-block sign-panel">
            <div class="container">
                <a href="/">
                    <img src="{{asset('assets/images/account-type/logo.png')}}" alt="Herbanapp logo"
                         class="img-responsive center-block">
                </a>
                <h2 class="text-center acc-type-title">Choose your account type</h2>
                <button type="submit" class="btn btn-default btn-acc-type">Free Account</button>
                <button type="submit" class="btn btn-default btn-sign">Premium Account</button>
            </div>
        </div>
    </div>
@endsection