@extends('layouts.non-admin')
@section('content')
    @include('layouts.header', ['slide' => false])
    @push('stylesheets')
    <link rel="stylesheet" href="{{ asset('css/clinic.css') }}">
    @endpush
    <div class="container-fluid">
        <div class="row cover-area">
        </div>
    </div>
    <div ng-controller="ClinicCtrl" class="container" ng-cloak>
        <div class="row clinic-row" ng-cloak>
            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-2">
                <img ng-if="clinic.logo" ng-src="@{{ clinic.logo.url }}" alt="Clinic Avatar"
                     class="clinic-avatar img-responsive center-block">
                <img ng-if="!clinic.logo" src="/assets/images/clinic/clinic-avatar.png" alt="Clinic Avatar"
                     class="clinic-avatar img-responsive center-block">
            </div>
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-center">
                            <span class="clinic-name">@{{ clinic.name }}</span>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-center show-hours">
                                <span class="open-close-hours"><i class="fa fa-clock-o"
                                                                  aria-hidden="true"></i> OPEN NOW</span><br>
                            <span class="hours">@{{  clinic.working_hours.friday }}h <i class="fa fa-caret-down"
                                                                                        aria-hidden="true"></i></span>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="row hidden-xs second-clinic-row">
                    <div class="col-xs-12 col-xs-offset-1">
                        <div class="clinic-rating">
                            <star-rating-comp star-type="'icon'" rating="clinic.average_rating" show-half-stars="true"
                                              read-only="true" size="'medium'" label-position="'right'"
                                              label-text="clinic.average_rating| number:1"></star-rating-comp>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-5">
                <div class="row">
                    <div class="col-xs-12 text-center col-md-push-2 col-lg-push-2">
                        <button class="btn btn-default btn-clinic-fav" data-toggle="modal" data-target="#writeReview">Write a review</button>
                        <button class="btn btn-default btn-clinic-fav"><i class="fa fa-star-o" aria-hidden="true"></i>
                            Favorite
                        </button>
                    </div>
                </div>
                <div class="row clinic-info-row">
                    <div class="col-xs-12 col-lg-pull-1" ng-show="!showLoader">
                        <a href="tel: @{{  clinic.business_phone }}" class="pull-right">
                            <span><i class="fa fa-mobile" aria-hidden="true"></i> @{{  clinic.business_phone }}</span>
                        </a>
                        <div class="clearfix"></div>
                        <a href="mail: @{{  clinic.business_email }}" class="pull-right">
                            <span>
                                <i class="fa fa-envelope-o"
                                   aria-hidden="true"></i> @{{  clinic.business_email }}
                            </span>
                        </a>
                    </div>
                    <div class="dropdown-menu no-click" role="menu">
                        <div class="hours-items">
                            <div class="hours-item" itemprop="openingHoursSpecification" itemscope=""
                                 itemtype="http://schema.org/OpeningHoursSpecification">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <div class="hours-item-label" itemprop="dayOfWeek">Sunday</div>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="hours-item-data">
                                            <span itemprop="opens">10:00am</span>
                                            &nbsp;-&nbsp;
                                            <span itemprop="closes">11:45pm</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="hours-item" itemprop="openingHoursSpecification" itemscope=""
                                 itemtype="http://schema.org/OpeningHoursSpecification">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <div class="hours-item-label" itemprop="dayOfWeek">Monday</div>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="hours-item-data">
                                            <span itemprop="opens">10:00am</span>
                                            &nbsp;-&nbsp;
                                            <span itemprop="closes">11:45pm</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="hours-item" itemprop="openingHoursSpecification" itemscope=""
                                 itemtype="http://schema.org/OpeningHoursSpecification">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <div class="hours-item-label" itemprop="dayOfWeek">Tuesday</div>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="hours-item-data">
                                            <span itemprop="opens">10:00am</span>
                                            &nbsp;-&nbsp;
                                            <span itemprop="closes">11:45pm</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="hours-item" itemprop="openingHoursSpecification" itemscope=""
                                 itemtype="http://schema.org/OpeningHoursSpecification">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <div class="hours-item-label" itemprop="dayOfWeek">Wednesday</div>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="hours-item-data">
                                            <span itemprop="opens">10:00am</span>
                                            &nbsp;-&nbsp;
                                            <span itemprop="closes">11:45pm</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="hours-item" itemprop="openingHoursSpecification" itemscope=""
                                 itemtype="http://schema.org/OpeningHoursSpecification">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <div class="hours-item-label" itemprop="dayOfWeek">Thursday</div>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="hours-item-data">
                                            <span itemprop="opens">10:00am</span>
                                            &nbsp;-&nbsp;
                                            <span itemprop="closes">11:45pm</span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="hours-item" itemprop="openingHoursSpecification" itemscope=""
                                 itemtype="http://schema.org/OpeningHoursSpecification">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <div class="hours-item-label" itemprop="dayOfWeek">Friday</div>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="hours-item-data">
                                            <span itemprop="opens">10:00am</span>
                                            &nbsp;-&nbsp;
                                            <span itemprop="closes">11:45pm</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="hours-item" itemprop="openingHoursSpecification" itemscope=""
                                 itemtype="http://schema.org/OpeningHoursSpecification">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <div class="hours-item-label" itemprop="dayOfWeek">Saturday</div>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="hours-item-data">
                                            <span itemprop="opens">10:00am</span>
                                            &nbsp;-&nbsp;
                                            <span itemprop="closes">11:45pm</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-10 clinic-btns-row">
                <ul class="nav nav-tabs clinic-tabs hidden-xs" role="tablist">
                    <li role="presentation" class="active">
                        <a class="btn btn-default" href="#menu" aria-controls="menu" role="tab" data-toggle="tab">
                            Menu
                        </a>
                    </li>
                    <li role="presentation">
                        <a class="btn btn-default" href="#about-us" aria-controls="about-us" role="tab"
                           data-toggle="tab">About Us
                        </a>
                    </li>
                    <li role="presentation">
                        <a class="btn btn-default" href="#reviews" aria-controls="reviews" role="tab"
                           data-toggle="tab">Reviews
                        </a>
                    </li>
                    <li role="presentation">
                        <a class="btn btn-default" href="#photos" aria-controls="photos" role="tab"
                           data-toggle="tab">Photos
                        </a>
                    </li>
                    <li role="presentation">
                        <a class="btn btn-default" href="#deals" aria-controls="deals" role="tab"
                           data-toggle="tab">Deals
                        </a>
                    </li>
                </ul>
                <div class="col-xs-12 col-sm-6 visible-xs">
                    <div class="dropdown">
                        <button class="btn btn-default dropdown-toggle clinic-menu-btn center-block" type="button"
                                data-toggle="dropdown">Clinic Menu
                            <span class="caret"></span></button>
                        <ul class="dropdown-menu clinic-nav-drop">
                            <li role="presentation" class="active"><a href="#menu" role="tab" data-toggle="tab">Menu</a>
                            </li>
                            <li role="presentation"><a href="#about-us" role="tab" data-toggle="tab">About Us</a></li>
                            <li role="presentation"><a href="#photos" role="tab" data-toggle="tab">Photos</a></li>
                            <li role="presentation"><a href="#deals" role="tab" data-toggle="tab">Deals</a></li>
                        </ul>
                    </div>
                </div>
                <br>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="writeReview" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Write review</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form name="reviewForm">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Review message </label>

                                    <textarea type="text" name="message" ng-model="newReview.message" class="form-control" ng-required="true"></textarea>
                                    <small id="emailHelp" class="form-text text-muted">Here you will write youre message</small>
                                </div>
                                <div class="form-group clinic-rating">
                                    <label for="exampleInputEmail1">Review rating </label>
                                    <small id="emailHelp" class="form-text text-muted">Here you will rate this clinic</small>
                                    <star-rating-comp star-type="'icon'" rating="newReview.rating" show-half-stars="true"
                                                      read-only="false" size="'medium'" on-rating-change="newReview.rating = $event.rating"  label-position="'right'"
                                    ></star-rating-comp>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary" ng-click="saveReview(newReview)" data-dismiss="modal" ng-disabled="!(reviewForm.$valid && newReview.rating != null)">Save changes</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active fade in" id="menu">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2 hidden-xs">
                        <div ng-show="!showLoader" class="input-group input-group-md clinic-input-group pull-right">
                    <span class="input-group-addon" id="sizing-addon3"><i class="fa fa-search"
                                                                          aria-hidden="true"></i></span>
                            <input type="text" class="form-control" placeholder="Search..."
                                   aria-describedby="sizing-addon3" ng-model="searchProducts">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-10">
                        <div ng-show="showLoader" class="herban-loader">
                            <div id="loader">
                                <div class="anim">
                                    <img src="{{ asset('assets/images/loader/1.svg') }}"/>
                                    <img src="{{ asset('assets/images/loader/2.svg') }}"/>
                                    <img src="{{ asset('assets/images/loader/3.svg') }}"/>
                                    <img src="{{ asset('assets/images/loader/1.svg') }}"/>
                                    <img src="{{ asset('assets/images/loader/2.svg') }}"/>
                                    <img src="{{ asset('assets/images/loader/3.svg') }}"/>
                                    <img src="{{ asset('assets/images/loader/1.svg') }}"/>
                                </div>
                            </div>
                            <h3 class="text-center herban-loading">Loading <span>.</span><span>.</span><span>.</span>
                            </h3>
                        </div>
                        <div ng-show="!showLoader" class="ng-fade">
                            <div ng-show="!showLoader && !clinic.products_with_category" class="no-products">There are
                                no products
                            </div>
                            <div ng-show="!showLoader" class="product-per-cat"
                                 ng-repeat="category in clinic.products_with_category">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <h4 class="clinic-product-cat-name">@{{ category.name }}
                                            (@{{ category.products.length }}
                                            )</h4>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <p ng-show="(category.products | filter: searchProducts).length == 0"
                                   class="text-muted">No
                                    results!</p>
                                <div class="table-responsive">
                                    <table class="table cart-table">
                                        <tbody>
                                        <tr ng-repeat="product in category.products | filter: searchProducts"
                                            class="panel panel-default product-card product-table"
                                            ng-click="showModal(product, category.name)">
                                            <th>
                                                <img src="{{ asset('assets/images/clinic/product-image.png') }}"
                                                     alt="Product Image"></th>
                                            <th class="clinic-name-th">
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <h4 class="clinic-product-name">@{{ product.name }}</h4>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <span class="clinic-product-category">@{{ category.name }}</span>
                                                    </div>
                                                </div>
                                            </th>
                                            <th>
                                                <div class="circle"><p class="text-center">1G</p></div>
                                                <span class="clinic-product-price"><span
                                                            class="first-price">@{{ product.prices.gram }}</span>.00</span>
                                            </th>
                                            <th>
                                                <div class="circle"><p class="text-center">2G</p></div>
                                                <span class="clinic-product-price"><span
                                                            class="first-price">@{{ product.prices.two_grams }}</span>.00</span>
                                            </th>
                                            <th>
                                                <div class="circle"><p class="text-center">1/8</p></div>
                                                <span class="clinic-product-price"><span
                                                            class="first-price">@{{ product.prices.eighth }}</span>.00</span>
                                            </th>
                                            <th>
                                                <div class="circle"><p class="text-center">1/4</p></div>
                                                <span class="clinic-product-price"><span
                                                            class="first-price">@{{ product.prices.fourth }}</span>.00</span>
                                            </th>
                                            <th>
                                                <div class="circle"><p class="text-center">1/2</p></div>
                                                <span class="clinic-product-price"><span
                                                            class="first-price">@{{ product.prices.half }}</span>.00</span>
                                            </th>
                                            <th>
                                                <div class="circle"><p class="text-center">OZ</p></div>
                                                <span class="clinic-product-price"><span
                                                            class="first-price">@{{ product.prices.ounce }}</span>.00 </span>
                                            </th>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane fade in" id="about-us">
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-2">
                        <div class="panel-group clinic-collapse">
                            <div class="panel panel-default">
                                <a data-toggle="collapse" href="#user-info">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            User informations
                                            <i class="fa fa-caret-down pull-right" aria-hidden="true"></i>
                                        </h4>
                                    </div>
                                </a>
                                <div id="user-info" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <h6>Address</h6>
                                        <p>@{{  clinic.address }}</p>
                                        <h6>Phone</h6>
                                        <p>@{{  clinic.business_phone }}</p>
                                        <h6>Email</h6>
                                        <p>@{{  clinic.business_email }}</p>
                                        <h6 ng-if="clinic.created_at">Member Since</h6>
                                        <p>@{{  clinic.created_at }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
                        <h4 class="clinic-product-cat-name">About Us</h4>
                        <div class="card-block clinic-card">
                            <h6 class="clinic-card-title">Announcement</h6>
                            <p> @{{  clinic.announcement }} </p>
                        </div>
                        <div class="card-block clinic-card">
                            <h6 class="clinic-card-title">About Us</h6>
                            <p>@{{  clinic.about_us }} </p>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane fade in" id="reviews">
                    <div class="col-xs-2 clinic-left-row hidden-xs">
                        <button class="btn btn-default clinic-page-btn-green" data-toggle="modal" data-target="#writeReview" >Add Review</button>
                        {{--<div class="clinic-collapse">--}}
                        {{--<div class="panel panel-default">--}}
                        {{--<a data-toggle="collapse" href="#sortby">--}}
                        {{--<div class="panel-heading">--}}
                        {{--<h4 class="panel-title">--}}
                        {{--Sort By--}}
                        {{--<i class="fa fa-caret-down pull-right" aria-hidden="true"></i>--}}
                        {{--</h4>--}}
                        {{--</div>--}}
                        {{--</a>--}}
                        {{--<div id="sortby" class="panel-collapse collapse">--}}
                        {{--<div class="panel-body">--}}
                        {{--<a href="#">Ascending</a>--}}
                        {{--<a href="#">Descending</a>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                    </div>
                    <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
                        <div class="col-xs-8">
                            <h4 class="clinic-product-cat-name">Reviews</h4>
                        </div>
                        <div class="col-xs-4">
                            <span class="reviews-counter pull-right"><span>@{{ clinic.reviews_count }}</span> reviews</span>
                        </div>
                        <div class="clearfix"></div>
                        <div ng-repeat="review in clinic.reviews" class="card-block clinic-card">
                            <div class="review-header">
                                <div class="row">
                                    <div class="col-xs-9">
                                        <div class="row">
                                            <div class="col-xs-9">
                                                <div class="row">
                                                    <div class="col-xs-3">
                                                        <img ng-src="@{{ '../' +review.user.avatar }}"
                                                             alt="user avatar" class="img-circle comment-avatar">
                                                    </div>
                                                    <div class="col-xs-9">
                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <h4 class="comment-username">@{{ review.user.name + " " + review.user.lastname}}</h4>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-12 comment-rating">
                                                                <star-rating-comp star-type="'icon'"
                                                                                  rating="review.rating"
                                                                                  show-half-stars="true"
                                                                                  read-only="true" size="'medium'"
                                                                                  label-position="'right'"
                                                                                  label-text="review.rating"></star-rating-comp>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-3">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-3 text-right">
                                        <span class="comment-date">@{{ review.created_at }}</span>
                                        <button class="btn btn-default btn-clinic-fav-invert">Change</button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <p class="review-description">@{{ review.message }} </p>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="input-group comment-group">
                                            <form name="replieForm"></form>
                                            <input type="text" class="form-control comment-input"
                                                   placeholder="Write a comment..." ng-model="replie" ng-required="true">
                                            <button class="btn btn-default comment-submit-btn"
                                                    type="button" ng-click="sendReplie(replie,review.id)" ng-disabled="replie == null">Post
                                            </button>
                                        </div><!-- /input-group -->
                                    </div>
                                    <div class="col-xs-12" ng-repeat="comment in review.comments">
                                        <hr class="sub-hr">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <h4 class="sub-comment-username">@{{ comment.user.name + " " + comment.user.lastname}}</h4>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <p class="sub-comment-description ng-binding">@{{ comment.message }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane fade in" id="photos">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="col-xs-8">
                            <h4 class="clinic-product-cat-name">Photos</h4>
                        </div>
                        <div class="col-xs-4">
                            <span class="reviews-counter pull-right"><span>@{{ clinic.gallery.length }}</span> photos</span>
                        </div>
                    </div>
                    <div class="col-lg-2"></div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-10 images-container">
                        <div ng-show="!showLoader && !clinic.gallery.length" class="empty-results">
                            <h3 class="text-muted text-center"><b>@{{ clinic.name }}</b> haven't added any photos yet.
                            </h3>
                        </div>
                        <div ng-repeat="image in clinic.gallery" class="col-xs-3">
                            <img class="lazy "
                                 ng-src="@{{ '/' + image.url }}"/>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane fade in" id="deals">
                    <div class="col-xs-12">
                        <h4 class="clinic-product-cat-name">Hot Deals</h4>
                        <div class="container-fluid">
                            <div class="row-eq-height filter-bar center-block">
                                <div class="col-xs-12 text-right">
                                    <!--   <div class="btn-group">
                                           <div class="dropdown">
                                               <button class="btn btn-default dropdown-toggle  dropdown-menu-left sort-btn" type="button"
                                                       id="sortByDeals"
                                                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                   Sort by
                                                   <span class="caret"></span>
                                               </button>
                                               <ul class="dropdown-menu text-left" aria-labelledby="sortByDeals">
                                                   <li><a href="#">Action</a></li>
                                                   <li><a href="#">Another action</a></li>
                                                   <li><a href="#">Something else here</a></li>
                                                   <li role="separator" class="divider"></li>
                                                   <li><a href="#">Separated link</a></li>
                                               </ul>
                                           </div>
                                           <button class="btn btn-default sort-btn pull-right">View all</button>
                                       </div> -->
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="hotDeals" class="carousel slide">

                                        <ol class="carousel-indicators">
                                            <li data-target="#hotDeals" data-slide-to="0" class="active"></li>
                                            <li data-target="#hotDeals" data-slide-to="1"></li>
                                            <li data-target="#hotDeals" data-slide-to="2"></li>
                                        </ol>
                                        <!-- Carousel items -->
                                        <div class="carousel-inner">
                                            <div class="item active">
                                                <div class="row">
                                                    <div class="col-sm-6 col-md-6 col-lg-3 mt-4">
                                                        <div class="card center-block">
                                                            <div class="overlay">
                                                                <div class="overlay-image"></div>
                                                                <div class="overlay">
                                                                    <div class="overlay-image"></div>
                                                                    <img class="card-img-top"
                                                                         src="http://placehold.it/300x100">
                                                                </div>
                                                            </div>
                                                            <div class="card-block">
                                                                <div class="price-info text-right">
                                                                    <h2 class="old-price">
                                                                        <del>2000$</del>
                                                                    </h2>
                                                                    <h1 class="hot-price">1000$</h1>
                                                                </div>
                                                                <div class="hot-deal-info">
                                                                    <h5>
                                                                        7 GRAMS FOR $50 TOP
                                                                        SHELF ONLY
                                                                    </h5>
                                                                    <star-rating-comp  star-type="'icon'" rating="4" show-half-stars="true"
                                                                                      read-only="true" size="'medium'"
                                                                                      label-position="'right'"
                                                                                      label-text="4"></star-rating-comp>
                                                                </div>
                                                                <p class="hot-deals-text">
                                                                    Lorem Ipsum is simply dummy text of the printing and
                                                                    typesetting
                                                                    industry.
                                                                </p>
                                                                <div>
                                                                    <button class="btn btn-default add-to-cart-btn"
                                                                            type="submit"><i
                                                                                class="fa fa-shopping-cart"
                                                                                aria-hidden="true"></i>
                                                                        Add to cart
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 col-md-6 col-lg-3 mt-4">
                                                        <div class="card center-block">
                                                            <div class="overlay">
                                                                <div class="overlay-image"></div>
                                                                <div class="overlay">
                                                                    <div class="overlay-image"></div>
                                                                    <img class="card-img-top"
                                                                         src="http://placehold.it/300x100">
                                                                </div>
                                                            </div>
                                                            <div class="card-block">
                                                                <div class="price-info text-right">
                                                                    <h2 class="old-price">
                                                                        <del>2000$</del>
                                                                    </h2>
                                                                    <h1 class="hot-price">1000$</h1>
                                                                </div>
                                                                <div class="hot-deal-info">
                                                                    <h5>
                                                                        7 GRAMS FOR $50 TOP
                                                                        SHELF ONLY
                                                                    </h5>
                                                                    <input name="input-name" type="number"
                                                                           class="rating" min=1 max=5
                                                                           showClear="false" step="2" data-size="xs"
                                                                           data-rtl="false"
                                                                           value="4" displayOnly>
                                                                    <span class="rating-number">5.00</span>
                                                                </div>
                                                                <p class="hot-deals-text">
                                                                    Lorem Ipsum is simply dummy text of the printing and
                                                                    typesetting
                                                                    industry.
                                                                </p>
                                                                <div>
                                                                    <button class="btn btn-default add-to-cart-btn"
                                                                            type="submit"><i
                                                                                class="fa fa-shopping-cart"
                                                                                aria-hidden="true"></i>
                                                                        Add to cart
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 col-md-6 col-lg-3 mt-4">
                                                        <div class="card center-block">
                                                            <div class="overlay">
                                                                <div class="overlay-image"></div>
                                                                <div class="overlay">
                                                                    <div class="overlay-image"></div>
                                                                    <img class="card-img-top"
                                                                         src="http://placehold.it/300x100">
                                                                </div>
                                                            </div>
                                                            <div class="card-block">
                                                                <div class="price-info text-right">
                                                                    <h2 class="old-price">
                                                                        <del>2000$</del>
                                                                    </h2>
                                                                    <h1 class="hot-price">1000$</h1>
                                                                </div>
                                                                <div class="hot-deal-info">
                                                                    <h5>
                                                                        7 GRAMS FOR $50 TOP
                                                                        SHELF ONLY
                                                                    </h5>
                                                                    <input name="input-name" type="number"
                                                                           class="rating" min=1 max=5
                                                                           showClear="false" step="2" data-size="xs"
                                                                           data-rtl="false"
                                                                           value="4" displayOnly>
                                                                    <span class="rating-number">5.00</span>
                                                                </div>
                                                                <p class="hot-deals-text">
                                                                    Lorem Ipsum is simply dummy text of the printing and
                                                                    typesetting
                                                                    industry.
                                                                </p>
                                                                <div>
                                                                    <button class="btn btn-default add-to-cart-btn"
                                                                            type="submit"><i
                                                                                class="fa fa-shopping-cart"
                                                                                aria-hidden="true"></i>
                                                                        Add to cart
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 col-md-6 col-lg-3 mt-4">
                                                        <div class="card center-block">
                                                            <div class="overlay">
                                                                <div class="overlay-image"></div>
                                                                <div class="overlay">
                                                                    <div class="overlay-image"></div>
                                                                    <img class="card-img-top"
                                                                         src="http://placehold.it/300x100">
                                                                </div>
                                                            </div>
                                                            <div class="card-block">
                                                                <div class="price-info text-right">
                                                                    <h2 class="old-price">
                                                                        <del>2000$</del>
                                                                    </h2>
                                                                    <h1 class="hot-price">1000$</h1>
                                                                </div>
                                                                <div class="hot-deal-info">
                                                                    <h5>
                                                                        7 GRAMS FOR $50 TOP
                                                                        SHELF ONLY
                                                                    </h5>
                                                                    <input name="input-name" type="number"
                                                                           class="rating" min=1 max=5
                                                                           showClear="false" step="2" data-size="xs"
                                                                           data-rtl="false"
                                                                           value="4" displayOnly>
                                                                    <span class="rating-number">5.00</span>
                                                                </div>
                                                                <p class="hot-deals-text">
                                                                    Lorem Ipsum is simply dummy text of the printing and
                                                                    typesetting
                                                                    industry.
                                                                </p>
                                                                <div>
                                                                    <button class="btn btn-default add-to-cart-btn"
                                                                            type="submit"><i
                                                                                class="fa fa-shopping-cart"
                                                                                aria-hidden="true"></i>
                                                                        Add to cart
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div><!--.row-->
                                            </div><!--.item-->
                                            <div class="item">
                                                <div class="row">
                                                    <div class="col-sm-6 col-md-6 col-lg-3 mt-4">
                                                        <div class="card center-block">
                                                            <div class="overlay">
                                                                <div class="overlay-image"></div>
                                                                <div class="overlay">
                                                                    <div class="overlay-image"></div>
                                                                    <img class="card-img-top"
                                                                         src="http://placehold.it/300x100">
                                                                </div>
                                                            </div>
                                                            <div class="card-block">
                                                                <div class="price-info text-right">
                                                                    <h2 class="old-price">
                                                                        <del>2000$</del>
                                                                    </h2>
                                                                    <h1 class="hot-price">1000$</h1>
                                                                </div>
                                                                <div class="hot-deal-info">
                                                                    <h5>
                                                                        7 GRAMS FOR $50 TOP
                                                                        SHELF ONLY
                                                                    </h5>
                                                                    <input name="input-name" type="number"
                                                                           class="rating" min=1 max=5
                                                                           showClear="false" step="2" data-size="xs"
                                                                           data-rtl="false"
                                                                           value="4" displayOnly>
                                                                    <span class="rating-number">5.00</span>
                                                                </div>
                                                                <p class="hot-deals-text">
                                                                    Lorem Ipsum is simply dummy text of the printing and
                                                                    typesetting
                                                                    industry.
                                                                </p>
                                                                <div>
                                                                    <button class="btn btn-default add-to-cart-btn"
                                                                            type="submit"><i
                                                                                class="fa fa-shopping-cart"
                                                                                aria-hidden="true"></i>
                                                                        Add to cart
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 col-md-6 col-lg-3 mt-4">
                                                        <div class="card center-block">
                                                            <div class="overlay">
                                                                <div class="overlay-image"></div>
                                                                <div class="overlay">
                                                                    <div class="overlay-image"></div>
                                                                    <img class="card-img-top"
                                                                         src="http://placehold.it/300x100">
                                                                </div>
                                                            </div>
                                                            <div class="card-block">
                                                                <div class="price-info text-right">
                                                                    <h2 class="old-price">
                                                                        <del>2000$</del>
                                                                    </h2>
                                                                    <h1 class="hot-price">1000$</h1>
                                                                </div>
                                                                <div class="hot-deal-info">
                                                                    <h5>
                                                                        7 GRAMS FOR $50 TOP
                                                                        SHELF ONLY
                                                                    </h5>
                                                                    <input name="input-name" type="number"
                                                                           class="rating" min=1 max=5
                                                                           showClear="false" step="2" data-size="xs"
                                                                           data-rtl="false"
                                                                           value="4" displayOnly>
                                                                    <span class="rating-number">5.00</span>
                                                                </div>
                                                                <p class="hot-deals-text">
                                                                    Lorem Ipsum is simply dummy text of the printing and
                                                                    typesetting
                                                                    industry.
                                                                </p>
                                                                <div>
                                                                    <button class="btn btn-default add-to-cart-btn"
                                                                            type="submit"><i
                                                                                class="fa fa-shopping-cart"
                                                                                aria-hidden="true"></i>
                                                                        Add to cart
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 col-md-6 col-lg-3 mt-4">
                                                        <div class="card center-block">
                                                            <div class="overlay">
                                                                <div class="overlay-image"></div>
                                                                <div class="overlay">
                                                                    <div class="overlay-image"></div>
                                                                    <img class="card-img-top"
                                                                         src="http://placehold.it/300x100">
                                                                </div>
                                                            </div>
                                                            <div class="card-block">
                                                                <div class="price-info text-right">
                                                                    <h2 class="old-price">
                                                                        <del>2000$</del>
                                                                    </h2>
                                                                    <h1 class="hot-price">1000$</h1>
                                                                </div>
                                                                <div class="hot-deal-info">
                                                                    <h5>
                                                                        7 GRAMS FOR $50 TOP
                                                                        SHELF ONLY
                                                                    </h5>
                                                                    <input name="input-name" type="number"
                                                                           class="rating" min=1 max=5
                                                                           showClear="false" step="2" data-size="xs"
                                                                           data-rtl="false"
                                                                           value="4" displayOnly>
                                                                    <span class="rating-number">5.00</span>
                                                                </div>
                                                                <p class="hot-deals-text">
                                                                    Lorem Ipsum is simply dummy text of the printing and
                                                                    typesetting
                                                                    industry.
                                                                </p>
                                                                <div>
                                                                    <button class="btn btn-default add-to-cart-btn"
                                                                            type="submit"><i
                                                                                class="fa fa-shopping-cart"
                                                                                aria-hidden="true"></i>
                                                                        Add to cart
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 col-md-6 col-lg-3 mt-4">
                                                        <div class="card center-block">
                                                            <div class="overlay">
                                                                <div class="overlay-image"></div>
                                                                <div class="overlay">
                                                                    <div class="overlay-image"></div>
                                                                    <img class="card-img-top"
                                                                         src="http://placehold.it/300x100">
                                                                </div>
                                                            </div>
                                                            <div class="card-block">
                                                                <div class="price-info text-right">
                                                                    <h2 class="old-price">
                                                                        <del>2000$</del>
                                                                    </h2>
                                                                    <h1 class="hot-price">1000$</h1>
                                                                </div>
                                                                <div class="hot-deal-info">
                                                                    <h5>
                                                                        7 GRAMS FOR $50 TOP
                                                                        SHELF ONLY
                                                                    </h5>
                                                                    <input name="input-name" type="number"
                                                                           class="rating" min=1 max=5
                                                                           showClear="false" step="2" data-size="xs"
                                                                           data-rtl="false"
                                                                           value="4" displayOnly>
                                                                    <span class="rating-number">5.00</span>
                                                                </div>
                                                                <p class="hot-deals-text">
                                                                    Lorem Ipsum is simply dummy text of the printing and
                                                                    typesetting
                                                                    industry.
                                                                </p>
                                                                <div>
                                                                    <button class="btn btn-default add-to-cart-btn"
                                                                            type="submit"><i
                                                                                class="fa fa-shopping-cart"
                                                                                aria-hidden="true"></i>
                                                                        Add to cart
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div><!--.row-->
                                            </div><!--.item-->
                                            <div class="item">
                                                <div class="row">
                                                    <div class="col-sm-6 col-md-6 col-lg-3 mt-4">
                                                        <div class="card center-block">
                                                            <div class="overlay">
                                                                <div class="overlay-image"></div>
                                                                <div class="overlay">
                                                                    <div class="overlay-image"></div>
                                                                    <img class="card-img-top"
                                                                         src="http://placehold.it/300x100">
                                                                </div>
                                                            </div>
                                                            <div class="card-block">
                                                                <div class="price-info text-right">
                                                                    <h2 class="old-price">
                                                                        <del>2000$</del>
                                                                    </h2>
                                                                    <h1 class="hot-price">1000$</h1>
                                                                </div>
                                                                <div class="hot-deal-info">
                                                                    <h5>
                                                                        7 GRAMS FOR $50 TOP
                                                                        SHELF ONLY
                                                                    </h5>
                                                                    <input name="input-name" type="number"
                                                                           class="rating" min=1 max=5
                                                                           showClear="false" step="2" data-size="xs"
                                                                           data-rtl="false"
                                                                           value="4" displayOnly>
                                                                    <span class="rating-number">5.00</span>
                                                                </div>
                                                                <p class="hot-deals-text">
                                                                    Lorem Ipsum is simply dummy text of the printing and
                                                                    typesetting
                                                                    industry.
                                                                </p>
                                                                <div>
                                                                    <button class="btn btn-default add-to-cart-btn"
                                                                            type="submit"><i
                                                                                class="fa fa-shopping-cart"
                                                                                aria-hidden="true"></i>
                                                                        Add to cart
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 col-md-6 col-lg-3 mt-4">
                                                        <div class="card center-block">
                                                            <div class="overlay">
                                                                <div class="overlay-image"></div>
                                                                <div class="overlay">
                                                                    <div class="overlay-image"></div>
                                                                    <img class="card-img-top"
                                                                         src="http://placehold.it/300x100">
                                                                </div>
                                                            </div>
                                                            <div class="card-block">
                                                                <div class="price-info text-right">
                                                                    <h2 class="old-price">
                                                                        <del>2000$</del>
                                                                    </h2>
                                                                    <h1 class="hot-price">1000$</h1>
                                                                </div>
                                                                <div class="hot-deal-info">
                                                                    <h5>
                                                                        7 GRAMS FOR $50 TOP
                                                                        SHELF ONLY
                                                                    </h5>
                                                                    <input name="input-name" type="number"
                                                                           class="rating" min=1 max=5
                                                                           showClear="false" step="2" data-size="xs"
                                                                           data-rtl="false"
                                                                           value="4" displayOnly>
                                                                    <span class="rating-number">5.00</span>
                                                                </div>
                                                                <p class="hot-deals-text">
                                                                    Lorem Ipsum is simply dummy text of the printing and
                                                                    typesetting
                                                                    industry.
                                                                </p>
                                                                <div>
                                                                    <button class="btn btn-default add-to-cart-btn"
                                                                            type="submit"><i
                                                                                class="fa fa-shopping-cart"
                                                                                aria-hidden="true"></i>
                                                                        Add to cart
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 col-md-6 col-lg-3 mt-4">
                                                        <div class="card center-block">
                                                            <div class="overlay">
                                                                <div class="overlay-image"></div>
                                                                <div class="overlay">
                                                                    <div class="overlay-image"></div>
                                                                    <img class="card-img-top"
                                                                         src="https://placehold.it/300x100">
                                                                </div>
                                                            </div>
                                                            <div class="card-block">
                                                                <div class="price-info text-right">
                                                                    <h2 class="old-price">
                                                                        <del>2000$</del>
                                                                    </h2>
                                                                    <h1 class="hot-price">1000$</h1>
                                                                </div>
                                                                <div class="hot-deal-info">
                                                                    <h5>
                                                                        7 GRAMS FOR $50 TOP
                                                                        SHELF ONLY
                                                                    </h5>
                                                                    <input name="input-name" type="number"
                                                                           class="rating" min=1 max=5
                                                                           showClear="false" step="2" data-size="xs"
                                                                           data-rtl="false"
                                                                           value="4" displayOnly>
                                                                    <span class="rating-number">5.00</span>
                                                                </div>
                                                                <p class="hot-deals-text">
                                                                    Lorem Ipsum is simply dummy text of the printing and
                                                                    typesetting
                                                                    industry.
                                                                </p>
                                                                <div>
                                                                    <button class="btn btn-default add-to-cart-btn"
                                                                            type="submit"><i
                                                                                class="fa fa-shopping-cart"
                                                                                aria-hidden="true"></i>
                                                                        Add to cart
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 col-md-6 col-lg-3 mt-4">
                                                        <div class="card center-block">
                                                            <div class="overlay">
                                                                <div class="overlay-image"></div>
                                                                <div class="overlay">
                                                                    <div class="overlay-image"></div>
                                                                    <img class="card-img-top"
                                                                         src="http://placehold.it/300x100">
                                                                </div>
                                                            </div>
                                                            <div class="card-block">
                                                                <div class="price-info text-right">
                                                                    <h2 class="old-price">
                                                                        <del>2000$</del>
                                                                    </h2>
                                                                    <h1 class="hot-price">1000$</h1>
                                                                </div>
                                                                <div class="hot-deal-info">
                                                                    <h5>
                                                                        7 GRAMS FOR $50 TOP
                                                                        SHELF ONLY
                                                                    </h5>
                                                                    <input name="input-name" type="number"
                                                                           class="rating" min=1 max=5
                                                                           showClear="false" step="2" data-size="xs"
                                                                           data-rtl="false"
                                                                           value="4" displayOnly>
                                                                    <span class="rating-number">5.00</span>
                                                                </div>
                                                                <p class="hot-deals-text">
                                                                    Lorem Ipsum is simply dummy text of the printing and
                                                                    typesetting
                                                                    industry.
                                                                </p>
                                                                <div>
                                                                    <button class="btn btn-default add-to-cart-btn"
                                                                            type="submit"><i
                                                                                class="fa fa-shopping-cart"
                                                                                aria-hidden="true"></i>
                                                                        Add to cart
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div><!--.row-->
                                            </div><!--.item-->
                                        </div><!--.carousel-inner-->
                                        <a data-slide="prev" href="#hotDeals" class="left carousel-control">‹</a>
                                        <a data-slide="next" href="#hotDeals" class="right carousel-control">›</a>
                                    </div><!--.Carousel-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div ng-cloak>
        @include('layouts.footer', ['mobile' => false])
    </div>
    @push('scripts')
    <script src="{{ asset('assets/js/mobile-swipe.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.lazy.min.js') }}"></script>
    <script>
        $('#hotDeals').bcSwipe({interval: false, threshold: 80});
        $('.show-hours').on('click', function () {
            $('.clinic-info-row>.dropdown-menu').toggle();
        });
        $(function () {
            $('.lazy').Lazy({
                // your configuration goes here
                scrollDirection: 'vertical',
                effect: 'fadeIn',
                visibleOnly: true,
                onError: function (element) {
                    console.log('error loading ' + element.data('src'));
                }
            });
        });
        $(document).ready(function () {
            $('.clinic-nav-drop a[data-toggle="tab"]').on('click', function (e) {
                console.log('test');
                $('.clinic-nav-drop li.active').removeClass('active');
//                $(this).parent('li').addClass('active');
            })
        });
    </script>
    @endpush
@endsection