@extends('layouts.non-admin')
@section('content')
    @include('layouts.header', ['slide' => false])

    @push('stylesheets')
    <link rel="stylesheet" href="{{ asset('css/orders.css') }}">
    <link rel="stylesheet" href="{{ asset('css/footer.css') }}">
    @endpush

    <div class="container has-herb-header">
        <div class="row">
            <div class="col-xs-12">
                <h4 class="page-title">Orders</h4>
            </div>
        </div>
        <div class="row cart-table-row">
            <div class="col-xs-12">
                <div class="table-responsive">
                    <table class="table cart-table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>CLINIC</th>
                            <th>PRICE</th>
                            <th>STATUS</th>
                            <th>CODE</th>
                            <th>DATE</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                            <th class="cart-table-number">1</th>
                            <th class="cart-clinic-name">Clinic name</th>
                            <th class="price-th">$<span class="price-span">270</span>.00</th>
                            <th>
                                <span class="cart-order-text">Done</span>
                            </th>
                            <th>
                                <span class="cart-order-code">DHLAR032</span>
                            </th>
                            <th>
                                <span class="cart-order-text">08.07.2017</span>
                            </th>
                        </tr>
                            <tr>
                                <th class="cart-table-number">1</th>
                                <th class="cart-clinic-name">Clinic name</th>
                                <th class="price-th">$<span class="price-span">270</span>.00</th>
                                <th>
                                    <span class="cart-order-text">Done</span>
                                </th>
                                <th>
                                    <span class="cart-order-code">DHLAR032</span>
                                </th>
                                <th>
                                    <span class="cart-order-text">08.07.2017</span>
                                </th>
                            </tr>
                            <tr>
                                <th class="cart-table-number">1</th>
                                <th class="cart-clinic-name">Clinic name</th>
                                <th class="price-th">$<span class="price-span">270</span>.00</th>
                                <th>
                                    <span class="cart-order-text">Done</span>
                                </th>
                                <th>
                                    <span class="cart-order-code">DHLAR032</span>
                                </th>
                                <th>
                                    <span class="cart-order-text">08.07.2017</span>
                                </th>
                            </tr>
                            <tr>
                                <th class="cart-table-number">1</th>
                                <th class="cart-clinic-name">Clinic name</th>
                                <th class="price-th">$<span class="price-span">270</span>.00</th>
                                <th>
                                    <span class="cart-order-text">Done</span>
                                </th>
                                <th>
                                    <span class="cart-order-code">DHLAR032</span>
                                </th>
                                <th>
                                    <span class="cart-order-text">08.07.2017</span>
                                </th>
                            </tr>
                            <tr>
                                <th class="cart-table-number">1</th>
                                <th class="cart-clinic-name">Clinic name</th>
                                <th class="price-th">$<span class="price-span">270</span>.00</th>
                                <th>
                                    <span class="cart-order-text">Done</span>
                                </th>
                                <th>
                                    <span class="cart-order-code">DHLAR032</span>
                                </th>
                                <th>
                                    <span class="cart-order-text">08.07.2017</span>
                                </th>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-xs-12">
                <button class="btn btn-default load-more-btn center-block has-spinner">Load more</button>
            </div>
        </div>
    </div>

    @include('layouts.footer', ['mobile'=> false])
    @push('scripts')
    @endpush
@endsection
