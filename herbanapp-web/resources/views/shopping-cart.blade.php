@extends('layouts.non-admin')
@section('content')
    @include('layouts.header', ['slide' => false])

    @push('stylesheets')
    <link rel="stylesheet" href="{{ asset('css/shopping-cart.css') }}">
    <link rel="stylesheet" href="{{ asset('css/footer.css') }}">
    @endpush

    <div class="container has-herb-header">
        <div class="row">
            <div class="col-xs-12">
                <h4 class="page-title">Shopping Cart</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="card-block">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xs-12 cart-card-header">
                                <div class="row">
                                    <div class="col-xs-1">
                                        <img src="{{asset('assets/images/user-profile/user-avatar.png')}}"
                                             alt="User avatar" class="img-circle cart-user-avatar">
                                    </div>
                                    <div class="col-xs-7">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <h4 class="cart-clinic-name">Clinic name</h4>
                                            </div>
                                            <div class="col-xs-12 cart-clinic-contact">
                                            <span>
                                                <i class="fa fa-mobile" aria-hidden="true"></i> 888-445-2807
                                            </span>
                                                <span>
                                                <i class="fa fa-envelope-o" aria-hidden="true"></i> contact@420recs.com
                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <button class="btn btn-default cart-remove-all-btn pull-right">Remove all
                                            products
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row cart-table-row">
                            <div class="col-xs-12">
                                <div class="table-responsive">
                                    <table class="table cart-table">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>PHOTO</th>
                                            <th>ITEM</th>
                                            <th>QUANTITY</th>
                                            <th>WEIGHT</th>
                                            <th>PRICE</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                            <th class="cart-table-number">1</th>
                                            <th><img src="{{ asset('assets/images/shopping-cart/product-avatar.png') }}" class="cart-product-avatar"
                                                     alt="product avatar"></th>
                                            <th>
                                                <h4 class="cart-product-name">Pink Champagne</h4>
                                                <span class="cart-product-cat">Indica</span>
                                            </th>
                                            <th>
                                                <div class="input-group quantity-input">
                                                            <span class="input-group-btn">
                                                                <button type="button"
                                                                        class="quantity-left-minus btn btn-default btn-number"
                                                                        data-type="minus" data-field="">
                                                                    <span class="glyphicon glyphicon-minus"></span>
                                                                </button>
                                                            </span>
                                                    <input type="text" class="quantity" name="quantity"
                                                           class="form-control input-number" value="10" min="1"
                                                           max="100">
                                                    <span class="input-group-btn">
                                                                <button type="button"
                                                                        class="quantity-right-plus btn btn-default btn-number"
                                                                        data-type="plus" data-field="">
                                                                    <span class="glyphicon glyphicon-plus"></span>
                                                                </button>
                                                            </span>
                                                </div>
                                            </th>
                                            <th>
                                                <div class="circle"><p class="text-center">1G</p></div>
                                            </th>
                                            <th class="price-th">$<span class="price-span">270</span>.00</th>
                                            <th class="delete-th"><a href="#"><i class="fa fa-trash-o"
                                                                                 aria-hidden="true"></i></a></th>
                                        </tr>
                                            <tr>
                                                <th class="cart-table-number">1</th>
                                                <th><img src="{{ asset('assets/images/shopping-cart/product-avatar.png') }}" class="cart-product-avatar"
                                                         alt="product avatar"></th>
                                                <th>
                                                    <h4 class="cart-product-name">Pink Champagne</h4>
                                                    <span class="cart-product-cat">Indica</span>
                                                </th>
                                                <th>
                                                    <div class="input-group quantity-input">
                                                            <span class="input-group-btn">
                                                                <button type="button"
                                                                        class="quantity-left-minus btn btn-default btn-number"
                                                                        data-type="minus" data-field="">
                                                                    <span class="glyphicon glyphicon-minus"></span>
                                                                </button>
                                                            </span>
                                                        <input type="text" class="quantity" name="quantity"
                                                               class="form-control input-number" value="10" min="1"
                                                               max="100">
                                                        <span class="input-group-btn">
                                                                <button type="button"
                                                                        class="quantity-right-plus btn btn-default btn-number"
                                                                        data-type="plus" data-field="">
                                                                    <span class="glyphicon glyphicon-plus"></span>
                                                                </button>
                                                            </span>
                                                    </div>
                                                </th>
                                                <th>
                                                    <div class="circle"><p class="text-center">1G</p></div>
                                                </th>
                                                <th class="price-th">$<span class="price-span">270</span>.00</th>
                                                <th class="delete-th"><a href="#"><i class="fa fa-trash-o"
                                                                                     aria-hidden="true"></i></a></th>
                                            </tr>
                                            <tr>
                                                <th class="cart-table-number">1</th>
                                                <th><img src="{{ asset('assets/images/shopping-cart/product-avatar.png') }}" class="cart-product-avatar"
                                                         alt="product avatar"></th>
                                                <th>
                                                    <h4 class="cart-product-name">Pink Champagne</h4>
                                                    <span class="cart-product-cat">Indica</span>
                                                </th>
                                                <th>
                                                    <div class="input-group quantity-input">
                                                            <span class="input-group-btn">
                                                                <button type="button"
                                                                        class="quantity-left-minus btn btn-default btn-number"
                                                                        data-type="minus" data-field="">
                                                                    <span class="glyphicon glyphicon-minus"></span>
                                                                </button>
                                                            </span>
                                                        <input type="text" class="quantity" name="quantity"
                                                               class="form-control input-number" value="10" min="1"
                                                               max="100">
                                                        <span class="input-group-btn">
                                                                <button type="button"
                                                                        class="quantity-right-plus btn btn-default btn-number"
                                                                        data-type="plus" data-field="">
                                                                    <span class="glyphicon glyphicon-plus"></span>
                                                                </button>
                                                            </span>
                                                    </div>
                                                </th>
                                                <th>
                                                    <div class="circle"><p class="text-center">1G</p></div>
                                                </th>
                                                <th class="price-th">$<span class="price-span">270</span>.00</th>
                                                <th class="delete-th"><a href="#"><i class="fa fa-trash-o"
                                                                                     aria-hidden="true"></i></a></th>
                                            </tr>
                                            <tr>
                                                <th class="cart-table-number">1</th>
                                                <th><img src="{{ asset('assets/images/shopping-cart/product-avatar.png') }}" class="cart-product-avatar"
                                                         alt="product avatar"></th>
                                                <th>
                                                    <h4 class="cart-product-name">Pink Champagne</h4>
                                                    <span class="cart-product-cat">Indica</span>
                                                </th>
                                                <th>
                                                    <div class="input-group quantity-input">
                                                            <span class="input-group-btn">
                                                                <button type="button"
                                                                        class="quantity-left-minus btn btn-default btn-number"
                                                                        data-type="minus" data-field="">
                                                                    <span class="glyphicon glyphicon-minus"></span>
                                                                </button>
                                                            </span>
                                                        <input type="text" class="quantity" name="quantity"
                                                               class="form-control input-number" value="10" min="1"
                                                               max="100">
                                                        <span class="input-group-btn">
                                                                <button type="button"
                                                                        class="quantity-right-plus btn btn-default btn-number"
                                                                        data-type="plus" data-field="">
                                                                    <span class="glyphicon glyphicon-plus"></span>
                                                                </button>
                                                            </span>
                                                    </div>
                                                </th>
                                                <th>
                                                    <div class="circle"><p class="text-center">1G</p></div>
                                                </th>
                                                <th class="price-th">$<span class="price-span">270</span>.00</th>
                                                <th class="delete-th"><a href="#"><i class="fa fa-trash-o"
                                                                                     aria-hidden="true"></i></a></th>
                                            </tr>
                                        </tbody>
                                    </table>

                                </div>
                                <div class="col-xs-12">
                                    <span class="price-th price-total">Total : $ <span class="price-span">1180</span>.00</span>
                                    <button class="btn btn-default purchase-btn pull-right">Purchase <i class="fa fa-angle-right" aria-hidden="true"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('layouts.footer', ['mobile'=> false])
    @push('scripts')
    <script>
        $(document).ready(function () {

            var quantitiy = 0;
            $('.quantity-right-plus').click(function (e) {
                // Stop acting like a button
                e.preventDefault();
                // Get the field name
                var quantity = parseInt($('.quantity').val());
                // If is not undefined
                $('.quantity').val(quantity + 1);
                // Increment
            });

            $('.quantity-left-minus').click(function (e) {
                // Stop acting like a button
                e.preventDefault();
                // Get the field name
                var quantity = parseInt($('.quantity').val());
                // If is not undefined
                // Increment
                if (quantity > 0) {
                    $('.quantity').val(quantity - 1);
                }
            });

        });
    </script>
    @endpush
@endsection
