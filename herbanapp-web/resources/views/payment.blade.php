@extends('layouts.non-admin')
@section('content')
    <div>
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel panel-default">
                        <div class="panel-heading">Premium</div>
                        <div class="panel-body">
                            <form action="{{ url('/subscribe') }}?token={{ Cookie::get('token')}}" method="post">
                                <div id="dropin-container"></div>
                                <input type="hidden" name="token" value="{{ Cookie::get('token') }}">
                                {{ csrf_field() }}
                                <hr>
                                <button id="payment-button" class="btn btn-primary btn-flat hidden" type="submit">Pay now</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @push('scripts')
    <script src="https://js.braintreegateway.com/js/braintree-2.30.0.min.js"></script>

    <script>
        $.ajax({
            url: '{{ url('braintree/token') }}'
        }).done(function (response) {
            braintree.setup(response.data.token, 'dropin', {
                container: 'dropin-container',
                onReady: function () {
                    $('#payment-button').removeClass('hidden');
                }
            });
        });
    </script>
    @endpush

@endsection