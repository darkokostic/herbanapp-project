@extends('layouts.non-admin')
@section('content')

    @push('stylesheets')
    <link rel="stylesheet" href="{{ asset('css/not-found.css') }}">
    @endpush

<div class="container-fluid">
    <div id="not-found">
        <div class="row">
            <div class="col-xs-12">
                <img src="{{ asset('assets/images/not-found/logo.png') }}" alt="Not Found logo 404" class="img-responsive center-block">
            </div>
            <div class="col-xs-12 text-center">
                <h1>Ooops!</h1>
                <h2>It’s looking like you may have taken a wrong turn. Don’t worry… it happens to the best of us.</h2>
                <p>Here’s a reset button to help you get back home: </p>
                <a class="btn btn-default back-btn" href="/">Back to Home</a>
            </div>
        </div>
    </div>
</div>

    @push('scripts')
    <script src="{{ asset('assets/js/404/404.js') }}"></script>
    @endpush

@endsection