<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
    <title>HerbanApp</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    @stack('stylesheets')
</head>
<body ng-app="myApp">
<div  ng-controller="SearchCtrl" id="search" class="overlay-search" >
    <div class="overlay-search-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                    <div id="imaginary_container" class="center-block">
                        <div class="input-group stylish-input-group">
                            <span class="input-group-addon">
                                <button type="submit">
                                    <span class="glyphicon glyphicon-search"></span>
                                </button>
                            </span>
                            <input type="text" class="form-control search-input"
                                   placeholder="Search for anything..." ng-model="search.search_key" ng-change="searchAll()">
                        </div>
                    </div>
                </div>
                <a href="javascript:void(0)" class="closebtn" onclick="closeSearch()">&times;</a>

            </div>
            <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs search-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#products" aria-controls="products" role="tab"
                                                              data-toggle="tab" ng-click="showAllProducts()">PRODUCTS</a></li>
                    <li role="presentation"><a href="#products" aria-controls="clinics" role="tab" data-toggle="tab" ng-click="showAllClinics()">CLINICS</a>
                    </li>
                    <li role="presentation" ng-repeat="category in categories"><a href="#products" ng-click="selectCategory(category)" aria-controls="category3" role="tab" data-toggle="tab">@{{category.name}}</a>
                    </li>

                </ul>
                <hr>
                <!-- Tab panes -->
                <div class="tab-content"  >
                    <div role="tabpanel" class="tab-pane active fade in" id="products">
                        <button class="btn btn-default btn-sub-cat" ng-repeat="subCategory in sub_categories" ng-click="selectSubCategory(subCategory)">@{{subCategory.name}}</button>
                        <div id="infinite" class="row card-row center-block"    >
                            <div class="col-sm-6 col-md-6 col-lg-3 mt-4"  ng-repeat="product in productsToShow" >

                                <div ng-if="isSelected.clinic">
                                    <a href="/clinic/@{{ product.id }}">
                                        <div class="search-card center-block">
                                            <div class="overlay">
                                                <div class="overlay-image"></div>
                                                <div class="overlay">
                                                    <div class="overlay-image"></div>
                                                    <img class="card-img-top" src="http://placehold.it/300x110">
                                                </div>
                                            </div>
                                            <div class="card-block">
                                                <div class="hot-deal-info">
                                                    <div class="row search-card-text-row">
                                                        <div class="col-xs-2">

                                                            <img ng-if="product.logo" src="@{{product.logo.url }}" style="max-height: 30px; max-width: 30px;" alt="image">
                                                            <img ng-if="!product.logo" src="uploads/avatar-clinic-delivery.png" style="max-height: 30px; max-width: 30px;" alt="image">
                                                        </div>
                                                        <div class="col-xs-6 text-left">
                                                            <h6 class="search-item-title">
                                                                @{{ product.name }}
                                                            </h6>
                                                            <star-rating-comp rating="product.average_rating" show-half-stars="true" read-only="true" size="'medium'" label-position="'right'" label-text="product.average_rating"></star-rating-comp>
                                                        </div>

                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div ng-if="!isSelected.clinic">
                                    <a href="/clinic/@{{ product.clinic }}">
                                        <div class="search-card center-block">
                                            <div class="overlay">
                                                <div class="overlay-image"></div>
                                                <div class="overlay">
                                                    <div class="overlay-image"></div>
                                                    <img class="card-img-top" src="http://placehold.it/300x110">
                                                </div>
                                            </div>
                                            <div class="card-block">
                                                <div class="hot-deal-info">
                                                    <div class="row search-card-text-row">
                                                        <div class="col-xs-2">
                                                            <img ng-if="product.logo" src="@{{ product.logo.url }}" style="max-height: 30px; max-width: 30px;" alt="image">
                                                            <img ng-if="!product.logo" src="uploads/avatar-clinic-delivery.png" style="max-height: 30px; max-width: 30px;" alt="image">

                                                        </div>
                                                        <div class="col-xs-6 text-left">
                                                            <h6 class="search-item-title">
                                                                @{{ product.name | limitTo: 15 }}@{{(product.name && product.name.length > 14) ? '...' : ''}}
                                                            </h6>
                                                        </div>
                                                        <div class="col-xs-4">
                                                            <h3 ng-if="product.pricing" class="search-hot-price">@{{product.pricing}}</h3>
                                                            <h3 ng-if="!product.pricing" class="search-hot-price">@{{product.prices.gram}}g</h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<div >
@yield('content')
</div>

<script src="https://use.fontawesome.com/b873a75d75.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.1/TweenMax.min.js"></script>
<script
        src="https://code.jquery.com/jquery-3.1.1.min.js"
        integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
        crossorigin="anonymous"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
<script src="{{ asset('node_modules/scrollmagic/scrollmagic/uncompressed/ScrollMagic.js') }}"></script>
<script src="{{ asset('node_modules/scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators.js') }}"></script>
<script src="{{ asset('node_modules/scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap.js') }}"></script>
<script src="{{ asset('node_modules/scrollmagic/scrollmagic/uncompressed/plugins/jquery.ScrollMagic.js') }}"></script>
<script src="{{ asset('assets/js/jquery.buttonLoader.min.js') }}"></script>

{{--ANGULAR--}}
<script src="{{ asset('bower_components/angular/angular.min.js') }}"></script>
<script src="{{ asset('bower_components/angular-cookies/angular-cookies.js') }}"></script>
<script src="{{ asset('bower_components/ngInfiniteScroll/build/ng-infinite-scroll.js') }}"></script>
<script src="{{ asset('node_modules/angular-animate/angular-animate.min.js') }}"></script>
<script src="{{ asset('node_modules/angular-ui-bootstrap/dist/ui-bootstrap.js') }}"></script>
<script src="{{ asset('node_modules/angular-ui-bootstrap/dist/ui-bootstrap-tpls.js') }}"></script>
<script src="{{ asset('node_modules/angular1-star-rating/dist/index.js') }}"></script>

<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/angular/footer/footer_controller.js') }}"></script>
<script src="{{ asset('js/angular/shared/my_user_handler.js') }}"></script>
<script src="{{ asset('js/angular/header/header_controller.js') }}"></script>
<script src="{{ asset('js/angular/header/header_service.js') }}"></script>
<script src="{{ asset('js/angular/home/home_controller.js') }}"></script>
<script src="{{ asset('js/angular/home/home_service.js') }}"></script>
<script src="{{ asset('js/angular/clinics/clinic_list_controller.js') }}"></script>
<script src="{{ asset('js/angular/clinics/clinic_list_service.js') }}"></script>
<script src="{{ asset('js/angular/clinic/clinic_controller.js') }}"></script>
<script src="{{ asset('js/angular/clinic/clinic_service.js') }}"></script>
<script src="{{ asset('js/angular/user/user_controller.js') }}"></script>
<script src="{{ asset('js/angular/user/user_service.js') }}"></script>
<script src="{{ asset('js/angular/search/search_controller.js') }}"></script>
<script src="{{ asset('js/angular/search/search_service.js') }}"></script>
<script src="{{ asset('js/angular/modals/clinic-product/clinic-product.js') }}"></script>
<script>
    function openSearch() {
        document.getElementById("search").style.opacity = "1";
        document.getElementById("search").style.zIndex = "1000";
        $('.navbar').hide();
    }

    function closeSearch() {
        document.getElementById("search").style.opacity = "0";
        document.getElementById("search").style.zIndex = "-1000";
        $('.navbar').show();
    }


    $(document).ready(function() {
        $('#trigger-overlay').click(function() {
            $('html').css('overflow', 'hidden');
            $('body').bind('touchmove', function(e) {
                e.preventDefault()
            });
        });
        $('.closebtn').click(function() {
            $('html').css('overflow', 'scroll');
            $('body').unbind('touchmove');
        });
    });
    $(document).ready(function () {

        $('.has-spinner').click(function () {
            var btn = $(this);
            $(btn).buttonLoader('start');
            setTimeout(function () {
                $(btn).buttonLoader('stop');
            }, 3000);
        });
    });
</script>
@stack('scripts')
</body>
</html>