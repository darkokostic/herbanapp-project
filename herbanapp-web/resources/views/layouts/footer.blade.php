@push('stylesheets')
<link rel="stylesheet" href="{{ asset('css/footer.css') }}">
@endpush
@if ($mobile)
<div class="container-fluid footer-segment">
    <div class="container">
        <div class="row ">
            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                <h1 class="footer-segment-title">Looking for an ongoing supply?</h1>
                <p class="footer-segment-subtitle">Download our free app.</p>
                <button class="btn default store-btn" id="google-play"><img id="google-img"
                                                                            src="{{ asset('assets/images/footer/google-play.png') }}"
                                                                            alt="Google play icon">
                    Google Play
                </button>
                <button class="btn default store-btn" id="apple-store"><img id="apple-img"
                                                                            src="{{ asset('assets/images/footer/apple-store.png') }}"
                                                                            alt="Apple store icon"> Apple Store
                </button>

            </div>
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                <img src="{{ asset('assets/images/footer/hand.png') }}" alt="Hand image"
                     class="footer-hand hidden-xs hidden-sm">
            </div>
        </div>
    </div>
</div>
@endif
<div class="container-fluid footer-bottom ng-fade" ng-controller="FooterCtrl">
    <div class="container">
        <div class="row footer-bottom-row">
            <div class="col-xs-12 col-sm-12 col-md-7">
                <img src="{{ asset('assets/images/header/herbanapp-logo-fixed.png') }}" alt="">
                <p class="footer-text">
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the
                    industry's standard dummy text ever since the 1500s,
                </p>
                <ul class="list-inline footer-social-networks">
                    <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-play" aria-hidden="true"></i></a></li>

                </ul>
                <p class="footer-copy-text">
                    Copyright © 2017 HerbanApp.All Rights Reserved.
                </p>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-5">
                <div class="row">
                    <div class="col-xs-4">
                        <ul class="footer-list">
                            <li><h3>QUICK LINKS</h3></li>
                            <li><a href="#">Home</a></li>
                            <li><a href="#">About us</a></li>
                            <li><a href="#">Services</a></li>
                            <li><a href="#">Get Listed</a></li>
                            <li><a href="#">Contact us</a></li>
                        </ul>
                    </div>
                    <div class="col-xs-4">
                        <ul class="footer-list">
                            <li><h3>Community</h3></li>
                            <li><a href="#">Facebook</a></li>
                            <li><a href="#">Instagram</a></li>
                            <li><a href="#">Google Plus</a></li>
                            <li><a href="#">Linked In</a></li>
                            <li><a href="#">Twitter</a></li>
                        </ul>
                    </div>
                    <div class="col-xs-4">
                        <ul class="footer-list">
                            <li><h3>MOBILE APP</h3></li>
                            <li><a href="#">Download the app</a></li>
                            <li><a href="#">Documentation</a></li>
                            <li><a href="#">Developer</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@push('scripts')
<script>
    $(document).ready(function () {
        $('#google-play').on({
            'mouseover': function () {
                $('#google-img').attr("src", "{{ asset('assets/images/footer/google-play-white.png') }}");
            }
        });
        $('#google-play').on({
            'mouseout': function () {
                $('#google-img').attr("src", "{{ asset('assets/images/footer/google-play.png') }}");
            }
        });

        $('#apple-store').on({
            'mouseover': function () {
                $('#apple-img').attr("src", "{{ asset('assets/images/footer/apple-store-white.png') }}");
            }
        });
        $('#apple-store').on({
            'mouseout': function () {
                $('#apple-img').attr("src", "{{ asset('assets/images/footer/apple-store.png') }}");
            }
        });
    });
</script>
@endpush