@push('stylesheets')
<link rel="stylesheet" href="{{ asset('css/header.css') }}">
<link rel="stylesheet" href="{{ asset('css/chat.css') }}">
@endpush

@if($slide)

    <nav class="navbar navbar-herb navbar-fixed-top" role="navigation" data-spy="affix" data-offset-top="50"
         ng-controller="HeaderCtrl" ng-cloak>
        @else
            <nav class="navbar navbar-herb no-slides navbar-fixed-top" role="navigation" data-spy="affix"
                 data-offset-top="50" ng-controller="HeaderCtrl" ng-cloak>
                @endif
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <div class="business-logo-align col-md-10">
                            <a class="navbar-brand" href="/">
                                <div class="main-logo"></div>
                            </a>
                        </div>

                    </div>
                    <div class="navbar-collapse collapse">
                        <ul class="nav navbar-nav navbar-right">
                            {{--<li class="col-md-12 col-xs-12 text-center">--}}
                            {{--<div ng-if="user">--}}
                            {{--<div class="row">--}}
                            {{--<a href="/user-profile">--}}
                            {{--<div class="col-xs-12">--}}
                            {{--<img src="{{ asset('assets/images/user-profile/user-avatar.png') }}" class="img-responsive img-circle nav-user-image pull-left" alt="User Avatar">--}}
                            {{--<span class="nav-username">@{{ user.name }}</span>--}}
                            {{--</div>--}}
                            {{--</a>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--<div ng-if="!user">--}}
                            {{--<div class="btn-group sign-group">--}}
                            {{--<a href="/login" class="btn btn-default nav-signin-btn">Sign in</a>--}}
                            {{--<a href="/register" class="btn btn-default nav-signup-btn">Sign up</a>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</li>--}}
                        </ul>
                        <ul class="nav navbar-nav  navbar-centerr">
                            <li><a href="/" class="home-btn busi-btn active">Home</a></li>
                            <li><a class="explore-btn busi-btn" href="/#explore" class="busi-btn">Explore</a></li>
                            <!-- <li><a href="#" class="busi-btn">Chat</a></li> -->
                            <li><a href="/#nearby-doctors" class="nearby-doctors-btn busi-btn">Find Doctors</a></li>
                            <li><a href="/#hot-deals" class="hot-deals-btn busi-btn">Hot Deals</a></li>

                            <li><a id="trigger-overlay" onclick="openSearch()" class="busi-btn">Search</a></li>
                            <!--<li><a href="#" class="busi-btn bars"><i class="fa fa-bars" aria-hidden="true"></i></a></li> -->
                            <li class="dropdown" ng-if="user">
                                <a class="dropdown-toggle dropdown-user-profile" data-toggle="dropdown">
                                    <img src="{{ asset('assets/images/user-profile/user-avatar.png') }}"
                                         class="profile-image img-circle"> @{{ user.name }} <b class="caret"></b></a>
                                <ul class="dropdown-menu home-profile-dropdown-menu">
                                    <li class="dropdown-header user-drop-title">YOUR ACCOUNTS</li>
                                    <li>
                                        <a href="/user-profile">
                                            <img src="{{ asset('assets/images/user-profile/user-avatar.png') }}"
                                                 class="profile-image img-circle"> @{{ user.name }}
                                        </a>
                                    </li>
                                    <li class="divider"></li>
                                    <li><a href="/orders">Orders</a></li>
                                    <li><a href="#" ng-click="logout()">Logout</a></li>
                                </ul>
                            </li>
                            <li ng-if="user">
                                <a href="/shopping-cart" class="cart-header"><i class="fa fa-cart-plus" aria-hidden="true"></i><span class="badge">0</span></a>
                            </li>
                            <li>
                                <div ng-if="!user" class="sign-in-up">
                                    <div class="btn-group sign-group">
                                        <a href="/login" class="btn btn-default nav-signin-btn">Sign in</a>
                                        <a href="/signup" class="btn btn-default nav-signup-btn">Sign up</a>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            @if ($slide)
                <div class="container-fluid">
                    <div class="row">
                        <div class="fog-container">
                            <div id="header" class="header-bg">
                                <canvas id="fog"></canvas>
                                <div class="container fluid">
                                    <div class="row">
                                        <div class="col-xs-12 text-center">
                                            <h1 id="header-title">Connect with the best medical <br>
                                                and recreative marijuana.</h1>
                                            <a href="/signup" type="button" class="btn btn-default create-acc-btn">Create
                                                your
                                                account</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <!-- Chat Box -->
                <div id="chatbox">
                    <div class="container-fluid">
                        <div class="row chat-header">
                            <div class="col-xs-12">
                                <span>Clinic name</span>
                                <a onclick="closeChat()" class="close-btn pull-right"><i class="fa fa-times"
                                                                                         aria-hidden="true"></i></a>
                            </div>
                        </div>
                        <div class="row chat-content">
                            <div class="col-xs-12 user-left">
                                <div class="col-xs-2 avatar-div">
                                    <img src="{{ asset('assets/images/user-profile/user-avatar.png') }}"
                                         alt="User avatar" class="img-circle chat-user-avatar">
                                </div>
                                <div class="col-xs-10">
                                    <p class="second-user-message">Lorem Ipsum has been the</p>
                                    <div class="clearfix"></div>
                                    <span class="message-time">10:20</span>
                                </div>
                            </div>
                            <div class="col-xs-12 user-right">
                                <p class="user-message pull-right">Lorem Ipsum is simply dummy text
                                    of the printing and typesetting
                                    industry. Lorem Ipsum has been
                                    the industry's standard dummy text.</p>
                                <div class="clearfix"></div>
                                <span class="message-time pull-right">10:20</span>
                            </div>
                        </div>
                        <div class="row chat-footer">
                            <div class="input-group">
                <span class="input-group-addon" id="basic-addon1">
                    <a href="#" class="chat-smiley">
                        <i class="fa fa-smile-o" aria-hidden="true"></i>
                    </a>
                </span>
                                <input type="text" class="form-control chat-input" placeholder="Type a message..."
                                       aria-describedby="basic-addon1">
                            </div>
                        </div>
                    </div>
                </div>

            @endif
            @push('scripts')
            <script src="{{ asset('assets/js/header/header.js') }}"></script>
            <script src="{{ asset('assets/js/smoke-effect.js') }}"></script>
            <script>
                function openSearch() {
                    document.getElementById("search").style.opacity = "1";
                    document.getElementById("search").style.zIndex = "1000";
                    $('.navbar').hide();
                }

                function closeSearch() {
                    document.getElementById("search").style.opacity = "0";
                    document.getElementById("search").style.zIndex = "-1000";
                    $('.navbar').show();
                }
                $(document).ready(function () {
                    $('.nav li a').click(function (e) {

                        $('.nav li a').removeClass('active');

                        var $this = $(this);
                        if (!$this.hasClass('active')) {
                            $this.addClass('active');
                        }
                    });
                });

                // Show chat at start
                //$('#chatbox').show();

                //CHAT INPUT HANDLE
                $('.chat-input').keypress(function (e) {
                    if (e.which == 13) {
                        var currentdate = new Date();
                        $('.chat-content').append('<div class="col-xs-12 user-right"> <p class="user-message pull-right">' + $('.chat-input').val() + '</p><div class="clearfix"></div><span class="message-time pull-right">' + currentdate.getHours() + ':' + currentdate.getMinutes() + '</span></div>');
                        $('.chat-content').animate({
                            scrollTop: $('.chat-content').get(0).scrollHeight
                        }, 500);
                        $('.chat-input').val('');
                    }
                });

                function closeChat() {
                    $('#chatbox').slideUp("normal", function () {
                        $(this).remove();
                    });
                }


            </script>
    @endpush