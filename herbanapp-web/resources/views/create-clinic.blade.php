@extends('layouts.non-admin')
@section('content')
    @include('layouts.header', ['slide' => false])

    @push('stylesheets')
    <link rel="stylesheet" href="{{ asset('css/create-clinic.css') }}">
    @endpush

    <div class="container create-cln-container">
        <div class="row">
            <div class="col-xs-12 create-clinic-title">
                <h4>Create a Profile</h4>
                <hr/>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <p class="create-clinic-label">CHOOSE ACCOUNT</p>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6 col-sm-3 col-lg-2">
                <div class="panel panel-default choose-acc-btn">
                    <div class="panel-body">
                        <div class="radio">
                            <label>
                                <p class="choose-radio-label">Clinic</p>
                                <input type="radio" checked name="chooseAcc" id="clinic">
                                <label for="clinic"></label>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-sm-3 col-lg-2">
                <div class="panel panel-default choose-acc-btn">
                    <div class="panel-body">
                        <div class="radio">
                            <label>
                                <p class="choose-radio-label">Doctor</p>
                                <input type="radio" name="chooseAcc" id="doctor">
                                <label for="doctor"></label>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-7 col-sm-3 col-lg-2">
                <div class="panel panel-default choose-acc-btn">
                    <div class="panel-body">
                        <div class="radio">
                            <label>
                                <p class="choose-radio-label">Delivery Service</p>
                                <input type="radio" name="chooseAcc" id="delivery">
                                <label for="delivery"></label>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-3 col-lg-6"></div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <p class="create-clinic-label">GENERAL INFORMATIONS</p>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-3 col-lg-3 create-clinic-input">
                <p>Clinic Name</p>
                <input type="text" class="form-control" placeholder="Clinic Name">
            </div>
            <div class="col-xs-12 col-sm-3 col-lg-3 create-clinic-input">
                <p>Clinic Address</p>
                <input type="text" class="form-control" placeholder="Clinic Address">
            </div>
            <div class="col-xs-12 col-sm-3 col-lg-3 create-clinic-input">
                <p>Number</p>
                <input type="text" class="form-control" placeholder="Number">
            </div>
            <div class="col-xs-12 col-sm-3 col-lg-3 create-clinic-input">
                <p>Email</p>
                <input type="text" class="form-control" placeholder="Email">
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-3 col-lg-3 create-clinic-input">
                <p>Website</p>
                <input type="text" class="form-control" placeholder="Link of Website">
            </div>
            <div class="col-xs-12 col-sm-3 col-lg-3 create-clinic-input">
                <p>Business Phone</p>
                <input type="text" class="form-control" placeholder="Business Phone">
            </div>
            <div class="col-xs-12 col-sm-3 col-lg-3 create-clinic-input">
                <p>Business Email</p>
                <input type="text" class="form-control" placeholder="Business Email">
            </div>
            <div class="col-xs-12 col-sm-3 col-lg-3 create-clinic-input">
                <p>Legal Name</p>
                <input type="text" class="form-control" placeholder="Legal Name">
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-3 col-lg-3 create-clinic-input">
                <p>Donus Number</p>
                <input type="text" class="form-control" placeholder="Donus Number">
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-4 col-lg-4">
                <p class="create-clinic-label">WORKING HOURS</p>
                <div class="row">
                    <div class="col-xs-12 create-clinic-input">
                        <p>Working Hours</p>
                        <div class="panel-group create-clinic-collapse">
                            <div class="panel panel-default">
                                <a data-toggle="collapse" href="#clinic-info">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            All working days
                                            <i class="fa fa-caret-down pull-right" aria-hidden="true"></i>
                                        </h4>
                                    </div>
                                </a>
                                <div id="clinic-info" class="panel-collapse collapse">
                                    <div class="panel-body collapse-subitems-body">
                                        <div class="row panel-collapse-subitem">
                                            <div class="col-xs-3"><h6>Monday</h6></div>
                                            <div class="col-xs-9 text-right"><h6>..|..|..</h6></div>
                                        </div>
                                        <div class="row panel-collapse-subitem">
                                            <div class="col-xs-3"><h6>Tuesday</h6></div>
                                            <div class="col-xs-9 text-right"><h6>..|..|..</h6></div>
                                        </div>
                                        <div class="row panel-collapse-subitem">
                                            <div class="col-xs-3"><h6>Wednesday</h6></div>
                                            <div class="col-xs-9 text-right"><h6>..|..|..</h6></div>
                                        </div>
                                        <div class="row panel-collapse-subitem">
                                            <div class="col-xs-3"><h6>Thursday</h6></div>
                                            <div class="col-xs-9 text-right"><h6>..|..|..</h6></div>
                                        </div>
                                        <div class="row panel-collapse-subitem">
                                            <div class="col-xs-3"><h6>Friday</h6></div>
                                            <div class="col-xs-9 text-right"><h6>..|..|..</h6></div>
                                        </div>
                                        <div class="row panel-collapse-subitem">
                                            <div class="col-xs-3"><h6>Saturday</h6></div>
                                            <div class="col-xs-9 text-right"><h6>..|..|..</h6></div>
                                        </div>
                                        <div class="row panel-collapse-subitem">
                                            <div class="col-xs-3"><h6>Sunday</h6></div>
                                            <div class="col-xs-9 text-right"><h6>..|..|..</h6></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-5 col-lg-5">
                <p class="create-clinic-label">UPLOADS</p>
                <div class="row">
                    <div class="col-xs-6 create-clinic-input">
                        <p>Avatar Photo</p>
                        <input type="file" name="avatar" id="avatar" class="choose-clinic-pic" />
                        <label for="avatar">Choose <i class="fa fa-upload" aria-hidden="true"></i></label>
                        <p class="choose-hint">* Recommended 300x300px</p>
                    </div>
                    <div class="col-xs-6 create-clinic-input">
                        <p>Cover Photo</p>
                        <input type="file" name="cover" id="cover" class="choose-clinic-pic" />
                        <label for="cover">Choose <i class="fa fa-upload" aria-hidden="true"></i></label>
                        <p class="choose-hint">* Recommended 1920x230px</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-6 create-clinic-input">
                        <p>Gallery</p>
                        <input type="file" name="gallery" id="gallery" class="choose-clinic-pic" />
                        <label for="gallery">Choose <i class="fa fa-upload" aria-hidden="true"></i></label>
                        <p class="choose-hint">* Max size of file is 20mb</p>
                    </div>
                    <div class="col-xs-6 create-clinic-input">
                        <p>Documents</p>
                        <input type="file" name="doc" id="doc" class="choose-clinic-pic" />
                        <label for="doc">Choose <i class="fa fa-upload" aria-hidden="true"></i></label>
                        <p class="choose-hint">* Max size of file is 5mb</p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-3 col-lg-3"></div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-3 col-lg-3">
                <button type="button" class="btn btn-lg btn-block create-cl-submit-btn">Create Clinic</button>
            </div>
        </div>
    </div>
    @include('layouts.footer', ['mobile' => false])
@endsection