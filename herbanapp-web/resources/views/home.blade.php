@extends('layouts.non-admin')
@section('content')
    @include('layouts.header', ['slide' => true])

    @push('stylesheets')
    <link rel="stylesheet" href="{{ asset('css/home.css') }}">
    @endpush
    <div ng-controller="HomeCtrl">
        <input id="user.token" type="text" ng-model="token" value="@if (session('token')){{ session('token') }}@endif"
               hidden>
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <h2 class="title-get-listed">How to get listed in <span>Herban App</span></h2>
                </div>
            </div>
            <div class="row row-eq-height center-block text-center icons-row">
                <ul id="nav-tabs-seg" class="nav nav-tabs text-center" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#register-tab" aria-controls="register-tab" role="tab" data-toggle="tab">
                            <img src="{{asset('assets/images/home/register-free-icon.png')}}"
                                 alt="Register free icon">
                            <p>Register Free</p>
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="#search-tab" aria-controls="search-tab" role="tab" data-toggle="tab">
                            <img src="{{asset('assets/images/home/searching-icon.png')}}"
                                 alt="Searching icon">
                            <p>Searching</p>
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="#claim-tab" aria-controls="claim-tab" role="tab" data-toggle="tab">
                            <img src="{{asset('assets/images/home/claim-or-create-icon.png')}}"
                                 alt="Claim or create icon">
                            <p>Claim or Create</p>
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="#premium-tab" aria-controls="premium-tab" role="tab" data-toggle="tab">
                            <img src="{{asset('assets/images/home/premium-features-icon.png')}}"
                                 alt="Premium features icon">
                            <p>Premium Features</p>
                        </a>
                    </li>
                </ul>
            </div>

            <div class="tab-content" id="slide01">
                <div role="tabpanel" class="tab-pane fade in active" id="register-tab">
                    <div class="row register-content">
                        <div class="col-md-6">
                            <img src="{{asset('assets/images/home/register-image.png')}}"
                                 class="img-responsive center-block" alt="Register image">
                        </div>
                        <div class="col-md-6">
                            <h2 class="content-title">Register Free</h2>
                            <p class="register-description">
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum
                                has
                                been the industry's standard dummy text ever since the 1500s, when an unknown printer
                                took a
                                galley of type and scrambled it to make a type specimen book.
                            </p>
                            <p class="content-sub-description">
                                t has survived not only five centuries, but also the leap into electronic typesetting,
                                remaining essentially unchanged. It was popularised in the 1960s with the release
                            </p>
                            <a href="/signup" class="btn btn-default home-signup-btn">Free Sign Up</a>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="search-tab">
                    <div class="row register-content">
                        <div class="col-md-6">
                            <img src="{{asset('assets/images/home/register-image.png')}}"
                                 class="img-responsive center-block" alt="Register image">
                        </div>
                        <div class="col-md-6">
                            <h2 class="content-title">Search</h2>
                            <p class="register-description">
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum
                                has
                                been the industry's standard dummy text ever since the 1500s, when an unknown printer
                                took a
                                galley of type and scrambled it to make a type specimen book.
                            </p>
                            <p class="content-sub-description">
                                t has survived not only five centuries, but also the leap into electronic typesetting,
                                remaining essentially unchanged. It was popularised in the 1960s with the release
                            </p>
                            <a href="/signup" class="btn btn-default home-signup-btn">Free Sign Up</a>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="claim-tab">
                    <div class="row register-content">
                        <div class="col-md-6">
                            <img src="{{asset('assets/images/home/register-image.png')}}"
                                 class="img-responsive center-block" alt="Register image">
                        </div>
                        <div class="col-md-6">
                            <h2 class="content-title">Claim or create clinic</h2>
                            <p class="register-description">
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum
                                has
                                been the industry's standard dummy text ever since the 1500s, when an unknown printer
                                took a
                                galley of type and scrambled it to make a type specimen book.
                            </p>
                            <p class="content-sub-description">
                                t has survived not only five centuries, but also the leap into electronic typesetting,
                                remaining essentially unchanged. It was popularised in the 1960s with the release
                            </p>
                            <a href="/signup" class="btn btn-default home-signup-btn">Free Sign Up</a>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="premium-tab">
                    <div class="row register-content">
                        <div class="col-md-6">
                            <img src="{{asset('assets/images/home/register-image.png')}}"
                                 class="img-responsive center-block" alt="Register image">
                        </div>
                        <div class="col-md-6">
                            <h2 class="content-title">Premium Features</h2>
                            <p class="register-description">
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum
                                has
                                been the industry's standard dummy text ever since the 1500s, when an unknown printer
                                took a
                                galley of type and scrambled it to make a type specimen book.
                            </p>
                            <p class="content-sub-description">
                                t has survived not only five centuries, but also the leap into electronic typesetting,
                                remaining essentially unchanged. It was popularised in the 1960s with the release
                            </p>
                            <a href="/signup" class="btn btn-default home-signup-btn">Free Sign Up</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="segment-two" class="container-fluid segment-two">
            <div class="row ">
                <div class="col-xs-12">
                    <h1 class="text-center segment-two-title">Our statistics counted from start</h1>
                </div>
            </div>
            <div class="row home-icons-container center-block text-center">
                <div class="col-xs-6 col-sm-3">
                    <img src="{{asset('assets/images/home/segment-two/people.png')}}" alt="People icon"
                         class="center-block img-responsive">
                    <h3>USERS</h3>
                    <h4 class="counter-value" ng-class="count" data-count="@{{statistics.users}}">0</h4>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <img src="{{asset('assets/images/home/segment-two/chat.png')}}" alt="Chat icon"
                         class="center-block img-responsive">
                    <h3>CHAT MESSAGES</h3>
                    <h4 class="counter-value" ng-class="count" data-count="1235">0</h4>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <img src="{{asset('assets/images/home/segment-two/building.png')}}" alt="Clinic icon"
                         class="center-block img-responsive">
                    <h3>CLINICS</h3>
                    <h4 class="counter-value" ng-class="count" data-count="@{{statistics.clinics}}">0</h4>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <img src="{{asset('assets/images/home/segment-two/handshake.png')}}" alt="Hand shake icon"
                         class="center-block img-responsive">
                    <h3>DEALS</h3>
                    <h4 class="counter-value" ng-class="count" data-count="@{{ statistics.deals }}">0</h4>
                </div>
            </div>
        </div>

        <div class="container segment-three" id="explore">
            <!-- Nearby clinics -->
            <div class="row-eq-height filter-bar center-block">
                <div class="col-xs-6">
                    <h1 class="nearby-title">Nearby Clinics</h1>
                </div>
                <div class="col-xs-6 text-right">
                    <div class="btn-group">

                        <a class="btn btn-default sort-btn pull-right" href="/clinics">View all</a>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div ng-show="showLoader" class="herban-loader">
                        <div id="loader" >
                            <div class="anim">
                                <img src="{{ asset('assets/images/loader/1.svg') }}" />
                                <img src="{{ asset('assets/images/loader/2.svg') }}" />
                                <img src="{{ asset('assets/images/loader/3.svg') }}" />
                                <img src="{{ asset('assets/images/loader/1.svg') }}" />
                                <img src="{{ asset('assets/images/loader/2.svg') }}" />
                                <img src="{{ asset('assets/images/loader/3.svg') }}" />
                                <img src="{{ asset('assets/images/loader/1.svg') }}" />
                            </div>
                        </div>
                        <h3 class="text-center herban-loading">Loading <span>.</span><span>.</span><span>.</span></h3>
                    </div>
                    <div ng-show="!showLoader && !nearByClinics">
                        There are no clinics!
                    </div>
                    <div ng-show="!showLoader && nearByClinics" id="nearByClinics" class="carousel slide ng-fade">
                        <ol class="carousel-indicators">
                            <li data-target="#nearByClinics" ng-repeat="i in getNumber(clinicPages) track by $index"
                                ng-class="$index == 0? 'active': ''" data-slide-to="@{{$index }}"></li>
                        </ol>
                        <!-- Carousel items -->
                        <div class="carousel-inner">
                            <div ng-repeat="clinics in nearByClinics track by $index"
                                 ng-class="$index == 0 ? 'active' : ''" class="item ">
                                <div class="row">
                                    <div ng-repeat="clinic in clinics " class="col-sm-6 col-md-6 col-lg-3 mt-4">
                                        <a href="/clinic/@{{ clinic.id }}">
                                            <div class="card center-block">
                                                <div class="overlay">
                                                    <div class="overlay-image"></div>
                                                    <img ng-if="clinic.cover" class="card-img-top" ng-src="@{{ clinic.cover.url }}">
                                                    <img ng-if="!clinic.cover" class="card-img-top" src="/assets/images/clinic/cover-image.png">
                                                </div>
                                                <div class="card-block">
                                                    <figure class="profile">
                                                        <img ng-if="clinic.logo"  ng-src="@{{ clinic.logo.url }}" class="profile-avatar"
                                                             alt="Clinic logo">
                                                        <img ng-if="!clinic.logo"  src="assets/images/clinic/clinic-avatar.png" class="profile-avatar"
                                                             alt="Clinic logo">
                                                    </figure>
                                                    <div class="profile-info">
                                                        <h1 class="clinic-name">@{{clinic.name | limitTo: 14}}@{{(clinic.name && clinic.name.length > 16) ? '...' : ''}}</h1>
                                                        <star-rating-comp rating="clinic.average_rating" show-half-stars="true" read-only="true" size="'medium'" label-position="'right'" label-text="clinic.average_rating"></star-rating-comp>
                                                    </div>
                                                    <div class="card-info">
                                                        <h4 class="card-title mt-3"><span><i class="fa fa-map-marker"
                                                                                             aria-hidden="true"></i></span>
                                                            @{{ clinic.address }}
                                                        </h4>
                                                        <h4 class="card-title mt-3"><span><i class="fa fa-clock-o"
                                                                                             aria-hidden="true"></i></span>
                                                            09:00 - 18:00</h4>
                                                        <h4 class="card-title mt-3"><span><i class="fa fa-map-marker"
                                                                                             aria-hidden="true"></i></span>

                                                            @{{ clinic.distance |number:2 }} miles away</h4>
                                                    </div>
                                                    <div class="card-text">
                                                        @{{ clinic.about_us | limitTo:80 }}...
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div><!--.row-->
                            </div><!--.item-->
                        </div><!--.carousel-inner-->
                        <a data-slide="prev" href="#nearByClinics" class="left carousel-control">‹</a>
                        <a data-slide="next" href="#nearByClinics" class="right carousel-control">›</a>
                    </div><!--.Carousel-->

                </div>
            </div>

            <!-- Nearby doctors -->
            <div id="nearby-doctors" class="row-eq-height filter-bar center-block">
                <div class="col-xs-6">
                    <h1 class="nearby-title">Nearby Doctors</h1>
                </div>
                <div class="col-xs-6 text-right">
                    <div class="btn-group">
                        <button class="btn btn-default sort-btn pull-right">View all</button>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div ng-show="showLoader" class="herban-loader">
                        <div id="loader" >
                            <div class="anim">
                                <img src="{{ asset('assets/images/loader/1.svg') }}" />
                                <img src="{{ asset('assets/images/loader/2.svg') }}" />
                                <img src="{{ asset('assets/images/loader/3.svg') }}" />
                                <img src="{{ asset('assets/images/loader/1.svg') }}" />
                                <img src="{{ asset('assets/images/loader/2.svg') }}" />
                                <img src="{{ asset('assets/images/loader/3.svg') }}" />
                                <img src="{{ asset('assets/images/loader/1.svg') }}" />
                            </div>
                        </div>
                        <h3 class="text-center herban-loading">Loading <span>.</span><span>.</span><span>.</span></h3>
                    </div>
                    <div ng-show="!showLoader && !nearByDoctors">
                        There are no clinics!
                    </div>
                    <div ng-show="!showLoader && nearByDoctors" id="nearByDoctors" class="carousel slide ng-fade">
                        <ol class="carousel-indicators">
                            <li data-target="#nearByDoctors" ng-repeat="i in getNumber(doctorPages) track by $index"
                                ng-class="$index == 0? 'active': ''" data-slide-to="@{{$index }}"></li>
                        </ol>
                        <!-- Carousel items -->
                        <div class="carousel-inner">
                            <div ng-repeat="doctors in nearByDoctors track by $index"
                                 ng-class="$index == 0 ? 'active' : ''" class="item ">
                                <div class="row">
                                    <div ng-repeat="doctor in doctors track by $index" class="col-sm-6 col-md-6 col-lg-3 mt-4">
                                        <a href="/doctor/@{{ doctor.id }}">
                                            <div class="card center-block">
                                                <div class="overlay">
                                                    <div class="overlay-image"></div>
                                                    <img ng-if="doctor.cover" class="card-img-top" ng-src="@{{ doctor.cover.url }}">
                                                    <img ng-if="!doctor.cover" class="card-img-top" src="/assets/images/clinic/cover-image.png">
                                                </div>
                                                <div class="card-block">
                                                    <figure class="profile">
                                                        <img ng-if="doctor.logo"  ng-src="@{{ doctor.logo.url }}" class="profile-avatar"
                                                             alt="Clinic logo">
                                                        <img ng-if="!doctor.logo"  src="assets/images/clinic/clinic-avatar.png" class="profile-avatar"
                                                             alt="Clinic logo">
                                                    </figure>
                                                    <div class="profile-info">
                                                        <h1 class="clinic-name">@{{doctor.name | limitTo: 14}}@{{(doctor.name && doctor.name.length > 16) ? '...' : ''}}</h1>
                                                        <star-rating-comp rating="doctor.average_rating" show-half-stars="true" read-only="true" size="'medium'" label-position="'right'" label-text="doctor.average_rating"></star-rating-comp>
                                                    </div>
                                                    <div class="card-info">
                                                        <h4 class="card-title mt-3"><span><i class="fa fa-map-marker"
                                                                                             aria-hidden="true"></i></span>
                                                            @{{ doctor.address     }}
                                                        </h4>
                                                        <h4 class="card-title mt-3"><span><i class="fa fa-clock-o"
                                                                                             aria-hidden="true"></i></span>
                                                            09:00 - 18:00</h4>
                                                        <h4 class="card-title mt-3"><span><i class="fa fa-map-marker"
                                                                                             aria-hidden="true"></i></span>

                                                            @{{ doctor.distance |number:2 }} miles away</h4>
                                                    </div>
                                                    <div class="card-text">
                                                        @{{ doctor.about_us | limitTo:80 }}...
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div><!--.row-->
                            </div><!--.item-->
                        </div><!--.carousel-inner-->
                        <a data-slide="prev" href="#nearByDoctors" class="left carousel-control">‹</a>
                        <a data-slide="next" href="#nearByDoctors" class="right carousel-control">›</a>
                    </div><!--.Carousel-->

                </div>
            </div>
        </div>

        <div id="segment-two" class="container-fluid segment-two">
            <div class="row ">
                <div class="col-xs-12">
                    <h1 class="text-center segment-two-title">Get started for free! Because together we are stoned.</h1>
                </div>
            </div>
            <div class="row center-block text-center">
                <div class="col-xs-12">
                    <p class="segment-subtitle">Lorem Ipsum is simply dummy text of the printing and typesetting
                        industry.</p>
                </div>
                <div class="col-xs-12 center-block">
                    <div class="input-group subcribe-mail-group center-block">
                        <form>
                            <input type="email" class="form-control" placeholder="Your email address" required>
                            <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">Start</button>
                                </span>
                        </form>
                    </div><!-- /input-group -->
                </div><!-- /.col-lg-6 -->
            </div>
        </div>

        <!-- Hot Deals -->
        <div class="container">
            <div id="hot-deals" class="row-eq-height filter-bar center-block">
                <div class="col-xs-6">
                    <h1 class="nearby-title">Hot Deals</h1>
                </div>
                <div class="col-xs-6 text-right">
                    <div class="btn-group">
                        {{--<div class="dropdown">--}}
                            {{--<button class="btn btn-default dropdown-toggle  dropdown-menu-left sort-btn" type="button"--}}
                                    {{--id="sortByDeals"--}}
                                    {{--data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">--}}
                                {{--Sort by--}}
                                {{--<span class="caret"></span>--}}
                            {{--</button>--}}
                            {{--<ul class="dropdown-menu text-left" aria-labelledby="sortByDeals">--}}
                                {{--<li><a href="#">Action</a></li>--}}
                                {{--<li><a href="#">Another action</a></li>--}}
                                {{--<li><a href="#">Something else here</a></li>--}}
                                {{--<li role="separator" class="divider"></li>--}}
                                {{--<li><a href="#">Separated link</a></li>--}}
                            {{--</ul>--}}
                        {{--</div>--}}
                        <button class="btn btn-default sort-btn pull-right">View all</button>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div id="hotDeals" class="carousel slide">

                        <ol class="carousel-indicators">
                            <li data-target="#hotDeals" data-slide-to="0" class="active"></li>
                            <li data-target="#hotDeals" data-slide-to="1"></li>
                            <li data-target="#hotDeals" data-slide-to="2"></li>
                        </ol>

                        <!-- Carousel items -->
                        <div class="carousel-inner">
                            <div class="item active">
                                <div class="row">
                                    <div class="col-sm-6 col-md-6 col-lg-3 mt-4">
                                        <div class="card center-block">
                                            <div class="overlay">
                                                <div class="overlay-image"></div>
                                                <div class="overlay">
                                                    <div class="overlay-image"></div>
                                                    <img class="card-img-top" src="http://placehold.it/300x100">
                                                </div>
                                            </div>
                                            <div class="card-block">
                                                <div class="price-info text-right">
                                                    <h2 class="old-price">
                                                        <del>2000$</del>
                                                    </h2>
                                                    <h1 class="hot-price">1000$</h1>
                                                </div>
                                                <div class="hot-deal-info">
                                                    <h5>
                                                        7 GRAMS FOR $50 TOP
                                                        SHELF ONLY
                                                    </h5>
                                                    <input name="input-name" type="number" class="rating" min=1 max=5
                                                           showClear="false" step="2" data-size="xs" data-rtl="false"
                                                           value="4" displayOnly>
                                                    <span class="rating-number">5.00</span>
                                                </div>
                                                <p class="hot-deals-text">
                                                    Lorem Ipsum is simply dummy text of the printing and typesetting
                                                    industry.
                                                </p>
                                                <div>
                                                    <button class="btn btn-default add-to-cart-btn" type="submit"><i
                                                                class="fa fa-shopping-cart" aria-hidden="true"></i>
                                                        Add to cart
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-3 mt-4">
                                        <div class="card center-block">
                                            <div class="overlay">
                                                <div class="overlay-image"></div>
                                                <div class="overlay">
                                                    <div class="overlay-image"></div>
                                                    <img class="card-img-top" src="http://placehold.it/300x100">
                                                </div>
                                            </div>
                                            <div class="card-block">
                                                <div class="price-info text-right">
                                                    <h2 class="old-price">
                                                        <del>2000$</del>
                                                    </h2>
                                                    <h1 class="hot-price">1000$</h1>
                                                </div>
                                                <div class="hot-deal-info">
                                                    <h5>
                                                        7 GRAMS FOR $50 TOP
                                                        SHELF ONLY
                                                    </h5>
                                                    <input name="input-name" type="number" class="rating" min=1 max=5
                                                           showClear="false" step="2" data-size="xs" data-rtl="false"
                                                           value="4" displayOnly>
                                                    <span class="rating-number">5.00</span>
                                                </div>
                                                <p class="hot-deals-text">
                                                    Lorem Ipsum is simply dummy text of the printing and typesetting
                                                    industry.
                                                </p>
                                                <div>
                                                    <button class="btn btn-default add-to-cart-btn" type="submit"><i
                                                                class="fa fa-shopping-cart" aria-hidden="true"></i>
                                                        Add to cart
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-3 mt-4">
                                        <div class="card center-block">
                                            <div class="overlay">
                                                <div class="overlay-image"></div>
                                                <div class="overlay">
                                                    <div class="overlay-image"></div>
                                                    <img class="card-img-top" src="http://placehold.it/300x100">
                                                </div>
                                            </div>
                                            <div class="card-block">
                                                <div class="price-info text-right">
                                                    <h2 class="old-price">
                                                        <del>2000$</del>
                                                    </h2>
                                                    <h1 class="hot-price">1000$</h1>
                                                </div>
                                                <div class="hot-deal-info">
                                                    <h5>
                                                        7 GRAMS FOR $50 TOP
                                                        SHELF ONLY
                                                    </h5>
                                                    <input name="input-name" type="number" class="rating" min=1 max=5
                                                           showClear="false" step="2" data-size="xs" data-rtl="false"
                                                           value="4" displayOnly>
                                                    <span class="rating-number">5.00</span>
                                                </div>
                                                <p class="hot-deals-text">
                                                    Lorem Ipsum is simply dummy text of the printing and typesetting
                                                    industry.
                                                </p>
                                                <div>
                                                    <button class="btn btn-default add-to-cart-btn" type="submit"><i
                                                                class="fa fa-shopping-cart" aria-hidden="true"></i>
                                                        Add to cart
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-3 mt-4">
                                        <div class="card center-block">
                                            <div class="overlay">
                                                <div class="overlay-image"></div>
                                                <div class="overlay">
                                                    <div class="overlay-image"></div>
                                                    <img class="card-img-top" src="http://placehold.it/300x100">
                                                </div>
                                            </div>
                                            <div class="card-block">
                                                <div class="price-info text-right">
                                                    <h2 class="old-price">
                                                        <del>2000$</del>
                                                    </h2>
                                                    <h1 class="hot-price">1000$</h1>
                                                </div>
                                                <div class="hot-deal-info">
                                                    <h5>
                                                        7 GRAMS FOR $50 TOP
                                                        SHELF ONLY
                                                    </h5>
                                                    <input name="input-name" type="number" class="rating" min=1 max=5
                                                           showClear="false" step="2" data-size="xs" data-rtl="false"
                                                           value="4" displayOnly>
                                                    <span class="rating-number">5.00</span>
                                                </div>
                                                <p class="hot-deals-text">
                                                    Lorem Ipsum is simply dummy text of the printing and typesetting
                                                    industry.
                                                </p>
                                                <div>
                                                    <button class="btn btn-default add-to-cart-btn" type="submit"><i
                                                                class="fa fa-shopping-cart" aria-hidden="true"></i>
                                                        Add to cart
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div><!--.row-->
                            </div><!--.item-->
                            <div class="item">
                                <div class="row">
                                    <div class="col-sm-6 col-md-6 col-lg-3 mt-4">
                                        <div class="card center-block">
                                            <div class="overlay">
                                                <div class="overlay-image"></div>
                                                <div class="overlay">
                                                    <div class="overlay-image"></div>
                                                    <img class="card-img-top" src="http://placehold.it/300x100">
                                                </div>
                                            </div>
                                            <div class="card-block">
                                                <div class="price-info text-right">
                                                    <h2 class="old-price">
                                                        <del>2000$</del>
                                                    </h2>
                                                    <h1 class="hot-price">1000$</h1>
                                                </div>
                                                <div class="hot-deal-info">
                                                    <h5>
                                                        7 GRAMS FOR $50 TOP
                                                        SHELF ONLY
                                                    </h5>
                                                    <input name="input-name" type="number" class="rating" min=1 max=5
                                                           showClear="false" step="2" data-size="xs" data-rtl="false"
                                                           value="4" displayOnly>
                                                    <span class="rating-number">5.00</span>
                                                </div>
                                                <p class="hot-deals-text">
                                                    Lorem Ipsum is simply dummy text of the printing and typesetting
                                                    industry.
                                                </p>
                                                <div>
                                                    <button class="btn btn-default add-to-cart-btn" type="submit"><i
                                                                class="fa fa-shopping-cart" aria-hidden="true"></i>
                                                        Add to cart
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-3 mt-4">
                                        <div class="card center-block">
                                            <div class="overlay">
                                                <div class="overlay-image"></div>
                                                <div class="overlay">
                                                    <div class="overlay-image"></div>
                                                    <img class="card-img-top" src="http://placehold.it/300x100">
                                                </div>
                                            </div>
                                            <div class="card-block">
                                                <div class="price-info text-right">
                                                    <h2 class="old-price">
                                                        <del>2000$</del>
                                                    </h2>
                                                    <h1 class="hot-price">1000$</h1>
                                                </div>
                                                <div class="hot-deal-info">
                                                    <h5>
                                                        7 GRAMS FOR $50 TOP
                                                        SHELF ONLY
                                                    </h5>
                                                    <input name="input-name" type="number" class="rating" min=1 max=5
                                                           showClear="false" step="2" data-size="xs" data-rtl="false"
                                                           value="4" displayOnly>
                                                    <span class="rating-number">5.00</span>
                                                </div>
                                                <p class="hot-deals-text">
                                                    Lorem Ipsum is simply dummy text of the printing and typesetting
                                                    industry.
                                                </p>
                                                <div>
                                                    <button class="btn btn-default add-to-cart-btn" type="submit"><i
                                                                class="fa fa-shopping-cart" aria-hidden="true"></i>
                                                        Add to cart
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-3 mt-4">
                                        <div class="card center-block">
                                            <div class="overlay">
                                                <div class="overlay-image"></div>
                                                <div class="overlay">
                                                    <div class="overlay-image"></div>
                                                    <img class="card-img-top" src="http://placehold.it/300x100">
                                                </div>
                                            </div>
                                            <div class="card-block">
                                                <div class="price-info text-right">
                                                    <h2 class="old-price">
                                                        <del>2000$</del>
                                                    </h2>
                                                    <h1 class="hot-price">1000$</h1>
                                                </div>
                                                <div class="hot-deal-info">
                                                    <h5>
                                                        7 GRAMS FOR $50 TOP
                                                        SHELF ONLY
                                                    </h5>
                                                    <input name="input-name" type="number" class="rating" min=1 max=5
                                                           showClear="false" step="2" data-size="xs" data-rtl="false"
                                                           value="4" displayOnly>
                                                    <span class="rating-number">5.00</span>
                                                </div>
                                                <p class="hot-deals-text">
                                                    Lorem Ipsum is simply dummy text of the printing and typesetting
                                                    industry.
                                                </p>
                                                <div>
                                                    <button class="btn btn-default add-to-cart-btn" type="submit"><i
                                                                class="fa fa-shopping-cart" aria-hidden="true"></i>
                                                        Add to cart
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-3 mt-4">
                                        <div class="card center-block">
                                            <div class="overlay">
                                                <div class="overlay-image"></div>
                                                <div class="overlay">
                                                    <div class="overlay-image"></div>
                                                    <img class="card-img-top" src="http://placehold.it/300x100">
                                                </div>
                                            </div>
                                            <div class="card-block">
                                                <div class="price-info text-right">
                                                    <h2 class="old-price">
                                                        <del>2000$</del>
                                                    </h2>
                                                    <h1 class="hot-price">1000$</h1>
                                                </div>
                                                <div class="hot-deal-info">
                                                    <h5>
                                                        7 GRAMS FOR $50 TOP
                                                        SHELF ONLY
                                                    </h5>
                                                    <input name="input-name" type="number" class="rating" min=1 max=5
                                                           showClear="false" step="2" data-size="xs" data-rtl="false"
                                                           value="4" displayOnly>
                                                    <span class="rating-number">5.00</span>
                                                </div>
                                                <p class="hot-deals-text">
                                                    Lorem Ipsum is simply dummy text of the printing and typesetting
                                                    industry.
                                                </p>
                                                <div>
                                                    <button class="btn btn-default add-to-cart-btn" type="submit"><i
                                                                class="fa fa-shopping-cart" aria-hidden="true"></i>
                                                        Add to cart
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div><!--.row-->
                            </div><!--.item-->
                            <div class="item">
                                <div class="row">
                                    <div class="col-sm-6 col-md-6 col-lg-3 mt-4">
                                        <div class="card center-block">
                                            <div class="overlay">
                                                <div class="overlay-image"></div>
                                                <div class="overlay">
                                                    <div class="overlay-image"></div>
                                                    <img class="card-img-top" src="http://placehold.it/300x100">
                                                </div>
                                            </div>
                                            <div class="card-block">
                                                <div class="price-info text-right">
                                                    <h2 class="old-price">
                                                        <del>2000$</del>
                                                    </h2>
                                                    <h1 class="hot-price">1000$</h1>
                                                </div>
                                                <div class="hot-deal-info">
                                                    <h5>
                                                        7 GRAMS FOR $50 TOP
                                                        SHELF ONLY
                                                    </h5>
                                                    <input name="input-name" type="number" class="rating" min=1 max=5
                                                           showClear="false" step="2" data-size="xs" data-rtl="false"
                                                           value="4" displayOnly>
                                                    <span class="rating-number">5.00</span>
                                                </div>
                                                <p class="hot-deals-text">
                                                    Lorem Ipsum is simply dummy text of the printing and typesetting
                                                    industry.
                                                </p>
                                                <div>
                                                    <button class="btn btn-default add-to-cart-btn" type="submit"><i
                                                                class="fa fa-shopping-cart" aria-hidden="true"></i>
                                                        Add to cart
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-3 mt-4">
                                        <div class="card center-block">
                                            <div class="overlay">
                                                <div class="overlay-image"></div>
                                                <div class="overlay">
                                                    <div class="overlay-image"></div>
                                                    <img class="card-img-top" src="http://placehold.it/300x100">
                                                </div>
                                            </div>
                                            <div class="card-block">
                                                <div class="price-info text-right">
                                                    <h2 class="old-price">
                                                        <del>2000$</del>
                                                    </h2>
                                                    <h1 class="hot-price">1000$</h1>
                                                </div>
                                                <div class="hot-deal-info">
                                                    <h5>
                                                        7 GRAMS FOR $50 TOP
                                                        SHELF ONLY
                                                    </h5>
                                                    <input name="input-name" type="number" class="rating" min=1 max=5
                                                           showClear="false" step="2" data-size="xs" data-rtl="false"
                                                           value="4" displayOnly>
                                                    <span class="rating-number">5.00</span>
                                                </div>
                                                <p class="hot-deals-text">
                                                    Lorem Ipsum is simply dummy text of the printing and typesetting
                                                    industry.
                                                </p>
                                                <div>
                                                    <button class="btn btn-default add-to-cart-btn" type="submit"><i
                                                                class="fa fa-shopping-cart" aria-hidden="true"></i>
                                                        Add to cart
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-3 mt-4">
                                        <div class="card center-block">
                                            <div class="overlay">
                                                <div class="overlay-image"></div>
                                                <div class="overlay">
                                                    <div class="overlay-image"></div>
                                                    <img class="card-img-top" src="https://placehold.it/300x100">
                                                </div>
                                            </div>
                                            <div class="card-block">
                                                <div class="price-info text-right">
                                                    <h2 class="old-price">
                                                        <del>2000$</del>
                                                    </h2>
                                                    <h1 class="hot-price">1000$</h1>
                                                </div>
                                                <div class="hot-deal-info">
                                                    <h5>
                                                        7 GRAMS FOR $50 TOP
                                                        SHELF ONLY
                                                    </h5>
                                                    <input name="input-name" type="number" class="rating" min=1 max=5
                                                           showClear="false" step="2" data-size="xs" data-rtl="false"
                                                           value="4" displayOnly>
                                                    <span class="rating-number">5.00</span>
                                                </div>
                                                <p class="hot-deals-text">
                                                    Lorem Ipsum is simply dummy text of the printing and typesetting
                                                    industry.
                                                </p>
                                                <div>
                                                    <button class="btn btn-default add-to-cart-btn" type="submit"><i
                                                                class="fa fa-shopping-cart" aria-hidden="true"></i>
                                                        Add to cart
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-3 mt-4">
                                        <div class="card center-block">
                                            <div class="overlay">
                                                <div class="overlay-image"></div>
                                                <div class="overlay">
                                                    <div class="overlay-image"></div>
                                                    <img class="card-img-top" src="http://placehold.it/300x100">
                                                </div>
                                            </div>
                                            <div class="card-block">
                                                <div class="price-info text-right">
                                                    <h2 class="old-price">
                                                        <del>2000$</del>
                                                    </h2>
                                                    <h1 class="hot-price">1000$</h1>
                                                </div>
                                                <div class="hot-deal-info">
                                                    <h5>
                                                        7 GRAMS FOR $50 TOP
                                                        SHELF ONLY
                                                    </h5>
                                                    <input name="input-name" type="number" class="rating" min=1 max=5
                                                           showClear="false" step="2" data-size="xs" data-rtl="false"
                                                           value="4" displayOnly>
                                                    <span class="rating-number">5.00</span>
                                                </div>
                                                <p class="hot-deals-text">
                                                    Lorem Ipsum is simply dummy text of the printing and typesetting
                                                    industry.
                                                </p>
                                                <div>
                                                    <button class="btn btn-default add-to-cart-btn" type="submit"><i
                                                                class="fa fa-shopping-cart" aria-hidden="true"></i>
                                                        Add to cart
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div><!--.row-->
                            </div><!--.item-->
                        </div><!--.carousel-inner-->
                        <a data-slide="prev" href="#hotDeals" class="left carousel-control">‹</a>
                        <a data-slide="next" href="#hotDeals" class="right carousel-control">›</a>
                    </div><!--.Carousel-->

                </div>
            </div>
        </div>

        <!--<div class="container sponsors-container">
            <hr class="sponsor-break">
            <img class="col-md-1 col-sm-3 col-xs-4 sponsor-logo img-responsive" src="http://placehold.it/70x70/"
                 alt="...">
            <img class="col-md-1 col-sm-3 col-xs-4 sponsor-logo img-responsive" src="http://placehold.it/70x70/"
                 alt="...">
            <img class="col-md-1 col-sm-3 col-xs-4 sponsor-logo img-responsive" src="http://placehold.it/70x70/"
                 alt="...">
            <img class="col-md-1 col-sm-3 col-xs-4 sponsor-logo img-responsive" src="http://placehold.it/70x70/"
                 alt="...">
            <img class="col-md-1 col-sm-3 col-xs-4 sponsor-logo img-responsive" src="http://placehold.it/70x70/"
                 alt="...">
            <img class="col-md-1 col-sm-3 col-xs-4 sponsor-logo img-responsive" src="http://placehold.it/70x70/"
                 alt="...">
            <img class="col-md-1 col-sm-3 col-xs-4 sponsor-logo img-responsive" src="http://placehold.it/70x70/"
                 alt="...">
            <img class="col-md-1 col-sm-3 col-xs-4 sponsor-logo img-responsive" src="http://placehold.it/70x70/"
                 alt="...">
            <img class="col-md-1 col-sm-3 col-xs-4 sponsor-logo img-responsive" src="http://placehold.it/70x70/"
                 alt="...">
            <img class="col-md-1 col-sm-3 col-xs-4 sponsor-logo img-responsive" src="http://placehold.it/70x70/"
                 alt="...">
            <img class="col-md-1 col-sm-3 col-xs-4 sponsor-logo img-responsive" src="http://placehold.it/70x70/"
                 alt="...">
            <img class="col-md-1 col-sm-3 col-xs-4 sponsor-logo img-responsive" src="http://placehold.it/70x70/"
                 alt="...">
        </div> -->
    </div>

    </div>

    @include('layouts.footer', ['mobile'=> true])
    @push('scripts')

    <script src="{{ asset('node_modules/waypoints/lib/jquery.waypoints.min.js') }}"></script>
    <script src="{{ asset('assets/js/mobile-swipe.js') }}"></script>
    <script src="{{ asset('assets/js/home/home.js') }}"></script>
    <script>
        var a = 0;
        setTimeout(function () {

            $(window).scroll(function () {

                var oTop = $('#register-tab').offset().top;
                if (a == 0 && $(window).scrollTop() > oTop) {
                    $('.counter-value').each(function () {
                        var $this = $(this),
                            countTo = $this.attr('data-count');
                        $({
                            countNum: $this.text()
                        }).animate({
                                countNum: countTo
                            },
                            {
                                duration: 1000,
                                easing: 'swing',
                                step: function () {
                                    $this.text(Math.floor(this.countNum));
                                },
                                complete: function () {
                                    $this.text(this.countNum);
                                    //alert('finished');
                                }
                            });
                    });
                    a = 1;
                }
            });
        }, 2000);

        /**
         * Init carousel Near by clinics
         */
        $(document).ready(function () {
            $('#nearByClinics').carousel({
                interval: 0
            })
        });

        /**
         * Make carousel swipe on mobile device
         */
        //Carousel swipe sensitivity
        $('#nearByClinics').bcSwipe({interval: false, threshold: 80});

        $('.explore-btn').on('click', function () {
            $(document.body).animate({
                'scrollTop': $('.segment-three').offset().top - 50
            }, 1000);
        });

        $('.home-btn').on('click', function (event) {
            event.preventDefault();
            $(document.body).animate({
                'scrollTop': 0
            }, 1000);
        });

        $('.nearby-doctors-btn').on('click', function (event) {
            event.preventDefault();
            $(document.body).animate({
                'scrollTop': $('#nearby-doctors').offset().top - 100
            }, 1000);
        });

        $('.hot-deals-btn').on('click', function (event) {
            event.preventDefault();
            $(document.body).animate({
                'scrollTop': $('#hot-deals').offset().top - 100
            }, 1000);
        });
    </script>
    @endpush
@endsection
