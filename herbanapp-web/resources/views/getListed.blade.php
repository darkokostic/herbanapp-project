@extends('layouts.non-admin')
@include('layouts.header')
@section('content')
    @push('stylesheets')
    <link rel="stylesheet" href="{{ asset('css/home.css') }}">
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-star-rating/css/star-rating.css') }}">
    <link rel="stylesheet" href="{{ asset('css/footer.css') }}">
    <link rel="stylesheet" href="{{ asset('css/getListed.css') }}">
    @endpush
<div class="container container-table" ng-app="myApp" ng-controller="GetListedCtrl">
    <div class="row vertical-center-row">
        <div class="text-center col-md-5 col-md-offset-4">
			<h4>HOW TO GET LISTED IN <span>HERBAN APP</span></h4>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-5 col-lg-5 col-xl-5   col-sm-offset-3  col-md-offset-4 col-lg-offset-4 col-xl-offset-4">
		<br>
			<div id="branching" class="" style="background-image: url('{{asset('assets/images/branching.png') }}'); background-repeat:no-repeat; 
			background-position: center; ">
			<div class="row">
				<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-xl-3">
					<img  class= "img-responsive listed-image first-two" ng-src="!# links[0] #!" ng-click="change(0)" ng-touchstart="change(0)">
				</div>
				<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-xl-3">
					<img class= "img-responsive listed-image first-two" ng-src="!# links[1] #!" ng-click="change(1)" ng-touchstart="change(1)">
				</div>
				<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-xl-3">
					<img id="a" class= "img-responsive listed-image third" ng-src="!# links[2] #!" 
					 ng-click="change(2)" ng-touchstart="change(2)">
					
					<img id="b" class= "img-responsive listed-image forth" ng-src="!# links[3] #!" 
					 ng-click="change(3)" ng-touchstart="change(3)">
				</div>	
				<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 col-xl-3">
					<img class= "img-responsive listed-image last" ng-src="!# links[4] #!"  
					ng-click="change(4)" ng-touchstart="change(4)">
				</div>	
			</div>
			</div>
		</div>
		<div class="col-md-6 col-md-offset-3">
			<div class="row">
				<div class="col-md-5">
					<img ng-src="!# monitor #!" class="img-responsive laptop">
				</div>
				<div class="col-md-7">
					<br>
					<h4 class="laptop-header"><b>!# headers[0] #! <span class="laptop-header-blue"><b> !# headers[1] #!</b></span></b></h4>
					<p class="laptop-text">!# text #! </p>
					<a href="/sign-up" class="btn btn-success signUp">Sign Up</a>
					<br>
				</div>
			</div>
			<br>
			<br>
		</div>
		<br>
    </div>
</div>
	@push('scripts')
	<script src="{{ asset('node_modules/angular/angular.min.js') }}"></script>
	<script src="{{ asset('node_modules/angular-touch/angular-touch.min.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/controllers.js') }}"></script>
    @endpush
    @include('layouts.footer')
   
@endsection