<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required|max:255|unique:users',
            'name' => 'required|max:255',
            'lastname' => 'required|max:255',
            'email' => 'required|max:255|unique:users',
            'state' => 'required|max:255',
            'city' => 'required|max:255',
            'birth_date' => 'required|date|before:-18 years',
        ];
    }
    /** * @param array $errors
     *
     * @return \Illuminate\Http\JsonResponse
     */

}
