<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Tymon\JWTAuth\Facades\JWTAuth;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required',
            'name' => 'required|max:255',
            'lastname' => 'required|max:255',
            'state' => 'required|max:255',
            'city' => 'required|max:255',
            'birth_date' => 'required',
        ];
    }

    /** * @param array $errors
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function response( array $errors ) {
        return  response()->custom(400, $errors, null);
    }
}
