<?php

namespace App\Http\Requests\Clinic;


use App\Helpers\Constant as Constant;
use Illuminate\Foundation\Http\FormRequest;
use JWTAuth;

class CreateClinicRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    // 'name', 'address', 'phone', 'email', 'website', 'business_phone', 'business_email', 'legal_name', 'latitude',
    // 'longitude', 'working_hours', 'announcement', 'about_us',


    public function rules()
    {
        return [
            'name' => 'required',
            'legal_name' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'address' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'announcement' => 'required',
            'about_us' => 'required',
            'business_phone' => 'required',
            'business_email' => 'required',
        ];
    }

    /** * @param array $errors
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function response(array $errors)
    {
        return response()->custom(400, $errors, null);
    }
}

