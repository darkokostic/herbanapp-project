<?php

namespace App\Http\Requests\Deal;

use Illuminate\Foundation\Http\FormRequest;

class CreateDealRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'description' => 'required',
            'deal_type' => 'required|in:HOURLY,ONE_DAY,PER_MEASUREMENT',
            'measurement_type' => 'required|in:QUANTITY,WEIGHT',

        ];
    }

    /** * @param array $errors
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function response( array $errors ) {
        return  response()->custom(400, $errors, null);
    }
}
