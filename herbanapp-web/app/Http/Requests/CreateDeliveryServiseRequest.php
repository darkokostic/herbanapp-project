<?php

namespace App\Http\Requests;

use App\Helpers\Constant;
use Illuminate\Foundation\Http\FormRequest;
use JWTAuth;

class CreateDeliveryServiseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = JWTAuth::parseToken()->authenticate();
        return $user->can('create', Constant::DELIVERY_SERVICE_IDENTIFIER);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */


    public function rules()
    {
        return [
            'name' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'address' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',

        ];
    }
    /** * @param array $errors
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function response( array $errors ) {
        return  response()->custom(400, $errors, null);
    }

}
