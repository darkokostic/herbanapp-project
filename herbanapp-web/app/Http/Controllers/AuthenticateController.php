<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Foundation\Auth\ThrottlesLogins;

class AuthenticateController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function authenticate(Request $request)
    {
        // Login with socialnetworks
        if($request->social_name) {
            $user = new User();
            $user->fill($request->all());
            if($user->save()) {
                $token = \Tymon\JWTAuth\JWTAuth::fromUser($user);
                return response()->json(compact('token'));
            } else {

            }
        } else {
            // grab credentials from the request
            $credentials = $request->only('email', 'password');

            try {
                // attempt to verify the credentials and create a token for the user
                if (! $token = JWTAuth::attempt($credentials)) {
                    return response()->json(['error' => 'invalid_credentials'], 401);
                }
            } catch (JWTException $e) {
                // something went wrong whilst attempting to encode the token
                return response()->json(['error' => 'could_not_create_token'], 500);
            }

            // all good so return the token
            return response()->json(compact('token'));
        }
    }

    /**
     * @param Request $request
     */
    public function register(CreateUserRequest $request)
    {
        try {
            $payload = $request->only('email', 'password');
            $entity = User::where('email', '=', $payload['email'])->where('password', '=', bcrypt($payload['password']))->first();
            if (!$found = $entity) {
                $entity = new User;
                $entity->fill($request->all());
                $entity->password = $request->password;
                if ($entity->save()) {
                    $token = JWTAuth::attempt($payload);
                    if ($token) {
                        $entity->token = $token;
                        $message = 'Successfully authorized.';
                        $code = 200;
                    }
                }
            }
            if ($found && (!isset($token) || !$token)) {
                throw new \Exception('Failed to register, try again later.');
            }
        } catch (\Exception $e) {
            $entity = null;
            $message = $e->getMessage() . ':' . $e->getLine();
            $code = 500;
        }
        return response()->json([
            'message' => $message,
            'entity' => $entity,
            'code' => $code
        ], $code);
    }
}