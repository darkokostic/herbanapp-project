<?php

namespace App\Http\Controllers;

use App\User;
use Berkayk\OneSignal\OneSignalClient;
use Berkayk\OneSignal\OneSignalFacade;
use Illuminate\Http\Request;
use JWTAuth;

class FriendshipController extends Controller
{
    public function getMyFriends()
    {
        $user = JWTAuth::parseToken()->authenticate();
        $friends = $user->getFriends();

        foreach ($friends as $friend)
        {
            $friend->isFriend = $friend->isFriendWith($user);
        }

        return response()->custom(200, "Frinds successfully retrived!", $friends);
    }

    public function getMyPandingRequests()
    {
        $user = JWTAuth::parseToken()->authenticate();
        $friends = $user->getPendingFriendships();
        $return = array();
        foreach ($friends as $friend) {
            if($friend->recipient_id == $user->id)
            array_push($return, User::find($friend->sender_id));
        }

        return response()->custom(200, "Pending frinds successfully retrived!", $return);
    }

    public function addFriend(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $userTwo = User::find($request->user_id);
        $user->befriend($userTwo);
        $signalData = ['page' => 'friend_request'];
        OneSignalFacade::sendNotificationToUser("Friend request from " . $user->username, $userTwo->player_id, $url = null, $data = $signalData, $buttons = null, $schedule = null);
        //OneSignal::sendNotificationToUser("Some Message", $userTwo->id, $url = null, $data = null, $buttons = null, $schedule = null);
        return response()->custom(200, "Friendship request sent!", null);
    }

    public function removeFriend(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $userTwo = User::find($request->user_id);
        $user->unfriend($userTwo);

        //OneSignal::sendNotificationToUser("Some Message", $userTwo->id, $url = null, $data = null, $buttons = null, $schedule = null);
        return response()->custom(200, "Removed friend!", null);
    }

    public function acceptFriendRequest(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $userTwo = User::find($request->user_id);
        $user->acceptFriendRequest($userTwo);
        $signalData = ['page' => 'friend_list'];

        OneSignalFacade::sendNotificationToUser($user->username . " accepted you as a friend", $userTwo->player_id, $url = null, $data = $signalData, $buttons = null, $schedule = null);

        return response()->custom(200, "Friendship accepted sent!", null);
    }

    public function denyFriendRequest(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $userTwo = User::find($request->user_id);
        $user->denyFriendRequest($userTwo);
        
        return response()->custom(200, "Friendship denied sent!", null);
    }

    public function searchFriends(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $friends = $user->searchFriends($request->search_key);

        return response()->custom(200, "Search complited", $friends);
    }

    public function testNotify() {
//        OneSignalFacade::sendNotificationToUser("Testing", "", $url = null, $data = null, $buttons = null, $schedule = null);

    }
}
