<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Faker\Factory as Faker;

class NotificationController extends Controller
{
    public function __construct()
    {

    }

    public function notifications()
    {
        $posts = array();
        $faker = Faker::create();
        $user = User::find(1);
        for ($i = 0; $i < 10; $i++) {
            array_push($posts, response()->notification($faker->dateTime()->format('U'), $faker->text, $faker->url, $faker->text(20), $user));
        }
        for ($i = 0; $i < 10; $i++) {
            array_push($posts, response()->news($faker->dateTime()->format('U'), $faker->text, $faker->url, $faker->imageUrl(), $faker->text(20), $user));
        }
        usort($posts, function ($a, $b) {
            return strtotime($b['date']) - strtotime($a['date']);
        });
//        return response()->custom(200, "Successful!", $posts);
        return response()->custom(200, "Successful!", []);
    }
}
