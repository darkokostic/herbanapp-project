<?php

namespace App\Http\Controllers;

use App\Deal;
use App\Helpers\Constant;
use App\Http\Requests\Deal\CreateDealRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DealController extends Controller
{
    public function createDeal(CreateDealRequest $request)
    {
        $deal = new Deal();
        $deal->fill($request->all());

        if ($deal->deal_type == Constant::DEAL_HOURLY) {

            $deal->data = json_encode([
                'hours' => $request->hours,
                'measurement_value' => $request->measurement_value,
                'price' => $request->price]);
            $deal->start_date = Carbon::now()->timestamp;
            $deal->end_date = Carbon::now()->addHours($request->hours)->timestamp;


        } else if($deal->deal_type == Constant::DEAL_TODAY) {

            $deal->data = json_encode([
                'date' => $request->date,
                'measurement_value' => $request->measurement_value,
                'price' => $request->price]);

        } else if($deal->deal_type == Constant::DEAL_ONGOING) {
            $deal->data = json_encode([
                'measurement_value' => $request->measurement_value,
                'price' => $request->price]);
        }

        if($deal->save()) {
            return response()->custom(200, "Successfully created deal!", $deal);
        }

        return response()->custom(400, "Not specific type!", null);
    }
}
