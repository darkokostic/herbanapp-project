<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateUserRequestMobile;
use App\Http\Requests\CreateUserWebRequest;
use App\Http\Requests\User\UpdateUserRequest;
use App\Image;
use Berkayk\OneSignal\OneSignalClient;
use Berkayk\OneSignal\OneSignalFacade;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;
use Socialite;
use App\Http\Requests\CreateUserRequest;
use App\User;
use Validator;
use Redirect;
use JWTAuth;

class LoginController extends Controller
{
    use AuthenticatesUsers;
    protected $maxLoginAttempts = 5;
    protected $lockoutTime = 300;

    /**
     * Redirect the user to the Facebook authentication page.
     *
     * @return Response
     */

    public function self()
    {

        $user = JWTAuth::parseToken()->authenticate();
        return response()->custom(200, "Successful!", $user);
        //catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
        //    return  response()->custom($e->getStatusCode(),"token_expired!",null);
        // } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
        //    return  response()->custom($e->getStatusCode(),"token_invalid!",null);
        // } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
        //    return  response()->custom($e->getStatusCode(),"token_absent!",null);
        // }
    }

    public function logout()
    {

        $user = JWTAuth::parseToken()->authenticate();
        $user->player_id = null;
        if($user->save()) {
            return response()->custom(200, "Successful logged out!", $user);
        }
        return response()->custom(400, "Can't loggout user!", null);

    }

    public function logInUser(Request $request)
    {
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return response()->custom(400, "Too many attempts!", null);
        }
        $credentials = $request->only('username', 'password');
        if ($token = JWTAuth::attempt($credentials)) {
            $this->incrementLoginAttempts($request);
            return redirect()->route('home')->with('token',$token);

        } else if ($token = JWTAuth::attempt(['email' => $request->input('username'), 'password' => $request->input('password')])) {

            return redirect()->route('home')->with('token',$token);
        } else {
            return redirect()->route('login');
        }
    }

    public function logInUserMobile(Request $request)
    {
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return response()->custom(400, "Too many attempts!", null);
        }
        $credentials = $request->only('username', 'password');
        if ($token = JWTAuth::attempt($credentials)) {
            $this->incrementLoginAttempts($request);
            $user = User::where('username', $request->username)->first();
            $user->player_id = $request->player_id;
            $user->save();
            $user->token = $token;
            $user->avatar();
            return response()->custom(200, "Loged In!", $user);
        } else if ($token = JWTAuth::attempt(['email' => $request->input('username'), 'password' => $request->input('password')])) {
            return response()->custom(200, "Loged In!", $token);
        } else {
            return response()->custom(403, "Faild", null);
        }


    }

    public function socialLogInMoblie(Request $request)
    {
        $isUser = User::where('social_id', $request->social_id)->where('social_name', $request->social_name)->get();
        if (sizeof($isUser) > 0) {
            $token = JWTAuth::fromUser($isUser[0]);
            $isUser[0]->token = $token;
            return response()->custom(200, "User loged in", $isUser[0]);
        } else {
            return response()->custom(200, "User not registerd", null);
        }
    }

    public function redirectToProviderFacebook()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Obtain the user information from Facebook.
     *
     * @return Response
     */
    public function handleProviderCallbackFacebook()
    {
        $user = Socialite::driver('facebook')->user();
        $name = explode(' ', $user->getName());
        $user->name = $name[0];
        $user->lastName = $name[sizeof($name) - 1];
        $user->social_name = 'facebook';

        $isUser = User::where('social_id', $user->id)->where('social_name', $user->social_name)->get();
        if (sizeof($isUser) > 0) {
            $token = JWTAuth::fromUser($isUser[0]);
            return redirect()->route('home')->with('token', $token);
        } else {
            return view('auth/register')->with('user', $user);
        }
    }

    /**
     * Redirect the user to the Twitter authentication page.
     *
     * @return Response
     */
    public function redirectToProviderTwitter()
    {
        return Socialite::driver('twitter')->redirect();
    }

    /**
     * Obtain the user information from Twitter.
     *
     * @return Response
     */
    public function handleProviderCallbackTwitter()
    {
        $user = Socialite::driver('twitter')->user();
        $name = explode(' ', $user->getName());
        $user->name = $name[0];
        $user->lastName = $name[sizeof($name) - 1];
        $user->social_name = 'twitter';
        $isUser = User::where('social_id', $user->id)->where('social_name', $user->social_name)->get();
        if (sizeof($isUser) > 0) {
            return redirect()->route('home');
        } else {
            return view('signUp')->with('user', $user);
        }
    }

    public function redirectToProviderInstagram()
    {
        return Socialite::driver('instagram')->redirect();
    }

    /**
     * Obtain the user information from Twitter.
     *
     * @return Response
     */
    public function handleProviderCallbackInstagram()
    {
        $user = Socialite::driver('instagram')->user();
        $name = explode(' ', $user->getName());
        $user->name = $name[0];
        $user->lastName = $name[sizeof($name) - 1];
        $user->social_name = 'instagram';
        $isUser = User::where('social_id', $user->id)->where('social_name', $user->social_name)->get();
        if (sizeof($isUser) > 0) {
            return redirect()->route('home');
        } else {
            return view('signUp')->with('user', $user);
        }
    }

    public function registerUserMobile(CreateUserRequestMobile $request)
    {


        $rules = array(
            'password' => 'required',
            'confirmPassword' => 'required|same:password'          // required and has to match the password field
        );

        // create custom validation messages ------------------
        $messages = array(
            'required' => 'The :attribute is really really really important.',
            'same' => 'The :others must match.'
        );

        // do the validation ----------------------------------
        // validate against the inputs from our form
        $validator = Validator::make($request->all(), $rules, $messages);


        if ($request->social_id) {
            $registerUser = new User();
            $registerUser->fill($request->all());
            $registerUser->password = " ";
            $registerUser->player_id = $request->player_id;
            $registerUser->save();
            $destinationPath = 'uploads/users';

            if ($request->image_url) {
                if ($request->social_name == 'facebook') {
                    $extension = 'jpg';
                } else {

                    $extension = pathinfo($request->image_url, PATHINFO_EXTENSION);
                }

                $fileName = "UserAvatar_" . $registerUser->name . rand(11111, 99999) . '.' . $extension;
                $file = file_get_contents($request->image_url);
                //$file->move($destinationPath, $fileName);
                $url = $destinationPath . "/" . $fileName;
                $move = file_put_contents($url, $file);

                $avatar = new Image(['url' => $url, 'avatar' => 1]);
                $registerUser->avatar()->delete();
                $registerUser->avatar()->save($avatar);
                $registerUser->player_id = $request->player_id;
                $registerUser->save();

            }
            $token = JWTAuth::fromUser($registerUser);
            $registerUser->token = $token;
            return response()->custom(200, "Loged In!", $registerUser);
        } else if ($validator->fails()) {

            return response()->custom(400, "Loged In Faild!", $validator);
        } else {
            $user = User::create($request->all());
            if ($request->avatar){
                $destinationPath = '/uploads/users/';
                $file = base64_decode($request->avatar);
                $fileName = 'user_' . str_random(10) . '.' . 'jpg';
                file_put_contents(public_path() . $destinationPath . $fileName, $file);
                $url = $destinationPath . $fileName;
                $avatar = new Image(['url' => $url, 'avatar' => 1]);
                $user->avatar()->delete();
                $user->avatar()->save($avatar);
            }


            $token = JWTAuth::fromUser($user);
            $user->token = $token;
            return response()->custom(200, "Loged In!", $user);
        }
        //TOKEN $token = JWTAuth::fromUser($user);
    }

    public function registerUser(CreateUserRequest $request)
    {


        $rules = array(
            'password' => 'required',
            'confirmPassword' => 'required|same:password'          // required and has to match the password field
        );

        // create custom validation messages ------------------
        $messages = array(
            'required' => 'The :attribute is really really really important.',
            'same' => 'The :others must match.'
        );

        // do the validation ----------------------------------
        // validate against the inputs from our form
        $validator = Validator::make($request->all(), $rules, $messages);


        if ($request->social_id) {
            $registerUser = new User();
            $registerUser->fill($request->all());
            $registerUser->password = " ";
            $registerUser->save();
            $token = JWTAuth::fromUser($registerUser);

            return redirect()->route('home')->with('token',$token);
        } else if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator);
        } else {
            $user = User::create($request->all());
            $token = JWTAuth::fromUser($user);

            return redirect()->route('home')->with('token',$token);

        }
        //TOKEN $token = JWTAuth::fromUser($user);
    }

    public function updateUser(UpdateUserRequest $request)
    {
        $user = User::find($request->id);
        $user->update($request->all());
        $destinationPath = '/uploads/users/';
        if ($request->avatar) {
            if (File::exists(public_path() . $user->avatar)) {
                File::delete(public_path() . $user->avatar);
                Image::where('imageable_id', $user->id)->delete();
            }
            $file = base64_decode($request->avatar);
            $fileName = 'user_' . str_random(10) . '.' . 'jpg';
            file_put_contents(public_path() . $destinationPath . $fileName, $file);
            $url = $destinationPath . $fileName;
            $avatar = new Image(['url' => $url, 'avatar' => 1]);
            $user->avatar()->delete();
            $user->avatar()->save($avatar);
        }

        return response()->custom(200, "Successfully updated user", $user);
    }

    public function getBraintreeToken()
    {

        return response()->json([
            'data' => [
                'token' => \Braintree_ClientToken::generate(),
            ]
        ]);
    }

    public function subscribe(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        // get the plan after submitting the form

        // subscribe the user
        $user->newSubscription('main', 'Premium')->create($request->payment_method_nonce);

        // redirect to home after a successful subscription
        return redirect()->route('home');
    }

    public function suggestedUsers()
    {
        $user = JWTAuth::parseToken()->authenticate();
        $i = 0;
        $users = User::where('id', '!=', $user->id)->simplePaginate(5)->filter(function ($query) use ($user) {
            if(!$user->isFriendWith($query))
            {
                return true;
            }
        });
        return response()->custom(200, "Successfully generated users", $users);
    }

    public function infoCount()
    {
        $users = DB::table('users')->count();
        $clinics = DB::table('clinics')->count();
        $orders = DB::table('orders')->count();
        $ret = array(
            'users' => $users,
            'clinics' => $clinics,
            'deals' => $orders
        );
        return response()->custom(200, "Successfully generated users", $ret);
    }

    public function testMail()
    {
//        $user = User::find(33);
////        Mail::send('email.test', ['user' => $user], function ($m) use ($user) {
////            $m->from('sale.tkd.lesa@gmail.com', 'Your Application');
////
////            $m->to('aleksandarjovanovic996@yahoo.com', $user->name)->subject('Your Reminder!');
////        });
////        return "a";
//
        $signalData = ['page' => 'friend_request'];
        OneSignalFacade::sendNotificationToUser("Ovo je poruka za: ", '5f6b4b4b-af23-46f9-8b94-b71c38348e41', $url = null, $data = $signalData, $buttons = null, $schedule = null);
    }

    public function changePassword(Request $request)
    {

        $user = JWTAuth::parseToken()->authenticate();


        if (Hash::check($request->current, $user->password) && $request->new == $request->check) {
            $user->password = $request->new;
            $user->save();

            return response()->custom(200, 'Password changed', null);
        }
        return response()->custom(400, 'Change password faild', null);

    }

    public function resetPassword(Request $request)
    {
        $token = str_random(40);
        $reset = DB::table('password_resets')->insertGetId([
            'email' => $request->email,
            'token' => $token,
            'created_at' => Carbon::now()->addMinutes(rand(0, 5)),
        ]);
        $link = URL::to('/reset/' . $token);
        Mail::send('email.test', ['email' => $request->email, 'link' => $link], function ($m) use ($request) {
            $m->from('sale.tkd.lesa@gmail.com', 'HerbanApp');

            $m->to($request->email)->subject('ResetLink!');
        });
        return response()->custom(200, 'Email sent!', null);


    }

    public function changePasswordView($token)
    {
        $email = DB::table('password_resets')->where('token', $token)->orderBy('created_at', 'desc')->first();
        return view('resetPassword')->with('email', $email->email);
    }

    public function newPassword(Request $request)
    {
        $user = User::where('email', $request->email)->first();
        $user->password = $request->password;
        $user->save();
        return redirect()->route('login');

    }

    public function contactUs(Request $request)
    {
        $mail = Mail::send('email.contact', ['email' => $request->email, 'msg' => $request->message], function ($m) use ($request) {
            $m->from($request->email, $request->name);

            $m->to('aleksandarjovanovic996@yahoo.com')->subject('Cotact');
        });
        return response()->custom(200, 'Email sent!', null);
    }

}