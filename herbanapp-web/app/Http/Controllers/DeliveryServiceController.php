<?php

namespace App\Http\Controllers;

use App\DeliveryService;

use App\Http\Requests\CreateDeliveryServiseRequest;

use App\Http\Requests\NearByDeliveryServiseRequest;
use Illuminate\Http\Request;
use DB;

class DeliveryServiceController extends Controller
{
    /**
     * Return nearby clinics
     *
     * @param  NearByClinicsRequest $request
     * @return Response
     */

    public function getNearByDeliveryService(NearByDeliveryServiseRequest $request)
    {
        $lat = $request->lat;
        $lng = $request->lng;
        $deliveryService = DeliveryService::select(DB::raw('*, ( 3959 * acos( cos( radians(' . $lat . ') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(' . $lng . ') ) + sin( radians(' . $lat . ') ) * sin( radians(latitude) ) ) ) AS distance'))
            ->orderBy('distance')->simplePaginate(4);
        return response()->custom(200, "Successfully retreved DeliveryService!", $deliveryService);
    }
    public function createDeliveryService(CreateDeliveryServiseRequest $request)
    {
        $deliveryService = new DeliveryService();
        $deliveryService->fill($request->all());
        $deliveryService->hits = 0;
        if ($deliveryService->save()) {
            return response()->custom(200, "Successfully created DeliveryService!", $deliveryService);
        } else {
            return response()->custom(400, "Something went wrong!", null);
        }
    }
    public function getSingleDeliveryService(Request $request)
    {
        $clinic = DeliveryService::find($request->delivery_service_id);
        if ($clinic) {
            return response()->custom(200, "Successfully retreved Delivery Service!", $clinic);
        } else {
            return response()->custom(400, "Delivery Service not found!", null);
        }
    }
}
