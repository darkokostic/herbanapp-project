<?php

namespace App\Http\Controllers;

use App\ChatNotification;
use App\ChatRoom;
use App\Events\ChatEvent;
use App\Http\Requests\Chat\CreateGroupChatRequest;
use App\Http\Requests\Chat\JoinGroupChatRequest;
use App\Http\Requests\Chat\LeaveGroupChatRequest;
use App\Http\Requests\Chat\SendMessageRequest;
use App\Message;
use App\User;
use Berkayk\OneSignal\OneSignalFacade;
use Carbon\Carbon;
use Illuminate\Http\Request;
use JWTAuth;

class ChatController extends Controller
{
    public function chat($chatId)
    {
        return view('chat')->with('chatId', $chatId);
    }

    public function sendMessage(SendMessageRequest $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $chatRoom = ChatRoom::findOrNew($request->chat_room_id);
        if ($chatRoom->save()) {
            $chatRoom = ChatRoom::find($chatRoom->id);
            $message = new Message();
            $message->user_id = $user->id;
            $message->chat_room_id = $chatRoom->id;
            $message->message = $request->message;
            $message->created_at = Carbon::now();

            if ($message->save()) {
                if ($request->user_id != null) {
                    $chatRoom->name = $user->name;
                    $chatRoom->save();
                    $chatNotification = new ChatNotification();
                    $chatNotification->chat_room_id = $chatRoom->id;
                    $chatNotification->user_id = $request->user_id;
                    $chatNotification->isRead = 0;
                    if ($chatNotification->save()) {
                        $userTwo = User::where('id', '=', $request->user_id)->first();
                        $signalData = ['chat_room' => $chatRoom, 'user' => $user, 'collapse_id' => 'chat'.$chatRoom->id];
                        try {
                            OneSignalFacade::sendNotificationToUser($user->name . ' ' . $user->lastname . ' sent you: ' . $message->message, $userTwo->player_id, $url = null, $data = $signalData, $buttons = null, $schedule = null);
                        } catch (\Exception $e) {

                        }
                    }

                } else {
                    $messages = ChatNotification::with('user')->where('chat_room_id', '=', $chatRoom->id)->where('user_id', '!=', $user->id)->groupBy('user_id');

                    foreach ($messages->get() as $msg) {

                        $chatNotification = new ChatNotification();
                        $chatNotification->chat_room_id = $chatRoom->id;
                        $chatNotification->user_id = $msg->user->id;
                        $chatNotification->isRead = 0;
                        if ($chatNotification->save()) {
                            $signalData = ['chat_room' => $chatRoom, 'collapse_id' => 'chat'];
                            try {
                                OneSignalFacade::sendNotificationToUser($user->name . ' ' . $user->lastname . ' sent: ' . $message->message, $msg->user->player_id, $url = null, $data = $signalData, $buttons = null, $schedule = null);
                            } catch (\Exception $e) {

                            }
                        }

                    }

                    event(new ChatEvent($user, $message, $chatRoom->id));
                    return response()->custom(200, "Retrieved chat_room_id", $chatRoom->id);
                }
                event(new ChatEvent($user, $message, $chatRoom->id));
                return response()->custom(200, "Retrieved chat_room_id", $chatRoom->id);
            }
        }
    }

    public function getMessages(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();

        if ($request->chat_room_id == null) {
            $chatNotification = ChatNotification::where('user_id', '=', $user->id)->groupBy('chat_room_id')->first();
            if ($chatNotification != null) {
                $messages = Message::with('user')->where('chat_room_id', '=', $chatNotification->chat_room_id)->get();

            } else {
                $messages = [];
            }
        } else {
            $messages = Message::with('user')->where('chat_room_id', '=', $request->chat_room_id)->get();
        }

        foreach ($messages as $message) {
            if ($message->user_id == $user->id) {
                $message->position = 'right';
            } else {
                $message->position = 'left';
            }
        }
        return response()->custom(200, "Retrieved chat messages", $messages);
    }

    public function getChatRoom(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $chatRooms = ChatNotification::with('chatRoom')->where('user_id', '=', $user->id)->groupBy('chat_room_id');

        if ($chatRooms->count() == 0) {
            $chatRooms = Message::with('chatRoom')->where('user_id', '=', $user->id)->groupBy('chat_room_id');
        }

        foreach ($chatRooms->get() as $chatRoom) {

            if (json_decode($chatRoom)->chat_room->isGroup != 1) {

                $room = Message::where('chat_room_id', '=', $chatRoom->chat_room_id)->where('user_id', '=', $request->user_id);

                if ($room->count() == 0) {
                    $room = ChatNotification::where('chat_room_id', '=', $chatRoom->chat_room_id)->where('user_id', '=', $request->user_id);
                    if ($room->count() != 0) {
                        return response()->custom(200, "Retrieved chat room id", $room->first(['chat_room_id']));
                    }
                }
                return response()->custom(200, "Retrieved chat room id", $room->pluck('chat_room_id')->first());

            }
        }

        return response()->custom(200, "Retrieved chat room id", null);
    }

    public function createGroupChat(CreateGroupChatRequest $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $chatRoom = new ChatRoom();
        $chatRoom->name = $request->name;
        $chatRoom->isGroup = 1;
        if ($chatRoom->save()) {
            $notification = new ChatNotification();
            $notification->chat_room_id = $chatRoom->id;
            $notification->user_id = $user->id;
            $notification->isRead = 1;

            $notification->save();
            foreach ($request->users as $user) {
                $notification = new ChatNotification();
                $notification->chat_room_id = $chatRoom->id;
                $notification->user_id = $user['user_id'];
                $notification->isRead = 0;

                $notification->save();
            }
            return response()->custom(200, "Successfully created group chat", $chatRoom);
        }
    }

    public function joinGroupChat(JoinGroupChatRequest $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $chatNotification = new ChatNotification();
        $chatNotification->chat_room_id = $request->chat_room_id;
        $chatNotification->user_id = $user->id;
        $chatNotification->isRead = 0;
        if ($chatNotification->save()) {
            return response()->custom(200, "Successfully joined to group chat", $chatNotification);
        }
    }

    public function leaveGroupChat(LeaveGroupChatRequest $request)
    {
        $user = JWTAuth::parseToken()->authenticate();
        ChatNotification::where('user_id', '=', $user->id)->where('chat_room_id', '=', $request->chat_room_id)->delete();
        Message::where('user_id', '=', $user->id)->where('chat_room_id', '=', $request->chat_room_id)->delete();
        return response()->custom(200, "Successfully leave group chat", null);

    }

    public function getGroupChats()
    {
        $chatRooms = ChatRoom::where('isGroup', '=', 1)->orderBy('name')->simplePaginate(10);
        return response()->custom(200, "Retrieved  group chat", $chatRooms);
    }

    public function getUserInbox()
    {
        $user = JWTAuth::parseToken()->authenticate();
        $chats = [];
        $uChats = [];
        $chatNotifications = ChatNotification::where('user_id', '=', $user->id)->get()->uniqueStrict(function ($notification) {
            return $notification->chat_room_id;
        })->pluck('chat_room_id');
        $chatMessages = Message::where('user_id', '=', $user->id)->get()->uniqueStrict(function ($notification) {
            return $notification->chat_room_id;
        })->pluck('chat_room_id');
        $toRet = $chatNotifications->merge($chatMessages);
        $chats = array_unique($toRet->all());


        $userChats = ChatRoom::find($chats);
        $userChats = $userChats->sortByDesc(function ($userChat) {
            return $userChat->lastMessage;
        });

        foreach ($userChats as $userChat) {
            if ($userChat->lastMessage != null) {
                $secondUser = ChatNotification::with('user')->where('chat_room_id', '=', $userChat->id)->where('user_id', '!=', $user->id);
                if ($secondUser->count() == 0) {
                    $secondUser = Message::with('user')->where('chat_room_id', '=', $userChat->id)->where('user_id', '!=', $user->id);
                }
                $userChat->user = $secondUser->first()->user;
                array_push($uChats, $userChat);
            }
        }

        return response()->custom(200, "Retrieved  user inbox", $uChats);
    }
}
