<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Http\Requests\CreateCommentRequest;
use App\Http\Requests\Review\CreateReviewRequest;
use App\Review;
use JWTAuth;

class ReviewController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  mixed
     * @return Response
     */

    public function createReview(CreateReviewRequest $request)
    {
        $review = new Review;
        $review->fill($request->all());

        if ($review->save()) {
            return response()->custom(200, "Successful created review!", $review);
        } else {
            return response()->custom(400, "There was an error!", null);
        }
    }

    /**
     * Create comment for specific review
     *
     * @param  mixed
     * @return Response
     */

    public function createReviewComment(CreateCommentRequest $request)
    {
        $comment = new Comment;
        $comment->fill($request->all());

        if ($comment->save()) {
            return response()->custom(200, "Successful created review comment!", $comment);
        } else {
            return response()->custom(400, "There was an error!", null);
        }
    }
    public function getUserReviews(){
        $user = JWTAuth::parseToken()->authenticate();
        $reviews = Review::where('user_id',$user->id)->get();
        return response()->custom(200, "Successful generated reviews ", $reviews);
    }
}
