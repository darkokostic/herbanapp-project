<?php

namespace App\Http\Controllers;

use App\Http\Requests\Order\CreateOrderRequest;
use App\Http\Requests\Order\UpdateOrderRequest;
use App\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::with('products')->get();

        return response()->custom(200, "Successfully retrieved orders!", $orders);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateOrderRequest $request)
    {
        $order = new Order;
        $order->clinic_id = $request->clinic_id;
        $order->user_id = $request->user_id;
        $order->status = 'pending';
        $order->order_number = unique_random('orders', 'order_number', 6);

        if ($order->save()) {
            return response()->custom(200, "Successfully saved order!", $order);
        } else {
            return response()->custom(400, "There was an problem!", null);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::with('products')->find($id);
        return response()->custom(200, "Successfully retrieved order!", $order);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function updateOrder(UpdateOrderRequest $request, $id)
    {
        $order = Order::find($id);
        $order->status = $request->status;

        if ($order->save()) {
            return response()->custom(200, "Successfully updated order!", $order);
        } else {
            return response()->custom(400, "There was an problem!", null);
        }
    }
}
