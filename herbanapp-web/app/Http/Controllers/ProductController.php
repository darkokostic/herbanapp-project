<?php

namespace App\Http\Controllers;

use App\Clinic;
use App\Http\Requests\Product\CreateProductRequest;
use App\Image;
use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function createProduct(CreateProductRequest $request){
        $product = new Product();
        $product->fill($request->all());
        if ($product->save()){
            $clinic = Clinic::find($request->clinic_id);
            $clinic->products()->attach($product->id);
            $images = collect($request->images);


            $images = $images->map(function($image, $key) use ($clinic,$product) {
                $destinationPath =  'uploads/' . $clinic->name .'products/'. $product->name;
                $extension = $image->getClientOriginalExtension();
                $fileName = "ProductImage_" . $product->name . rand(11111, 99999) . '.' . $extension;
                $image->move($destinationPath, $fileName);
                $url = $destinationPath . "/" . $fileName;
                $image = new Image(['url' => $url]);
                return $image;
            });
            $avatar = $request->avatar;
            $destinationPath =  'uploads/' . $clinic->name .'products/'. $product->name;
            $extension = $avatar->getClientOriginalExtension();
            $fileName = "ProductAvatar_" . $product->name . rand(11111, 99999) . '.' . $extension;
            $avatar->move($destinationPath, $fileName);
            $url = $destinationPath . "/" . $fileName;
            $avatar = new Image(['url' => $url, 'avatar' => 1]);
            $product->gallery()->saveMany($images);
            $product->avatar()->save($avatar);
            return response()->custom(200, "Successfully created product!", $product);
        }else{
            return response()->custom(400, "Something went wrong!", null);
        }
    }
    public function singleProduct($id){
        $product = Clinic::find($id);
        return response()->custom(200, "Successfully created product!", $product);
    }
}
