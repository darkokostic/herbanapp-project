<?php

namespace App\Http\Controllers;

use App\Category;
use App\Clinic;
use App\Product;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function mainSearch(Request $request){
        $search_key = $request->search_key;
        Clinic::$withoutAppends = true;
        Clinic::$onlyAppend = ['logo','cover','average_rating'];

        if ($search_key == "null" || $search_key == ""){
            $products = Product::orderBy('created_at');
            $products_ids = $products->get()->pluck('id');
            $products = $products->paginate(8);
            $clinics = Clinic::orderBy('created_at')->paginate(8);


            $categorys = Category::where('parent_id',null)->with('children.products')->get();
            $categorys = $categorys->filter(function ($category) use ($products_ids){

                        $category->children = $category->children->filter(function ($child)  use ($products_ids) {

                            $products = $child->products->filter(function ($product)  use ($products_ids) {

                                if (in_array($product->id,$products_ids->toArray())){
                                    return $product;
                                }
                            })->take(8);
                            $child->productsToShow =$products;
                            return $child;
                        });
                        return $category;
            });
            $return = array(
                'products' =>$products,
                'categorys' => $categorys,
                'clinics' => $clinics
            );
            return response()->custom(200, "Categorys successfully generated!", $return);

        }
        $products = Product::where('name', 'like', '%' . $request->search_key . '%')->orderBy('created_at');
        $products_ids = $products->get()->pluck('id');
        $products = $products->paginate(8);

        $clinics = Clinic::where('name', 'like', '%' . $request->search_key . '%')->orderBy('created_at')->paginate(8);



        $categorys = Category::where('parent_id',null)->with('children.products')->get();
        $categorys2 = $categorys->filter(function ($category) use ($products_ids){

            $category->children = $category->children->filter(function ($child)  use ($products_ids) {

                $s= $child->products->filter(function ($product)  use ($products_ids) {

                    if (in_array($product->id,$products_ids->toArray())){

                        return $product;
                    }
                })->values()->take(8);
                $child->productsToShow = $s;

                return $child;
            });
            return $category;
        });

        $return = array(
            'products' =>$products,
            'categorys' => $categorys2,
            'clinics' => $clinics
        );
        return response()->custom(200, "Categorys successfully generated!", $return);



    }
    public function getAllCategorys(){

        $categorys = Category::where('parent_id',null)->with('children')->get();

        return response()->custom(200, "Categorys successfully generated!", $categorys);

    }
}
