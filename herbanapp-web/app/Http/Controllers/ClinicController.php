<?php

namespace App\Http\Controllers;

use App\ClinicsFavorite;
use App\Doctor;
use App\Http\Requests\Clinic\ApproveClaimRequest;
use App\Http\Requests\Clinic\ClaimClinicRequest;
use App\Http\Requests\Clinic\CreateClinicRequest;
use App\Http\Requests\Clinic\SearchClinicRequest;
use App\Http\Requests\Clinic\ToggleFavoriteRequest;
use App\Http\Requests\NearByClinicsRequest;
use \App\Clinic as Clinic;
use App\Image;
use App\PendingClaim;
use \App\User as User;
use DB;
use \App\WorkingHours as WorkingHours;
use Dompdf\Exception;
use Illuminate\Http\Request;
use JWTAuth;

class ClinicController extends Controller
{
    /**
     * Return nearby clinics
     *
     * @param  NearByClinicsRequest $request
     * @return Response
     */

    public function getNearByClinics(NearByClinicsRequest $request)
    {
        $perPage = 4;
        if($request->per_page) {
            $perPage = 8;
        }
        $lat = $request->lat;
        $lng = $request->lng;
        $clinics = Clinic::select(DB::raw('*, ( 3959 * acos( cos( radians(' . $lat . ') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(' . $lng . ') ) + sin( radians(' . $lat . ') ) * sin( radians(latitude) ) ) ) AS distance'))
            ->orderBy('distance')->simplePaginate($perPage);
        return response()->custom(200, "Successfully retreved Clinics!", $clinics);
    }
    public function getNearByDoctors(NearByClinicsRequest $request)
    {
        $lat = $request->lat;
        $lng = $request->lng;
        $doctors = Doctor::select(DB::raw('*, ( 3959 * acos( cos( radians(' . $lat . ') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(' . $lng . ') ) + sin( radians(' . $lat . ') ) * sin( radians(latitude) ) ) ) AS distance'))
            ->orderBy('distance')->simplePaginate(4);
        return response()->custom(200, "Successfully retreved doctors!", $doctors);
    }

    /**
     * Return specific clinic
     *
     * @param  Request $request
     * @return Response
     */

    public function getSingleClinic(Request $request)
    {
        $clinic = Clinic::with('reviews')->find($request->clinic_id);
        if ($clinic) {
            return response()->custom(200, "Successfully retreved Clinic!", $clinic);
        } else {
            return response()->custom(400, "Clinic not found!", null);
        }
    }
    public function getSingleDoctor(Request $request)
    {
        $doctor = Doctor::with('reviews')->find($request->doctor_id);
        if ($doctor) {
            return response()->custom(200, "Successfully retreved Doctor!", $doctor);
        } else {
            return response()->custom(400, "Doctor not found!", null);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Clinic\CreateClinicRequest $request
     * @return \Illuminate\Http\Response
     */

    public function createClinic(CreateClinicRequest $request)
    {
        $workingHours = new WorkingHours();
        $workingHours->fill($request->all());
        if ($workingHours->save()) {
            $clinic = new Clinic();
            $clinic->fill($request->all());
            $clinic->working_hours_id = $workingHours->id;
            $images = collect($request->images);


            $images = $images->map(function ($image, $key) use ($clinic) {
                $destinationPath = 'uploads/' . $clinic->name;
                $extension = $image->getClientOriginalExtension();
                $fileName = "ClinicImage_" . $clinic->name . rand(11111, 99999) . '.' . $extension;
                $image->move($destinationPath, $fileName);
                $url = $destinationPath . "/" . $fileName;
                $image = new Image(['url' => $url]);
                return $image;
            });
            $avatar = $request->avatar;
            $destinationPath = 'uploads/' . $clinic->name;
            $extension = $avatar->getClientOriginalExtension();
            $fileName = "ClinicAvatar_" . $clinic->name . rand(11111, 99999) . '.' . $extension;
            $avatar->move($destinationPath, $fileName);
            $url = $destinationPath . "/" . $fileName;
            $avatar = new Image(['url' => $url, 'avatar' => 1]);
            if ($clinic->save()) {
                $clinic->gallery()->saveMany($images);
                $clinic->logo()->save($avatar);

                return response()->custom(200, "Successfully created Clinic!", $clinic);
            } else {
                return response()->custom(400, "Something went wrong!", null);
            }
        } else {
            return response()->custom(400, "Something went wrong!", null);
        }

    }

    public function claimClinic(ClaimClinicRequest $request)
    {
        $clinic = Clinic::where([['id', $request->clinic_id], ['admin_id', null]])->first();
        $user = JWTAuth::parseToken()->authenticate();
        try {
            $clinic->pending()->attach($user->id);
            return response()->custom(200, "Clinic successfully claimed!", $clinic);
        } catch (Exception $exception) {
            return response()->custom(400, "Something went wrong!", null);
        }


    }

    public function getUnapprovedClinicClaims()
    {
        return response()->custom(200, "Successfull!", PendingClaim::where('approved', 0)->get());
    }

    public function approveClinicClaim(ApproveClaimRequest $request)
    {
        $pending = PendingClaim::find($request->pending_id);
        $pending->approved = 1;
        $clinic = $pending->clinic;
        $clinic->admin_id = $pending->user_id;

        if ($pending->save() && $clinic->save()) {
            return response()->custom(200, "Clinic successfully claimed!", $clinic);
        } else {
            return response()->custom(400, "Something went wrong!", null);
        }
    }

    public function searchClinicsAndUsers(SearchClinicRequest $request)
    {
        $request->search_key = strtolower($request->search_key);
        $users = User::where('username', 'like', '%' . $request->search_key . '%')->orWhere('name', 'like', '%' . $request->search_key . '%')
            ->orWhere('lastname', 'like', '%' . $request->search_key . '%')->get();
        $clinics = Clinic::where('name', 'like', '%' . $request->search_key . '%')->orWhere('legal_name', 'like', '%' . $request->search_key . '%')
            ->get();
        $user = JWTAuth::parseToken()->authenticate();
        foreach ($users as $u)
        {
            $u->isFriend = $u->isFriendWith($user);
        }
        return response()->custom(200, "Clinic successfully claimed!", array('users' => $users, 'clinics' => $clinics));
    }

    public function getFavoriteClinics($id)
    {
        $favorites = User::find($id)->favoriteClinics()->get();
        return response()->custom(200, "Successfully get all favorites!", $favorites);

    }

    public function toggleFavorite(ToggleFavoriteRequest $request)
    {
        $favorite = ClinicsFavorite::where([
            ['user_id', '=', $request->user_id],
            ['clinic_id', '=', $request->clinic_id]
        ]);

        if ($favorite->count() == 0) {
            $fav = new ClinicsFavorite;
            $fav->user_id = $request->user_id;
            $fav->clinic_id = $request->clinic_id;
            if($fav->save()) {
                return response()->custom(200, "Successfully updated favorite!", $fav);
            } else {
                return response()->custom(400, "Successfully updated favorite!", null);
            }
        } else {
            if($favorite->delete()){
                return response()->custom(200, "Successfully updated favorite!", $favorite);
            }
        }

        return response()->custom(200, "Successfully updated favorite!", null);

    }
}
