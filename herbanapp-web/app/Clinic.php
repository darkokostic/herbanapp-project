<?php

namespace App;

use Dompdf\Exception;
use Illuminate\Database\Eloquent\Model;
use Tymon\JWTAuth\Facades\JWTAuth;

class Clinic extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'address', 'phone', 'email', 'website', 'business_phone', 'business_email', 'legal_name', 'latitude', 'longitude', 'working_hours', 'announcement', 'about_us',
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    protected $dateFormat = 'U';
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'duns_number', 'working_hours_id',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['average_rating', 'reviews_count', 'gallery','working_hours','logo','cover', 'products','products_with_category', 'isFavorite', 'reviews'];


    public static $withoutAppends = false;
    public static $onlyAppend = [];
    protected function getArrayableAppends()
    {
        if(self::$withoutAppends){
            return self::$onlyAppend;
        }
        return parent::getArrayableAppends();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function gallery()
    {
        return $this->morphMany('\App\Image', 'imageable')->where('avatar', false)->where('cover', false);
    }

    /**
     * @return mixed
     */
    public function logo()
    {
        return $this->morphOne('\App\Image', 'imageable')->where('avatar', true);
    }
    /**
     * @return mixed
     */
    public function cover()
    {
        return $this->morphOne('\App\Image', 'imageable')->where('cover', true);
    }

    public function documents()
    {
        return $this->morphMany('\App\Document', 'documentable');
    }

    public function working_hours()
    {
        return $this->belongsTo('\App\WorkingHours');
    }

    public function reviews()
    {
        return $this->hasMany('\App\Review');
    }

    public function pending()
    {
        return $this->belongsToMany('App\User', 'pending_claims','clinic_id','user_id');
    }

    public function products()
    {
        return $this->belongsToMany('App\Product', 'clinic_products','clinic_id','product_id');
    }



    public function getProductsWithCategoryAttribute()
    {
        $products = $this->products()->pluck('product_id');
        $category = Category::with(array('products'=> function($q) use ($products) {

                $q->whereIn('product_id', $products);


    }))->get();
        $category = $category->filter(function($cat){
            if (sizeof($cat->products) > 0){
                return $cat;
            }
        })->values()->all();


        return $category;
    }
    /**
     * Get the average rating for the clinic.
     *
     * @return float
     */
    public function getAverageRatingAttribute()
    {
        return floor($this->reviews()->avg('rating') * 2) / 2;
    }

    /**
     * Get the number of reviews for the clinic.
     *
     * @return int
     */
    public function getReviewsCountAttribute()
    {
        return $this->reviews()->count();
    }


    /**
     * Get the products for the clinic.
     *
     * @return object
     */
    public function getProductsAttribute()
    {
        return $this->products()->get();
    }


    public function getReviewsAttribute()
    {
        return $this->reviews()->get();
    }

    public function getGalleryAttribute()
    {
        return $this->gallery()->get();
    }
    public function getWorkingHoursAttribute()
    {
        return $this->working_hours()->orderBy('created_at', 'desc')->first();
    }
    public function getLogoAttribute(){
        return $this->logo()->orderBy('created_at', 'desc')->first();
    }
    public function getCoverAttribute(){
        return $this->cover()->orderBy('created_at', 'desc')->first();
    }

    public function isFavorite() {
        try{
            $user = JWTAuth::parseToken()->authenticate();
            $favorite = ClinicsFavorite::where([
                ['user_id', '=',$user->id],
                ['clinic_id', '=', $this->id]
            ]);
            if($favorite->count() == 0) {
                return 0;
            } else {
                return 1;
            }
        }catch ( \Tymon\JWTAuth\Exceptions\JWTException $e){
            return 0;
        }

    }

    public function getIsFavoriteAttribute()
    {
        return $this->isFavorite();
    }
}