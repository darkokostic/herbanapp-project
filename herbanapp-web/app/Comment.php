<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'message', 'user_id', 'review_id',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['user'];


    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    protected $dateFormat = 'U';

    public function review()
    {
        return $this->hasOne('\App\Review');
    }

    public function user()
    {
        return $this->hasOne('App\User');
    }

    public function getUserAttribute()
    {
        return User::where('id', $this->user_id)->first();
    }
}
