<?php

namespace App\Events;

use Carbon\Carbon;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;



class ChatEvent implements ShouldBroadcast
{
    use SerializesModels;

    public $user, $message, $created_at;
    protected  $chatId;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($user, $text, $chatId)
    {
        $this->message = $text->message;
        $carbonDate = new Carbon($text->created_at);
        $this->created_at = $carbonDate->timestamp; // return epoach date
        $this->user = $user;
        $this->chatId = $chatId;
    }

    public function broadcastOn()
    {
        return ['chat-channel.'. $this->chatId];
    }
}
