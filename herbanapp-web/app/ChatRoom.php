<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChatRoom extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'isGroup',
    ];

    /* The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['lastMessage'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    protected $dateFormat = 'U';

    public function getLastMessageAttribute()
    {
        return Message::where('chat_room_id', '=', $this->id)->orderBy('created_at', 'desc')->first();
    }
}
