<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PendingClaim extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'clinic_id','user_id','approved'
    ];
    public $timestamps = true;

    protected $dateFormat = 'U';

    public function clinic()
    {
        return $this->belongsTo('App\Clinic');
    }
}
