<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClinicsFavorite extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'clinics_favorite';

    protected $fillable = [
        'user_id', 'clinic_id',
    ];

    public $timestamps = true;
    protected $dateFormat = 'U';
}

