<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'documentable_id', 'documentable_type','documentable_file',
    ];
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    protected $dateFormat = 'U';
    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function documentable() {
        return $this->morphTo();
    }
}
