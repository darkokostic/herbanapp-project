<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'clinic_id', 'user_id', 'rating', 'message',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['comments', 'user'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    protected $dateFormat = 'U';

    public function clinic()
    {
        return $this->hasOne('\App\Clinic');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    /**
     * Get the number of reviews for the clinic.
     *
     * @return int
     */
    public function getCommentsAttribute()
    {
        return $this->comments()->get();
    }

    public function user() {
        return $this->hasOne('App\User');
    }

    public function getUserAttribute() {
        return User::where('id', $this->user_id)->first();
    }
}
