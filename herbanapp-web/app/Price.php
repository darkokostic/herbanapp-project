<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'gram', 'two_grams', 'eighth','fourth','half','ounce'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    protected $dateFormat = 'U';

    public function product()
    {
        return $this->belongsTo('App\Product');
    }
}
