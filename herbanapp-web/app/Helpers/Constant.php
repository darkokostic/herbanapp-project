<?php

namespace App\Helpers;


class Constant
{
    //Class identifiers
    const CLINIC_IDENTIFIER = 'App\Clinic';
    const DELIVERY_SERVICE_IDENTIFIER = 'App\DeliveryService';

    //Roles
    const ROLE_USER = 'user';
    const ROLE_CLINIC_ADMIN = 'clinic_admin';
    const ROLE_CLINIC_MANAGER = 'clinic_manager';
    const ROLE_APP_ADMIN = 'app_admin';
    const ROLE_JUNIOR_MANAGER = 'app_junior_manager';
    const ROLE_SENIOR_MANAGER = 'app_senior_manager';
    const IMAGES_PATH = '/uploads/';

    //Deals
    const DEAL_HOURLY = 'HOURLY';
    const DEAL_TODAY = 'TODAY';
    const DEAL_ONGOING = 'ONGOING';


    /**
     * @param $constant
     * @return string
     */
    public static function absolutePath($constant)
    {
        return public_path() . constant('self::' . $constant);
    }
}