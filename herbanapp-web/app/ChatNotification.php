<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChatNotification extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'chat_room_id','user_id', 'isRead',
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;


    protected $dateFormat = 'U';
    /**
     * Get the subcategories for the category.
     */

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function chatRoom() {
        return $this->belongsTo('App\ChatRoom');
    }
}
