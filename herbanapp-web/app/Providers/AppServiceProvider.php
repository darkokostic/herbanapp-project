<?php

namespace App\Providers;

use Http\Adapter\Guzzle6\Client;
use Illuminate\Support\ServiceProvider;
use Braintree_Configuration;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Braintree_Configuration::environment(env('BRAINTREE_ENV'));
        Braintree_Configuration::merchantId(env('BRAINTREE_MERCHANT_ID'));
        Braintree_Configuration::publicKey(env('BRAINTREE_PUBLIC_KEY'));
        Braintree_Configuration::privateKey(env('BRAINTREE_PRIVATE_KEY'));
        // Cashier::useCurrency('eur', '€'); default is  USD
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
//        $this->app->bind('mailgun.client', function() {
//            return  Client::createWithConfig([
//                // your Guzzle6 configuration
//            ]);
//        });
    }
}
