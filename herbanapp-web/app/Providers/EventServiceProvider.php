<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\SomeEvent' => [
            'App\Listeners\EventListener',
        ],
        \SocialiteProviders\Manager\SocialiteWasCalled::class => [
            // add your listeners (aka providers) here
            'SocialiteProviders\Instagram\InstagramExtendSocialite@handle',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();



        Event::listen('tymon.jwt.invalid', function ($exception) {
            return response()->custom($exception->getStatusCode(), "token_invalid!", null);
        });



        // fired when the token has expired
        Event::listen('tymon.jwt.expired', function ($exception) {
            return response()->custom($exception->getStatusCode(), "token_expired!", null);
        });

        // fired if the user could not be found (shouldn't really happen)
        Event::listen('tymon.jwt.user_not_found', function ($exception) {
            return response()->custom($exception->getStatusCode(), "user_not_found!", null);
        });

        // fired when the token could not be found in the request
        Event::listen('tymon.jwt.absent', function () {
            return response()->custom(400, "token_not_found!", null);
        });
    }
}
