<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Response;

class CustomResponseNews extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(ResponseFactory $factory)
    {
        $factory->macro('news', function ($datePosted, $description, $newsLink, $imageLink, $title, $user) {
            return [
                'date' => $datePosted,
                'description' => $description,
                'post_link' => $newsLink,
                'image_link' => $imageLink,
                'title' => $title,
                'user' => $user
            ];
        });
        $factory->macro('notification', function ($datePosted, $text, $notificationLink, $title, $user) {
            return [
                'date' => $datePosted,
                'notification_text' => $text,
                'notification_link' => $notificationLink,
                'title' => $title,
                'user' => $user
            ];
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
