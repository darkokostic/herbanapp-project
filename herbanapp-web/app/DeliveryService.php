<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliveryService extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'address', 'latitude', 'longitude', 'phone', 'email', 'website', 'hits', 'first_time_patients', 'announcement', 'about_us', 'social_networks', 
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    protected $dateFormat = 'U';

    public function gallery()
    {
        return $this->morphMany('\App\Image', 'imageable')->where('galley', false);
    }

    /**
     * @return mixed
     */
    public function logo()
    {
        return $this->morphOne('\App\Image', 'imageable')->where('logo', true);
    }
}
