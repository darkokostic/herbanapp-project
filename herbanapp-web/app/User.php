<?php

namespace App;

use Hootlex\Friendships\Traits\Friendable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Cache;
use Laravel\Cashier\Billable;
use Tymon\JWTAuth\Facades\JWTAuth;

class User extends Authenticatable
{
    use Notifiable;
    use Billable;
    use Friendable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'name', 'lastname', 'phone', 'role', 'account_type', 'email', 'password', 'state', 'city', 'birth_date', 'social_id', 'social_name', 'player_id','about_me'
    ];

    protected $dateFormat = 'U';
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = ['avatar', 'friendCount', 'clinicsCount', 'isOnline'];

    public function avatar()
    {
        return $this->morphOne('\App\Image', 'imageable')->where('avatar', true);
    }

    public function setPasswordAttribute($password)
    {

        $this->attributes['password'] = bcrypt($password);
    }

    public function getAvatarAttribute()
    {
        if ($this->avatar()->count() == 0) {
            return null;
        } else {
            return $this->avatar()->pluck('url')->first();
        }
    }

    public function searchFriends($key)
    {
        return $this->getFriends()->filter(function ($friend) use ($key) {
            similar_text($friend->username, $key, $percentUsername);

            similar_text($friend->name, $key, $percentName);

            similar_text($friend->lastname, $key, $percentLastname);

            similar_text($friend->email, $key, $percentEmail);
            return ($percentEmail > 40 || $percentLastname > 40 || $percentName > 40 || $percentUsername > 40);
        });
    }

    public function getFriendCountAttribute()
    {
        return $this->getFriendsCount();
    }

    public function favoriteClinics()
    {
        return $this->belongsToMany('\App\Clinic', 'clinics_favorite', 'user_id', 'clinic_id');
    }

    public function getClinicsCountAttribute()
    {
        return ClinicsFavorite::where('user_id', '=', $this->id)->count();
    }

    public function isOnline()
    {
        return Cache::has('user-is-online-' . $this->id);
    }

    public function getIsOnlineAttribute()
    {
        return $this->isOnline();
    }
}
