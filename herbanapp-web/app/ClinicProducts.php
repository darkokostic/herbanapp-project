<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClinicProducts extends Model
{
    protected $fillable = [
        'clinic_id','product_id',
    ];
    public $timestamps = true;

    protected $dateFormat = 'U';

}
