<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'pricing', 'description','price_id'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    protected $dateFormat = 'U';


    protected $appends = ['gallery', 'avatar','prices','clinic'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function gallery()
    {
        return $this->morphMany('\App\Image', 'imageable')->where('avatar', false);
    }

    /**
     * @return mixed
     */
    public function avatar()
    {
        return $this->morphOne('\App\Image', 'imageable')->where('avatar', true);
    }

    public function clinic()
    {
        return $this->belongsToMany('App\Clinic', 'clinic_products','product_id','clinic_id');
    }
    public function category()
    {
        return $this->belongsToMany('App\Category', 'product_categorys', 'product_id', 'category_id');
    }

    public function prices()
    {
        return $this->hasOne('App\Price','id');
    }
    public function getPricesAttribute(){
        return $this->prices()->first();
    }
    public function getGalleryAttribute()
    {
        return $this->gallery()->get();
    }
    public function getClinicAttribute()
    {
        return $this->clinic()->pluck('clinic_id')->first();
    }

    public function getAvatarAttribute()
    {
        if($this->avatar()->count() == 0) {
            return null;
        } else {
            return $this->avatar()->get();
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders()
    {
        return $this->hasMany('\App\Order');
    }


}
