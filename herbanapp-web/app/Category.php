<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','parent_id',
    ];

    protected $dateFormat = 'U';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Get the subcategories for the category.
     */
    protected $appends = ['products'];
    public function products(){
        return $this->belongsToMany('\App\Product','product_categorys','category_id','product_id');
    }

    public function getProductsAttribute(){
        return $this->products()->get();
    }
    public function children(){
        return $this->hasMany('\App\Category','parent_id');

    }
}
