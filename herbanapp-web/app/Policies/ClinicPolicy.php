<?php

namespace App\Policies;

use App\Clinic;
use App\Helpers\Constant;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ClinicPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine whether the user can create clinic.
     *
     * @param  \App\User $user
     * @return boolean
     */

    public function create(User $user)
    {

        return ($user->role == Constant::ROLE_USER);
    }

    /**
     * Determine whether the user can update the clinic.
     *
     * @param  \App\User $user
     * @param  \App\Clinic $clinic
     * @return boolean
     */

    public function update(User $user, Clinic $clinic)
    {
        return ($user->role == Constant::ROLE_USER);
    }

    /**
     * Determine whether the user can delete the clinic.
     *
     * @param  \App\User $user
     * @param  \App\Clinic $clinic
     * @return boolean
     */

    public function delete(User $user, Clinic $clinic)
    {
        return ($user->role == Constant::ROLE_USER);
    }
}
