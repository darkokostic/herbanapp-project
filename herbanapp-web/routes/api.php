<?php

use Illuminate\Support\Facades\Redis;
use App\Events\ChatEvent;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 *  Auth Route
 */

Route::group(['middleware' => ['jwt.auth', \App\Http\Middleware\LogLastUserActivity::class]], function () {
    /**
     * User
     */
    Route::get('self', 'LoginController@self');
    Route::post('user/update', 'LoginController@updateUser');
    Route::post('user/logout', 'LoginController@logout');
    Route::get('suggested-users', 'LoginController@suggestedUsers');
    Route::post('change-password', 'LoginController@changePassword');
    Route::post('contact-us', 'LoginController@contactUs');

    /**
     * Clinics
     */
    Route::get('nearby-clinics', 'ClinicController@getNearByClinics');
    Route::get('clinic/{clinic_id}', 'ClinicController@getSingleClinic');
    Route::get('unapprovedClinicClaims', 'ClinicController@getUnapprovedClinicClaims');
    Route::get('clinic/favorites/{id}', 'ClinicController@getFavoriteClinics');
    Route::post('clinic/favorites', 'ClinicController@toggleFavorite');

    Route::post('claim/clinic', 'ClinicController@claimClinic');
    Route::post('approveClinicClaim', 'ClinicController@approveClinicClaim');
    Route::post('create/clinic', 'ClinicController@createClinic');
    Route::post('search', 'ClinicController@searchClinicsAndUsers');
    /**
     * Delivery Services
     */
    Route::get('nearby-delivery-service', 'DeliveryServiceController@getNearByDeliveryService');
    Route::get('delivery-service/{delivery_service_id}', 'DeliveryServiceController@getSingleDeliveryService');
    Route::post('create/delivery-service', 'DeliveryServiceController@createDeliveryService');


    Route::get('nearby-doctors', 'ClinicController@getNearByDoctors');

    /**
     * Products
     */
    Route::post('createProduct', 'ProductController@createProduct');

    /**
     *  Orders
     */
    Route::get('orders', 'OrderController@index');
    Route::post('orders/store', 'OrderController@store');
    Route::get('orders/{id}', 'OrderController@show');
    Route::patch('orders/{id}', 'OrderController@updateOrder');

    /**
     * Friends
     */
    Route::get('friend-list', 'FriendshipController@getMyFriends');
    Route::get('friend-request', 'FriendshipController@getMyPandingRequests');
    Route::post('search-friends', 'FriendshipController@searchFriends');
    Route::post('add-friend', 'FriendshipController@addFriend');
    Route::post('remove-friend', 'FriendshipController@removeFriend');
    Route::post('accept-friend-request', 'FriendshipController@acceptFriendRequest');
    Route::post('cancel-friend-request', 'FriendshipController@denyFriendRequest');

    /**
     * Reviews
     */
    Route::post('create-review', 'ReviewController@createReview');
    Route::post('create-review/comment', 'ReviewController@createReviewComment');
    Route::get('review/list','ReviewController@getUserReviews');

    /**
     * Deals
     */
    Route::post('deal/create', 'DealController@createDeal');
    /**
     * Notifications
     */
    Route::get('notifications', 'NotificationController@notifications');
    Route::post('chat', 'ChatController@chat');

    /**
     * Chat
     */
    Route::post('send/message', 'ChatController@sendMessage');
    Route::post('get/chat-room', 'ChatController@getChatRoom');
    Route::post('create/group-chat', 'ChatController@createGroupChat');
    Route::post('chat/join', 'ChatController@joinGroupChat');
    Route::post('chat/leave', 'ChatController@leaveGroupChat');
    Route::get('get/group-chats', 'ChatController@getGroupChats');
    Route::get('get/user-inbox', 'ChatController@getUserInbox');

});

Route::post('login', 'LoginController@logInUserMobile');
Route::post('login-social', 'LoginController@socialLogInMoblie');

Route::post('register', 'LoginController@registerUserMobile');


Route::post('reset-password', 'LoginController@resetPassword');

Route::get('chat/{chatId}', 'ChatController@chat');
Route::post('chat/messages', 'ChatController@getMessages');
Route::get('product/{id}','ProductController@singleProduct');


Route::get('category/list','SearchController@getAllCategorys');
Route::get('search/main','SearchController@mainSearch');
