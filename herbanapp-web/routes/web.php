<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('home');
})->name('home');
Route::get('logout',function (){
    \Illuminate\Support\Facades\Auth::logout();
    \Illuminate\Support\Facades\Session::flush();
   return redirect('home') ;
})->name('logout');

Route::get('login', function () {
    return view('auth/login');
})->name('login');
Route::get('signup', function () {
    return view('auth/register');
})->name('sign up');

Route::get('payment', function () {
    return view('payment');
})->name('payment');
Route::get('choose/account-type', function () {
    return view('choose-acc-type');
})->name('Choose Acccount Type');

Route::get('shopping-cart', function () {
    return view('shopping-cart');
})->name('Shopping Cart');

Route::get('orders', function () {
    return view('orders');
})->name('Orders');

Route::get('braintree/token', 'LoginController@getBraintreeToken');
Route::post('subscribe', 'LoginController@subscribe');
Route::get('clinic/{id}', function ($id) {
    return view('clinic');
});

Route::get('doctor/{id}', function ($id) {
    return view('doctor');
});

Route::get('clinics', function () {

    return view('clinic-list');
})->name('Clinics');
Route::get('test', function () {
    return view('test');
});
Route::get('get-listed', function () {
    return view('getListed');
});

Route::get('create-clinic', function () {
    return view('create-clinic');
});
Route::get('user-profile', function () {
    return view('user-profile');
});

/**
 *  Login
 */

Route::get('login/facebook', 'LoginController@redirectToProviderFacebook');
Route::get('login/facebook/callback', 'LoginController@handleProviderCallbackFacebook');
Route::get('login/twitter', 'LoginController@redirectToProviderTwitter');
Route::get('login/twitter/callback', 'LoginController@handleProviderCallbackTwitter');
Route::get('login/instagram', 'LoginController@redirectToProviderInstagram');
Route::get('login/instagram/callback', 'LoginController@handleProviderCallbackInstagram');

Route::post('login', 'LoginController@logInUser');

/**
 * Clinic
 */

Route::post('create/clinic', 'ClinicController@createClinic');

Route::get('reset/{token}', 'LoginController@changePasswordView');
Route::post('reset/new', 'LoginController@newPassword');


Route::group(['prefix' => 'web'], function () {
    Route::get('info-count', 'LoginController@infoCount');
    Route::get('nearby-clinics', 'ClinicController@getNearByClinics');
    Route::get('nearby-delivery-service', 'DeliveryServiceController@getNearByDeliveryService');
    Route::get('clinic/{clinic_id}', 'ClinicController@getSingleClinic');
    Route::get('doctor/{doctor_id}', 'ClinicController@getSingleDoctor');
    Route::get('nearby-doctors', 'ClinicController@getNearByDoctors');

    Route::post('create-review', 'ReviewController@createReview');
    Route::post('create-review/comment', 'ReviewController@createReviewComment');
});

Route::get('chat/{chatId}', 'ChatController@chat');

//Auth::routes();

Route::get('/home', 'HomeController@index');
Route::post('register', 'LoginController@registerUser');

