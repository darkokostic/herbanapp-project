angular.module('myApp.header_controller', [])
    .controller('HeaderCtrl', ['HeaderService','$scope','MyUserHandler','$rootScope', function(HeaderService,$scope,MyUserHandler,$rootScope) {
        var responseSelf = MyUserHandler.self();
        responseSelf.then(function(response) {
                // console.log(response);
                $rootScope.user = response;
            },
            function(response) {
                // console.log(response);
            });
        $scope.logout = function () {
            localStorage.removeItem('token')
            $rootScope.user = null;
        }
    }]);