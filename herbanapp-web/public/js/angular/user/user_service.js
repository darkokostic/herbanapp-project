angular.module('myApp.user_service', [])
    .service('UserService', function ($http,$q) {
        return {
            getFriends :function () {
                var defer = $q.defer();
                $http({
                    method: 'GET',
                    data:{

                    },
                    url: 'api/friend-list'
                }).then(function successCallback(response) {
                    defer.resolve(response.data.entity);
                    return response;
                }, function errorCallback(response) {
                    defer.reject(response);
                });
                return defer.promise;
            },
            getReviews :function () {
                var defer = $q.defer();
                $http({
                    method: 'GET',
                    data:{

                    },
                    url: 'api/review/list'
                }).then(function successCallback(response) {
                    defer.resolve(response.data.entity);
                    return response;
                }, function errorCallback(response) {
                    defer.reject(response);
                });
                return defer.promise;
            }
        }
    });
