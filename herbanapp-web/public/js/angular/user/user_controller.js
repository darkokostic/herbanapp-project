angular.module('myApp.user_controller', [])
    .controller('UserCtrl', ['UserService','$scope','MyUserHandler','$rootScope', function(UserService,$scope,MyUserHandler,$rootScope) {
        var responseSelf = MyUserHandler.self();
        responseSelf.then(function(response) {
                console.log(response);
                $rootScope.user = response;
            },
            function(response) {
                console.log(response);
            });
        var responseFriends = UserService.getFriends();
        responseFriends.then(function(response) {
                console.log(response);
                $scope.friends = response;
            },
            function(response) {
                console.log(response);
            });
        var responseReviews = UserService.getReviews();
        responseReviews.then(function(response) {
                console.log(response);
                $scope.reviews = response;
            },
            function(response) {
                console.log(response);
            });


    }]);