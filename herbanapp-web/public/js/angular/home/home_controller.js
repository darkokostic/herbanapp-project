angular.module('myApp.home_controller', [])
    .controller('HomeCtrl', ['HomeService','$scope','MyUserHandler','$rootScope', function(HomeService,$scope,MyUserHandler,$rootScope) {
        $scope.statistics = {};
        $scope.showLoader = true;
        $scope.nearByClinics = [];
        $scope.nearByDoctors = [];
        $scope.geolocation = {
            lat:null,
            lng:null
        };
        $scope.clinicPages = 0;
        $scope.doctorPages = 0;
        $scope.token = document.getElementById("user.token").value;

        if($scope.token){
            localStorage.setItem('token',$scope.token);
            var responseSelf = MyUserHandler.self($scope.token);
            responseSelf.then(function(response) {
                    $rootScope.user = response;
                },
                function(response) {
                    //console.log(response);
                });

        }
        var responseStatistics = HomeService.getStatistics();
            responseStatistics.then(function(response) {
                $scope.statistics = response;
            },
            function(response) {
                console.log(response);
            });
        $scope.getNumber = function(num) {
            return new Array(num);
        }
        $scope.getClinic = function(page) {
            var responseNearByClinics = HomeService.getNearByClinics($scope.geolocation,page);
            responseNearByClinics.then(function (response) {
                    $scope.clinicPages ++;
                    $scope.nearByClinics.push(response.data);
                    if(response.next_page_url){
                        $scope.getClinic(response.current_page+1);
                    }
                    $scope.showLoader = false;
                },
                function (response) {
                    //console.log(response);
                });
        }
        $scope.getDoctor = function(page) {
            var responseNearByClinics = HomeService.getNearByDoctorsService($scope.geolocation,page);
            responseNearByClinics.then(function (response) {
                    $scope.doctorPages ++;
                    $scope.nearByDoctors.push(response.data);
                    if(response.next_page_url){
                        $scope.getDoctor(response.current_page+1);
                    }
                    $scope.showLoader = false;
                },
                function (response) {
                    //console.log(response);
                });
        }

            var responseLocation = HomeService.getGeolocation($scope.geolocation);
            responseLocation.then(function(response) {
                    $scope.geolocation.lat=response.data.location.lat;
                    $scope.geolocation.lng=response.data.location.lng;
                    // console.log($scope.geolocation);
                    $scope.getClinic(1);
                    $scope.getDoctor(1);
                },
                function(response) {
                    // console.log(response);
                });


    }]);