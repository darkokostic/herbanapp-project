angular.module('myApp.home_service', [])
    .service('HomeService', function ($http,$q) {
        return {
            getStatistics: function () {
                var defer = $q.defer();
                $http({
                    method: 'GET',
                    data:{

                    },
                    url: 'web/info-count'
                }).then(function successCallback(response) {
                    defer.resolve(response.data.entity);
                    return response;
                }, function errorCallback(response) {
                    defer.reject(response);
                });
                return defer.promise;
            },

            getNearByClinics: function (cordinate,page) {
                var defer = $q.defer();
                $http({
                    method: 'GET',
                    data:{

                    },
                    url: 'web/nearby-clinics?lat='+cordinate.lat+"&lng="+cordinate.lng+'&page='+page
                }).then(function successCallback(response) {
                    defer.resolve(response.data.entity);
                    return response;
                }, function errorCallback(response) {
                    defer.reject(response);
                });
                return defer.promise;
            },
            getGeolocation: function() {
                var defer = $q.defer();
                $http({
                    method: 'POST',
                    url: 'https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyBnICk37tkvyjby446VXMfe1c0-A34AUHU'
                }).then(function successCallback(response) {
                    defer.resolve(response);
                    return response;
                }, function errorCallback(response) {
                    defer.reject(response);
                });
                return defer.promise;
            },
            getNearByDoctorsService: function (cordinate,page) {
                var defer = $q.defer();
                $http({
                    method: 'GET',
                    data:{

                    },
                    url: 'web/nearby-doctors?lat='+cordinate.lat+"&lng="+cordinate.lng+'&page='+page
                }).then(function successCallback(response) {
                    defer.resolve(response.data.entity);
                    return response;
                }, function errorCallback(response) {
                    defer.reject(response);
                });
                return defer.promise;
            }
        }
    });
