angular.module('myApp.clinic_service', [])
    .service('ClinicService', function ($http,$q) {
        return {
            getClinic : function (id) {
                var defer = $q.defer();
                $http({
                    method: 'GET',
                    data:{

                    },
                    url: '../web/clinic/'+ id
                }).then(function successCallback(response) {
                    defer.resolve(response.data.entity);
                    return response;
                }, function errorCallback(response) {
                    defer.reject(response);
                });
                return defer.promise;
            },
            sendReview : function (review) {
                var defer = $q.defer();
                $http({
                    method: 'POST',
                    data:review,
                    url: '../web/create-review'
                }).then(function successCallback(response) {
                    defer.resolve(response.data.entity);
                    return response;
                }, function errorCallback(response) {
                    defer.reject(response);
                });
                return defer.promise;
            },
            sendReplie : function (review) {
                var defer = $q.defer();
                $http({
                    method: 'POST',
                    data:review,
                    url: '../web/create-review/comment'
                }).then(function successCallback(response) {
                    defer.resolve(response.data.entity);
                    return response;
                }, function errorCallback(response) {
                    defer.reject(response);
                });
                return defer.promise;
            },

            getDoctor : function (id) {
                var defer = $q.defer();
                $http({
                    method: 'GET',
                    data:{

                    },
                    url: '../web/clinic/'+ id
                }).then(function successCallback(response) {
                    defer.resolve(response.data.entity);
                    return response;
                }, function errorCallback(response) {
                    defer.reject(response);
                });
                return defer.promise;
            }
        }
    });
