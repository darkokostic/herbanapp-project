angular.module('myApp.clinic_controller', [])
    .controller('ClinicCtrl', ['ClinicService', '$scope','$rootScope' ,'modalConfirmService','MyUserHandler', function (ClinicService, $scope, $rootScope,modalConfirmService,MyUserHandler) {
        $scope.clinic = {};
        $scope.showLoader = true;
        var responseSelf = MyUserHandler.self();
        responseSelf.then(function(response) {
                // console.log(response);
                $rootScope.user = response;
            },
            function(response) {
                // console.log(response);
            });
        $scope.getUrlId = function () {
            var path = window.location.pathname;
            return path.replace('/clinic/', '');
        }
        var responseGetClinic = ClinicService.getClinic($scope.getUrlId());
        responseGetClinic.then(function (response) {
                $scope.clinic = response;
                console.log($scope.clinic);
                $scope.showLoader = false;
            },
            function (response) {
                // console.log(response);
            });

        /* Modal */
        $scope.showModal = function(product, categoryName){
            product.categoryName = categoryName;
            var options = {
                product: product
            };

            modalConfirmService.showModal({}, options).then(function (result) {
                $scope.msg = 'You have clicked Yes';
            });
        }
        $scope.saveReview = function (review) {
            if ($rootScope.user){
                review.user_id = $rootScope.user.id;
                review.clinic_id = $scope.clinic.id;
                var responseReview = ClinicService.sendReview(review);
                responseReview.then(function (response) {
                        var responseGetClinic = ClinicService.getClinic($scope.getUrlId());
                        responseGetClinic.then(function (response) {
                                $scope.clinic = response;
                                console.log($scope.clinic);
                                $scope.showLoader = false;
                            },
                            function (response) {
                                // console.log(response);
                            });
                    },
                    function (response) {
                         console.log(response);
                    });
            }else{
                window.location.href = '../../login';
            }

            console.log(review);
        }
        $scope.sendReplie = function (replie,id) {
            if ($rootScope.user){
                var review = {
                    user_id:$rootScope.user.id,
                    review_id:id,
                    message:replie
                };

                var responseReview = ClinicService.sendReplie(review);
                responseReview.then(function (response) {
                        var responseGetClinic = ClinicService.getClinic($scope.getUrlId());
                        responseGetClinic.then(function (response) {
                                $scope.clinic = response;
                                console.log($scope.clinic);
                                $scope.showLoader = false;
                            },
                            function (response) {
                                // console.log(response);
                            });
                    },
                    function (response) {
                        console.log(response);
                    });
            }else{
                window.location.href = '../../login';
            }
        }

    }]);
angular.module('myApp.doctor_controller', [])
.controller('DoctorCtrl', ['ClinicService', '$scope', 'modalConfirmService', function (ClinicService, $scope, modalConfirmService) {
    $scope.clinic = {};
    $scope.showLoader = true;
    $scope.getUrlId = function () {
        var path = window.location.pathname;
        return path.replace('/doctor/', '');
    }

    var responseGetClinic = ClinicService.getDoctor($scope.getUrlId());
    responseGetClinic.then(function (response) {
            $scope.clinic = response;
            console.log($scope.clinic);
            $scope.showLoader = false;
        },
        function (response) {
            // console.log(response);
        });

    /* Modal */
    $scope.showModal = function(product, categoryName){
        product.categoryName = categoryName;
        var options = {
            product: product
        };

        modalConfirmService.showModal({}, options).then(function (result) {
            $scope.msg = 'You have clicked Yes';
        });
    }

}]);
