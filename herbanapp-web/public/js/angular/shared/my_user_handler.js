angular.module('myApp.my_user_handler', [])
    .service('MyUserHandler', function ($http,$q) {
        return {
            self: function (token) {
                var url = '../api/self';
            if (!(typeof token === 'undefined')){
                url = '../api/self?token='+token;
            }
                var defer = $q.defer();
                $http({
                    method: 'GET',
                    data:{

                    },
                    url: url
                }).then(function successCallback(response) {
                    defer.resolve(response.data.entity);
                    return response;
                }, function errorCallback(response) {
                    defer.reject(response);
                });
                return defer.promise;
            }
        }
    });
