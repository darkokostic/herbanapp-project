angular.module('myApp.search_service', [])
    .service('SearchService', function ($http,$q) {
        return {
            getCategories  : function (id) {
                var defer = $q.defer();
                $http({
                    method: 'GET',
                    data:{

                    },
                    url: '../../../api/category/list'
                }).then(function successCallback(response) {
                    defer.resolve(response.data.entity);
                    return response;
                }, function errorCallback(response) {
                    defer.reject(response);
                });
                return defer.promise;
            },
            getAllData  : function (search,url) {
                if (url == null){
                    url = '../../../api/search/main?search_key='+search.search_key;
                }else{
                    url = url + '&search_key=' + +search.search_key;
                }
                console.log(url)
                var defer = $q.defer();
                $http({
                    method: 'GET',
                    param: search,
                    url: url
                }).then(function successCallback(response) {
                    defer.resolve(response.data.entity);
                    return response;
                }, function errorCallback(response) {
                    defer.reject(response);
                });
                return defer.promise;
            }
        }
    });

