angular.module('myApp.search_controller', [])
    .controller('SearchCtrl', ['SearchService', '$scope', 'modalConfirmService', function (SearchService, $scope, modalConfirmService) {
        $scope.loadingData = false;
        $scope.categories = [];
        $scope.sub_categories = [];
        $scope.allProducts = [];
        $scope.clinics = [];
        $scope.productsToShow = [];
        $scope.nextPageUrl = null;
        $scope.isSelected = {
            product:true,
            clinic:false,
            category:{
                id:0,
                child:0
            }
        };
        $scope.search = {
            search_key: null
        };


        $scope.retriveAll = function () {
            var responseAll = SearchService.getAllData($scope.search,$scope.nextPageUrl);
            responseAll .then(function(response) {

                    $scope.nextPageUrl = response.products.next_page_url;
                    $scope.categories = response.categorys;
                    $scope.allProducts = response.products.data;
                    $scope.clinics = response.clinics.data;
                    $scope.productsToShow = $scope.allProducts;
                    $scope.loadingData = true;
                    console.log(response.products);
                    $scope.showAllProducts();
                },
                function(response) {
                    console.log(response);
                });

        };
        $scope.searchAll = function () {
            console.log($scope.nextPageUrl)
            $scope.loadingData = false;
            var responseAll = SearchService.getAllData($scope.search,null);
            responseAll .then(function(response) {

                    $scope.nextPageUrl = response.clinics.next_page_url;
                    $scope.categories = response.categorys;
                    $scope.allProducts = response.products.data;
                    $scope.clinics = response.clinics.data;
                    $scope.showSelected(response);
                    console.log(response);
                    console.log($scope.allProducts);

                    $scope.loadingData = true;
                },
                function(response) {
                    console.log(response);
                });
        };
        $scope.paginate = function () {
            console.log("Uso u paginaciju")
            var responseAll = SearchService.getAllData($scope.search,$scope.nextPageUrl);
            responseAll .then(function(response) {

                    $scope.nextPageUrl = response.clinics.next_page_url;
                    $scope.categories = response.categorys;
                    $scope.allProducts = response.products.data;
                    $scope.clinics = response.clinics.data;
                    $scope.showSelected(response);

                    $scope.loadingData = true;
                },
                function(response) {
                    console.log(response);
                });
        }
        $scope.retriveAll();

        $scope.showAllProducts = function () {
            $scope.isSelected = {
                product:true,
                clinic:false,
                category:{
                    id:0,
                    child:0
                }
            }

            $scope.sub_categories = [];
            $scope.productsToShow = $scope.allProducts;
        }
        $scope.showAllClinics = function () {
            $scope.isSelected = {
                product:false,
                clinic:true,
                category:{
                    id:0,
                    child:0
                }
            }

            $scope.sub_categories = [];
            $scope.productsToShow = $scope.clinics;
        }


        $scope.selectCategory = function (category) {
            $scope.isSelected = {
                product:false,
                clinic:false,
                category:{
                    id:category.id,
                    child:0
                }
            }
            $scope.productsToShow = [];
            $scope.sub_categories = [];
            $scope.sub_categories = category.children;
            console.log(category);
            angular.forEach($scope.sub_categories,function (value,key) {
                $scope.productsToShow = $scope.productsToShow.concat(value.productsToShow);
                console.log(value.productsToShow)

            });
        }
        $scope.selectSubCategory = function (category) {
            $scope.isSelected = {
                product:true,
                clinic:false,
                category:{
                    id:0,
                    child:category.id
                }
            }
            $scope.productsToShow = [];
            console.log(category.productsToShow)
            $scope.productsToShow=  $scope.productsToShow.concat(category.productsToShow);
        }
        $scope.showSelected = function (data) {
            if ( $scope.isSelected.product){
                console.log("Product");
                $scope.productsToShow = data.products.data;
            }else if($scope.isSelected.clinic){
                console.log("Clinic");
                $scope.productsToShow = data.clinics.data;
            }else if($scope.isSelected.category.child != 0){
                console.log("Child");
                angular.forEach($scope.categories,function (value,key) {
                    angular.forEach(value.children,function (chield,key) {
                            if (chield.id == $scope.isSelected.category.child){
                                $scope.selectSubCategory(chield);
                            }

                    });
                });
            }else{
                console.log("Category");
                angular.forEach($scope.categories,function (value,key) {
                        if (value.id == $scope.isSelected.category.id){
                            $scope.selectCategory(value);
                        }

                });
            }
        }
    }]);

