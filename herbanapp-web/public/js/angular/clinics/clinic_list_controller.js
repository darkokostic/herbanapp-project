angular.module('myApp.clinic_list_controller', [])
    .controller('ClinicListCtrl', ['$scope', 'HomeService', function ($scope, HomeService) {
        $scope.nearByClinics = [];
        $scope.nearByDeliveryService = {};
        $scope.geolocation = {
            lat: null,
            lng: null
        };
        $scope.clinicPages = 0;

        HomeService.getGeolocation($scope.geolocation)
            .then(function (response) {
                    $scope.geolocation.lat = response.data.location.lat;
                    $scope.geolocation.lng = response.data.location.lng;
                    $scope.getClinics();
                },
                function (response) {
                    // console.log(response);
                });

        $scope.getClinics = function () {
            $scope.showLoader = true;
            HomeService.getNearByClinics($scope.geolocation, $scope.clinicPages + '&per_page=8')
                .then(function (response) {
                        $scope.clinicPages++;
                        angular.forEach(response.data, function (clinic) {
                            $scope.nearByClinics.push(clinic);
                        });

                        if (response.next_page_url) {
                            $scope.clinicPages++;
                        } else {
                            $scope.clinicPages = null;
                        }

                        $scope.showLoader = false;
                    },
                    function (response) {
                        //console.log(response);
                    });
        };
    }]);
