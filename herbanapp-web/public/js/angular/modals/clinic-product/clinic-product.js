angular.module('myApp.modal_service', [])
    .service('modalConfirmService', ['$uibModal',
        function ($modal) {
            var modalDefaults = {
                backdrop: true,
                keyboard: true,
                modalFade: true,
                templateUrl: '/js/angular/modals/clinic-product/clinic-product.html'
            };

            var modalOptions = {
                closeButtonText: 'Close',
                actionButtonText: 'OK'
            };

            this.showModal = function (customModalDefaults, customModalOptions) {
                //product passed
                console.log(customModalOptions.product);


                if (!customModalDefaults) customModalDefaults = {};
                customModalDefaults.backdrop = 'static';
                //Create temp objects to work with since we're in a singleton service
                var tempModalDefaults = {};
                var tempModalOptions = {};

                //Map angular-ui modal custom defaults to modal defaults defined in service
                angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

                //Map modal.html $scope custom properties to defaults defined in service
                angular.extend(tempModalOptions, modalOptions, customModalOptions);

                if (!tempModalDefaults.controller) {
                    tempModalDefaults.controller = function ($scope, $uibModalInstance) {
                        $scope.product = customModalOptions.product;
                        $scope.modalOptions = tempModalOptions;
                        $scope.modalOptions.ok = function (result) {
                            $uibModalInstance.close(result);
                        };
                        $scope.modalOptions.close = function (result) {
                            $uibModalInstance.dismiss('cancel');
                        };
                    }
                }

                return $modal.open(tempModalDefaults).result;
            };

        }]);