var myApp = angular.module('myApp', [
    'ui.bootstrap',
    'star-rating',
    'myApp.my_user_handler',
    'myApp.header_controller',
    'myApp.header_service',
    'myApp.footer_controller',
    'myApp.home_controller',
    'myApp.home_service',
    'myApp.clinic_list_controller',
    'myApp.clinic_list_service',
    'myApp.clinic_controller',
    'myApp.doctor_controller',
    'myApp.clinic_service',
    'myApp.user_controller',
    'myApp.user_service',
    'myApp.search_controller',
    'myApp.search_service',
    'myApp.modal_service',
    'ngAnimate',
    'ngCookies',
    'infinite-scroll'
]);
myApp.config(function ($interpolateProvider) {
    //$interpolateProvider.startSymbol('!#').endSymbol('#!');
});
myApp.run(['$http', '$rootScope', '$location', '$timeout', function ($http, $rootScope, $location, $timeout) {


    if (localStorage.getItem('token')) {
        $http.defaults.headers.common.Authorization = 'Bearer ' + localStorage.getItem('token');
    }
}]);