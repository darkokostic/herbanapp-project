// init controller
var headerController = new ScrollMagic.Controller();

// create a scene
// new ScrollMagic.Scene({duration: 0})
//     .setTween(new TweenMax.fromTo("#header-title", 1, {opacity: 0, x: -100}, {opacity: 1, x: 0, ease: Elastic.easeOut}))
//     .addIndicators({name: "Text appear"}) // add indicators (requires plugin)
//     .addTo(headerController); // assign the scene to the controller
CSSPlugin.useSVGTransformAttr = true;
TweenMax.fromTo("#header-title", 0.5, {opacity: 0, y: -100,}, {opacity: 1, y: 0, ease: Power0.easeOut})

TweenLite.set(".header-bg")

var total = 3;
var warp = document.getElementById("header"), w = window.innerWidth, h = window.innerHeight;

for (i = 0; i < total; i++) {
    var Div = document.createElement('div');
    TweenLite.set(Div, {attr: {class: 'dot'}, x: R(0, w-100), y: R(-500, -500), z: R(-200, 100)});
    warp.appendChild(Div);
    animm(Div);
}

function animm(elm) {
    TweenMax.to(elm, R(6, 15), {y: -70, ease: Linear.easeNone, repeat: -1, delay: -15});
    TweenMax.to(elm, R(4, 8), {x: '+=50', rotationZ: R(0, 180), repeat: -1, yoyo: true, ease: Sine.easeInOut});
    TweenMax.to(elm, R(2, 8), {
        rotationX: R(0, 360),
        rotationY: R(0, 360),
        repeat: -1,
        yoyo: true,
        ease: Sine.easeInOut,
        delay: -5
    });
};

function R(min, max) {
    return min + Math.random() * (max - min)
};


function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
        throw new TypeError("Cannot call a class as a function");
    }
}

var FogParticle = function () {
    function FogParticle(ctx, canvasWidth, canvasHeight) {
        _classCallCheck(this, FogParticle);

        this.ctx = ctx;
        this.canvasWidth = canvasWidth;
        this.canvasHeight = canvasHeight;
        this.x = 0;
        this.y = 0;
    }

    FogParticle.prototype.setPosition = function setPosition(x, y) {
        this.x = x;
        this.y = y;
    };

    FogParticle.prototype.setVelocity = function setVelocity(x, y) {
        this.xVelocity = x;
        this.yVelocity = y;
    };

    FogParticle.prototype.setImage = function setImage(image) {
        this.image = image;
    };

    FogParticle.prototype.render = function render() {
        if (!this.image) return;

        this.ctx.drawImage(this.image, this.x - this.image.width / 2, this.y - this.image.height / 2, 400, 400);

        this.x += this.xVelocity;
        this.y += this.yVelocity;

        // Check if has crossed the right edge
        if (this.x >= this.canvasWidth) {
            this.xVelocity = -this.xVelocity;
            this.x = this.canvasWidth;
        }
        // Check if has crossed the left edge
        else if (this.x <= 0) {
            this.xVelocity = -this.xVelocity;
            this.x = 0;
        }

        // Check if has crossed the bottom edge
        if (this.y >= this.canvasHeight) {
            this.yVelocity = -this.yVelocity;
            this.y = this.canvasHeight;
        }
        // Check if has crossed the top edge
        else if (this.y <= 0) {
            this.yVelocity = -this.yVelocity;
            this.y = 0;
        }
    };

    return FogParticle;
}();

var Fog = function () {
    function Fog() {
        var _this = this;

        var _ref = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];

        var selector = _ref.selector;
        var _ref$density = _ref.density;
        var density = _ref$density === undefined ? 50 : _ref$density;
        var _ref$velocity = _ref.velocity;
        var velocity = 0.8;
        var particle = _ref.particle;
        var bgi = _ref.bgi;

        _classCallCheck(this, Fog);

        var canvas = document.querySelector(selector);
        var bcr = canvas.parentElement.getBoundingClientRect();
        this.ctx = canvas.getContext('2d');
        this.canvasWidth = canvas.width = bcr.width;
        this.canvasHeight = canvas.height = bcr.height;

        this.particleCount = density;
        this.maxVelocity = velocity;
        this.particle = particle;
        this.bgi = bgi;

        this._createParticles();
        this._setImage();

        if (!this.bgi) return;

        var img = new Image();
        img.onload = function () {
            var size = coverImg(img, _this.canvasWidth, _this.canvasHeight);
            _this.bgi = {img: img, w: size.w, h: size.h};
            _this._render();
        };
        img.src = this.bgi;
    }

    Fog.prototype._createParticles = function _createParticles() {
        this.particles = [];

        var random = function random(min, max) {
            return Math.random() * (max - min) + min;
        };

        for (var i = 0; i < this.particleCount; i++) {
            var particle = new FogParticle(this.ctx, this.canvasWidth, this.canvasHeight);

            particle.setPosition(random(0, this.canvasWidth), random(0, this.canvasHeight));
            particle.setVelocity(random(-this.maxVelocity, this.maxVelocity), random(-this.maxVelocity, this.maxVelocity));

            this.particles.push(particle);
        }
    };

    Fog.prototype._setImage = function _setImage() {
        var _this2 = this;

        if (!this.particle) return;

        var img = new Image();
        img.onload = function () {
            return _this2.particles.forEach(function (p) {
                return p.setImage(img);
            });
        };
        img.src = this.particle;
    };

    Fog.prototype._render = function _render() {
        if (this.bgi) {
            this.ctx.drawImage(this.bgi.img, 0, 0, this.bgi.w, this.bgi.h);
        } else {
            this.ctx.fillStyle = "rgba(0, 0, 0, 1)";
            this.ctx.fillRect(0, 0, this.canvasWidth, this.canvasHeight);
        }

        this.particles.forEach(function (p) {
            return p.render();
        });

        requestAnimationFrame(this._render.bind(this));
    };

    return Fog;
}();

var coverImg = function coverImg(img, width, height) {
    var ratio = img.width / img.height;
    var w = width;
    var h = w / ratio;
    if (h < height) {
        h = height;
        w = h * ratio;
    }
    return {w: w, h: h};
};

var bgi = '/assets/images/header/background.png';

new Fog({
    selector: '#fog',
    particle: '/assets/images/header/smoke.png',
    density: 30,
    bgi: bgi
});