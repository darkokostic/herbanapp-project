TweenLite.set("#not-found")

var total = 10;
var warp = document.getElementById("not-found"), w = window.innerWidth, h = window.innerHeight;

for (i = 0; i < total; i++) {
    var Div = document.createElement('div');
    TweenLite.set(Div, {attr: {class: 'dot'}, x: R(0, w-100), y: R(-1000, -1300), z: R(-200, 100)});
    warp.appendChild(Div);
    animm(Div);
}

function animm(elm) {
    TweenMax.to(elm, R(6, 15), {y: -50, ease: Linear.easeNone, repeat: -1, delay: -15});
    TweenMax.to(elm, R(4, 8), {x: '+=50', rotationZ: R(0, 180), repeat: -1, yoyo: true, ease: Sine.easeInOut});
    TweenMax.to(elm, R(2, 8), {
        rotationX: R(0, 360),
        rotationY: R(0, 360),
        repeat: -1,
        yoyo: true,
        ease: Sine.easeInOut,
        delay: -5
    });
};

function R(min, max) {
    return min + Math.random() * (max - min)
};