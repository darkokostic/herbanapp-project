<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Carbon\Carbon;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $flower = new \App\Category();
        $flower->name = "Flower";
        $flower->save();

        $edible = new \App\Category();
        $edible->name = "Edible";
        $edible->save();

        $preroll = new \App\Category();
        $preroll->name = "Preroll";
        $preroll->save();

        $wax = new \App\Category();
        $wax->name = "Wax";
        $wax->save();


        $sativa = new \App\Category();
        $sativa->name = "Sativa";
        $sativa->parent_id = $flower->id;
        $sativa->save();

        $indica = new \App\Category();
        $indica->name = "Indica";
        $indica->parent_id = $flower->id;
        $indica->save();

        $brownies = new \App\Category();
        $brownies->name = "Brownies";
        $brownies->parent_id = $edible->id;
        $brownies->save();

        $joint = new \App\Category();
        $joint->name = "Joint";
        $joint->parent_id = $preroll->id;
        $joint->save();

        $shatter = new \App\Category();
        $shatter->name = "Shatter";
        $shatter->parent_id = $wax->id;
        $shatter->save();

        foreach (range(1, 50) as $index){
            $oz = $faker->numberBetween(300, 400);
            $price = new \App\Price();
            $price->ounce = $oz;
            $price->half = $oz/2;
            $price->fourth= $oz/4;
            $price->eighth = $oz/8;
            $price->two_grams= ($oz/28) *2;
            $price->gram= $oz/28;
            $price->save();
            $product_id = DB::table('products')->insertGetId([
                'name' => $faker->name,
                'price_id' =>$price->id ,
                'description' => $faker->text(),
                'created_at' => Carbon::now()->timestamp
            ]);
            $i = $index % 10 ;
            $i++;
            $order_id = DB::table('orders')->insertGetId([
                'clinic_id' => $i,
                'user_id' => 1,
                'status' => 'pending',
                'order_number' => $faker->unique()->numberBetween(100000, 999999),
                'created_at' => Carbon::now()->timestamp
            ]);
            DB::table('clinic_products')->insert([
                'clinic_id' => 1,
                'product_id' => $product_id,
                'created_at' => Carbon::now()->timestamp
            ]);
            if($index < 10){
                DB::table('product_categorys')->insert([
                    'category_id' => $sativa->id,
                    'product_id' => $product_id,
                    'created_at' => Carbon::now()->timestamp
                ]);
            }else if($index < 20 ){
                DB::table('product_categorys')->insert([
                    'category_id' => $indica->id,
                    'product_id' => $product_id,
                    'created_at' => Carbon::now()->timestamp
                ]);
            } else if($index < 30){
                DB::table('product_categorys')->insert([
                    'category_id' => $brownies->id,
                    'product_id' => $product_id,
                    'created_at' => Carbon::now()->timestamp
                ]);
            }else if($index < 40){
                DB::table('product_categorys')->insert([
                    'category_id' => $joint->id,
                    'product_id' => $product_id,
                    'created_at' => Carbon::now()->timestamp
                ]);
            }else {
                DB::table('product_categorys')->insert([
                    'category_id' => $shatter->id,
                    'product_id' => $product_id,
                    'created_at' => Carbon::now()->timestamp
                ]);
            }


        }


    }
}
