<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Carbon\Carbon;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $url = 'uploads/avatar-user.png';


        $avatar = new \App\Image(['url' => $url, 'avatar' => 1]);

        $darko = new \App\User();
        $darko->name = 'Test';
        $darko->lastName = 'Demo';
        $darko->username = 'test';
        $darko->email = 'test@demo.com';
        $darko->phone = '066/22-5218';
        $darko->role = 'user';
        $darko->account_type = 'free';
        $darko->state = 'USA';
        $darko->city = 'Nis';
        $darko->password = 'test';
        $darko->birth_date = $faker->date;
        $darko->save();
        $darko->avatar()->save($avatar);


        $aleksandar = new \App\User();
        $url = 'uploads/avatar-user.png';
        $avatar = new \App\Image(['url' => $url, 'avatar' => 1]);
        $aleksandar->name = 'Aleksandar';
        $aleksandar->lastName = 'Vukasinovic';
        $aleksandar->username = 'aleksandar';
        $aleksandar->email = 'aleksandar@test.com';
        $aleksandar->phone = '066/22-5218';
        $aleksandar->role = 'user';
        $aleksandar->account_type = 'free';
        $aleksandar->state = 'USA';
        $aleksandar->city = 'Nis';
        $aleksandar->password = 'test';
        $aleksandar->birth_date = $faker->date;
        $aleksandar->save();
        $aleksandar->avatar()->save($avatar);


        $sale = new \App\User();
        $url = 'uploads/avatar-user.png';
        $avatar = new \App\Image(['url' => $url, 'avatar' => 1]);
        $sale->name = 'Aleksandar';
        $sale->lastName = 'Jovanovic';
        $sale->username = 'Lesa';
        $sale->email = 'aleksandarjovanovic996@yahoo.com';
        $sale->phone = $faker->phoneNumber;
        $sale->role = 'user';
        $sale->account_type = 'free';
        $sale->state = 'USA';
        $sale->city = 'Nis';
        $sale->password = 'test';
        $sale->birth_date = $faker->date;
        $sale->social_id = "1436822393057937";
        $sale->social_name = "facebook";
        $sale->save();
        $sale->avatar()->save($avatar);


        $userOne = new \App\User();
        $url = 'uploads/avatar-user.png';
        $avatar = new \App\Image(['url' => $url, 'avatar' => 1]);
        $userOne->name = $faker->name;
        $userOne->lastName = $faker->lastName;
        $userOne->username = 'Test2';
        $userOne->email = 'sale996@yahoo.com';
        $userOne->phone = $faker->phoneNumber;
        $userOne->role = 'user';
        $userOne->account_type = 'free';
        $userOne->state = 'USA';
        $userOne->city = 'Nis';
        $userOne->password = 'test';
        $userOne->birth_date = $faker->date;
        $userOne->save();
        $userOne->avatar()->save($avatar);


        foreach (range(1, 30) as $index){
            $userTwo = new \App\User();
            $url = 'uploads/avatar-user.png';
            $avatar = new \App\Image(['url' => $url, 'avatar' => 1]);
            $userTwo->name = $faker->name;
            $userTwo->lastName = $faker->lastName;
            $userTwo->username = $faker->userName;
            $userTwo->email = $faker->email;
            $userTwo->phone = $faker->phoneNumber;
            $userTwo->role = 'user';
            $userTwo->account_type = 'free';
            $userTwo->state = 'USA';
            $userTwo->city = 'Nis';
            $userTwo->password = 'test';
            $userTwo->birth_date = $faker->date;

            $userTwo->save();
            $userTwo->avatar()->save($avatar);
            $sale->befriend($userTwo);
            $userTwo->acceptFriendRequest($sale);
            $userOne->befriend($userTwo);
            if ($index % 2 == 0){
                $userTwo->acceptFriendRequest($userOne);
            }
        }


    }
}
