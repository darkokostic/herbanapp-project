<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Carbon\Carbon;

class ClinicTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        $working_hours_id = DB::table('working_hours')->insertGetId([
            'monday' => '08-12',
            'tuesday' => '08-12',
            'wednesday' => '08-12',
            'thursday' => '08-12',
            'friday' => '08-12',
            'saturday' => '08-12',
            'sunday' => '08-12',
        ]);

        $clinic_id = DB::table('clinics')->insertGetId([
            'name' => $faker->name,
            'address' => $faker->address,
            'phone' => $faker->phoneNumber,
            'email' => $faker->email,
            'website' => $faker->url,
            'latitude' => $faker->latitude,
            'longitude' => $faker->longitude,
            'business_phone' => $faker->phoneNumber,
            'business_email' => $faker->email,
            'announcement' => $faker->text(),
            'about_us' => $faker->text(),
            'legal_name' => $faker->name,
            'duns_number' => $faker->numberBetween(100, 200),
            'working_hours_id' => $working_hours_id,
//            'admin_id' => $userOne->id
        ]);

        foreach (range(1, 10) as $index) {
            $working_hours_id = DB::table('working_hours')->insertGetId([
                'monday' => '08-12',
                'tuesday' => '08-12',
                'wednesday' => '08-12',
                'thursday' => '08-12',
                'friday' => '08-12',
                'saturday' => '08-12',
                'sunday' => '08-12',
            ]);
            $clinic_id = DB::table('clinics')->insertGetId([
                'name' => $faker->name,
                'address' => $faker->address,
                'phone' => $faker->phoneNumber,
                'email' => $faker->email,
                'website' => $faker->url,
                'latitude' => $faker->latitude,
                'longitude' => $faker->longitude,
                'business_phone' => $faker->phoneNumber,
                'business_email' => $faker->email,
                'announcement' => $faker->text(),
                'about_us' => $faker->text(),
                'legal_name' => $faker->name,
                'duns_number' => $faker->numberBetween(100, 200),
                'working_hours_id' => $working_hours_id,
                'admin_id' => 2
            ]);
            $doctor_id = DB::table('doctors')->insertGetId([
                'name' => $faker->name,
                'address' => $faker->address,
                'phone' => $faker->phoneNumber,
                'email' => $faker->email,
                'website' => $faker->url,
                'latitude' => $faker->latitude,
                'longitude' => $faker->longitude,
                'business_phone' => $faker->phoneNumber,
                'business_email' => $faker->email,
                'announcement' => $faker->text(),
                'about_us' => $faker->text(),
                'legal_name' => $faker->name,
                'duns_number' => $faker->numberBetween(100, 200),
                'working_hours_id' => $working_hours_id,
                'admin_id' => 2
            ]);
            $clinic = \App\Clinic::find($clinic_id);
            $url_clinic = 'uploads/avatar-clinic-delivery.png';
            $url_cover = 'uploads/cover.png';
            $logo = new \App\Image(['url' => $url_clinic, 'avatar' => 1]);
            $cover = new \App\Image(['url' => $url_cover, 'cover' => 1]);
            $clinic->logo()->save($logo);
            $clinic->cover()->save($cover);
            foreach (range(1, 10) as $index) {
                $url = 'uploads/gallery.png';
                $gallery = new \App\Image(['url' => $url]);
                $clinic->gallery()->save($gallery);
            }



            DB::table('delivery_services')->insert([
                'name' => $faker->name,
                'address' => $faker->address,
                'phone' => $faker->phoneNumber,
                'email' => $faker->email,
                'website' => $faker->url,
                'latitude' => $faker->latitude,
                'longitude' => $faker->longitude,
                'hits' => $faker->numberBetween(50, 1000),
            ]);
        }


    }
}
