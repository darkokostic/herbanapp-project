<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Carbon\Carbon;

class ReviewTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1, 10) as $index) {
            $review_id = DB::table('reviews')->insertGetId([
                'user_id' => 1,
                'clinic_id' => $index,
                'rating' => $faker->numberBetween(1, 5),
                'message' => $faker->text(),
            ]);
            $review_id_sale = DB::table('reviews')->insertGetId([
                'user_id' => 3,
                'clinic_id' => $index,
                'rating' => $faker->numberBetween(1, 5),
                'message' => $faker->text(),
            ]);
            DB::table('comments')->insert([
                'review_id' => $review_id_sale,
                'user_id' => 3,
                'message' => $faker->text(),
            ]);

            DB::table('comments')->insert([
                'review_id' => $review_id,
                'user_id' => 1,
                'message' => $faker->text(),
            ]);
        }

    }
}
