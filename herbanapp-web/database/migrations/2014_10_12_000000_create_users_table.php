<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('lastname');
            $table->enum('account_type', ['free', 'premium']);
            $table->enum('role', ['user', 'clinic_admin', 'clinic_manager', 'app_admin', 'app_junior_manager', 'app_senior_manager']);
            $table->string('phone')->nullable();
            $table->string('username')->unique();
            $table->string('email')->unique();
            $table->string('state');
            $table->string('city');
            $table->date('birth_date');
            $table->string('about_me')->nullable();
            $table->string('social_id')->nullable();
            $table->string('social_name')->nullable();
            $table->string('password')->nullable();
            $table->string('player_id')->nullable();
            $table->rememberToken();
            $table->integer('created_at')->nullable();
            $table->integer('updated_at')->nullable();
            $table->integer('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
