<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClinicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clinics', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('legal_name');
            $table->string('phone');
            $table->string('email');
            $table->string('address');
            $table->string('latitude');
            $table->string('longitude');
            $table->string('business_phone');
            $table->string('business_email');
            $table->string('announcement');
            $table->string('about_us');
            $table->string('duns_number')->nullable();
            $table->string('website')->nullable();
            $table->integer('working_hours_id')->unsigned();
            $table->integer('admin_id')->unsigned()->nullable();
            $table->integer('created_at')->nullable();
            $table->integer('updated_at')->nullable();
            $table->integer('deleted_at')->nullable();
        });

        Schema::table('clinics', function (Blueprint $table) {
            $table->index('address');
            $table->index('latitude');
            $table->index('longitude');

            $table->foreign('working_hours_id')->references('id')->on('working_hours');
            $table->foreign('admin_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clinics');
    }
}
