import {Injectable} from "@angular/core";
import {Http, ConnectionBackend, RequestOptions, RequestOptionsArgs, Response, Headers} from "@angular/http";
import {Observable} from "rxjs";
import {RestResponse} from "../shared/restResponse";
import {Constants} from "../shared/constants";
import { LocalStorageService } from 'angular-2-local-storage';

@Injectable()
export class HttpInterceptor extends Http {
    constructor(backend: ConnectionBackend, defaultOptions: RequestOptions, public localStorage: LocalStorageService) {
        super(backend, defaultOptions);
    }

    get(url: string, options?: RequestOptionsArgs): Observable<Response> {
        return super.get(url, this.getAuthTokenHeader(options))
            .do(this.successfulRequest,
            (err: Response) => {
                return this.handleError(err);
            });
    }

    post(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
        return super.post(url, body, this.getAuthTokenHeader(options))
            .do(this.successfulRequest,
            (err: Response) => {
                return this.handleError(err);
            });
    }

    private handleError (err: Response | any) {
        switch (err.status) {
            case 401:
                err._body = new RestResponse();
                err.message = Constants.SESSION_EXPIRED_MESSAGE;
                break;
            case 403:
                err._body = new RestResponse();
                err.message = Constants.FORBIDDEN_MESSAGE;
                break;
            case 404:
                err._body = new RestResponse();
                err.message = Constants.NOT_FOUND_MESSAGE;
                break;
            case 413:
                err._body = new RestResponse();
                err.message = Constants.IMAGE_TOO_LARGE_MESSAGE;
                break;
            case 500:
                err._body = new RestResponse();
                err.message = Constants.SERVER_ERROR_MESSAGE;
                break;
            case 0:
                err._body = new RestResponse();
                err.message = Constants.NO_INTERNET_CONNECTION_MESSAGE;
                break;
        }
        return Observable.throw(err);
    }

    private getAuthTokenHeader(options: RequestOptionsArgs): RequestOptions {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        if(this.localStorage.get(Constants.TOKEN_KEY)) {
            headers.append('Authorization', 'Bearer '+ this.localStorage.get(Constants.TOKEN_KEY));
        }

        if(options && options.search) {
            return new RequestOptions({ headers: headers, search: options.search });
        } else {
            return new RequestOptions({ headers: headers });
        }
    }

    private successfulRequest(res: Response) {
        return Observable;
    }
}