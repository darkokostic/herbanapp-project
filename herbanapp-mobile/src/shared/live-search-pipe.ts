import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class LiveSearchPipe {
    constructor(public http: Http) {
    }

    filterUsers(users: any, searchTerm: string){
        return users.filter((user: any) => {
            return user.name.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
        });
    }
}