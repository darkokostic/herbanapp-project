import { Injectable } from '@angular/core';

@Injectable()
export class Constants {
    // public static ENDPOINT: string = "http://192.168.0.109:8000/";
    public static SOCKET: string = "http://67.205.186.174:3000";
    public static ENDPOINT: string = "http://67.205.186.174/";
    public static LOGIN_API: string = Constants.ENDPOINT + "api/login";
    public static REGISTER_API: string = Constants.ENDPOINT + "api/register";
    public static UPDATE_USER_API: string = Constants.ENDPOINT + "api/user/update";
    public static LOGOUT_API: string = Constants.ENDPOINT + "api/user/logout";
    public static CHANGE_PASSWORD_API: string = Constants.ENDPOINT + "api/change-password";
    public static FORGOT_PASSWORD_API: string = Constants.ENDPOINT + "api/reset-password";
    public static CONTACT_API: string = Constants.ENDPOINT + "api/contact-us";
    public static NEARBY_CLINICS_API: string = Constants.ENDPOINT + "api/nearby-clinics";
    public static NEWS_API: string = Constants.ENDPOINT + "api/notifications";
    public static PROFILE_API: string = Constants.ENDPOINT + "api/self";
    public static SUGGESTED_USERS_API: string = Constants.ENDPOINT + "api/suggested-users";
    public static SEARCH_API: string = Constants.ENDPOINT + "api/search";
    public static LOGIN_SOCIAL_API: string = Constants.ENDPOINT + "api/login-social";
    public static ADD_FRIEND_API: string = Constants.ENDPOINT + "api/add-friend";
    public static ACCEPT_FRIEND_REQUEST_API: string = Constants.ENDPOINT + "api/accept-friend-request";
    public static REMOVE_FRIEND_API: string = Constants.ENDPOINT + "api/remove-friend";
    public static CANCEL_FRIEND_REQUEST_API: string = Constants.ENDPOINT + "api/cancel-friend-request";
    public static FRIEND_REQUESTS_API: string = Constants.ENDPOINT + "api/friend-request";
    public static FRIENDS_LIST_API: string = Constants.ENDPOINT + "api/friend-list";
    public static SEARCH_FRIENDS_API: string = Constants.ENDPOINT + "api/search-friends";
    public static ADD_REVIEW_API: string = Constants.ENDPOINT + "api/create-review";
    public static GET_SINGLE_CLINIC: string = Constants.ENDPOINT + "api/clinic/";
    public static ADD_REVIEW_COMMENT: string = Constants.ENDPOINT + "api/create-review/comment";
    public static GET_FAVORITE_CLINICS: string = Constants.ENDPOINT + "api/clinic/favorites/";
    public static FAVORITE_CLINIC: string = Constants.ENDPOINT + "api/clinic/favorites/";
    public static GET_CHAT_ROOM: string = Constants.ENDPOINT + "api/get/chat-room";
    public static GET_MESSAGES: string = Constants.ENDPOINT + "api/chat/messages";
    public static SEND_MESSAGE: string = Constants.ENDPOINT + "api/send/message";
    public static CREATE_CHAT_GROUP: string = Constants.ENDPOINT + "api/create/group-chat";
    public static GROUPS_CHAT_LIST: string = Constants.ENDPOINT + "api/get/group-chats";
    public static GET_USER_INBOX: string = Constants.ENDPOINT + "api/get/user-inbox";

    // messages
    public static LOADING_MESSAGE: string = "Please wait...";
    public static NO_USERNAME_MESSAGE: string = "Enter your username or e-mail";
    public static NO_PASSWORD: string = "Enter your password";
    public static SENT_REQUEST_MESSAGE: string = "Friendship request sent";
    public static ACCEPTED_REQUEST_MESSAGE: string = "Congratulations, you have a new friend!";
    public static CANCEL_REQUEST_MESSAGE: string = "Request cancel";
    public static REMOVE_FRIEND_MESSAGE: string = "Removed from firend!";
    public static PASSWORD_NOT_MATCH_MESSAGE: string = "Password must match!";
    public static PASSWORD_CHANGED_MESSAGE: string = "Your password has changed!";
    public static FORGOT_PASSWORD_MESSAGE: string = "Your request for reset password is sent!";
    public static CONTACT_MESSAGE: string = "Thank you for contacting us!";
    public static REVIEW_MESSAGE: string = "Thank you for your rate!";
    public static ADDED_TO_FAVORITE: string = "Added to favorite!";
    public static REMOVED_FROM_FAVORITE: string = "Removed from favorite!";
    public static ADD_REVIEW_COMMENT_MESSAGE: string = "Thank you for comment!";
    public static SIGNUP_FORM_INVALID_MESSAGE: string = "Please fill all fields correct.";
    public static SIGNUP_FORM_YEAR_MESSAGE: string = "You are not old enough";
    public static SESSION_EXPIRED_MESSAGE: string = "Your session has expired. Please Login again!";
    public static FORBIDDEN_MESSAGE: string = "Forbidden";
    public static NOT_FOUND_MESSAGE: string = "Not Found";
    public static NO_INTERNET_CONNECTION_MESSAGE: string = "NO INTERNET CONNECTION";
    public static IMAGE_TOO_LARGE_MESSAGE: string = "Failed to upload, image size too large";
    public static ADD_REVIEW_ERROR_MESSAGE: string = "Please fill all fields";
    public static PASSWORD_LENGTH_MESSAGE: string = "Password must have minimum 6 characters";
    public static PASSWORD_MATCH_MESSAGE: string = "Passwords don't match";
    public static SERVER_ERROR_MESSAGE: string = "Something went wrong with server connection, please try again later."
    public static TEXT_TOO_LONG_MESSAGE: string = "The message may not be greater than 255 characters";
    public static USERNAME_OR_PASSWORD_INCORRECT_MESSAGE: string = "Wrong username or password.";

    // intro localstorage key
    public static INTRO: string = "intro";

    // TABS INDEX
    public static DISCOVER_TAB_INDEX: number = 0;
    public static NEWS_TAB_INDEX: number = 1;
    public static CLINICS_TAB_INDEX: number = 2;
    public static CHATS_TAB_INDEX: number = 3;

    // Clinic Tabs Index
    public static MENU_TAB_INDEX: number = 0;
    public static ABOUT_TAB_INDEX: number = 1;
    public static REVIEWS_TAB_INDEX: number = 2;
    public static GALLERY_TAB_INDEX: number = 3;

    // MINIMUM YEARS REQUIRED
    public static REQUIRED_YEARS: number = 19;

    // storage keys
    public static TOKEN_KEY: string = "token";
    public static CONNECTED_SOCIAL_NETWORK = "socialNetwork";
    public static ONESIGNAL_ID_KEY: string = "onesignalID";

    // Herban User key
    public static USER_INFO: string = "userInfo";

    // Facebook App Key
    public static FB_APP_ID: number = 103820240150805;

    // default avatars
    public static USER_AVATAR: string = "assets/images/avatar-user.png";
    public static CLINIC_LOGO: string = "assets/images/avatar-clinic-delivery.png";
    public static CLINIC_COVER: string = "assets/images/clinic-cover.png";
    public static GROUP_AVATAR: string = "assets/images/avatar-group.png";
    public static FACEBOOK_INVITE_IMAGE: string = Constants.ENDPOINT + "assets/images/facebook-invite.png";
    public static FACEBOOK_INVITE_URL: string = "https://fb.me/196798750823458";

    // List of states
    public static AVAILABLE_STATES: any = ["USA", "Serbia", "Spain"];

    // List of cities
    public static AVAILABLE_CITIES: any = ["New York", "Belgrade", "Madrid"];

    public static SELECT_STATE_MESSAGE: string = "Select your state";
    public static SELECT_CITY_MESSAGE: string = "Select your city";

    constructor() {}
}
