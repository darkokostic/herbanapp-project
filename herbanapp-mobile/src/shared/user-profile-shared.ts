import { Injectable } from '@angular/core';

@Injectable()
export class UserProfileShared {
    public isFriendRequest: boolean;
    public isFriend: boolean;
    constructor() {
    }

    getIsFriendRequest(): boolean {
        return this.isFriendRequest;
    }

    setIsFriendRequest(value: boolean): void {
        this.isFriendRequest = value;
    }

    getIsFriend(): boolean {
        return this.isFriend;
    }

    setIsFriend(value: boolean): void {
        this.isFriend = value;
    }
}