import { Injectable } from '@angular/core';

@Injectable()
export class HomeTabsShared {
    public tabIndex: number;
    constructor() {
    }

    getSelectedTab(): number {
        return this.tabIndex;
    }

    setSelectedTab(value: number): void {
        this.tabIndex = value;
    }
}