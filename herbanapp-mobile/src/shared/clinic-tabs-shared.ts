import { Injectable } from '@angular/core';

@Injectable()
export class ClinicTabsShared {
    public tabIndex: any;
    constructor() {
    }

    getSelectedTab(): number {
        return this.tabIndex;
    }

    setSelectedTab(value: number): void {
        this.tabIndex = value;
    }
}