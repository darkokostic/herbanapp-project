import {Pipe} from '@angular/core';

@Pipe({
    name: 'limitTo'
})
export class LimitToPipe {
    transform(value: string, limit: number) : string {
        let dots = '...';
        if(value.length > limit) {
            return value.substring(0, limit) + dots;
        } else {
            return value;
        }
    }
}
