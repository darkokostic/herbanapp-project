import { Injectable } from '@angular/core';

@Injectable()
export class ClinicProfileShared {
    public isFavorite: boolean;
    public avargeRating: number;
    constructor() {
    }

    getIsFavorite(): boolean {
        return this.isFavorite;
    }

    setIsFavorite(value: boolean): void {
        this.isFavorite = value;
    }

    getAvargeRating(): number {
        return this.avargeRating;
    }

    setAvargeRating(value: number): void {
        this.avargeRating = value;
    }
}