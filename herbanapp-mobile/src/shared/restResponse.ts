export class RestResponse {
    message: string;
    statusCode: number;
    errorCode: string;
    constructor() {
        this.message = '';
        this.statusCode = null;
        this.errorCode = '';
    }
}