import {Component, ViewChild} from '@angular/core';
import {Platform, NavController, App} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {Constants} from "../shared/constants";
import {TabsPage} from "../components/tabs/tabs";
import {LocalStorageService} from 'angular-2-local-storage';
import {OneSignal} from '@ionic-native/onesignal';
import {FriendRequestsPage} from "../pages/friend-requests/friend-requests";
import {FriendsListPage} from "../pages/friends-list/friends-list";
import {SingleChatPage} from "../pages/single-chat/single-chat";
import {GroupChatPage} from "../pages/group-chat/group-chat";
import {IntroPage} from "../pages/intro/intro";
import {FirstPage} from "../pages/first/first";
import {HomeTabsShared} from "../shared/home-tabs-shared";

@Component({
    templateUrl: 'app.html'
})
export class MyApp {
    rootPage: any;
    @ViewChild('rootNav') navCtrl: NavController

    constructor(public app: App, platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, public localStorage: LocalStorageService, public oneSignal: OneSignal, public tabsShared: HomeTabsShared) {
        let self = this;
        platform.ready().then(() => {
            if(!this.localStorage.get(Constants.INTRO)) {
                self.rootPage = IntroPage;
            } else {
                if (self.localStorage.get(Constants.TOKEN_KEY)) {
                    self.rootPage = TabsPage;
                } else {
                    self.rootPage = FirstPage;
                }
            }
            statusBar.styleDefault();
            setTimeout(() => {
                splashScreen.hide();
            }, 100);

            this.oneSignal.startInit('2841536d-d7ac-493f-a23e-2b639b55ad6b', '479880899641');

            this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);

            this.oneSignal.handleNotificationReceived().subscribe((response) => {
                let notificationData = response.payload.additionalData;
                if (notificationData) {
                    if (notificationData.chat_room_id != undefined) {
                        this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.None);
                    } else {
                        this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);
                    }
                }
            });

            this.oneSignal.handleNotificationOpened().subscribe((response) => {
                let notificationData = response.notification.payload.additionalData;
                if (notificationData && this.localStorage.get(Constants.TOKEN_KEY)) {
                    if(notificationData.page) {
                        if (notificationData.page == "friend_request") {
                            if(!(this.navCtrl.getActive().name == "FriendRequestsPage")) {
                                this.app.getRootNav().push(FriendRequestsPage);
                            }
                        } else if (notificationData.page == "friend_list") {
                            if(!(this.navCtrl.getActive().name == "FriendsListPage")) {
                                this.app.getRootNav().push(FriendsListPage);
                            }
                        }
                    } else if(notificationData.chat_room) {
                        if(notificationData.chat_room.isGroup == 0) {
                            if(this.navCtrl.getActive().name == "SingleChatPage") {
                                this.tabsShared.setSelectedTab(Constants.CHATS_TAB_INDEX);
                                this.rootPage = TabsPage;
                            } else {
                                this.app.getRootNav().push(SingleChatPage, {chat_room_id: notificationData.chat_room.id, user: notificationData.user});
                            }
                        } else {
                            if(this.navCtrl.getActive().name == "GroupChatPage") {
                                this.tabsShared.setSelectedTab(Constants.CHATS_TAB_INDEX);
                                this.rootPage = TabsPage;
                            } else {
                                this.app.getRootNav().push(GroupChatPage, {group: notificationData.chat_room});
                            }
                        }
                    }
                }
            });

            this.oneSignal.getIds()
                .then((response: any) => {
                    this.localStorage.set(Constants.ONESIGNAL_ID_KEY, response.userId);
                });

            this.oneSignal.endInit();
        });
    }
}
