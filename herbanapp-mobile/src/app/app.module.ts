import {NgModule, ErrorHandler} from '@angular/core';
import {IonicApp, IonicModule, IonicErrorHandler} from 'ionic-angular';
import {MyApp} from './app.component';
import {ClinicsPage} from '../pages/clinics/clinics';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {NewsPage} from "../pages/news/news";
import {ProfilePage} from "../pages/profile/profile";
import {ChatListPage} from "../pages/chat-list/chat-list";
import {Ionic2RatingModule} from 'ionic2-rating';
import {SingleChatPage} from "../pages/single-chat/single-chat";
import {GroupChatPage} from "../pages/group-chat/group-chat";
import {LoginPage} from "../pages/login/login";
import {Constants} from "../shared/constants";
import {HttpInterceptor} from "../interceptors/httpInterceptor";
import {Http, XHRBackend, RequestOptions} from "@angular/http";
import {NoGPSPage} from "../pages/no-gps/no-gps";
import {TabsPage} from "../components/tabs/tabs";
import {LocalStorageModule, LocalStorageService} from 'angular-2-local-storage';
import {DiscoverPage} from "../pages/discover/discover";
import { CustomIconsModule } from 'ionic2-custom-icons';
import {SearchPage} from "../pages/search/search";
import {ClinicSinglePage} from "../pages/clinic-single/clinic-single";
import {SuggestFriendsPage} from "../pages/suggest-friends/suggest-friends";
import {ShoppingCartPage} from "../pages/shopping-cart/shopping-cart";
import {ClinicMenuPage} from "../pages/clinic-menu/clinic-menu";
import {ClinicAboutPage} from "../pages/clinic-about/clinic-about";
import {ClinicReviewsPage} from "../pages/clinic-reviews/clinic-reviews";
import {ClinicGalleryPage} from "../pages/clinic-gallery/clinic-gallery";
import {ClinicDealsPage} from "../pages/clinic-deals/clinic-deals";
import {FaqPage} from "../pages/faq/faq";
import {Facebook} from "@ionic-native/facebook";
import {TwitterConnect} from "@ionic-native/twitter-connect";
import { CloudSettings, CloudModule } from '@ionic/cloud-angular';
import {SignupPage} from "../pages/signup/signup";
import {TermsConditionPage} from "../pages/terms-condition/terms-condition";
import {ContactUsPage} from "../pages/contact-us/contact-us";
import {MyAccountPage} from "../pages/my-account/my-account";
import {PopoverPagePage} from "../pages/popover-page/popover-page";
import {FriendRequestsPage} from "../pages/friend-requests/friend-requests";
import {FriendsListPage} from "../pages/friends-list/friends-list";
import { OneSignal } from '@ionic-native/onesignal';
import {Transfer} from "@ionic-native/transfer";
import {Camera} from "@ionic-native/camera";
import {AddReviewPage} from "../pages/add-review/add-review";
import {ProductSinglePage} from "../pages/product-single/product-single";
import {FavoritesPage} from "../pages/favorites/favorites";
import {ChatBubblePage} from "../components/chat-bubble/chat-bubble";
import {CreateNewChatPage} from "../pages/create-new-chat/create-new-chat";
import {GroupChatsListPage} from "../pages/group-chats-list/group-chats-list";
import {CreateGroupChatPage} from "../pages/create-group-chat/create-group-chat";
import {LiveSearchPipe} from "../shared/live-search-pipe";
import {LimitToPipe} from "../shared/limit-to-pipe";
import {NewsSinglePage} from "../pages/news-single/news-single";
import {ClinicHeader} from "../components/clinic-header/clinic-header";
import {UserProfileTabs} from "../components/user-profile-tabs/user-profile-tabs";
import {UserAboutPage} from "../pages/user-about/user-about";
import {UserFavoritesPage} from "../pages/user-favorites/user-favorites";
import {UserProfileShared} from "../shared/user-profile-shared";
import {UserHeader} from "../components/user-header/user-header";
import {ClinicProfileShared} from "../shared/clinic-profile-shared";
import {HomeTabsShared} from "../shared/home-tabs-shared";
import {ClinicTabsShared} from "../shared/clinic-tabs-shared";
import {IntroPage} from "../pages/intro/intro";
import {FirstPage} from "../pages/first/first";
import {ElasticModule} from "angular2-elastic";
import {GalleryModal} from "../components/gallery-modal/gallery-modal";

const cloudSettings: CloudSettings = {
    'core': {
        'app_id': '134da43b'
    }
};

export function httpInterceptor(backend: XHRBackend, defaultOptions: RequestOptions, localStorage: LocalStorageService) {
    return new HttpInterceptor(backend, defaultOptions, localStorage);
}

@NgModule({
    declarations: [
        MyApp,
        TabsPage,
        ClinicsPage,
        NewsPage,
        ProfilePage,
        ChatListPage,
        SingleChatPage,
        GroupChatPage,
        DiscoverPage,
        FriendRequestsPage,
        FriendsListPage,
        GalleryModal,
        LoginPage,
        SignupPage,
        ClinicSinglePage,
        SearchPage,
        MyAccountPage,
        NoGPSPage,
        SuggestFriendsPage,
        ShoppingCartPage,
        ClinicMenuPage,
        ClinicAboutPage,
        ClinicReviewsPage,
        ClinicGalleryPage,
        ClinicDealsPage,
        FaqPage,
        TermsConditionPage,
        ContactUsPage,
        PopoverPagePage,
        AddReviewPage,
        ProductSinglePage,
        CreateNewChatPage,
        FavoritesPage,
        GroupChatsListPage,
        CreateGroupChatPage,
        ChatBubblePage,
        LimitToPipe,
        NewsSinglePage,
        ClinicHeader,
        UserProfileTabs,
        UserFavoritesPage,
        UserAboutPage,
        UserHeader,
        IntroPage,
        FirstPage
    ],
    imports: [
        IonicModule.forRoot(MyApp),
        CloudModule.forRoot(cloudSettings),
        Ionic2RatingModule,
        LocalStorageModule.withConfig({
            storageType: 'localStorage'
        }),
        CustomIconsModule,
        ElasticModule
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        TabsPage,
        ClinicsPage,
        NewsPage,
        ProfilePage,
        ChatListPage,
        SingleChatPage,
        GroupChatPage,
        GalleryModal,
        DiscoverPage,
        FriendRequestsPage,
        FriendsListPage,
        LoginPage,
        SignupPage,
        SearchPage,
        MyAccountPage,
        ClinicSinglePage,
        NoGPSPage,
        SuggestFriendsPage,
        ShoppingCartPage,
        ClinicMenuPage,
        ClinicAboutPage,
        ClinicReviewsPage,
        ClinicGalleryPage,
        ClinicDealsPage,
        FaqPage,
        TermsConditionPage,
        ContactUsPage,
        PopoverPagePage,
        AddReviewPage,
        ProductSinglePage,
        CreateNewChatPage,
        GroupChatsListPage,
        FavoritesPage,
        CreateGroupChatPage,
        ChatBubblePage,
        NewsSinglePage,
        ClinicHeader,
        UserProfileTabs,
        UserFavoritesPage,
        UserAboutPage,
        UserHeader,
        IntroPage,
        FirstPage
    ],
    providers: [
        StatusBar,
        SplashScreen,
        Constants,
        LocalStorageService,
        Facebook,
        TwitterConnect,
        OneSignal,
        Transfer,
        Camera,
        LiveSearchPipe,
        UserProfileShared,
        ClinicProfileShared,
        HomeTabsShared,
        ClinicTabsShared,
        {
            provide: Http,
            useFactory: httpInterceptor,
            deps: [XHRBackend, RequestOptions, LocalStorageService]
        },
        {provide: ErrorHandler, useClass: IonicErrorHandler},
    ]
})
export class AppModule {
}
