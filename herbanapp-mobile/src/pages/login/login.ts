import {Component, ViewChild, ElementRef} from '@angular/core';
import {NavController, Platform} from 'ionic-angular';
import { UserProvider } from "../../providers/user-provider";
import { Constants } from "../../shared/constants";
import { ToastController, LoadingController } from 'ionic-angular';
import { LocalStorageService } from 'angular-2-local-storage';
import { RestResponse } from "../../shared/restResponse";
import { TabsPage } from "../../components/tabs/tabs";
import { AlertController } from 'ionic-angular';
import {SignupPage} from "../signup/signup";
import {FormGroup, FormControl, Validators} from "@angular/forms";
import {FirstPage} from "../first/first";
import gsap from "gsap";

@Component({
    selector: 'page-login',
    templateUrl: 'login.html',
    providers: [UserProvider]
})
export class LoginPage {
    private form: FormGroup;
    @ViewChild('headerTop') headerTop: ElementRef;
    private firstLeftTop: number;
    private firstLeftLeft: number;
    private secondLeftTop: number;
    private secondLeftLeft: number;
    private centerTop: number;
    private centerLeft: number;
    private firstRightTop: number;
    private firstRightLeft: number;
    private secondRightTop: number;
    private secondRightLeft: number;
    private isViewLoaded: boolean;
    private firstLeftW: number;
    private secondLeftW: number;
    private firstRightW: number;
    private secondRightW: number;
    private centerW: number;
    private firstLeftWidth: number;
    private secondLeftWidth: number;
    private firstRightWidth: number;
    private secondRightWidth: number;
    private centerWidth: number;
    private tl: any;

    constructor(public navCtrl: NavController, public userProvider: UserProvider, public localStorage: LocalStorageService, public toastCtrl: ToastController, public loadingCtrl: LoadingController, public alertCtrl: AlertController, public platform: Platform) {
        this.form = new FormGroup({
            username: new FormControl('', Validators.required),
            password: new FormControl('', Validators.required),
        });
        this.isViewLoaded = false;
        this.platform.ready().then(() => {
            this.platform.registerBackButtonAction(() => {
                if(this.navCtrl.canGoBack()) {
                    this.navCtrl.pop();
                } else {
                    this.navCtrl.setRoot(FirstPage);
                }
            });
        });
    }

    ionViewDidEnter() {
        this.isViewLoaded = false;
        this.setPins();
    }

    goBack(): void {
        if(this.navCtrl.canGoBack()) {
            this.navCtrl.pop();
        } else {
            this.navCtrl.setRoot(FirstPage);
        }
    }

    goSignUp(): void {
        let previousPage = "login";
        this.navCtrl.setRoot(SignupPage, {previousPage});
    }

    login(username: string, pass: string):void {
        let form: any = this.form;
        if(this.form.valid == false) {
            if(form.value.username == "") {
                let toast = this.toastCtrl.create({
                    message: Constants.NO_USERNAME_MESSAGE,
                    duration: 3000
                });
                toast.present();
            } else if(form.value.password == "") {
                let toast = this.toastCtrl.create({
                    message: Constants.NO_PASSWORD,
                    duration: 3000
                });
                toast.present();
            }
        } else {
            this.loginRequest(form.value.username, form.value.password);
        }
    }

    loginRequest(username: string, pass: string):void {
        let loader = this.loadingCtrl.create({
            content: Constants.LOADING_MESSAGE,
        });
        loader.present();
        const params = {
            username: username,
            password: pass,
            player_id: this.localStorage.get(Constants.ONESIGNAL_ID_KEY)
        };
        this.userProvider.login(params)
            .subscribe(
                (data: any) => {
                    this.localStorage.set(Constants.TOKEN_KEY, data.entity.token);
                    this.localStorage.set(Constants.USER_INFO, data.entity);
                    loader.dismiss();
                    this.navCtrl.setRoot(TabsPage, {}, {
                        animate: true,
                        direction: 'forward'
                    });
                },
                (error: RestResponse) => {
                    loader.dismiss();
                    this.checkResponseStatus(error);
                }
            )
    }

    forgotPassword() {
        let alert = this.alertCtrl.create({
            title: 'Enter your email',
            inputs: [
                {
                    name: 'email',
                    placeholder: 'Email',
                    type: 'email'
                }
            ],
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: (data: any) => {
                    }
                },
                {
                    text: 'Send',
                    handler: (data: any) => {
                        if(data.email != undefined && data.email != "") {
                            let loader = this.loadingCtrl.create();
                            loader.present();
                            this.userProvider.forgotPassword(data.email)
                                .subscribe(
                                    (response: any) => {
                                        loader.dismiss();
                                        let toast = this.toastCtrl.create({
                                            message: Constants.FORGOT_PASSWORD_MESSAGE,
                                            duration: 3000
                                        });
                                        toast.present();
                                    },
                                    (error: any) => {
                                        loader.dismiss();
                                        this.checkResponseStatus(error);
                                        return false;
                                    }
                                )
                        } else {
                            return false;
                        }
                    }
                }
            ]
        });
        alert.present();
    }

    checkResponseStatus(error: any):void {
        let toast = this.toastCtrl.create({
            message: error.message,
            duration: 3000
        });
        switch (error.status) {
            case 401:
                this.userProvider.logout();
                toast.present();
                this.navCtrl.setRoot(FirstPage);
                break;
            case 403:
                let newToast = this.toastCtrl.create({
                    message: Constants.USERNAME_OR_PASSWORD_INCORRECT_MESSAGE,
                    duration: 3000
                });
                newToast.present();
                break;
            case 0:
                toast.present();
                break;
            case 500:
                toast.present();
                break;
        }
    }

    setPins(): void {
        this.platform.ready().then(() => {
            this.initializePinsDimensPositions();
        });
    }

    initializePinsDimensPositions(): void {
        // set width values
        this.firstLeftW = this.headerTop.nativeElement.clientWidth / 9.2;
        this.firstRightW = this.headerTop.nativeElement.clientWidth / 9.7;
        this.secondLeftW = this.headerTop.nativeElement.clientWidth / 6;
        this.secondRightW = this.headerTop.nativeElement.clientWidth / 6.8;
        this.centerW = this.headerTop.nativeElement.clientWidth / 2.7;

        // set pins width
        this.firstLeftWidth = this.firstLeftW;
        this.firstRightWidth = this.firstRightW;
        this.secondLeftWidth = this.secondLeftW;
        this.secondRightWidth = this.secondRightW;
        this.centerWidth = this.centerW;

        // set pins margin
        this.firstLeftTop = this.headerTop.nativeElement.clientWidth / 2.2;
        this.firstLeftLeft = this.headerTop.nativeElement.clientWidth / 4;
        this.secondLeftTop = this.headerTop.nativeElement.clientWidth / 1.8;
        this.secondLeftLeft = this.headerTop.nativeElement.clientWidth / 9.3;
        this.centerTop = this.headerTop.nativeElement.clientWidth / 1.58;
        this.centerLeft = this.headerTop.nativeElement.clientWidth / 2.4;
        this.firstRightTop = this.headerTop.nativeElement.clientWidth / 2.3;
        this.firstRightLeft = this.headerTop.nativeElement.clientWidth / 1.7;
        this.secondRightTop = this.headerTop.nativeElement.clientWidth / 1.8;
        this.secondRightLeft = this.headerTop.nativeElement.clientWidth / 1.25;
        this.isViewLoaded = true;
        this.rotatePins();
    }

    rotatePins(): void {
        gsap.TweenMax.staggerFrom('.pin', 0.5, {opacity: 0, y: 80}, 0.2);

        this.tl = new gsap.TimelineLite({delay: 2});

        // First Rotate
        // left 1 <=> right 2
        this.tl.to('.left-1-pin', 3, {left: this.headerTop.nativeElement.clientWidth / 1.75, top: this.headerTop.nativeElement.clientWidth / 10, width: this.secondRightW});
        this.tl.to('.right-2-pin', 3, {right: this.headerTop.nativeElement.clientWidth / 1.5, bottom: this.headerTop.nativeElement.clientWidth / 4.6, width: this.firstLeftW}, "-=3");

        // left 2 <=> right 1
        this.tl.to('.left-2-pin', 3, {left: this.headerTop.nativeElement.clientWidth / 2, bottom: this.headerTop.nativeElement.clientWidth / 4, width: this.firstRightW}, "-=1.5");
        this.tl.to('.right-1-pin', 3, {right: this.headerTop.nativeElement.clientWidth / 1.35, top: this.headerTop.nativeElement.clientWidth / 8, width: this.secondLeftW}, "-=3");

        // right 2 (left 1) <=> center
        this.tl.to('.right-2-pin', 3, {right: this.headerTop.nativeElement.clientWidth / 4.7, top: this.headerTop.nativeElement.clientWidth / 10, width: this.centerW}, "-=1.5");
        this.tl.to('.center-pin', 3, {right: this.headerTop.nativeElement.clientWidth / 1.5, bottom: this.headerTop.nativeElement.clientWidth / 4.85, width: this.firstLeftW}, "-=3");

        // SECOND ROTATE
        // left 2 (right 1) <=> left 1 (right 2)
        this.tl.to('.left-2-pin', 3, {x: this.headerTop.nativeElement.clientWidth / 4.9, y: this.headerTop.nativeElement.clientWidth / 6, width: this.secondRightW}, "+=1");
        this.tl.to('.left-1-pin', 3, {x: -(this.headerTop.nativeElement.clientWidth / 4.9), y: -(this.headerTop.nativeElement.clientWidth / 8.2), width: this.firstRightW}, "-=3");

        // right 1 (left 2) <=> right 2 (center)
        this.tl.to('.right-1-pin', 3, {x: this.headerTop.nativeElement.clientWidth / 1.9, y: this.headerTop.nativeElement.clientWidth / 9.3, width: this.centerW}, "-=1.5");
        this.tl.to('.right-2-pin', 3, {x: -(this.headerTop.nativeElement.clientWidth / 1.9), y: -(this.headerTop.nativeElement.clientWidth / 9.3), width: this.secondLeftW}, "-=3");

        // center (left 1) <=> left 2 (right 2)
        this.tl.to('.center-pin', 3, {x: this.headerTop.nativeElement.clientWidth / 1.7, y: this.headerTop.nativeElement.clientWidth / 8, width: this.secondRightW}, "-=1.5");
        this.tl.to('.left-2-pin', 3, {x: -(this.headerTop.nativeElement.clientWidth / 2.75), y: -(this.headerTop.nativeElement.clientWidth / 500), width: this.firstLeftW}, "-=3");

        // THIRD ROTATE
        // right 1 (center) <=> left 1 (right 1)
        this.tl.to('.right-1-pin', 3, {x: this.headerTop.nativeElement.clientWidth / 2.28, y: -(this.headerTop.nativeElement.clientWidth / 8), width: this.firstRightW}, "+=1");
        this.tl.to('.left-1-pin', 3, {x: -(this.headerTop.nativeElement.clientWidth / 3), y: this.headerTop.nativeElement.clientWidth / 9.5, width: this.centerW}, "-=3");

        // right 2 (left 2) <=> left 2 (left 1)
        this.tl.to('.left-2-pin', 3, {x: -(this.headerTop.nativeElement.clientWidth / 2), y: this.headerTop.nativeElement.clientWidth / 5, width: this.secondLeftW}, "-=1.5");
        this.tl.to('.right-2-pin', 3, {x: -(this.headerTop.nativeElement.clientWidth / 2.2),  y: -(this.headerTop.nativeElement.clientWidth / 5.3), width: this.firstLeftW}, "-=3");

        // center (right 2) <=> left 1 (center)
        this.tl.to('.center-pin', 3, {left: -(this.headerTop.nativeElement.clientWidth / 1.85), bottom: -(this.headerTop.nativeElement.clientWidth / 5.8), width: this.centerW}, "-=1.5");
        this.tl.to('.left-1-pin', 3, {x: this.headerTop.nativeElement.clientWidth / 80, y: -(this.headerTop.nativeElement.clientWidth / 30), width: this.secondRightW}, "-=3");

        // left 1 (right 2) <=> right 2 (left 1)
        this.tl.to('.left-1-pin', 3, {x: -(this.headerTop.nativeElement.clientWidth / 1.75), y: -(this.headerTop.nativeElement.clientWidth / 10), width: this.firstLeftW}, "-=1.5");
        this.tl.to('.right-2-pin', 3, {x: this.headerTop.nativeElement.clientWidth / 5.8,  top: this.headerTop.nativeElement.clientWidth / 6.3, width: this.secondRightW}, "-=3");

        this.tl.play();
    }
}
