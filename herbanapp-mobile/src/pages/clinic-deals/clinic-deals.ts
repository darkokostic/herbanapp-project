import {Component} from '@angular/core';
import {App, NavController, NavParams, ToastController, LoadingController} from 'ionic-angular';
import {UserProvider} from "../../providers/user-provider";
import {Constants} from "../../shared/constants";
import {FavoritesProvider} from "../favorites/favorites-provider";
import {LocalStorageService} from "angular-2-local-storage";
import {NearbyClinicsProvider} from "../clinics/nearby-clinics-provider";
import {ClinicProfileShared} from "../../shared/clinic-profile-shared";
import {FirstPage} from "../first/first";

@Component({
    selector: 'page-clinic-deals',
    templateUrl: 'clinic-deals.html',
    providers: [FavoritesProvider, UserProvider, NearbyClinicsProvider]
})
export class ClinicDealsPage {
    private clinic: any;
    private userInfo: any;
    private isFavorite: boolean;

    constructor(public app: App, public navCtrl: NavController, public navParams: NavParams, public clinicShared: ClinicProfileShared, public userProvider: UserProvider, public favoritesProvider: FavoritesProvider, public clinicsProvider: NearbyClinicsProvider, public localStorage: LocalStorageService, public toastCtrl: ToastController, public loadingCtrl: LoadingController) {
        this.clinic = this.navParams.data;
        this.userInfo = this.localStorage.get(Constants.USER_INFO);
    }

    ionViewDidEnter() {
        this.isFavorite = this.clinicShared.getIsFavorite();
        this.fetchClinicData();
    }

    fetchClinicData(): void {
        this.clinicsProvider.getClinic(this.clinic.id)
            .subscribe(
                (data: any) => {
                    this.clinic = data.entity;
                },
                (error: any) => {
                    this.checkResponseStatus(error);
                }
            )
    }

    favorite(): void {
        this.favoritesProvider.favorite(this.userInfo.id, this.clinic.id)
            .subscribe(
                (response: any) => {
                    if(this.clinicShared.getIsFavorite() == false) {
                        this.clinicShared.setIsFavorite(true);
                        let toast = this.toastCtrl.create({
                            message: Constants.ADDED_TO_FAVORITE,
                            duration: 3000
                        });
                        toast.present();
                    } else {
                        this.clinicShared.setIsFavorite(false);
                        let toast = this.toastCtrl.create({
                            message: Constants.REMOVED_FROM_FAVORITE,
                            duration: 3000
                        });
                        toast.present();
                    }
                },
                (error: any) => {
                    this.checkResponseStatus(error);
                }
            )
    }

    checkResponseStatus(error: any):void {
        let toast = this.toastCtrl.create({
            message: error.message,
            duration: 3000
        });
        switch (error.status) {
            case 401:
                this.userProvider.logout();
                toast.present();
                this.navCtrl.setRoot(FirstPage);
                break;
            case 0:
                toast.present();
                break;
            case 500:
                toast.present();
                break;
        }
    }

    swipeEvent(e: any):void {
        switch(e.direction) {
            case 4:
                this.navCtrl.parent.select(3);
                break;
        }
    }
}
