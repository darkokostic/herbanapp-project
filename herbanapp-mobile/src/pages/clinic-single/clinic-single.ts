import {Component, ViewChild} from '@angular/core';
import {
    App, NavController, NavParams, PopoverController, Tabs, Platform, ViewController,
    IonicApp
} from 'ionic-angular';
import {ShoppingCartPage} from "../shopping-cart/shopping-cart";
import {ClinicMenuPage} from "../clinic-menu/clinic-menu";
import {ClinicAboutPage} from "../clinic-about/clinic-about";
import {ClinicReviewsPage} from "../clinic-reviews/clinic-reviews";
import {ClinicGalleryPage} from "../clinic-gallery/clinic-gallery";
import {Constants} from "../../shared/constants";
import {AddReviewPage} from "../add-review/add-review";
import {PopoverPagePage} from "../popover-page/popover-page";
import {ClinicProfileShared} from "../../shared/clinic-profile-shared";
import {ClinicTabsShared} from "../../shared/clinic-tabs-shared";
import {TabsPage} from "../../components/tabs/tabs";

@Component({
    selector: 'page-clinic-single',
    templateUrl: 'clinic-single.html',
})
export class ClinicSinglePage {
    private tabMenu: any = ClinicMenuPage;
    private tabAbout: any = ClinicAboutPage;
    private tabReviews: any = ClinicReviewsPage;
    private tabGallery: any = ClinicGalleryPage;
    // private tabDeals: any = ClinicDealsPage;

    public clinic: any;
    private endpoint: string;
    private reviews: any;
    @ViewChild('myTabs') tabRef: Tabs;

    constructor(public app: App, public platform: Platform, public ionicApp: IonicApp, public navCtrl: NavController, public navParams: NavParams, public popoverCtrl: PopoverController, public clinicShared: ClinicProfileShared, public tabsShared: ClinicTabsShared, public viewCtrl: ViewController) {
        this.clinic = this.navParams.get('clinicObject');
        this.endpoint = Constants.ENDPOINT;
        this.reviews = this.clinic.reviews;
        if(this.clinic.isFavorite == 1) {
            this.clinicShared.setIsFavorite(true);
        } else {
            this.clinicShared.setIsFavorite(false);
        }

        this.platform.ready().then(() => {
            this.platform.registerBackButtonAction(() => {
                let activeModal = this.ionicApp._modalPortal.getActive();
                if (activeModal) {
                    activeModal.dismiss();
                } else {
                    if(this.navCtrl.canGoBack()) {
                        this.navCtrl.pop();
                    } else {
                        this.app.getRootNav().setRoot(TabsPage);
                    }
                }
            });
        });
    }

    ionViewDidLoad() {
        this.tabsShared.setSelectedTab(Constants.MENU_TAB_INDEX);
    }

    ionViewWillEnter() {
        if(this.tabsShared.getSelectedTab() != undefined) {
            switch (this.tabsShared.getSelectedTab()) {
                case Constants.MENU_TAB_INDEX:
                    this.app.getActiveNav().setRoot(this.tabMenu, this.clinic);
                    break;
                case Constants.ABOUT_TAB_INDEX:
                    this.app.getActiveNav().setRoot(this.tabAbout, this.clinic);
                    break;
                case Constants.REVIEWS_TAB_INDEX:
                    this.app.getActiveNav().setRoot(this.tabReviews, this.clinic);
                    break;
                case Constants.GALLERY_TAB_INDEX:
                    this.app.getActiveNav().setRoot(this.tabGallery, this.clinic);
                    break;
            }
        }
    }

    goBack(): void {
        if(this.navCtrl.canGoBack()) {
            this.navCtrl.pop();
        } else {
            this.app.getRootNav().setRoot(TabsPage);
        }
    }

    goReview():void {
        this.app.getRootNav().push(AddReviewPage, {clinic: this.clinic});
    }

    presentPopover(myEvent: any) {
        let popover = this.popoverCtrl.create(PopoverPagePage, this.clinic.working_hours);
        popover.present({
            ev: myEvent
        });
    }

    cart():void {
        this.navCtrl.push(ShoppingCartPage);
    }

}
