import {Component, ViewChild} from '@angular/core';
import {NavController, NavParams, Content} from 'ionic-angular';
import {Constants} from "../../shared/constants";

@Component({
    selector: 'page-product-single',
    templateUrl: 'product-single.html'
})
export class ProductSinglePage {
    private newsHeader: string;
    private product: any;
    private endpoint: string;
    private productLogo: string;

    @ViewChild(Content) content: Content;
    constructor(public navCtrl: NavController, public navParams: NavParams) {
        this.newsHeader = "header-transparent";
        this.product = this.navParams.data.product;
        this.endpoint = Constants.ENDPOINT;
        this.productLogo = Constants.CLINIC_LOGO;
    }

    onPageScroll() {
        if(this.content.scrollTop > 0) {
            this.newsHeader = "header-colored";
        } else {
            this.newsHeader = "header-transparent";
        }
    }

    ionViewDidLoad() {
        this.content.ionScroll.subscribe((event: any) => this.onPageScroll());
    }
}
