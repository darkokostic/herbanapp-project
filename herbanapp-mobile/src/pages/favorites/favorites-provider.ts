import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {Observable} from "rxjs";
import {Constants} from "../../shared/constants";

@Injectable()
export class FavoritesProvider {

    constructor(public http: Http) {}

    getFavorites(userId: any): Observable<any> {
        return this.http.get(Constants.GET_FAVORITE_CLINICS + userId, {})
            .map((res: Response) => res.json())
    }

    favorite(userId: any, clinicId: any): Observable<any> {
        let params: any = {
            user_id: userId,
            clinic_id: clinicId,
        }
        return this.http.post(Constants.FAVORITE_CLINIC, JSON.stringify(params), {})
            .map((res: Response) => res.json())
    }
}