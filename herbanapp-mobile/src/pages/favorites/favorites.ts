import {Component} from '@angular/core';
import {NavController, NavParams, ToastController, LoadingController} from 'ionic-angular';
import {FavoritesProvider} from "./favorites-provider";
import {UserProvider} from "../../providers/user-provider";
import {LocalStorageService} from "angular-2-local-storage";
import {Constants} from "../../shared/constants";
import {ClinicSinglePage} from "../clinic-single/clinic-single";
import {FirstPage} from "../first/first";

@Component({
    selector: 'page-favorites',
    templateUrl: 'favorites.html',
    providers: [FavoritesProvider, UserProvider]
})
export class FavoritesPage {
    private clinics: any;
    private endpoint: string;
    private userInfo: any;
    private clinicLogo: string;
    private hasData: boolean;

    constructor(public navCtrl: NavController, public navParams: NavParams, public localStorage: LocalStorageService, public favoritesProvider: FavoritesProvider, public userProvider: UserProvider, public loadingCtrl: LoadingController, public toastCtrl: ToastController) {
        this.hasData = true;
    }

    ionViewDidEnter() {
        this.endpoint = Constants.ENDPOINT;
        this.clinicLogo = Constants.CLINIC_LOGO;
        let loader = this.loadingCtrl.create();
        loader.present();
        this.userInfo = this.localStorage.get(Constants.USER_INFO);
        this.favoritesProvider.getFavorites(this.userInfo.id)
            .subscribe(
                (response: any) => {
                    loader.dismiss();
                    this.clinics = response.entity;
                    if(response.entity.length == 0 || response.entity == undefined) {
                        this.hasData = false;
                    } else {
                        this.hasData = true;
                    }
                },
                (error: any) => {
                    loader.dismiss();
                    this.checkResponseStatus(error);
                }
            )
    }

    goClinic(clinicObject: any):void {
        this.navCtrl.push(ClinicSinglePage, {
            clinicObject: clinicObject
        });
    }

    checkResponseStatus(error: any):void {
        let toast = this.toastCtrl.create({
            message: error.message,
            duration: 3000
        });
        switch (error.status) {
            case 401:
                this.userProvider.logout();
                toast.present();
                this.navCtrl.setRoot(FirstPage);
                break;
            case 0:
                toast.present();
                break;
            case 500:
                toast.present();
                break;
        }
    }
}
