import {Component, ViewChild} from '@angular/core';
import {NavController, Slides} from 'ionic-angular';
import {LocalStorageService} from "angular-2-local-storage";
import {Constants} from "../../shared/constants";
import {FirstPage} from "../first/first";
import gsap from "gsap";

@Component({
    selector: 'page-intro',
    templateUrl: 'intro.html'
})
export class IntroPage {
    @ViewChild(Slides) slides: Slides;

    constructor(public navCtrl: NavController, public localStorage: LocalStorageService) {}

    ionViewDidEnter() {
        gsap.TweenMax.from('.skip-btn', 1, {opacity: 0, y: -80});
        gsap.TweenMax.from('.ilustration', 1, {opacity: 0, x: 150});
        gsap.TweenMax.from('.intro-content', 1, {scale: 0.5, opacity: 0});
        gsap.TweenMax.staggerFrom('.swiper-pagination-bullet', 1, {opacity: 0, y: 80}, 0.2);
    }

    goToHome(): void {
        this.navCtrl.setRoot(FirstPage);
        this.localStorage.set(Constants.INTRO, true);
    }

    next(): void {
        this.slides.slideNext(500, true);
    }

    skip(): void {
        this.slides.slideTo(3, 500, true);
    }
}
