import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {Observable} from "rxjs";
import {Constants} from "../../shared/constants";

@Injectable()
export class AddReviewProvider {

    constructor(public http: Http) {}

    addReview(message: string, rating: number, clinicId: number, userId: number): Observable<any> {
        let params: any = {
            message: message,
            rating: rating,
            clinic_id: clinicId,
            user_id: userId
        }
        return this.http.post(Constants.ADD_REVIEW_API, JSON.stringify(params), {})
            .map((res: Response) => res.json())
    }

    addComment(message: string, reviewId: number, userId: number): Observable<any> {
        let params: any = {
            message: message,
            review_id: reviewId,
            user_id: userId
        }
        return this.http.post(Constants.ADD_REVIEW_COMMENT, JSON.stringify(params), {})
            .map((res: Response) => res.json())
    }
}
