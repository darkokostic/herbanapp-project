import {Component} from '@angular/core';
import {NavController, NavParams, ToastController, LoadingController} from 'ionic-angular';
import {LocalStorageService} from "angular-2-local-storage";
import {Constants} from "../../shared/constants";
import {AddReviewProvider} from "./add-review-provider";
import {UserProvider} from "../../providers/user-provider";
import {FirstPage} from "../first/first";

@Component({
    selector: 'page-add-review',
    templateUrl: 'add-review.html',
    providers: [AddReviewProvider, UserProvider]
})
export class AddReviewPage {
    private clinicId: number;
    private clinic: any;
    private user: any;
    private userId: number;
    constructor(public navCtrl: NavController, public navParams: NavParams, public localStorage: LocalStorageService, public toastCtrl: ToastController, public addReviewProvider: AddReviewProvider, public loadingCtrl: LoadingController, public userProvider: UserProvider) {
        this.clinic = this.navParams.get('clinic');
        this.clinicId = this.clinic.id;
        this.user = localStorage.get(Constants.USER_INFO);
        this.userId = this.user.id;
    }

    rate(message: string, rate: number): void {
        if(message != undefined && message != "" && rate != undefined) {
            let loader = this.loadingCtrl.create();
            loader.present();
            this.addReviewProvider.addReview(message, rate, this.clinicId, this.userId)
                .subscribe(
                    (response: any) => {
                        loader.dismiss();
                        let toast = this.toastCtrl.create({
                            message: Constants.REVIEW_MESSAGE,
                            duration: 3000
                        });
                        toast.present();
                        this.navCtrl.pop();
                    },
                    (error: any) => {
                        loader.dismiss();
                        this.checkResponseStatus(error);
                    }
                )
        } else {
            let toast = this.toastCtrl.create({
                message: Constants.ADD_REVIEW_ERROR_MESSAGE,
                duration: 3000
            });
            toast.present();
        }
    }

    checkResponseStatus(error: any):void {
        let toast = this.toastCtrl.create({
            message: error.message,
            duration: 3000
        });
        switch (error.status) {
            case 401:
                this.userProvider.logout();
                toast.present();
                this.navCtrl.setRoot(FirstPage);
                break;
            case 0:
                toast.present();
                break;
            case 500:
                toast.present();
                break;
        }
    }
}
