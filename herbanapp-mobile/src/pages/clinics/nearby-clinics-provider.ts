import { Injectable } from '@angular/core';
import {Http, URLSearchParams, Response} from '@angular/http';
import 'rxjs/add/operator/map';
import {Constants} from "../../shared/constants";
import {Observable} from "rxjs";

@Injectable()
export class NearbyClinicsProvider {

    constructor(public http: Http) {}

    getClinics(location:any):Observable<any> {
        let params = new URLSearchParams();
        params.set('lat', location.lat);
        params.set('lng', location.lng);
        return this.http.get(Constants.NEARBY_CLINICS_API, {search: params})
            .map((res: Response) => res.json())
    }

    getClinic(clinicId: any): Observable<any> {
        return this.http.get(Constants.GET_SINGLE_CLINIC + clinicId, {})
            .map((res: Response) => res.json())
    }

    loadMore(url:string, location:any):Observable<any> {
        let params = new URLSearchParams();
        params.set('lat', location.lat);
        params.set('lng', location.lng);
        return this.http.get(url, {search: params})
            .map((res: Response) => res.json())
    }

}
