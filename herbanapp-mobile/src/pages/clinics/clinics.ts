import {Component} from '@angular/core';
import {NavController, App, LoadingController, ToastController} from 'ionic-angular';
import { Diagnostic } from "@ionic-native/diagnostic";
import { Geolocation } from '@ionic-native/geolocation';
import { GpsProvider } from "../../providers/gps-provider";
import { NoGPSPage } from "../no-gps/no-gps";
import { Platform } from 'ionic-angular';
import { NearbyClinicsProvider } from "./nearby-clinics-provider";
import {ClinicSinglePage} from "../clinic-single/clinic-single";
import {UserProvider} from "../../providers/user-provider";
import {Constants} from "../../shared/constants";
import {LocalStorageService} from "angular-2-local-storage";
import {HomeTabsShared} from "../../shared/home-tabs-shared";
import {FirstPage} from "../first/first";

@Component({
    selector: 'page-clinics',
    templateUrl: 'clinics.html',
    providers: [Diagnostic, Geolocation, GpsProvider, NearbyClinicsProvider, UserProvider]
})
export class ClinicsPage {
    private clinics: any;
    private loadMoreUrl: string;
    private location: any;
    private endpoint: string;
    private clinicLogo: string;
    private userInfo: any;
    private hasData: boolean;

    constructor(public app: App, public platform: Platform, public navCtrl: NavController, public tabsShared: HomeTabsShared, public gpsProvider: GpsProvider, public localStorage: LocalStorageService, public clinicsProvider: NearbyClinicsProvider, public userProvider: UserProvider, public loadingCtrl: LoadingController, public toastCtrl: ToastController) {
        this.endpoint = Constants.ENDPOINT;
        this.clinicLogo = Constants.CLINIC_LOGO;
        this.userInfo = this.localStorage.get(Constants.USER_INFO);
        this.hasData = true;
    }

    ionViewDidEnter() {
        this.tabsShared.setSelectedTab(Constants.CLINICS_TAB_INDEX);
        this.fetchClinics();
    }

    fetchClinics(): void {
        this.gpsProvider.getLocation()
            .then((response) => {
                if(response != undefined) {
                    if(response) {
                        this.location = response;
                        this.getClinics(response);
                    } else {
                        this.noGPS();
                    }
                }
            })
            .catch(() => {
                this.noGPS();
            });
    }

    getClinics(location: any):void {
        let loader = this.loadingCtrl.create();
        loader.present();
        this.clinicsProvider.getClinics(location)
            .subscribe(
                (data: any) => {
                    loader.dismiss();
                    this.clinics = data.entity.data;
                    this.loadMoreUrl = data.entity.next_page_url;
                    if(data.entity.data.length == 0 || data.entity.data == undefined) {
                        this.hasData = false;
                    } else {
                        this.hasData = true;
                    }
                },
                (error: any) => {
                    loader.dismiss();
                    this.checkResponseStatus(error);
                }
            )
    }

    noGPS():void {
        this.navCtrl.setRoot(NoGPSPage, {}, {
            animate: true,
            direction: 'forward'
        });
    }

    sync(event: any) {
        this.gpsProvider.getLocation()
            .then((response) => {
                if(response != undefined) {
                    if(response) {
                        this.location = response;
                        this.getClinics(response);
                        event.complete();
                    } else {
                        this.noGPS();
                        event.complete();
                    }
                }
            })
            .catch(() => {
                this.noGPS();
                event.complete();
            });
    }

    loadMore(infiniteScroll: any):void {
        if(this.loadMoreUrl != null) {
            this.clinicsProvider.loadMore(this.loadMoreUrl, this.location)
                .subscribe(
                    (data: any) => {
                        for(let i = 0; i < data.entity.data.length; i++) {
                            this.clinics.push(data.entity.data[i]);
                        }
                        this.loadMoreUrl = data.entity.next_page_url;
                        infiniteScroll.complete();
                    },
                    (error: any) => {
                        infiniteScroll.complete();
                        this.checkResponseStatus(error);
                    }
                )
        } else {
            infiniteScroll.complete();
        }
    }

    swipeEvent(e: any):void {
        switch(e.direction) {
            case 2:
                this.navCtrl.parent.select(3);
                break;
            case 4:
                this.navCtrl.parent.select(1);
                break;
        }
    }

    goClinic(clinicObject: any):void {
        this.app.getRootNav().setRoot(ClinicSinglePage, {clinicObject: clinicObject});
    }

    checkResponseStatus(error: any):void {
        let toast = this.toastCtrl.create({
            message: error.message,
            duration: 3000
        });
        switch (error.status) {
            case 401:
                this.userProvider.logout();
                toast.present();
                this.app.getRootNav().setRoot(FirstPage);
                break;
            case 0:
                toast.present();
                break;
            case 500:
                toast.present();
                break;
        }
    }
}
