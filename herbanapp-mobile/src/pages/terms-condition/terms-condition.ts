import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
    selector: 'page-terms-condition',
    templateUrl: 'terms-condition.html'
})
export class TermsConditionPage {

    constructor(public navCtrl: NavController, public navParams: NavParams) {}
}
