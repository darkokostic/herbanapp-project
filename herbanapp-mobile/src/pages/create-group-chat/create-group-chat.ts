import {Component} from '@angular/core';
import {NavController, NavParams, LoadingController, ToastController, ViewController} from 'ionic-angular';
import {FriendsProvider} from "../../providers/friends-provider";
import {UserProvider} from "../../providers/user-provider";
import {Constants} from "../../shared/constants";
import {FormGroup, FormControl} from "@angular/forms";
import {GroupChatProvider} from "../../providers/group-chat-provider";
import {LiveSearchPipe} from "../../shared/live-search-pipe";
import {FirstPage} from "../first/first";

@Component({
    selector: 'page-create-group-chat',
    templateUrl: 'create-group-chat.html',
    providers: [FriendsProvider, UserProvider, GroupChatProvider, LiveSearchPipe]
})
export class CreateGroupChatPage {
    private users: any;
    private searchQuery: string;
    private endpoint: string;
    private userAvatar: string;
    private form: FormGroup;
    private allUsers: any;
    private hasData: boolean;

    constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, public toastCtrl: ToastController, public userProvider: UserProvider, public friendsProvider: FriendsProvider, public viewCtrl: ViewController, public groupChatProvider: GroupChatProvider, public liveSearch: LiveSearchPipe) {
        this.searchQuery = "";
        this.endpoint = Constants.ENDPOINT;
        this.userAvatar = Constants.USER_AVATAR;
        this.form = new FormGroup({
            searchQuery: new FormControl(''),
        });
        this.form.valueChanges.debounceTime(700).subscribe(() => {
            this.search();
        });
        this.hasData = true;
    }

    ionViewDidEnter() {
        let loader = this.loadingCtrl.create();
        loader.present();
        this.friendsProvider.getFriendsList()
            .subscribe(
                (response: any) => {
                    loader.dismiss();
                    response.entity.forEach((user: any) => {
                        user.checked = false;
                    })
                    this.users = response.entity;
                    this.allUsers = response.entity;
                    if(response.entity.length == 0 || response.entity == undefined) {
                        this.hasData = false;
                    } else {
                        this.hasData = true;
                    }
                },
                (error: any) => {
                    loader.dismiss();
                    this.checkResponseStatus(error);
                }
            )
    }

    search():void {
        this.users = this.liveSearch.filterUsers(this.allUsers, this.form.value.searchQuery);
    }

    createChatGroup(groupName: string): void {
        let checkedUsers: any = [];
        for(let i = 0; i < this.users.length; i++) {
            if(this.users[i].checked == true) {
                checkedUsers.push(this.users[i]);
            }
        }
        if(groupName != undefined && groupName != "" && checkedUsers.length > 0) {
            let loader = this.loadingCtrl.create();
            loader.present();
            this.groupChatProvider.createChatGroup(groupName, checkedUsers)
                .subscribe(
                    (response: any) => {
                        loader.dismiss();
                        this.viewCtrl.dismiss(response.entity);
                    },
                    (error: any) => {
                        loader.dismiss();
                        this.checkResponseStatus(error);
                    }
                )
        }
    }

    modalDismiss(): void {
        this.viewCtrl.dismiss();
    }

    checkResponseStatus(error: any):void {
        let toast = this.toastCtrl.create({
            message: error.message,
            duration: 3000
        });
        switch (error.status) {
            case 401:
                this.userProvider.logout();
                toast.present();
                this.navCtrl.setRoot(FirstPage);
                break;
            case 0:
                toast.present();
                break;
            case 500:
                toast.present();
                break;
        }
    }

}


