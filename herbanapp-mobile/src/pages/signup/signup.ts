import {Component} from '@angular/core';
import {
    NavController, NavParams, ToastController, LoadingController, ActionSheetController,
    Platform
} from 'ionic-angular';
import {User} from '@ionic/cloud-angular';
import {LocalStorageService} from 'angular-2-local-storage';
import {Constants} from "../../shared/constants";
import {FormGroup, Validators, FormControl} from "@angular/forms";
import {UserProvider} from "../../providers/user-provider";
import {TabsPage} from "../../components/tabs/tabs";
import {Camera} from "@ionic-native/camera";
import {LoginPage} from "../login/login";

@Component({
    selector: 'page-signup',
    templateUrl: 'signup.html',
    providers: [UserProvider]
})
export class SignupPage {
    private social_id: any;
    private full_name: string;
    private social_name: string;
    private name: string;
    private lastname: string;
    private picture: string;
    private username: string;
    private city: string;
    private state: string;
    private newAvatar: string;
    private isSocial: boolean;
    private previousPage: string;
    private form: FormGroup;
    private formErrors: any;
    private validationMessages: any;
    private states: any;
    private cities: any;
    private selectState: any;
    private selectCity: any;

    constructor(public navCtrl: NavController, public navParams: NavParams, public actionSheetCtrl: ActionSheetController, public user: User, public localStorage: LocalStorageService, public toastCtrl: ToastController, public loadingCtrl: LoadingController, public userProvider: UserProvider, public camera: Camera, public platform: Platform) {
        this.states = Constants.AVAILABLE_STATES;
        this.cities = Constants.AVAILABLE_CITIES;
        this.selectState = {
            title: Constants.SELECT_STATE_MESSAGE
        };
        this.selectCity = {
            title: Constants.SELECT_CITY_MESSAGE
        };
        this.social_id = null;
        this.social_name = null;
        this.picture = null;
        this.newAvatar = null;
        this.name = "";
        this.lastname = "";
        this.username = "";
        this.city = "";
        this.state = "";

        if(this.navParams.data.previousPage) {
            this.previousPage = this.navParams.data.previousPage;
        }

        this.platform.ready().then(() => {
            this.platform.registerBackButtonAction(() => {
                if(this.previousPage == "login") {
                    this.navCtrl.setRoot(LoginPage);
                } else {
                    this.navCtrl.pop();
                }
            });
        });

        if(this.navParams.data.user) {
            this.isSocial = true;
            this.full_name = this.navParams.data.user.full_name;
            this.social_id = this.navParams.data.user.social_id;
            this.social_name = this.navParams.data.user.social_name;
            this.picture = this.navParams.data.user.picture;
            let userFullName = this.full_name.split(' ');
            this.name = userFullName[0];
            this.lastname = userFullName[1];
            if (this.navParams.data.user.username) {
                this.username = this.navParams.data.user.username;
            }

            this.form = new FormGroup({
                name: new FormControl(this.name, Validators.required),
                lastname: new FormControl(this.lastname, Validators.required),
                avatar: new FormControl(this.newAvatar),
                picture: new FormControl(this.picture),
                username: new FormControl(this.username, Validators.required),
                email: new FormControl('', Validators.compose([
                    Validators.required,
                    Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
                ])),
                state: new FormControl(this.state, Validators.required),
                city: new FormControl(this.city, Validators.required),
                birth_date: new FormControl('', Validators.required)
            });

        } else {
            this.form = new FormGroup({
                name: new FormControl(this.name, Validators.required),
                lastname: new FormControl(this.lastname, Validators.required),
                avatar: new FormControl(this.newAvatar),
                picture: new FormControl(this.picture),
                username: new FormControl(this.username, Validators.required),
                email: new FormControl('', Validators.compose([
                    Validators.required,
                    Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
                ])),
                password: new FormControl('', Validators.compose([
                    Validators.required,
                    Validators.minLength(6)
                ])),
                confirmPassword: new FormControl('', Validators.compose([
                    Validators.required,
                    Validators.minLength(6)
                ])),
                state: new FormControl(this.state, Validators.required),
                city: new FormControl(this.city, Validators.required),
                birth_date: new FormControl('', Validators.required)
            });
        }

        this.form.valueChanges
            .debounceTime(1000)
            .subscribe(data => this.onValueChanged(data));

        this.formErrors = {
            'name': [],
            'lastname': [],
            'username': [],
            'email': [],
            'password': [],
            'confirmPassword': [],
            'state': [],
            'city': [],
            'birth_date': [],
        };

        this.validationMessages = {
            'name': {
                'required': 'Name is required.'
            },
            'lastname': {
                'required': 'Lastname is required.'
            },
            'username': {
                'required': 'Username is required.',
            },
            'email': {
                'required': 'Email is required.'
            },
            'password': {
                'required': 'Password is required'
            },
            'confirmPassword': {
                'required': 'Please confirm password'
            },
            'state': {
                'required': 'State is required.'
            },
            'city': {
                'required': 'City is required.'
            },
            'birth_date': {
                'required': 'Birthday is required.'
            },
        }
    }

    goBack(): void {
        if(this.previousPage == "login") {
            this.navCtrl.setRoot(LoginPage);
        } else {
            this.navCtrl.pop();
        }
    }

    uploadImage(): void {
        let imageOptions: any;
        let actionSheet = this.actionSheetCtrl.create({
            buttons: [
                {
                    text: 'Take a picture',
                    role: 'destructive',
                    handler: () => {
                        imageOptions = {
                            quality: 100,
                            sourceType: this.camera.PictureSourceType.CAMERA,
                            encodingType: this.camera.EncodingType.JPEG,
                            destinationType: this.camera.DestinationType.DATA_URL,
                            mediaType: this.camera.MediaType.PICTURE,
                            correctOrientation: true,
                            saveToPhotoAlbum: true
                        }
                        this.getPicture(imageOptions);
                    }
                },{
                    text: 'Choose from library',
                    role: 'destructive',
                    handler: () => {
                        imageOptions = {
                            quality: 100,
                            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
                            encodingType: this.camera.EncodingType.JPEG,
                            destinationType: this.camera.DestinationType.DATA_URL,
                            mediaType: this.camera.MediaType.PICTURE,
                            correctOrientation: true
                        }
                        this.getPicture(imageOptions);
                    }
                },{
                    text: 'Cancel',
                    role: 'cancel',
                    handler: () => {
                    }
                }
            ]
        });
        actionSheet.present();
    }

    getPicture(imageOptions: any): void {
        this.camera.getPicture(imageOptions)
            .then((imageData: any) => {
                this.newAvatar = imageData;
                this.picture = 'data:image/jpeg;base64,' + imageData;
            });
    }

    signUp(): void {
        if (this.form.valid == false) {
            if(!this.navParams.data.user) {
                if(!(this.form.value.password == "" || this.form.value.password == undefined || this.form.value.confirmPassword == "" || this.form.value.confirmPassword == undefined)) {
                    if(!(this.form.value.password.length > 5 || this.form.value.confirmPassword.length > 5)) {
                        let toast = this.toastCtrl.create({
                            message: Constants.PASSWORD_LENGTH_MESSAGE,
                            duration: 3000
                        });
                        toast.present();
                    } else if(!(this.form.value.password == this.form.value.confirmPassword)) {
                        let toast = this.toastCtrl.create({
                            message: Constants.PASSWORD_MATCH_MESSAGE,
                            duration: 3000
                        });
                        toast.present();
                    }
                } else {
                    let toast = this.toastCtrl.create({
                        message: Constants.SIGNUP_FORM_INVALID_MESSAGE,
                        duration: 3000
                    });
                    toast.present();
                }
            } else {
                let toast = this.toastCtrl.create({
                    message: Constants.SIGNUP_FORM_INVALID_MESSAGE,
                    duration: 3000
                });
                toast.present();
            }
        } else {
            this.form.value.avatar = this.newAvatar;
            let birth_year = this.form.value.birth_date.split("-")[0];
            let currentYear = new Date().getFullYear();
            if(!this.navParams.data.user) {
                if(!(this.form.value.password == this.form.value.confirmPassword)) {
                    let toast = this.toastCtrl.create({
                        message: Constants.PASSWORD_MATCH_MESSAGE,
                        duration: 3000
                    });
                    toast.present();
                } else if((currentYear - birth_year) >= Constants.REQUIRED_YEARS) {
                    this.register(this.form.value);
                } else {
                    let toast = this.toastCtrl.create({
                        message: Constants.SIGNUP_FORM_YEAR_MESSAGE,
                        duration: 3000
                    });
                    toast.present();
                }
            } else if((currentYear - birth_year) >= Constants.REQUIRED_YEARS) {
                this.register(this.form.value);
            } else {
                let toast = this.toastCtrl.create({
                    message: Constants.SIGNUP_FORM_YEAR_MESSAGE,
                    duration: 3000
                });
                toast.present();
            }
        }
    }

    register(formData: any): void {
        let loader = this.loadingCtrl.create();
        loader.present();
        let socialData: any = {
            social_id: this.social_id,
            social_name: this.social_name,
            player_id: this.localStorage.get(Constants.ONESIGNAL_ID_KEY)
        };
        this.userProvider.register(formData, socialData)
            .subscribe(
                (response: any) => {
                    loader.dismiss();
                    this.localStorage.set(Constants.TOKEN_KEY, response.entity.token);
                    this.localStorage.set(Constants.USER_INFO, response.entity);
                    this.navCtrl.setRoot(TabsPage, {}, {
                        animate: true,
                        direction: 'forward'
                    });
                },
                (error: any) => {
                    loader.dismiss();
                    this.checkResponseStatus(error);
                }
            )
    }

    onValueChanged(data?: any) {
        if (!this.form) {
            return;
        }
        const form = this.form;
        for (const field in this.formErrors) {
            this.formErrors[field] = [];
            this.form[field] = '';
            const control = form.get(field);
            if (control && control.dirty && !control.valid) {
                const messages = this.validationMessages[field];
                for (const key in control.errors) {
                    this.formErrors[field].push(messages[key]);
                }
            }
        }
    }

    checkResponseStatus(error: any):void {
        let toast = this.toastCtrl.create({
            message: error.message,
            duration: 3000
        });
        switch (error.status) {
            case 413:
                toast.present();
                this.picture = null;
                this.newAvatar = null;
                break;
            case 400:
                if (error.message.email != undefined) {
                    let toast = this.toastCtrl.create({
                        message: error.message.email[0],
                        duration: 3000
                    });
                    toast.present();
                } else if (error.message.username != undefined) {
                    let toast = this.toastCtrl.create({
                        message: error.message.username[0],
                        duration: 3000
                    });
                    toast.present();
                }
                break;
            case 0:
                toast.present();
                break;
            case 500:
                toast.present();
                break;
        }
    }
}
