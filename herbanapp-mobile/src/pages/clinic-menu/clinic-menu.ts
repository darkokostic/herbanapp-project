import { Component } from '@angular/core';
import {App, NavController, NavParams, ToastController, LoadingController} from 'ionic-angular';
import {Constants} from "../../shared/constants";
import {ProductSinglePage} from "../product-single/product-single";
import {LocalStorageService} from "angular-2-local-storage";
import {UserProvider} from "../../providers/user-provider";
import {FavoritesProvider} from "../favorites/favorites-provider";
import {NearbyClinicsProvider} from "../clinics/nearby-clinics-provider";
import {ClinicProfileShared} from "../../shared/clinic-profile-shared";
import {ClinicTabsShared} from "../../shared/clinic-tabs-shared";
import {FirstPage} from "../first/first";

@Component({
    selector: 'page-clinic-menu',
    templateUrl: 'clinic-menu.html',
    providers: [FavoritesProvider, UserProvider, NearbyClinicsProvider]
})
export class ClinicMenuPage {
    private clinic: any;
    private products: any;
    private userInfo: any;
    private isFavorite: boolean;
    private endpoint: string;
    private productLogo: string;
    private categories: any;
    private activeCategory: any;
    private selectOptions: any;

    constructor(public app: App, public navCtrl: NavController, public navParams: NavParams, public tabsShared: ClinicTabsShared, public clinicShared: ClinicProfileShared, public localStorage: LocalStorageService, public clinicsProvider: NearbyClinicsProvider, public toastCtrl: ToastController, public favoritesProvider: FavoritesProvider, public loadingCtrl: LoadingController, public userProvider: UserProvider) {
        this.clinic = this.navParams.data;
        this.categories = this.clinic.products_with_category;
        this.products = this.clinic.products;
        this.userInfo = this.localStorage.get(Constants.USER_INFO);
        this.endpoint = Constants.ENDPOINT;
        this.productLogo = Constants.CLINIC_LOGO;
        this.selectOptions = {
            title: 'Choose products category',
        };
    }

    ionViewDidLoad() {
        this.activeCategory = "all";
    }

    ionViewDidEnter() {
        this.tabsShared.setSelectedTab(Constants.MENU_TAB_INDEX);
        this.isFavorite = this.clinicShared.getIsFavorite();
        this.fetchClinicData();
    }

    singleProduct(product: any):void {
        this.app.getRootNav().push(ProductSinglePage, {product: product});
    }

    fetchClinicData(): void {
        this.clinicsProvider.getClinic(this.clinic.id)
            .subscribe(
                (data: any) => {
                    this.activeCategory = "all";
                    this.clinic = data.entity;
                    this.categories = data.entity.products_with_category;
                    let allProducts: any = [];
                    for(let i = 0; i < data.entity.products_with_category.length; i++) {
                        data.entity.products_with_category[i].products.forEach((product: any) => {
                            allProducts.push(product);
                        });
                    }
                    this.products = allProducts;
                },
                (error: any) => {
                    this.checkResponseStatus(error);
                }
            )
    }

    selectCategory(activeCategory: any): void {
        if(activeCategory != "all") {
            for(let i = 0; i < this.clinic.products_with_category.length; i++) {
                if(activeCategory == this.clinic.products_with_category[i].id) {
                    this.products = this.clinic.products_with_category[i].products;
                }
            }
        } else {
            let allProducts: any = [];
            for(let i = 0; i < this.clinic.products_with_category.length; i++) {
                this.clinic.products_with_category[i].products.forEach((product: any) => {
                    allProducts.push(product);
                });
            }
            this.products = allProducts;
        }
    }

    favorite(): void {
        this.favoritesProvider.favorite(this.userInfo.id, this.clinic.id)
            .subscribe(
                (response: any) => {
                    if(this.clinicShared.getIsFavorite() == false) {
                        this.clinicShared.setIsFavorite(true);
                        let toast = this.toastCtrl.create({
                            message: Constants.ADDED_TO_FAVORITE,
                            duration: 3000
                        });
                        toast.present();
                    } else {
                        this.clinicShared.setIsFavorite(false);
                        let toast = this.toastCtrl.create({
                            message: Constants.REMOVED_FROM_FAVORITE,
                            duration: 3000
                        });
                        toast.present();
                    }
                },
                (error: any) => {
                    this.checkResponseStatus(error);
                }
            )
    }

    swipeEvent(e: any):void {
        switch(e.direction) {
            case 2:
                this.navCtrl.parent.select(1);
                break;
        }
    }

    checkResponseStatus(error: any):void {
        let toast = this.toastCtrl.create({
            message: error.message,
            duration: 3000
        });
        switch (error.status) {
            case 401:
                this.userProvider.logout();
                toast.present();
                this.navCtrl.setRoot(FirstPage);
                break;
            case 0:
                toast.present();
                break;
            case 500:
                toast.present();
                break;
        }
    }
}
