import {Component} from '@angular/core';
import {NavController, NavParams, LoadingController, ToastController, Platform} from 'ionic-angular';
import {FriendsProvider} from "../../providers/friends-provider";
import {UserProvider} from "../../providers/user-provider";
import {Constants} from "../../shared/constants";
import {FormGroup, FormControl} from "@angular/forms";
import {SingleChatPage} from "../single-chat/single-chat";
import {LiveSearchPipe} from "../../shared/live-search-pipe";
import {UserProfileTabs} from "../../components/user-profile-tabs/user-profile-tabs";
import {FirstPage} from "../first/first";

@Component({
    selector: 'page-friends-list',
    templateUrl: 'friends-list.html',
    providers: [FriendsProvider, UserProvider]
})
export class FriendsListPage {
    private users: any;
    private allUsers: any;
    private endpoint: string;
    private userAvatar: string;
    private form: FormGroup;
    private hasData: boolean;

    constructor(public navCtrl: NavController, public navParams: NavParams, public platform: Platform, public loadingCtrl: LoadingController, public toastCtrl: ToastController, public userProvider: UserProvider, public friendsProvider: FriendsProvider, public liveSearch: LiveSearchPipe) {
        this.endpoint = Constants.ENDPOINT;
        this.userAvatar = Constants.USER_AVATAR;
        this.form = new FormGroup({
            searchQuery: new FormControl(''),
        });
        this.form.valueChanges.debounceTime(700).subscribe(() => {
            this.search();
        });
        this.hasData = true;
    }

    ionViewDidEnter() {
        let loader = this.loadingCtrl.create();
        loader.present();
        this.friendsProvider.getFriendsList()
            .subscribe(
                (response: any) => {
                    loader.dismiss();
                    this.users = response.entity;
                    this.allUsers = response.entity;
                    if(this.users.length == 0 || this.users == undefined) {
                        this.hasData = false;
                    } else {
                        this.hasData = true;
                    }
                },
                (error: any) => {
                    loader.dismiss();
                    this.checkResponseStatus(error);
                }
            );
    }

    search():void {
        this.users = this.liveSearch.filterUsers(this.allUsers, this.form.value.searchQuery);
    }

    chat(user: any):void {
        this.navCtrl.push(SingleChatPage, {user: user});
    }

    goUser(user: any) {
        this.navCtrl.push(UserProfileTabs, {user: user, friendRequest: false});
    }

    remove(user: any):void {
        let loader = this.loadingCtrl.create();
        loader.present();
        this.friendsProvider.removeFriend(user.id)
            .subscribe(
                (response: any) => {
                    loader.dismiss();
                    let index: number = this.users.indexOf(user);
                    this.users.splice(index, 1);
                    let toast = this.toastCtrl.create({
                        message: Constants.REMOVE_FRIEND_MESSAGE,
                        duration: 3000
                    });
                    toast.present();
                    if(this.users.length == 0 || this.users == undefined) {
                        this.hasData = false;
                    } else {
                        this.hasData = true;
                    }
                },
                (error: any) => {
                    loader.dismiss();
                    this.checkResponseStatus(error);
                }
            )
    }

    checkResponseStatus(error: any):void {
        let toast = this.toastCtrl.create({
            message: error.message,
            duration: 3000
        });
        switch (error.status) {
            case 401:
                this.userProvider.logout();
                toast.present();
                this.navCtrl.setRoot(FirstPage);
                break;
            case 0:
                toast.present();
                break;
            case 500:
                toast.present();
                break;
        }
    }

}
