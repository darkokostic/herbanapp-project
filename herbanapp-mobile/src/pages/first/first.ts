import {Component} from '@angular/core';
import {NavController, NavParams, LoadingController, Platform, ToastController} from 'ionic-angular';
import {UserProvider} from "../../providers/user-provider";
import {AppAvailability} from "@ionic-native/app-availability";
import {InAppBrowser} from "@ionic-native/in-app-browser";
import {User} from "@ionic/cloud-angular";
import {SignupPage} from "../signup/signup";
import {Constants} from "../../shared/constants";
import {TabsPage} from "../../components/tabs/tabs";
import {RestResponse} from "../../shared/restResponse";
import {LocalStorageService} from "angular-2-local-storage";
import {LoginPage} from "../login/login";

@Component({
    selector: 'page-first',
    templateUrl: 'first.html',
    providers: [UserProvider, AppAvailability, InAppBrowser]
})
export class FirstPage {

    constructor(public navCtrl: NavController, public navParams: NavParams, public userProvider: UserProvider, public loadingCtrl: LoadingController, public user: User, public platform: Platform, public appAvailability: AppAvailability, public localStorage: LocalStorageService, public toastCtrl: ToastController) {
    }

    goLogin(): void {
        this.navCtrl.push(LoginPage);
    }

    goSignUp(): void {
        let previousPage = "first";
        this.navCtrl.push(SignupPage, {previousPage});
    }

    loginFacebook():void {
        this.userProvider.facebookLogin()
            .then((response: any) => {
                if(response != false) {
                    let loader = this.loadingCtrl.create();
                    loader.present();
                    this.checkSocialUser(response, loader);
                }
            }, (error: any) => {
            })
    }

    loginInstagram():void {
        this.userProvider.instagramLogin()
            .then((response: any) => {
                if(response != false) {
                    let instUser: any = this.user.social.instagram;
                    let user: any = {
                        social_id: instUser.uid,
                        full_name: instUser.data.full_name,
                        social_name: "instagram",
                        picture: instUser.data.profile_picture,
                        username: instUser.data.username
                    };
                    let loader = this.loadingCtrl.create();
                    loader.present();
                    this.checkSocialUser(user, loader);
                }
            }, (error: any) => {
            });
    }

    loginTwitter():void {
        let app: string;
        if(this.platform.is('ios')) {
            app = 'twitter://';
        } else if(this.platform.is('android')) {
            app = 'com.twitter.android';
        }

        this.appAvailability.check(app)
            .then(
                (yes: boolean) => {
                    this.twitterNativeLogin();
                },
                (no: boolean) => {
                    this.twitterWebLogin();
                }
            );
    }

    twitterNativeLogin():void {
        let loader = this.loadingCtrl.create();
        loader.present();
        this.userProvider.twitterNativeLogin()
            .then((response: any) => {
                loader.dismiss();
                if(response != false) {
                    let loader = this.loadingCtrl.create();
                    loader.present();
                    this.checkSocialUser(response, loader);
                }
            }, (error: any) => {
            })
    }

    twitterWebLogin():void {
        this.userProvider.twitterWebLogin()
            .then((response: any) => {
                if(response != false) {
                    let twUser: any = this.user.social.twitter;
                    let user: any = {
                        social_id: twUser.uid,
                        full_name: twUser.data.full_name,
                        social_name: "twitter",
                        picture: twUser.data.profile_picture,
                        username: twUser.data.username,
                    };
                    let loader = this.loadingCtrl.create();
                    loader.present();
                    this.checkSocialUser(user, loader);
                }
            }, (error: any) => {
            });
    }

    checkSocialUser(user: any, loader: any):void {
        this.userProvider.loginSocial(user.social_id, user.social_name)
            .subscribe(
                (response: any) => {
                    loader.dismiss();
                    if(response.entity == null) {
                        this.navCtrl.push(SignupPage, {user}, {
                            animate: true,
                            direction: 'forward'
                        });
                    } else {
                        this.localStorage.set(Constants.TOKEN_KEY, response.entity.token);
                        this.localStorage.set(Constants.USER_INFO, response.entity);
                        this.navCtrl.setRoot(TabsPage, {}, {
                            animate: true,
                            direction: 'forward'
                        });
                    }
                },
                (error: RestResponse) => {
                    loader.dismiss();
                    let toast = this.toastCtrl.create({
                        message: error.message,
                        duration: 3000
                    });
                    toast.present();
                }
            )
    }
}
