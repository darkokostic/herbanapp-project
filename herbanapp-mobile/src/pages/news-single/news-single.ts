import {Component, ViewChild} from '@angular/core';
import {NavController, NavParams, Content} from 'ionic-angular';
import { SocialSharing } from '@ionic-native/social-sharing';

@Component({
    selector: 'page-news-single',
    templateUrl: 'news-single.html',
    providers: [SocialSharing]
})
export class NewsSinglePage {
    private newsHeader: string;
    private newsCover: string;
    private newsContent: string;
    private newsObject: any;

    @ViewChild(Content) content: Content;
    constructor(public navCtrl: NavController, public navParams: NavParams, public socialSharing: SocialSharing) {
        this.newsHeader = "header-transparent";
        this.newsCover = "'http://placehold.it/350x350'";
        this.newsObject = this.navParams.get('params');
        if(this.newsObject.image_link != undefined) {
            this.newsContent = "news-content-has-cover";
        } else {
            this.newsContent = "news-content-no-cover";
        }
    }

    onPageScroll() {
        if(this.content.scrollTop > 0) {
            this.newsHeader = "header-colored";
        } else {
            this.newsHeader = "header-transparent";
        }
    }

    ionViewDidLoad() {
        this.content.ionScroll.subscribe((event: any) => this.onPageScroll());
    }

    share():void {
        this.socialSharing.share(null, null, null, this.newsObject.post_link)
            .then(() => {
                // Success!
            }).catch(() => {
            // Error!
        });
    }
}
