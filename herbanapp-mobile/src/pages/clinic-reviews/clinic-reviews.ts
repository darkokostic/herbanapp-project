import {Component} from '@angular/core';
import {App, NavController, NavParams, LoadingController, ToastController, FabContainer} from 'ionic-angular';
import {AddReviewPage} from "../add-review/add-review";
import {Constants} from "../../shared/constants";
import {UserProvider} from "../../providers/user-provider";
import {FavoritesProvider} from "../favorites/favorites-provider";
import {NearbyClinicsProvider} from "../clinics/nearby-clinics-provider";
import {LocalStorageService} from "angular-2-local-storage";
import {AddReviewProvider} from "../add-review/add-review-provider";
import {ClinicProfileShared} from "../../shared/clinic-profile-shared";
import {ClinicTabsShared} from "../../shared/clinic-tabs-shared";
import {FirstPage} from "../first/first";

@Component({
    selector: 'page-clinic-reviews',
    templateUrl: 'clinic-reviews.html',
    providers: [NearbyClinicsProvider, UserProvider, AddReviewProvider, FavoritesProvider]
})
export class ClinicReviewsPage {
    private clinic: any;
    private endpoint: string;
    private reviews: any;
    private userInfo: any;
    private userAvatar: string;
    private userDefaultAvatar: string;
    private showFabList: boolean;
    public comments: any;
    private isFavorite: boolean;

    constructor(public app: App, public navCtrl: NavController, public navParams: NavParams, public tabsShared: ClinicTabsShared, public clinicShared: ClinicProfileShared, public localStorage: LocalStorageService, public clinicsProvider: NearbyClinicsProvider, public userProvider: UserProvider, public favoritesProvider: FavoritesProvider, public toastCtrl: ToastController, public loadingCtrl: LoadingController, public addReviewProvider: AddReviewProvider) {
        this.endpoint = Constants.ENDPOINT;
        this.clinic = this.navParams.data;
        this.showFabList = false;
        this.reviews = this.clinic.reviews;
        this.comments = [];

        if(this.clinic.reviews.length > 0) {
            this.clinic.reviews.forEach((review: any) => {
                this.comments.push({'comment': ''});
            });
        }

        this.userDefaultAvatar = Constants.USER_AVATAR;

        this.userInfo = this.localStorage.get(Constants.USER_INFO);
        if(this.userInfo.avatar != null) {
            this.userAvatar = this.endpoint + this.userInfo.avatar;
        } else {
            this.userAvatar = Constants.USER_AVATAR;
        }
    }


    ionViewDidEnter() {
        this.tabsShared.setSelectedTab(Constants.REVIEWS_TAB_INDEX);
        this.isFavorite = this.clinicShared.getIsFavorite();
        this.fetchClinicData();
    }

    fetchClinicData(): void {
        this.clinicsProvider.getClinic(this.clinic.id)
            .subscribe(
                (data: any) => {
                    data.entity.reviews.forEach((review: any) => {
                        this.comments.push({'comment': ''});
                    });
                    this.clinic = data.entity;
                    this.reviews = data.entity.reviews;
                },
                (error: any) => {
                    this.checkResponseStatus(error);
                }
            )
    }
  
    goReview(fab: FabContainer):void {
        fab.close();
        this.app.getRootNav().push(AddReviewPage, {clinic: this.clinic});
    }

    addComment(message: string, reviewId: number): void {
        if(message != undefined && message != "") {
            let loader = this.loadingCtrl.create();
            loader.present();
            this.addReviewProvider.addComment(message, reviewId, this.userInfo.id)
                .subscribe(
                    (response: any) => {
                        loader.dismiss();
                        let toast = this.toastCtrl.create({
                            message: Constants.ADD_REVIEW_COMMENT_MESSAGE,
                            duration: 3000
                        });
                        toast.present();
                        this.fetchClinicData();
                    },
                    (error: any) => {
                        loader.dismiss();
                        this.checkResponseStatus(error);
                    }
                )
        }
    }

    favorite(fab: FabContainer): void {
        fab.close();
        this.favoritesProvider.favorite(this.userInfo.id, this.clinic.id)
            .subscribe(
                (response: any) => {
                    if(this.clinicShared.getIsFavorite() == false) {
                        this.clinicShared.setIsFavorite(true);
                        let toast = this.toastCtrl.create({
                            message: Constants.ADDED_TO_FAVORITE,
                            duration: 3000
                        });
                        toast.present();
                    } else {
                        this.clinicShared.setIsFavorite(false);
                        let toast = this.toastCtrl.create({
                            message: Constants.REMOVED_FROM_FAVORITE,
                            duration: 3000
                        });
                        toast.present();
                    }
                },
                (error: any) => {
                    this.checkResponseStatus(error);
                }
            )
    }

    swipeEvent(e: any):void {
        switch(e.direction) {
            case 2:
                this.navCtrl.parent.select(3);
                break;
            case 4:
                this.navCtrl.parent.select(1);
                break;
        }
    }

    checkResponseStatus(error: any):void {
        let toast = this.toastCtrl.create({
            message: error.message,
            duration: 3000
        });
        switch (error.status) {
            case 400:
                let newToast = this.toastCtrl.create({
                    message: Constants.TEXT_TOO_LONG_MESSAGE,
                    duration: 3000
                });
                newToast.present();
                break;
            case 401:
                this.userProvider.logout();
                toast.present();
                this.app.getRootNav().setRoot(FirstPage);
                break;
            case 0:
                toast.present();
                break;
            case 500:
                toast.present();
                break;
        }
    }
}
