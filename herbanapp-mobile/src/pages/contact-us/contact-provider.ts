import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {Observable} from "rxjs";
import {Constants} from "../../shared/constants";

@Injectable()
export class ContactProvider {

    constructor(public http: Http) {}

    sendMessage(name: string, email: string, message: string): Observable<any> {
        let params: any = {
            name: name,
            email: email,
            message: message
        }
        return this.http.post(Constants.CONTACT_API, JSON.stringify(params), {})
            .map((res: Response) => res.json())
    }
}
