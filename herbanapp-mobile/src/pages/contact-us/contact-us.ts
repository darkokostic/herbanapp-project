import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {ContactProvider} from "./contact-provider";
import { ToastController, LoadingController } from 'ionic-angular';
import {UserProvider} from "../../providers/user-provider";
import {Constants} from "../../shared/constants";
import {FormGroup, FormControl, Validators} from "@angular/forms";
import {FirstPage} from "../first/first";

@Component({
    selector: 'page-contact-us',
    templateUrl: 'contact-us.html',
    providers: [ContactProvider, UserProvider]
})
export class ContactUsPage {
    private form: FormGroup;
    private formErrors: any;
    private validationMessages: any;

    constructor(public navCtrl: NavController, public navParams: NavParams, public contactProvider: ContactProvider, public userProvider: UserProvider, public toastCtrl: ToastController, public loadingCtrl: LoadingController) {
        this.form = new FormGroup({
            name: new FormControl('', Validators.required),
            message: new FormControl('', Validators.required),
            email: new FormControl('', Validators.compose([
                Validators.required,
                Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
            ])),
        });

        this.form.valueChanges
            .debounceTime(1000)
            .subscribe(data => this.onValueChanged(data));

        this.formErrors = {
            'name': [],
            'message': [],
            'email': [],
        };

        this.validationMessages = {
            'name': {
                'required':      'Name is required.'
            },
            'message': {
                'required':      'Message is required.'
            },
            'email': {
                'required':      'Email is required.'
            }
        }
    }

    contactUs(): void {
        let form: any = this.form;

        if(this.form.valid == false) {
            let toast = this.toastCtrl.create({
                message: "Please fill all fields correct.",
                duration: 3000
            });
            toast.present();
        } else {
            this.sendEmail(form.value);
        }
    }

    sendEmail(formData: any):void {
        let loader = this.loadingCtrl.create();
        loader.present();
        this.contactProvider.sendMessage(formData.name, formData.email, formData.message)
            .subscribe(
                (response: any) => {
                    this.form = new FormGroup({
                        name: new FormControl('', Validators.required),
                        message: new FormControl('', Validators.required),
                        email: new FormControl('', Validators.compose([
                            Validators.required,
                            Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
                        ])),
                    });
                    loader.dismiss();
                    let toast = this.toastCtrl.create({
                        message: Constants.CONTACT_MESSAGE,
                        duration: 3000
                    });
                    toast.present();
                },
                (error: any) => {
                    this.form = new FormGroup({
                        name: new FormControl('', Validators.required),
                        message: new FormControl('', Validators.required),
                        email: new FormControl('', Validators.compose([
                            Validators.required,
                            Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
                        ])),
                    });
                    loader.dismiss();
                    this.checkResponseStatus(error);
                }
            )
    }

    onValueChanged(data?: any) {
        if (!this.form) { return; }
        const form = this.form;
        for (const field in this.formErrors) {
            this.formErrors[field] = [];
            this.form[field] = '';
            const control = form.get(field);
            if (control && control.dirty && !control.valid) {
                const messages = this.validationMessages[field];
                for (const key in control.errors) {
                    this.formErrors[field].push(messages[key]);
                }
            }
        }
    }

    checkResponseStatus(error: any):void {
        let toast = this.toastCtrl.create({
            message: error.message,
            duration: 3000
        });
        switch (error.status) {
            case 400:
                let newToast = this.toastCtrl.create({
                    message: Constants.TEXT_TOO_LONG_MESSAGE,
                    duration: 3000
                });
                newToast.present();
                break;
            case 401:
                this.userProvider.logout();
                toast.present();
                this.navCtrl.setRoot(FirstPage);
                break;
            case 0:
                toast.present();
                break;
            case 500:
                toast.present();
                break;
        }
    }
}
