import {Component} from '@angular/core';
import {NavController, App, NavParams, LoadingController, ToastController, FabContainer} from 'ionic-angular';
import {UserProfileShared} from "../../shared/user-profile-shared";
import {FriendsProvider} from "../../providers/friends-provider";
import {UserProvider} from "../../providers/user-provider";
import {Constants} from "../../shared/constants";
import {FavoritesProvider} from "../favorites/favorites-provider";
import {ClinicSinglePage} from "../clinic-single/clinic-single";
import {SingleChatPage} from "../single-chat/single-chat";
import {FirstPage} from "../first/first";

@Component({
    selector: 'page-user-favorites',
    templateUrl: 'user-favorites.html',
    providers: [UserProvider, FriendsProvider, FavoritesProvider]
})
export class UserFavoritesPage {
    private user: any;
    private showRequestHeader: boolean;
    private showFabList: boolean;
    private isFriend: boolean;
    private clinicLogo: string;
    private clinics: any;
    private endpoint: string;
    constructor(public app: App, public navCtrl: NavController, public navParams: NavParams, public userProfileShared: UserProfileShared, public favoritesProvider: FavoritesProvider, public loadingCtrl: LoadingController, public toastCtrl: ToastController, public userProvider: UserProvider, public friendsProvider: FriendsProvider) {
        this.user = this.navParams.data;
        this.endpoint = Constants.ENDPOINT;
        this.showFabList = false;
        this.clinicLogo = Constants.CLINIC_LOGO;
        this.clinics = [];
    }

    ionViewDidEnter() {
        this.showRequestHeader = this.userProfileShared.getIsFriendRequest();
        this.isFriend = this.userProfileShared.getIsFriend();
        this.favoritesProvider.getFavorites(this.user.id)
            .subscribe(
                (response: any) => {
                    this.clinics = response.entity;
                },
                (error: any) => {
                    this.checkResponseStatus(error);
                }
            )
    }

    chat(fab: FabContainer):void {
        fab.close();
        this.app.getRootNav().push(SingleChatPage, {user: this.user});
    }

    goClinic(clinicObject: any):void {
        this.app.getRootNav().push(ClinicSinglePage, {
            clinicObject: clinicObject
        });
    }

    accept(user: any):void {
        this.friendsProvider.acceptRequest(user.id)
            .subscribe(
                () => {
                    this.userProfileShared.setIsFriendRequest(false);
                    let toast = this.toastCtrl.create({
                        message: Constants.ACCEPTED_REQUEST_MESSAGE,
                        duration: 3000
                    });
                    toast.present();
                    this.isFriend = true;
                },
                (error: any) => {
                    this.checkResponseStatus(error);
                }
            )
    }

    cancel(user:any):void {
        this.friendsProvider.cancelRequest(user.id)
            .subscribe(
                () => {
                    this.userProfileShared.setIsFriendRequest(false);
                    let toast = this.toastCtrl.create({
                        message: Constants.CANCEL_REQUEST_MESSAGE,
                        duration: 3000
                    });
                    toast.present();
                },
                (error: any) => {
                    this.checkResponseStatus(error);
                }
            )
    }

    add(fab: FabContainer):void {
        fab.close();
        this.friendsProvider.add(this.user.id)
            .subscribe(
                (response: any) => {
                    this.userProfileShared.setIsFriend(true);
                    let toast = this.toastCtrl.create({
                        message: Constants.SENT_REQUEST_MESSAGE,
                        duration: 3000
                    });
                    toast.present();
                },
                (error: any) => {
                    this.checkResponseStatus(error);
                }
            )
    }

    remove(fab: FabContainer):void {
        fab.close();
        this.friendsProvider.removeFriend(this.user.id)
            .subscribe(
                (response: any) => {
                    this.userProfileShared.setIsFriend(false);
                    let toast = this.toastCtrl.create({
                        message: Constants.REMOVE_FRIEND_MESSAGE,
                        duration: 3000
                    });
                    toast.present();
                    this.isFriend = false;
                },
                (error: any) => {
                    this.checkResponseStatus(error);
                }
            )
    }

    swipeEvent(e: any):void {
        switch(e.direction) {
            case 4:
                this.navCtrl.parent.select(0);
                break;
        }
    }

    checkResponseStatus(error: any):void {
        let toast = this.toastCtrl.create({
            message: error.message,
            duration: 3000
        });
        switch (error.status) {
            case 401:
                this.userProvider.logout();
                toast.present();
                this.navCtrl.setRoot(FirstPage);
                break;
            case 0:
                toast.present();
                break;
            case 500:
                toast.present();
                break;
        }
    }
}
