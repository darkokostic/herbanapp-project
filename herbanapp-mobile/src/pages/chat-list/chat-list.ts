import { Component } from '@angular/core';
import {NavController, NavParams, App, ToastController, FabContainer} from 'ionic-angular';
import { GroupChatPage } from "../group-chat/group-chat";
import {CreateNewChatPage} from "../create-new-chat/create-new-chat";
import {ChatProvider} from "../../providers/chat-provider";
import {SingleChatPage} from "../single-chat/single-chat";
import {Constants} from "../../shared/constants";
import {UserProvider} from "../../providers/user-provider";
import {GroupChatsListPage} from "../group-chats-list/group-chats-list";
import {HomeTabsShared} from "../../shared/home-tabs-shared";
import {FirstPage} from "../first/first";

@Component({
    selector: 'chat-list',
    templateUrl: 'chat-list.html',
    providers: [ChatProvider, UserProvider]
})
export class ChatListPage {
    public inbox: any;
    private endpoint: string;
    private userAvatar: string;
    private groupAvatar: string;
    private showFabList: boolean;
    private hasData: boolean;
    constructor(public app: App, public navCtrl: NavController, public navParams: NavParams, public tabsShared: HomeTabsShared, public chatProvider: ChatProvider, public userProvider: UserProvider, public toastCtrl: ToastController) {
        this.endpoint = Constants.ENDPOINT;
        this.userAvatar = Constants.USER_AVATAR;
        this.groupAvatar = Constants.GROUP_AVATAR;
        this.showFabList = false;
        this.hasData = true;
    }

    ionViewDidEnter() {
        this.tabsShared.setSelectedTab(Constants.CHATS_TAB_INDEX);
        this.chatProvider.getUserInbox()
            .subscribe(
                (response: any) => {
                    response.entity.forEach((object: any) => {
                        let date = new Date(0);
                        date.setUTCSeconds(parseInt(object.lastMessage.created_at));
                        let time = date.toString().split(" ")[4];
                        object.lastMessage.created_at = time.split(":")[0] + ":" + time.split(":")[1];
                    })
                    this.inbox = response.entity;
                    if(response.entity.length == 0 || response.entity == undefined) {
                        this.hasData = false;
                    } else {
                        this.hasData = true;
                    }
                },
                (error: any) => {
                    this.checkResponseStatus(error);
                }
            );
    }

    chat(object: any): void {
        if(object.isGroup == 0) {
            this.app.getRootNav().push(SingleChatPage, {chat_room_id: object.lastMessage.chat_room_id, user: object.user});
        } else {
            this.app.getRootNav().push(GroupChatPage, {group: object});
        }
    }

    swipeEvent(e: any):void {
        switch(e.direction) {
            case 4:
                this.navCtrl.parent.select(2);
                break;
        }
    }

    goSingleChat(fab: FabContainer): void {
        fab.close();
        this.app.getRootNav().push(CreateNewChatPage);
    }

    goGroupChat(fab: FabContainer): void {
        fab.close();
        this.app.getRootNav().push(GroupChatsListPage);
    }

    checkResponseStatus(error: any):void {
        let toast = this.toastCtrl.create({
            message: error.message,
            duration: 3000
        });
        switch (error.status) {
            case 401:
                this.userProvider.logout();
                toast.present();
                this.app.getRootNav().setRoot(FirstPage);
                break;
            case 0:
                toast.present();
                break;
            case 500:
                toast.present();
                break;
        }
    }
}
