import {Component} from '@angular/core';
import {NavController, NavParams, LoadingController, ToastController, ModalController} from 'ionic-angular';
import {UserProvider} from "../../providers/user-provider";
import {Constants} from "../../shared/constants";
import {FormGroup, FormControl} from "@angular/forms";
import {GroupChatProvider} from "../../providers/group-chat-provider";
import {GroupChatPage} from "../group-chat/group-chat";
import {CreateGroupChatPage} from "../create-group-chat/create-group-chat";
import {LiveSearchPipe} from "../../shared/live-search-pipe";
import {FirstPage} from "../first/first";

@Component({
    selector: 'page-group-chats-list',
    templateUrl: 'group-chats-list.html',
    providers: [GroupChatProvider, UserProvider, LiveSearchPipe]
})
export class GroupChatsListPage {
    private groups: any;
    private groupAvatar: string;
    private form: FormGroup;
    private allGroups: any;
    private groupsLoadMoreUrl: string;
    private hasData: boolean;

    constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController, public loadingCtrl: LoadingController, public toastCtrl: ToastController, public userProvider: UserProvider, public groupChatProvider: GroupChatProvider, public liveSearch: LiveSearchPipe) {
        this.groupAvatar = Constants.GROUP_AVATAR;
        this.form = new FormGroup({
            searchQuery: new FormControl(''),
        });
        this.form.valueChanges.debounceTime(700).subscribe(() => {
            this.search();
        });
        this.hasData = true;
    }

    ionViewDidEnter() {
        let loader = this.loadingCtrl.create();
        loader.present();
        this.groupChatProvider.getGroupsList()
            .subscribe(
                (response: any) => {
                    loader.dismiss();
                    this.groups = response.entity.data;
                    this.allGroups = response.entity.data;
                    this.groupsLoadMoreUrl = response.entity.next_page_url;
                    if(response.entity.data.length == 0 || response.entity.data == undefined) {
                        this.hasData = false;
                    } else {
                        this.hasData = true;
                    }
                },
                (error: any) => {
                    loader.dismiss();
                    this.checkResponseStatus(error);
                }
            )
    }

    loadMoreGroups(infiniteScroll: any):void {
        if(this.groupsLoadMoreUrl != null) {
            this.groupChatProvider.loadMoreGroups(this.groupsLoadMoreUrl)
                .subscribe(
                    (data: any) => {
                        for(let i = 0; i < data.entity.data.length; i++) {
                            this.groups.push(data.entity.data[i]);
                        }
                        this.groupsLoadMoreUrl = data.entity.next_page_url;
                        infiniteScroll.complete();
                    },
                    (error: any) => {
                        this.checkResponseStatus(error);
                        infiniteScroll.complete();
                    }
                )
        } else {
            infiniteScroll.complete();
        }
    }

    search():void {
        this.groups = this.liveSearch.filterUsers(this.allGroups, this.form.value.searchQuery);
    }

    createGroupChat(): void {
        let modal = this.modalCtrl.create(CreateGroupChatPage);
        modal.present();
        modal.onDidDismiss((data: any) => {
            if(data) {
                this.navCtrl.push(GroupChatPage, {group: data});
            }
        });
    }

    chat(group: any):void {
        this.navCtrl.push(GroupChatPage, {group: group});
    }

    checkResponseStatus(error: any):void {
        let toast = this.toastCtrl.create({
            message: error.message,
            duration: 3000
        });
        switch (error.status) {
            case 401:
                this.userProvider.logout();
                toast.present();
                this.navCtrl.setRoot(FirstPage);
                break;
            case 0:
                toast.present();
                break;
            case 500:
                toast.present();
                break;
        }
    }

}
