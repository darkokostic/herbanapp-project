import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Diagnostic } from "@ionic-native/diagnostic";
import { Geolocation } from '@ionic-native/geolocation';
import {GpsProvider} from "../../providers/gps-provider";
import {ClinicsPage} from "../clinics/clinics";

@Component({
    selector: 'page-no-gps',
    templateUrl: 'no-gps.html',
    providers: [Diagnostic, Geolocation, GpsProvider]
})
export class NoGPSPage {

    constructor(public navCtrl: NavController, public navParams: NavParams, public gpsProvider: GpsProvider) {}
  
    checkGPS():void {
        this.gpsProvider.getLocation()
            .then((response) => {
                if(response) {
                    this.navCtrl.setRoot(ClinicsPage, {}, {
                        animate: true,
                        direction: 'forward'
                    });
                }
            })
            .catch((error) => {
            });
    }
}
