import {Component} from '@angular/core';
import {NavController, App, NavParams, ToastController, LoadingController, FabContainer} from 'ionic-angular';
import {UserProfileShared} from "../../shared/user-profile-shared";
import {FriendsProvider} from "../../providers/friends-provider";
import {UserProvider} from "../../providers/user-provider";
import {Constants} from "../../shared/constants";
import {SingleChatPage} from "../single-chat/single-chat";
import {FirstPage} from "../first/first";

@Component({
    selector: 'page-user-about',
    templateUrl: 'user-about.html',
    providers: [UserProvider, FriendsProvider]
})
export class UserAboutPage {
    private user: any;
    private showRequestHeader: boolean;
    private showFabList: boolean;
    private isFriend: boolean;
    constructor(public app: App, public navCtrl: NavController, public navParams: NavParams, public userProfileShared: UserProfileShared, public loadingCtrl: LoadingController, public toastCtrl: ToastController, public userProvider: UserProvider, public friendsProvider: FriendsProvider) {
        this.user = this.navParams.data;
        this.showFabList = false;
    }

    ionViewDidEnter() {
        this.showRequestHeader = this.userProfileShared.getIsFriendRequest();
        this.isFriend = this.userProfileShared.getIsFriend();
    }

    chat(fab: FabContainer):void {
        fab.close();
        this.app.getRootNav().push(SingleChatPage, {user: this.user});
    }

    accept(user: any):void {
        this.friendsProvider.acceptRequest(user.id)
            .subscribe(
                () => {
                    this.userProfileShared.setIsFriendRequest(false);
                    let toast = this.toastCtrl.create({
                        message: Constants.ACCEPTED_REQUEST_MESSAGE,
                        duration: 3000
                    });
                    toast.present();
                    this.isFriend = true;
                },
                (error: any) => {
                    this.checkResponseStatus(error);
                }
            )
    }

    cancel(user:any):void {
        this.friendsProvider.cancelRequest(user.id)
            .subscribe(
                () => {
                    this.userProfileShared.setIsFriendRequest(false);
                    let toast = this.toastCtrl.create({
                        message: Constants.CANCEL_REQUEST_MESSAGE,
                        duration: 3000
                    });
                    toast.present();
                },
                (error: any) => {
                    this.checkResponseStatus(error);
                }
            )
    }

    add(fab: FabContainer):void {
        fab.close();
        this.friendsProvider.add(this.user.id)
            .subscribe(
                (response: any) => {
                    this.userProfileShared.setIsFriend(true);
                    let toast = this.toastCtrl.create({
                        message: Constants.SENT_REQUEST_MESSAGE,
                        duration: 3000
                    });
                    toast.present();
                },
                (error: any) => {
                    this.checkResponseStatus(error);
                }
            )
    }

    remove(fab: FabContainer):void {
        fab.close();
        this.friendsProvider.removeFriend(this.user.id)
            .subscribe(
                (response: any) => {
                    this.userProfileShared.setIsFriend(false);
                    let toast = this.toastCtrl.create({
                        message: Constants.REMOVE_FRIEND_MESSAGE,
                        duration: 3000
                    });
                    toast.present();
                    this.isFriend = false;
                },
                (error: any) => {
                    this.checkResponseStatus(error);
                }
            )
    }

    swipeEvent(e: any):void {
        switch(e.direction) {
            case 2:
                this.navCtrl.parent.select(1);
                break;
        }
    }

    checkResponseStatus(error: any):void {
        let toast = this.toastCtrl.create({
            message: error.message,
            duration: 3000
        });
        switch (error.status) {
            case 401:
                this.userProvider.logout();
                toast.present();
                this.navCtrl.setRoot(FirstPage);
                break;
            case 0:
                toast.present();
                break;
            case 500:
                toast.present();
                break;
        }
    }
}
