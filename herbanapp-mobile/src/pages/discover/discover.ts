import { Component } from '@angular/core';
import {NavController, NavParams, LoadingController, ToastController, App} from 'ionic-angular';
import {DiscoverProvider} from "./discover-provider";
import {UserProvider} from "../../providers/user-provider";
import {FriendsProvider} from "../../providers/friends-provider";
import {Constants} from "../../shared/constants";
import {GroupChatProvider} from "../../providers/group-chat-provider";
import {GroupChatPage} from "../group-chat/group-chat";
import {UserProfileTabs} from "../../components/user-profile-tabs/user-profile-tabs";
import {HomeTabsShared} from "../../shared/home-tabs-shared";
import {FirstPage} from "../first/first";

@Component({
    selector: 'page-discover',
    templateUrl: 'discover.html',
    providers: [DiscoverProvider, UserProvider, FriendsProvider, GroupChatProvider]
})
export class DiscoverPage {
    private users: any;
    private groups: any;
    private loadMoreUrl: string;
    private chatType: string;
    private userAvatar: string;
    private groupAvatar: string;
    private endpoint: string;
    private groupsLoadMoreUrl: string;
    private hasUsers: boolean;
    private hasGroups: boolean;

    constructor(public app: App, public navCtrl: NavController, public tabsShared: HomeTabsShared, public navParams: NavParams, public discoverProvider: DiscoverProvider, public loadingCtrl: LoadingController, public userProvider: UserProvider, public toastCtrl: ToastController, public friendsProvider: FriendsProvider, public groupChatProvider: GroupChatProvider) {
        this.chatType = "people";
        this.endpoint = Constants.ENDPOINT;
        this.userAvatar = Constants.USER_AVATAR;
        this.groupAvatar = Constants.GROUP_AVATAR;
        this.hasUsers = true;
        this.hasGroups = true;
    }

    ionViewDidEnter() {
        this.tabsShared.setSelectedTab(Constants.DISCOVER_TAB_INDEX);
        this.getSuggestedUsers();
        this.getGroups();
    }

    getSuggestedUsers():void {
        let loader = this.loadingCtrl.create();
        loader.present();
        this.discoverProvider.getSuggestedUsers()
            .subscribe(
                (data: any) => {
                    loader.dismiss();
                    // this.users = data.entity.data;
                    // this.loadMoreUrl = data.entity.next_page_url;
                    // if(data.entity.data.length == 0 || data.entity.data.length == undefined) {
                    //     this.hasUsers = false;
                    // } else {
                    //     this.hasUsers = true;
                    // }
                    this.users = data.entity;
                    this.loadMoreUrl = data.entity.next_page_url;
                    if(data.entity.length == 0 || data.entity.length == undefined) {
                        this.hasUsers = false;
                    } else {
                        this.hasUsers = true;
                    }
                },
                (error: any) => {
                    loader.dismiss();
                    this.checkResponseStatus(error);
                }
            )
    }

    loadMoreUsers(infiniteScroll: any):void {
        if(this.loadMoreUrl != null) {
            this.discoverProvider.loadMoreUsers(this.loadMoreUrl)
                .subscribe(
                    (data: any) => {
                        for(let i = 0; i < data.entity.data.length; i++) {
                            this.users.push(data.entity.data[i]);
                        }
                        this.loadMoreUrl = data.entity.next_page_url;
                        infiniteScroll.complete();
                    },
                    (error: any) => {
                        this.checkResponseStatus(error);
                        infiniteScroll.complete();
                    }
                )
        } else {
            infiniteScroll.complete();
        }
    }

    getGroups(): void {
        this.groupChatProvider.getGroupsList()
            .subscribe(
                (response: any) => {
                    this.groups = response.entity.data;
                    this.groupsLoadMoreUrl = response.entity.next_page_url;
                    if(response.entity.data.length == 0 || response.entity.data == undefined) {
                        this.hasGroups = false;
                    } else {
                        this.hasGroups = true;
                    }
                },
                (error: any) => {
                    this.checkResponseStatus(error);
                }
            )
    }

    loadMoreGroups(infiniteScroll: any):void {
        if(this.groupsLoadMoreUrl != null) {
            this.groupChatProvider.loadMoreGroups(this.groupsLoadMoreUrl)
                .subscribe(
                    (data: any) => {
                        for(let i = 0; i < data.entity.data.length; i++) {
                            this.groups.push(data.entity.data[i]);
                        }
                        this.groupsLoadMoreUrl = data.entity.next_page_url;
                        infiniteScroll.complete();
                    },
                    (error: any) => {
                        this.checkResponseStatus(error);
                        infiniteScroll.complete();
                    }
                )
        } else {
            infiniteScroll.complete();
        }
    }

    swipeEvent(e: any):void {
        switch (e.direction) {
            case 2:
                this.navCtrl.parent.select(1);
                break;
        }
    }

    selectPeople():void {
        this.chatType = "people";
    }

    selectGroups():void {
        this.chatType = "groups";
    }

    chat(group: any):void {
        this.app.getRootNav().push(GroupChatPage, {group: group});
    }

    goUser(user: any) {
        this.app.getRootNav().setRoot(UserProfileTabs, {user: user, friendRequest: false});
    }

    checkResponseStatus(error: any):void {
        let toast = this.toastCtrl.create({
            message: error.message,
            duration: 3000
        });
        switch (error.status) {
            case 401:
                this.userProvider.logout();
                toast.present();
                this.app.getRootNav().setRoot(FirstPage);
                break;
            case 0:
                toast.present();
                break;
            case 500:
                toast.present();
                break;
        }
    }
}
