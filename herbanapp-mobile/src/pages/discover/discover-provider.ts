import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from "rxjs";
import {Constants} from "../../shared/constants";

@Injectable()
export class DiscoverProvider {

    constructor(public http: Http) {}

    getSuggestedUsers():Observable<any> {
        return this.http.get(Constants.SUGGESTED_USERS_API)
            .map((res: Response) => res.json())
    }

    loadMoreUsers(url:string):Observable<any> {
        return this.http.get(url)
            .map((res: Response) => res.json())
    }
}