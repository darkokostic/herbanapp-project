import {Component} from '@angular/core';
import {App, NavController, NavParams, LoadingController, ToastController} from 'ionic-angular';
import {AddReviewPage} from "../add-review/add-review";
import {Constants} from "../../shared/constants";
import {UserProvider} from "../../providers/user-provider";
import {FavoritesProvider} from "../favorites/favorites-provider";
import {LocalStorageService} from "angular-2-local-storage";
import {NearbyClinicsProvider} from "../clinics/nearby-clinics-provider";
import {ClinicProfileShared} from "../../shared/clinic-profile-shared";
import {ClinicTabsShared} from "../../shared/clinic-tabs-shared";
import {FirstPage} from "../first/first";

@Component({
    selector: 'page-clinic-about',
    templateUrl: 'clinic-about.html',
    providers: [FavoritesProvider, UserProvider, NearbyClinicsProvider]
})
export class ClinicAboutPage {
    private clinic: any;
    private userInfo: any;
    private isFavorite: boolean;

    constructor(public app: App, public navCtrl: NavController, public navParams: NavParams, public tabsShared: ClinicTabsShared, public clinicShared: ClinicProfileShared, public localStorage: LocalStorageService, public clinicsProvider: NearbyClinicsProvider, public toastCtrl: ToastController, public favoritesProvider: FavoritesProvider, public loadingCtrl: LoadingController, public userProvider: UserProvider) {
        this.clinic = this.navParams.data;
        this.userInfo = this.localStorage.get(Constants.USER_INFO);
    }
  
    goReview():void {
        this.app.getRootNav().push(AddReviewPage, {clinic: this.clinic});
    }

    ionViewDidEnter() {
        this.tabsShared.setSelectedTab(Constants.ABOUT_TAB_INDEX);
        this.isFavorite = this.clinicShared.getIsFavorite();
        this.fetchClinicData();
    }

    fetchClinicData(): void {
        this.clinicsProvider.getClinic(this.clinic.id)
            .subscribe(
                (data: any) => {
                    this.clinic = data.entity;
                },
                (error: any) => {
                    this.checkResponseStatus(error);
                }
            )
    }

    favorite(): void {
        this.favoritesProvider.favorite(this.userInfo.id, this.clinic.id)
            .subscribe(
                (response: any) => {
                    if(this.clinicShared.getIsFavorite() == false) {
                        this.clinicShared.setIsFavorite(true);
                        let toast = this.toastCtrl.create({
                            message: Constants.ADDED_TO_FAVORITE,
                            duration: 3000
                        });
                        toast.present();
                    } else {
                        this.clinicShared.setIsFavorite(false);
                        let toast = this.toastCtrl.create({
                            message: Constants.REMOVED_FROM_FAVORITE,
                            duration: 3000
                        });
                        toast.present();
                    }
                },
                (error: any) => {
                    this.checkResponseStatus(error);
                }
            )
    }

    swipeEvent(e: any):void {
        switch(e.direction) {
            case 2:
                this.navCtrl.parent.select(2);
                break;
            case 4:
                this.navCtrl.parent.select(0);
                break;
        }
    }

    checkResponseStatus(error: any):void {
        let toast = this.toastCtrl.create({
            message: error.message,
            duration: 3000
        });
        switch (error.status) {
            case 401:
                this.userProvider.logout();
                toast.present();
                this.navCtrl.setRoot(FirstPage);
                break;
            case 0:
                toast.present();
                break;
            case 500:
                toast.present();
                break;
        }
    }
}
