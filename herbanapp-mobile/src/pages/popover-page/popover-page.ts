import {Component} from '@angular/core';
import {NavController, NavParams, ViewController} from 'ionic-angular';

@Component({
    selector: 'page-popover-page',
    templateUrl: 'popover-page.html'
})
export class PopoverPagePage {
    private workingHours: any;
    constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
        this.workingHours = this.navParams.data;
    }

    close() {
        this.viewCtrl.dismiss();
    }
}
