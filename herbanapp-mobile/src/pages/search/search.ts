import { Component } from '@angular/core';
import {NavController, NavParams, LoadingController, ToastController} from 'ionic-angular';
import {SearchProvider} from "./search-provider";
import {ClinicSinglePage} from "../clinic-single/clinic-single";
import {UserProvider} from "../../providers/user-provider";
import {FriendsProvider} from "../../providers/friends-provider";
import {Constants} from "../../shared/constants";
import {FormGroup, FormControl} from "@angular/forms";
import {SingleChatPage} from "../single-chat/single-chat";
import {FavoritesProvider} from "../favorites/favorites-provider";
import {LocalStorageService} from "angular-2-local-storage";
import {UserProfileTabs} from "../../components/user-profile-tabs/user-profile-tabs";
import {FirstPage} from "../first/first";

@Component({
    selector: 'page-search',
    templateUrl: 'search.html',
    providers: [SearchProvider, UserProvider, FriendsProvider, FavoritesProvider]
})
export class SearchPage {
    private searchFilter:string;
    private searchQuery:string;
    private users:any;
    private clinics:any;
    private endpoint: string;
    private userAvatar: string;
    private clinicLogo: string;
    private form: FormGroup;
    private userInfo: any;
    private hasUsers: boolean;
    private hasClinics: boolean;

    constructor(public navCtrl: NavController, public navParams: NavParams, public searchProvider: SearchProvider, public localStorage: LocalStorageService, public loadingCtrl: LoadingController, public userProvider: UserProvider, public favoritesProvider: FavoritesProvider, public toastCtrl: ToastController, public friendsProvider: FriendsProvider) {
        this.searchFilter = "clinics";
        this.searchQuery = "";
        this.endpoint = Constants.ENDPOINT;
        this.userAvatar = Constants.USER_AVATAR;
        this.clinicLogo = Constants.CLINIC_LOGO;
        this.userInfo = this.localStorage.get(Constants.USER_INFO);
        this.form = new FormGroup({
            searchQuery: new FormControl(''),
        });
        this.hasUsers = true;
        this.hasClinics = true;
    }

    search():void {
        let form: any = this.form;
        if(form.value.searchQuery != '') {
            let loader = this.loadingCtrl.create();
            loader.present();
            this.searchProvider.getSearchItems(form.value.searchQuery)
                .subscribe(
                    (data: any) => {
                        loader.dismiss();
                        this.users = data.entity.users;
                        this.clinics = data.entity.clinics;
                        if(data.entity.users.length == 0 || data.entity.users == undefined) {
                            this.hasUsers = false;
                        } else {
                            this.hasUsers = true;
                        }
                        if(data.entity.clinics.length == 0 || data.entity.clinics == undefined) {
                            this.hasClinics = false;
                        } else {
                            this.hasClinics = true;
                        }
                    },
                    (error: any) => {
                        loader.dismiss();
                        this.checkResponseStatus(error);
                    }
                )
        }
    }

    add(user: any):void {
        let loader = this.loadingCtrl.create();
        loader.present();
        this.friendsProvider.add(user.id)
            .subscribe(
                (response: any) => {
                    loader.dismiss();
                    let index: number = this.users.indexOf(user);
                    this.users.splice(index, 1);
                    let toast = this.toastCtrl.create({
                        message: Constants.SENT_REQUEST_MESSAGE,
                        duration: 3000
                    });
                    toast.present();
                },
                (error: any) => {
                    loader.dismiss();
                    this.checkResponseStatus(error);
                }
            )
    }

    chat(user: any):void {
        this.navCtrl.push(SingleChatPage, {user: user});
    }

    goUser(user: any) {
        this.navCtrl.push(UserProfileTabs, {user: user, friendRequest: false});
    }

    selectPeople():void {
        this.searchFilter = "people";
    }

    selectClinics():void {
        this.searchFilter = "clinics";
    }

    goClinic(clinicObject: any):void {
        this.navCtrl.push(ClinicSinglePage, {
            clinicObject: clinicObject
        });
    }

    favorite(clinicId: number, isFavorite: number): void {
        this.favoritesProvider.favorite(this.userInfo.id, clinicId)
            .subscribe(
                (response: any) => {
                    if(isFavorite == 0) {
                        let toast = this.toastCtrl.create({
                            message: Constants.ADDED_TO_FAVORITE,
                            duration: 3000
                        });
                        toast.present();
                    } else {
                        let toast = this.toastCtrl.create({
                            message: Constants.REMOVED_FROM_FAVORITE,
                            duration: 3000
                        });
                        toast.present();
                    }
                    this.search();
                },
                (error: any) => {
                    this.checkResponseStatus(error);
                }
            )
    }

    checkResponseStatus(error: any):void {
        let toast = this.toastCtrl.create({
            message: error.message,
            duration: 3000
        });
        switch (error.status) {
            case 401:
                this.userProvider.logout();
                toast.present();
                this.navCtrl.setRoot(FirstPage);
                break;
            case 0:
                toast.present();
                break;
            case 500:
                toast.present();
                break;
        }
    }
}
