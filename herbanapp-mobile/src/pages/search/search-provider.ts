import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from "rxjs";
import {Constants} from "../../shared/constants";

@Injectable()
export class SearchProvider {

    constructor(public http: Http) {}

    getSearchItems(query:string):Observable<any> {
        let params: any = {
            search_key: query
        };
        return this.http.post(Constants.SEARCH_API, JSON.stringify(params), {})
            .map((res: Response) => res.json())
    }
}