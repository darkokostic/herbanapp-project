import {Component, ViewChild} from '@angular/core';
import {NavController, NavParams, Content, LoadingController, ToastController, Platform} from 'ionic-angular';
import {LocalStorageService} from "angular-2-local-storage";
import {Constants} from "../../shared/constants";
import * as io from 'socket.io-client';
import {ChatProvider} from "../../providers/chat-provider";
import {UserProvider} from "../../providers/user-provider";
import {FormGroup, FormControl} from "@angular/forms";
import {FirstPage} from "../first/first";

@Component({
    selector: 'page-single-chat',
    templateUrl: 'single-chat.html',
    queries: {
        content: new ViewChild('content')
    },
    providers: [ChatProvider, UserProvider]
})
export class SingleChatPage {

    @ViewChild(Content) content: Content;

    private userInfo: any;
    private secondUser: any;

    private socket: any;
    private messages: any;
    private chatRoomId: number;
    private isListen: boolean;
    private form: FormGroup;

    constructor(public navCtrl: NavController, public navParams: NavParams, public platform: Platform, public localStorage: LocalStorageService, public chatProvider: ChatProvider, public userProvider: UserProvider, public loadingCtrl: LoadingController, public toastCtrl: ToastController) {
        this.userInfo = this.localStorage.get(Constants.USER_INFO);
        this.secondUser = this.navParams.get('user');
        this.messages = [];
        this.socket = io(Constants.SOCKET);
        this.isListen = false;
        this.form = new FormGroup({
            myMessage: new FormControl('')
        });
    }

    ionViewDidLoad() {
        this.userInfo = this.localStorage.get(Constants.USER_INFO);
        if(this.navParams.get('chat_room_id') != undefined) {
            this.chatRoomId = this.navParams.get('chat_room_id');
            this.getMessages(this.chatRoomId);
        } else {
            this.chatProvider.getChatRoom(this.secondUser.id)
                .subscribe(
                    (response: any) => {
                        if(response.entity != null) {
                            this.chatRoomId = response.entity;
                            this.getMessages(this.chatRoomId);
                        } else {
                            this.chatRoomId = null;
                        }
                    },
                    (error: any) => {
                        this.checkResponseStatus(error);
                    }
                );
        }
    }

    ionViewWillLeave() {
        this.socket.disconnect();
    }

    getMessages(chatRoomId: number): void {
        this.chatProvider.getMessages(chatRoomId)
            .subscribe(
                (response: any) => {
                    response.entity.forEach((message: any) => {
                        let date = new Date(0);
                        date.setUTCSeconds(parseInt(message.created_at));
                        let time = date.toString().split(" ")[4];
                        message.created_at = time.split(":")[0] + ":" + time.split(":")[1];
                    });
                    this.messages = response.entity;
                    this.listenChannel();
                    setTimeout(() => {
                        this.content.scrollToBottom();
                    }, 400);
                },
                (error: any) => {
                    this.checkResponseStatus(error);
                }
            )
    }

    sendMessage() {
        if(this.form.value.myMessage != '') {
            this.chatProvider.sendMessage(this.chatRoomId, this.form.value.myMessage, this.secondUser.id)
                .subscribe(
                    (response: any) => {
                        if(this.isListen == false) {
                            this.chatRoomId = response.entity;
                            this.getMessages(this.chatRoomId);
                        }
                    },
                    (error: any) => {
                        this.checkResponseStatus(error);
                    }
                );
            this.form = new FormGroup({
                myMessage: new FormControl('')
            });
        }
    }

    listenChannel(): void {
        this.isListen = true;
        this.socket.on('chat-channel.'+ this.chatRoomId +':App\\Events\\ChatEvent', (response: any) => {
            if(response.data.user.id == this.userInfo.id) {
                response.data.position = "right";
            } else {
                response.data.position = "left";
            }
            let date = new Date(0);
            date.setUTCSeconds(parseInt(response.data.created_at));
            let time = date.toString().split(" ")[4];
            response.data.created_at = time.split(":")[0] + ":" + time.split(":")[1];
            this.messages.push(response.data);
            setTimeout(() => {
                this.content.scrollToBottom();
            }, 400);
        });
    }

    checkResponseStatus(error: any):void {
        let toast = this.toastCtrl.create({
            message: error.message,
            duration: 3000
        });
        switch (error.status) {
            case 0:
                toast.present();
                break;
            case 400:
                let newToast = this.toastCtrl.create({
                    message: Constants.TEXT_TOO_LONG_MESSAGE,
                    duration: 3000
                });
                newToast.present();
                break;
            case 401:
                this.userProvider.logout();
                toast.present();
                this.navCtrl.setRoot(FirstPage);
                break;
            case 500:
                toast.present();
                break;
        }
    }
}
