import {Component} from '@angular/core';
import {NavController, NavParams, AlertController} from 'ionic-angular';
import {LocalStorageService} from "angular-2-local-storage";
import {Constants} from "../../shared/constants";
import { UserProvider } from "../../providers/user-provider";
import { ToastController, LoadingController } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';
import { ActionSheetController } from 'ionic-angular';
import {FirstPage} from "../first/first";

@Component({
    selector: 'page-my-account',
    templateUrl: 'my-account.html',
    providers: [UserProvider]
})
export class MyAccountPage {
    private isEditable: boolean;

    private name: string;
    private lastname: string;
    private username: string;
    private email: string;
    private phone: string;
    private birthday: any;
    private state: string;
    private city: string;
    private friendCount: any;
    private clinicsCount: number;
    private newAvatar: string;
    private hasNewPicture: boolean;
    private userAvatar: string;
    private userInfo: any;
    private isSocial: boolean;
    private states: any;
    private cities: any;
    private selectState: any;
    private selectCity: any;

    constructor(public navCtrl: NavController, public navParams: NavParams, public actionSheetCtrl: ActionSheetController, public localStorage: LocalStorageService, public alertCtrl: AlertController, public userProvider: UserProvider, public loadingCtrl: LoadingController, public toastCtrl: ToastController, public camera: Camera) {
        this.isEditable = false;
        this.hasNewPicture = false;
        this.userInfo = this.localStorage.get(Constants.USER_INFO);
        if(this.localStorage.get(Constants.CONNECTED_SOCIAL_NETWORK)) {
            this.isSocial = true;
        } else {
            this.isSocial = false;
        }
        this.states = Constants.AVAILABLE_STATES;
        this.cities = Constants.AVAILABLE_CITIES;
        this.selectState = {
            title: Constants.SELECT_STATE_MESSAGE
        };
        this.selectCity = {
            title: Constants.SELECT_CITY_MESSAGE
        };
    }

    ionViewDidLoad() {
        this.fetchUserData();
    }

    fetchUserData():void {
        let loader = this.loadingCtrl.create();
        loader.present();
        this.userProvider.getUserInfo()
            .subscribe(
                (data: any) => {
                    loader.dismiss();
                    this.localStorage.set(Constants.USER_INFO, data.entity);
                    if(data.entity.avatar != null) {
                        this.userAvatar = Constants.ENDPOINT + data.entity.avatar;
                    } else {
                        this.userAvatar = Constants.USER_AVATAR;
                    }
                    this.name = data.entity.name;
                    this.lastname = data.entity.lastname;
                    this.username = data.entity.username;
                    this.email = data.entity.email;
                    this.phone = data.entity.phone;
                    this.birthday = data.entity.birth_date;
                    this.state = data.entity.state;
                    this.city = data.entity.city;
                    this.friendCount = data.entity.friendCount;
                    this.clinicsCount = data.entity.clinicsCount;
                    this.hasNewPicture = false;
                },
                (error: any) => {
                    if(this.userInfo.avatar != null) {
                        this.userAvatar = Constants.ENDPOINT + this.userInfo.avatar;
                    } else {
                        this.userAvatar = Constants.USER_AVATAR;
                    }
                    this.name = this.userInfo.name;
                    this.lastname = this.userInfo.lastname;
                    this.username = this.userInfo.username;
                    this.email = this.userInfo.email;
                    this.phone = this.userInfo.phone;
                    this.birthday = this.userInfo.birth_date;
                    this.state = this.userInfo.state;
                    this.city = this.userInfo.city;
                    this.friendCount = this.userInfo.friendCount;
                    this.hasNewPicture = false;
                    loader.dismiss();
                }
            )
    }

    edit():void {
        this.isEditable = true;
    }

    cancel():void {
        this.isEditable = false;
        this.userInfo = this.localStorage.get(Constants.USER_INFO);
        if(this.userInfo.avatar != null) {
            this.userAvatar = Constants.ENDPOINT + this.userInfo.avatar;
        } else {
            this.userAvatar = Constants.USER_AVATAR;
        }
    }

    save():void {
        let loader = this.loadingCtrl.create();
        loader.present();
        let editData: any;
        if(this.hasNewPicture == true) {
            editData = {
                id: this.userInfo.id,
                name: this.name,
                lastname: this.lastname,
                state: this.state,
                city: this.city,
                birth_date: this.birthday,
                avatar: this.newAvatar
            };
        } else {
            editData = {
                id: this.userInfo.id,
                name: this.name,
                lastname: this.lastname,
                state: this.state,
                city: this.city,
                birth_date: this.birthday
            };
        }
        this.userProvider.updateUser(editData)
            .subscribe(
                (response: any) => {
                    loader.dismiss();
                    this.localStorage.set(Constants.USER_INFO, response.entity);
                    if(response.entity.avatar != null) {
                        this.userAvatar = Constants.ENDPOINT + response.entity.avatar;
                    } else {
                        this.userAvatar = "assets/images/avatar-user.png";
                    }
                    this.isEditable = false;
                },
                (error: any) => {
                    loader.dismiss();
                    this.checkResponseStatus(error);
                    this.isEditable = false;
                }
            )
    }

    uploadImage(): void {
        let imageOptions: any;
        let actionSheet = this.actionSheetCtrl.create({
            buttons: [
                {
                    text: 'Take a picture',
                    role: 'destructive',
                    handler: () => {
                        imageOptions = {
                            quality: 100,
                            sourceType: this.camera.PictureSourceType.CAMERA,
                            encodingType: this.camera.EncodingType.JPEG,
                            destinationType: this.camera.DestinationType.DATA_URL,
                            mediaType: this.camera.MediaType.PICTURE,
                            correctOrientation: true,
                            saveToPhotoAlbum: true
                        }
                        this.getPicture(imageOptions);
                    }
                },{
                    text: 'Choose from library',
                    role: 'destructive',
                    handler: () => {
                        imageOptions = {
                            quality: 100,
                            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
                            encodingType: this.camera.EncodingType.JPEG,
                            destinationType: this.camera.DestinationType.DATA_URL,
                            mediaType: this.camera.MediaType.PICTURE,
                            correctOrientation: true
                        }
                        this.getPicture(imageOptions);
                    }
                },{
                    text: 'Cancel',
                    role: 'cancel',
                    handler: () => {
                    }
                }
            ]
        });
        actionSheet.present();
    }

    getPicture(imageOptions: any): void {
        this.camera.getPicture(imageOptions)
            .then((imageData: any) => {
                this.newAvatar = imageData;
                this.userAvatar = 'data:image/jpeg;base64,' + imageData;
                this.hasNewPicture = true;
            });
    }

    changePassword() {
        let alert = this.alertCtrl.create({
            title: 'Change Password',
            inputs: [
                {
                    name: 'currentPassword',
                    placeholder: 'Current Password',
                    type: 'password'
                },
                {
                    name: 'newPassword',
                    placeholder: 'New Password',
                    type: 'password'
                },
                {
                    name: 'repeatedPassword',
                    placeholder: 'Repeat password',
                    type: 'password'
                }
            ],
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: (data: any) => {
                    }
                },
                {
                    text: 'Save',
                    handler: (data: any) => {
                        if (data.repeatedPassword == data.newPassword) {
                            let loader = this.loadingCtrl.create();
                            loader.present();
                            this.userProvider.changePassword(data.newPassword, data.repeatedPassword, data.currentPassword)
                                .subscribe(
                                    (response: any) => {
                                        loader.dismiss();
                                        let toast = this.toastCtrl.create({
                                            message: Constants.PASSWORD_CHANGED_MESSAGE,
                                            duration: 3000
                                        });
                                        toast.present();
                                    },
                                    (error: any) => {
                                        loader.dismiss();
                                        this.checkResponseStatus(error);
                                        return false;
                                    }
                                )
                            return true;
                        } else {
                            let toast = this.toastCtrl.create({
                                message: Constants.PASSWORD_NOT_MATCH_MESSAGE,
                                duration: 3000
                            });
                            toast.present();
                            return false;
                        }
                    }
                }
            ]
        });
        alert.present();
    }

    checkResponseStatus(error: any):void {
        let toast = this.toastCtrl.create({
            message: error.message,
            duration: 3000
        });
        switch (error.status) {
            case 401:
                this.userProvider.logout();
                toast.present();
                this.navCtrl.setRoot(FirstPage);
                break;
            case 413:
                toast.present();
            case 0:
                toast.present();
                break;
            case 500:
                toast.present();
                break;
        }
        this.userInfo = this.localStorage.get(Constants.USER_INFO);
        if(this.userInfo.avatar != null) {
            this.userAvatar = Constants.ENDPOINT + this.userInfo.avatar;
        } else {
            this.userAvatar = Constants.USER_AVATAR;
        }
    }
}
