import {Component} from '@angular/core';
import {NavController, NavParams, LoadingController, ToastController} from 'ionic-angular';
import {FriendsProvider} from "../../providers/friends-provider";
import {UserProvider} from "../../providers/user-provider";
import {Constants} from "../../shared/constants";
import {FormGroup, FormControl} from "@angular/forms";
import {SingleChatPage} from "../single-chat/single-chat";
import {LiveSearchPipe} from "../../shared/live-search-pipe";
import {UserProfileTabs} from "../../components/user-profile-tabs/user-profile-tabs";
import {FirstPage} from "../first/first";

@Component({
    selector: 'page-create-new-chat',
    templateUrl: 'create-new-chat.html',
    providers: [FriendsProvider, UserProvider, LiveSearchPipe]
})
export class CreateNewChatPage {
    private users: any;
    private endpoint: string;
    private userAvatar: string;
    private form: FormGroup;
    private allUsers: any;
    private hasData: boolean;

    constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, public toastCtrl: ToastController, public userProvider: UserProvider, public friendsProvider: FriendsProvider, public liveSearch: LiveSearchPipe) {
        this.endpoint = Constants.ENDPOINT;
        this.userAvatar = Constants.USER_AVATAR;
        this.form = new FormGroup({
            searchQuery: new FormControl(''),
        });
        this.form.valueChanges.debounceTime(700).subscribe(() => {
            this.search();
        });
        this.hasData = true;
    }

    ionViewDidEnter() {
        let loader = this.loadingCtrl.create();
        loader.present();
        this.friendsProvider.getFriendsList()
            .subscribe(
                (response: any) => {
                    loader.dismiss();
                    this.users = response.entity;
                    this.allUsers = response.entity;
                    if(response.entity.length == 0 || response.entity == undefined) {
                        this.hasData = false;
                    } else {
                        this.hasData = true;
                    }
                },
                (error: any) => {
                    loader.dismiss();
                    this.checkResponseStatus(error);
                }
            )
    }

    search():void {
        this.users = this.liveSearch.filterUsers(this.allUsers, this.form.value.searchQuery);
    }

    chat(user: any):void {
        this.navCtrl.push(SingleChatPage, {user: user});
    }

    goUser(user: any) {
        this.navCtrl.push(UserProfileTabs, {user: user, friendRequest: false});
    }

    checkResponseStatus(error: any):void {
        let toast = this.toastCtrl.create({
            message: error.message,
            duration: 3000
        });
        switch (error.status) {
            case 401:
                this.userProvider.logout();
                toast.present();
                this.navCtrl.setRoot(FirstPage);
                break;
            case 0:
                toast.present();
                break;
            case 500:
                toast.present();
                break;
        }
    }

}