import {Component} from '@angular/core';
import {NavController, NavParams, LoadingController, ToastController, Platform, App} from 'ionic-angular';
import {FriendsProvider} from "../../providers/friends-provider";
import {UserProvider} from "../../providers/user-provider";
import {Constants} from "../../shared/constants";
import {UserProfileTabs} from "../../components/user-profile-tabs/user-profile-tabs";
import {FirstPage} from "../first/first";

@Component({
    selector: 'page-friend-requests',
    templateUrl: 'friend-requests.html',
    providers: [FriendsProvider, UserProvider]
})
export class FriendRequestsPage {
    private users: any;
    private endpoint: string;
    private userAvatar: string;
    private hasData: boolean;

    constructor(public app: App, public navCtrl: NavController, public navParams: NavParams, public platform: Platform, public loadingCtrl: LoadingController, public toastCtrl: ToastController, public userProvider: UserProvider, public friendsProvider: FriendsProvider) {
        this.endpoint = Constants.ENDPOINT;
        this.userAvatar = Constants.USER_AVATAR;
        this.hasData = true;
    }

    ionViewDidEnter() {
        let loader = this.loadingCtrl.create({});
        loader.present();
        this.friendsProvider.getFriendRequests()
            .subscribe(
                (response: any) => {
                    loader.dismiss();
                    this.users = response.entity;
                    if(response.entity.length == 0 || response.entity == undefined) {
                        this.hasData = false;
                    } else {
                        this.hasData = true;
                    }
                },
                (error: any) => {
                    loader.dismiss();
                    this.checkResponseStatus(error);
                }
            )
    }

    goUser(user: any) {
        this.navCtrl.push(UserProfileTabs, {user: user, friendRequest: true});
    }

    accept(user: any):void {
        let loader = this.loadingCtrl.create();
        loader.present();
        this.friendsProvider.acceptRequest(user.id)
            .subscribe(
                (response: any) => {
                    loader.dismiss();
                    let index: number = this.users.indexOf(user);
                    this.users.splice(index, 1);
                    let toast = this.toastCtrl.create({
                        message: Constants.ACCEPTED_REQUEST_MESSAGE,
                        duration: 3000
                    });
                    toast.present();
                    if(this.users.length == 0 || this.users == undefined) {
                        this.hasData = false;
                    } else {
                        this.hasData = true;
                    }
                },
                (error: any) => {
                    loader.dismiss();
                    this.checkResponseStatus(error);
                }
            )
    }

    cancel(user:any):void {
        let loader = this.loadingCtrl.create();
        loader.present();
        this.friendsProvider.cancelRequest(user.id)
            .subscribe(
                (response: any) => {
                    loader.dismiss();
                    let index: number = this.users.indexOf(user);
                    this.users.splice(index, 1);
                    let toast = this.toastCtrl.create({
                        message: Constants.CANCEL_REQUEST_MESSAGE,
                        duration: 3000
                    });
                    toast.present();
                    if(this.users.length == 0 || this.users == undefined) {
                        this.hasData = false;
                    } else {
                        this.hasData = true;
                    }
                },
                (error: any) => {
                    loader.dismiss();
                    this.checkResponseStatus(error);
                }
            )
    }

    checkResponseStatus(error: any):void {
        let toast = this.toastCtrl.create({
            message: error.message,
            duration: 3000
        });
        switch (error.status) {
            case 401:
                this.userProvider.logout();
                toast.present();
                this.navCtrl.setRoot(FirstPage);
                break;
            case 0:
                toast.present();
                break;
            case 500:
                toast.present();
                break;
        }
    }
}
