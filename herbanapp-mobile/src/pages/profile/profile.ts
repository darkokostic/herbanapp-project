import { Component } from '@angular/core';
import {NavController, NavParams, LoadingController, ToastController} from 'ionic-angular';
import { UserProvider } from "../../providers/user-provider";
import {SuggestFriendsPage} from "../suggest-friends/suggest-friends";
import {LocalStorageService} from "angular-2-local-storage";
import {Constants} from "../../shared/constants";
import {Facebook} from "@ionic-native/facebook";
import {FaqPage} from "../faq/faq";
import {User, Auth} from '@ionic/cloud-angular';
import {TermsConditionPage} from "../terms-condition/terms-condition";
import {ContactUsPage} from "../contact-us/contact-us";
import {MyAccountPage} from "../my-account/my-account";
import {FriendRequestsPage} from "../friend-requests/friend-requests";
import {FriendsListPage} from "../friends-list/friends-list";
import {FavoritesPage} from "../favorites/favorites";
import {FirstPage} from "../first/first";

@Component({
    selector: 'page-profile',
    templateUrl: 'profile.html',
    providers: [UserProvider, Facebook]
})
export class ProfilePage {
    private name: string;
    private username: string;
    private userAvatar: string = "";
    private userInfo: any;

    constructor(public navCtrl: NavController, public navParams: NavParams, public userProvider: UserProvider, public loadingCtrl: LoadingController, public localStorage: LocalStorageService, public fb: Facebook, public auth: Auth, public toastCtrl: ToastController, public user: User) {}

    ionViewWillEnter() {
        this.fetchUserData();
    }

    fetchUserData():void {
        this.userInfo = this.localStorage.get(Constants.USER_INFO);
        if(this.userInfo.avatar != null) {
            this.userAvatar = Constants.ENDPOINT + this.userInfo.avatar;
        } else {
            this.userAvatar = Constants.USER_AVATAR;
        }
        this.name = this.userInfo.name + " " + this.userInfo.lastname;
        this.username = this.userInfo.username;
    }

    inviteFacebookFriends():void {
        let options:any = {
            url: Constants.FACEBOOK_INVITE_URL,
            picture: Constants.FACEBOOK_INVITE_IMAGE
        };
        this.fb.appInvite(options)
            .then((response) => {
            })
            .catch((error) => {
            })
    }
    goFavorites():void {
      this.navCtrl.push(FavoritesPage);
    }
  
    goAccount():void {
        this.navCtrl.push(MyAccountPage);
    }

    goFriendRequests():void {
        this.navCtrl.push(FriendRequestsPage);
    }

    goFriendsList():void {
        this.navCtrl.push(FriendsListPage);
    }

    goSuggest():void {
        this.navCtrl.push(SuggestFriendsPage);
    }

    goFaq():void {
      this.navCtrl.push(FaqPage);
    }

    goTerms():void {
        this.navCtrl.push(TermsConditionPage);
    }

    goContactUs():void {
        this.navCtrl.push(ContactUsPage);
    }

    logout():void {
        this.userProvider.logoutNotifications()
            .subscribe(
                (data: any) => {
                    this.userProvider.logout();
                    this.navCtrl.setRoot(FirstPage, {}, {
                        animate: true,
                        direction: 'back'
                    });
                },
                (error: any) => {
                    this.checkResponseStatus(error);
                }
            );
    }

    checkResponseStatus(error: any):void {
        let toast = this.toastCtrl.create({
            message: error.message,
            duration: 3000
        });
        switch (error.status) {
            case 401:
                toast.present();
                break;
            case 0:
                toast.present();
                break;
            case 500:
                toast.present();
                break;
        }
    }
}
