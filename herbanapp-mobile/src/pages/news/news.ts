import { Component } from '@angular/core';
import {NavController, NavParams, LoadingController, ToastController, App} from 'ionic-angular';
import { NewsProvider } from "./news-provider";
import { SocialSharing } from '@ionic-native/social-sharing';
import {UserProvider} from "../../providers/user-provider";
import {NewsSinglePage} from "../news-single/news-single";
import {Constants} from "../../shared/constants";
import {HomeTabsShared} from "../../shared/home-tabs-shared";
import {FirstPage} from "../first/first";

@Component({
    selector: 'page-news',
    templateUrl: 'news.html',
    providers: [NewsProvider, SocialSharing, UserProvider]
})
export class NewsPage {
    private userAvatar: string;
    private endpoint: string;
    private news: any;
    private hasData: boolean;
    constructor(public app: App, public navCtrl: NavController, public navParams: NavParams, public newsProvider: NewsProvider, public loadingCtrl: LoadingController, public socialSharing: SocialSharing, public userProvider: UserProvider, public toastCtrl: ToastController, public tabsShared: HomeTabsShared) {
        this.endpoint = Constants.ENDPOINT;
        this.userAvatar = Constants.USER_AVATAR;
        this.hasData = true;
    }

    ionViewDidEnter() {
        this.tabsShared.setSelectedTab(Constants.NEWS_TAB_INDEX);
        let loader = this.loadingCtrl.create();
        loader.present();
        this.newsProvider.getNews()
            .subscribe(
                (data: any) => {
                    loader.dismiss();
                    data.entity.forEach((post: any) => {
                        this.convertDate(post);
                    });
                    this.news = data.entity;
                    if(data.entity.length == 0 || data.entity == undefined) {
                        this.hasData = false;
                    } else {
                        this.hasData = true;
                    }
                },
                (error: any) => {
                    loader.dismiss();
                    this.checkResponseStatus(error);
                }
            )
    }

    sync(event: any) {
        this.newsProvider.getNews()
            .subscribe(
                (data: any) => {
                    data.entity.forEach((post: any) => {
                        this.convertDate(post);
                    });
                    this.news = data.entity;
                    event.complete();
                },
                (error: any) => {
                    event.complete();
                    this.checkResponseStatus(error);
                }
            )
    }

    goNewsSingle(newsObject: any): void {
        this.app.getRootNav().push(NewsSinglePage, {params: newsObject});
    }

    swipeEvent(e: any):void {
        switch(e.direction) {
            case 2:
                this.navCtrl.parent.select(2);
                break;
            case 4:
                this.navCtrl.parent.select(0);
                break;
        }
    }

    convertDate(post: any): void {
        let date: any = new Date(0);
        date.setUTCSeconds(parseInt(post.date));
        post.fullDate = date.toString().split(" ")[1] + " " + date.toString().split(" ")[2] + ", " + date.toString().split(" ")[3];
        let currentDate: any = new Date();
        let diffMs = (currentDate - date);
        let diffDays = Math.floor(diffMs / 86400000);
        let diffHrs = Math.floor((diffMs % 86400000) / 3600000);
        let diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000);
        if(diffDays > 365) {
            if(diffDays < 730) {
                post.date = "1 year ago";
            } else {
                post.date = Math.round(diffDays / 365) + " years ago";
            }
        } else if(diffDays > 30) {
            if(diffDays < 60) {
                post.date = "1 month ago";
            } else {
                post.date = Math.round(diffDays / 30) + " months ago";
            }
        } else if(diffHrs > 24) {
            if(diffHrs < 48) {
                post.date = "1 day ago";
            } else {
                post.date = diffDays + " days ago";
            }
        } else if(diffMins > 59) {
            if(diffMins < 120) {
                post.date = "1 hour ago";
            } else {
                post.date = diffHrs + " hours ago";
            }
        } else {
            if(diffMins > 1) {
                post.date = diffMins + " minutes ago";
            } else {
                post.date = "1 minute ago";
            }
        }
    }

    checkResponseStatus(error: any):void {
        let toast = this.toastCtrl.create({
            message: error.message,
            duration: 3000
        });
        switch (error.status) {
            case 401:
                this.userProvider.logout();
                toast.present();
                this.app.getRootNav().setRoot(FirstPage);
                break;
            case 0:
                toast.present();
                break;
            case 500:
                toast.present();
                break;
        }
    }
}
