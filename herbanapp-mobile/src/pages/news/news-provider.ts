import {Injectable} from "@angular/core";
import {Http, Response} from "@angular/http";
import {Observable} from "rxjs";
import {Constants} from "../../shared/constants";

@Injectable()
export class NewsProvider {

    constructor(public http: Http) {}

    getNews():Observable<any> {
        return this.http.get(Constants.NEWS_API)
            .map((res: Response) => res.json())
    }
}

