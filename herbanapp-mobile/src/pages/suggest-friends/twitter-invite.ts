import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from "rxjs";

@Injectable()
export class TwitterInvite {

    constructor(public http: Http) {}

    getUserFriends(token:any):Observable<any> {
        return this.http.get("https://api.twitter.com/1.1/friends/list.json")
            .map((res: Response) => res.json())
    }
}
