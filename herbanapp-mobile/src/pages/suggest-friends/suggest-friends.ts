import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {LocalStorageService} from "angular-2-local-storage";
import {Constants} from "../../shared/constants";
import {Facebook} from "@ionic-native/facebook";
import {TwitterInvite} from "./twitter-invite";

@Component({
    selector: 'page-suggest-friends',
    templateUrl: 'suggest-friends.html',
    providers: [Facebook, TwitterInvite]
})
export class SuggestFriendsPage {
    private isInvited:boolean;

    constructor(public navCtrl: NavController, public navParams: NavParams, public localStorage: LocalStorageService, public fb: Facebook, public twitterInvite: TwitterInvite) {}

    ionViewDidLoad() {
        this.getTwitterFriends();
    }

    getTwitterFriends():void {
        this.twitterInvite.getUserFriends(this.localStorage.get(Constants.TOKEN_KEY))
            .subscribe(
                (data: any) => {
                },
                (error: any) => {
                }
            )
    }

    loadMore(infiniteScroll: any) {
        infiniteScroll.complete();
    }

    invite():void {
        this.isInvited = true;
    }

    remove():void {
        this.isInvited = false;
    }
}
