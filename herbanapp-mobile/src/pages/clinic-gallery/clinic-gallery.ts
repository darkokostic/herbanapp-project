import {Component} from '@angular/core';
import {
    App, ModalController, NavController, NavParams, LoadingController, ToastController
} from 'ionic-angular';
import {Constants} from "../../shared/constants";
import {UserProvider} from "../../providers/user-provider";
import {FavoritesProvider} from "../favorites/favorites-provider";
import {LocalStorageService} from "angular-2-local-storage";
import {NearbyClinicsProvider} from "../clinics/nearby-clinics-provider";
import {ClinicProfileShared} from "../../shared/clinic-profile-shared";
import {ClinicTabsShared} from "../../shared/clinic-tabs-shared";
import {FirstPage} from "../first/first";
import {GalleryModal} from "../../components/gallery-modal/gallery-modal";

@Component({
    selector: 'page-clinic-gallery',
    templateUrl: 'clinic-gallery.html',
    providers: [FavoritesProvider, UserProvider, NearbyClinicsProvider]
})
export class ClinicGalleryPage {
  
    private photos: any[] = [];
    private clinic: any;
    private endpoint: string;
    private userInfo: any;
    private isFavorite: boolean;

    constructor(public app: App, public navCtrl: NavController, public navParams: NavParams, public tabsShared: ClinicTabsShared, public clinicShared: ClinicProfileShared, public modalCtrl: ModalController, public clinicsProvider: NearbyClinicsProvider, public favoritesProvider: FavoritesProvider, public localStorage: LocalStorageService, public toastCtrl: ToastController, public loadingCtrl: LoadingController, public userProvider: UserProvider) {
        this.clinic = this.navParams.data;
        this.endpoint = Constants.ENDPOINT;
        this.userInfo = this.localStorage.get(Constants.USER_INFO);
        this.createPhotos();
    }

    ionViewDidEnter() {
        this.tabsShared.setSelectedTab(Constants.GALLERY_TAB_INDEX);
        this.isFavorite = this.clinicShared.getIsFavorite();
        this.fetchClinicData();
    }

    createPhotos(): void {
        this.photos = [];
        for (let i = 0; i < this.clinic.gallery.length; i++) {
            this.photos.push({
                url: this.endpoint + this.clinic.gallery[i].url,
            });
        }
    }
    
    photoModal(image: any) {
        let index: number = this.clinic.gallery.indexOf(image);
        let modal = this.modalCtrl.create(GalleryModal, {
            photos: this.photos,
            initialSlide: index,
        });
        modal.present();
    }

    fetchClinicData(): void {
        this.clinicsProvider.getClinic(this.clinic.id)
            .subscribe(
                (data: any) => {
                    this.clinic = data.entity;
                    this.createPhotos();
                },
                (error: any) => {
                    this.checkResponseStatus(error);
                }
            )
    }

    favorite(): void {
        this.favoritesProvider.favorite(this.userInfo.id, this.clinic.id)
            .subscribe(
                (response: any) => {
                    if(this.clinicShared.getIsFavorite() == false) {
                        this.clinicShared.setIsFavorite(true);
                        let toast = this.toastCtrl.create({
                            message: Constants.ADDED_TO_FAVORITE,
                            duration: 3000
                        });
                        toast.present();
                    } else {
                        this.clinicShared.setIsFavorite(false);
                        let toast = this.toastCtrl.create({
                            message: Constants.REMOVED_FROM_FAVORITE,
                            duration: 3000
                        });
                        toast.present();
                    }
                },
                (error: any) => {
                    this.checkResponseStatus(error);
                }
            )
    }

    swipeEvent(e: any):void {
        switch(e.direction) {
            case 4:
                this.navCtrl.parent.select(2);
                break;
        }
    }

    checkResponseStatus(error: any):void {
        let toast = this.toastCtrl.create({
            message: error.message,
            duration: 3000
        });
        switch (error.status) {
            case 401:
                this.userProvider.logout();
                toast.present();
                this.navCtrl.setRoot(FirstPage);
                break;
            case 0:
                toast.present();
                break;
            case 500:
                toast.present();
                break;
        }
    }
}
