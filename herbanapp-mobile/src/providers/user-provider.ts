import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {Observable} from "rxjs";
import {Constants} from "../shared/constants";
import { LocalStorageService } from 'angular-2-local-storage';
import {FacebookLoginResponse, Facebook} from "@ionic-native/facebook";
import {TwitterConnectResponse, TwitterConnect} from "@ionic-native/twitter-connect";
import { Auth, User } from '@ionic/cloud-angular';

@Injectable()
export class UserProvider {

    constructor(public http: Http, public localStorage: LocalStorageService, public fb: Facebook, public twitter: TwitterConnect, public auth: Auth, public user: User) {
        this.fb.browserInit(Constants.FB_APP_ID, "v2.8");
    }

    updateUser(params: any): Observable<any> {
        return this.http.post(Constants.UPDATE_USER_API, JSON.stringify(params), {})
            .map((res: Response) => res.json())
    }

    changePassword(newPass: any, checkPass: any, currentPass: any): Observable<any> {
        let params: any = {
            new: newPass,
            check: checkPass,
            current: currentPass
        }
        return this.http.post(Constants.CHANGE_PASSWORD_API, JSON.stringify(params), {})
            .map((res: Response) => res.json())
    }

    forgotPassword(email: any): Observable<any> {
        let params: any = {
            email: email,
        }
        return this.http.post(Constants.FORGOT_PASSWORD_API, JSON.stringify(params), {})
            .map((res: Response) => res.json())
    }

    login(params: any): Observable<any> {
        return this.http.post(Constants.LOGIN_API, JSON.stringify(params), {})
            .map((res: Response) => res.json())
    }

    register(formData: any, socialData: any): Observable<any> {
        let playerId = this.localStorage.get(Constants.ONESIGNAL_ID_KEY);
        let params: any = {
            username: formData.username,
            image_url: formData.picture,
            avatar: formData.avatar,
            name: formData.name,
            lastname: formData.lastname,
            email: formData.email,
            password: formData.password,
            confirmPassword: formData.confirmPassword,
            state: formData.state,
            city: formData.city,
            birth_date: formData.birth_date,
            social_id: socialData.social_id,
            social_name: socialData.social_name,
            player_id: playerId
        };
        return this.http.post(Constants.REGISTER_API, JSON.stringify(params), {})
            .map((res: Response) => res.json())
            .catch((err:any) => {
                if(err.status == 413 || err.status == 500) {
                    let details = {
                        entity: null,
                        message: err.message,
                        status: err.status
                    };
                    return Observable.throw(details);
                } else {
                    let details = err.json();
                    return Observable.throw(details);
                }
            })
    }

    facebookLogin():Promise<FacebookLoginResponse> {
        return this.fb.login(['public_profile', 'user_friends', 'email'])
            .then((res: FacebookLoginResponse) => {
                if(res.status == 'connected') {
                    return this.fb.api("/me?fields=name", ['public_profile'])
                        .then((user: any) => {
                            let fbUser = {
                                social_id: res.authResponse.userID,
                                full_name: user.name,
                                social_name: "facebook",
                                picture: "https://graph.facebook.com/" + res.authResponse.userID + "/picture?type=large"
                            };
                            this.localStorage.set(Constants.CONNECTED_SOCIAL_NETWORK, "facebook");
                            return fbUser;
                        })
                        .catch((e: any) => {
                            return false;
                        });
                }
            })
            .catch((e: FacebookLoginResponse) => {
                return false;
            });
    }

    twitterWebLogin():Promise<any> {
        return this.auth.login('twitter')
            .then((response:any) => {
                this.localStorage.set(Constants.CONNECTED_SOCIAL_NETWORK, "twitter");
                return response;
            })
            .catch((e: any) => {
                return false;
            });
    }

    twitterNativeLogin():Promise<TwitterConnectResponse> {
        return this.twitter.login()
            .then((res: TwitterConnectResponse) => {
                return this.twitter.showUser()
                    .then((user: any) => {
                        let twitterUser = {
                            social_id: res.userId,
                            full_name: user.name,
                            social_name: "twitter",
                            picture: user.profile_image_url,
                            username: res.userName,
                        };
                        this.localStorage.set(Constants.CONNECTED_SOCIAL_NETWORK, "twitter");
                        return twitterUser;
                    })
                    .catch((e: any) => {
                        return false;
                    });
            })
            .catch((e: TwitterConnectResponse) => {
                return false;
            });
    }

    instagramLogin():Promise<any> {
        return this.auth.login('instagram')
            .then((response:any) => {
                this.localStorage.set(Constants.CONNECTED_SOCIAL_NETWORK, "instagram");
                return response;
            })
            .catch((e: any) => {
                return false;
            });
    }

    loginSocial(id: any, name: any): Observable<any> {
        let params: any = {
            social_id: id,
            social_name: name,
            player_id: this.localStorage.get(Constants.ONESIGNAL_ID_KEY)
        };
        return this.http.post(Constants.LOGIN_SOCIAL_API, JSON.stringify(params), {})
            .map((res: Response) => res.json())
    }

    logoutNotifications(): any {
        return this.http.post(Constants.LOGOUT_API, {})
            .map((res: Response) => res.json())
    }

    logout(): any {
        this.localStorage.remove(Constants.TOKEN_KEY);
        this.localStorage.remove(Constants.USER_INFO);
        this.localStorage.remove(Constants.CONNECTED_SOCIAL_NETWORK);
        this.auth.logout();
    }

    getUserInfo():Observable<any> {
        return this.http.get(Constants.PROFILE_API)
            .map((res: Response) => res.json())
    }
}
