import {Injectable} from '@angular/core';
import {Diagnostic} from "@ionic-native/diagnostic";
import {Geolocation} from '@ionic-native/geolocation';

@Injectable()
export class GpsProvider {
    constructor(private diagnostic: Diagnostic, private geolocation: Geolocation) {
    }

    getLocation(): Promise<any> {
        return this.diagnostic.isLocationEnabled()
            .then((isAvailable) => {
                if (isAvailable) {
                    return this.geolocation.getCurrentPosition()
                        .then((response) => {
                            const location = {
                                lat: response.coords.latitude,
                                lng: response.coords.longitude
                            }
                            return location;
                        })
                        .catch((error) => {
                            return false;
                        });
                } else {
                    this.diagnostic.switchToLocationSettings();
                }
            })
            .catch((error) => {
                this.diagnostic.switchToLocationSettings();
            });
    }

    // getLocation(): Promise<any> {
    //     return this.geolocation.getCurrentPosition()
    //         .then((response) => {
    //             const location = {
    //                 lat: response.coords.latitude,
    //                 lng: response.coords.longitude
    //             }
    //             return location;
    //         })
    //         .catch((error) => {
    //             return false;
    //         });
    // }

    checkGPS(): Promise<any> {
        return this.diagnostic.isLocationEnabled()
            .then((isAvailable) => {
                if (isAvailable) {
                    return true;
                } else {
                    return false;
                }
            })
            .catch((error) => {
                return false;
            });
    }

}
