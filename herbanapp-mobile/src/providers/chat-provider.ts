import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {Observable} from "rxjs";
import {Constants} from "../shared/constants";

@Injectable()
export class ChatProvider {
    constructor(public http: Http) {}

    getChatRoom(id: number): Observable<any> {
        let params: any = {
            user_id: id
        };
        return this.http.post(Constants.GET_CHAT_ROOM, JSON.stringify(params), {})
            .map((res: Response) => res.json())
    }

    getUserInbox(): Observable<any> {
        return this.http.get(Constants.GET_USER_INBOX, {})
            .map((res: Response) => res.json())
    }

    getMessages(chatRoomId: number): Observable<any> {
        let params: any = {
            chat_room_id: chatRoomId
        };
        return this.http.post(Constants.GET_MESSAGES, JSON.stringify(params), {})
            .map((res: Response) => res.json())
            .catch((err: Response) => {
                let details = err.json();
                return Observable.throw(details);
            });
    }

    sendMessage(chatRoomId: number, message: string, secondUserId: number): Observable<any> {
        let params: any = {
            chat_room_id: chatRoomId,
            message: message,
            user_id: secondUserId
        };
        return this.http.post(Constants.SEND_MESSAGE, JSON.stringify(params), {})
            .map((res: Response) => res.json())
            .catch((err: Response) => {
                let details = err.json();
                return Observable.throw(details);
            });
    }
}