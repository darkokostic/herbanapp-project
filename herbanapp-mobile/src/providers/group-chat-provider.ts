import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {Observable} from "rxjs";
import {Constants} from "../shared/constants";

@Injectable()
export class GroupChatProvider {

    constructor(public http: Http) {}

    createChatGroup(name: string, checkedUsers: any): Observable<any> {
        let users: any = [];
        for(let i = 0; i < checkedUsers.length; i++) {
            users.push({
                user_id: checkedUsers[i].id
            });
        }
        let params: any = {
            users: users,
            name: name
        };
        return this.http.post(Constants.CREATE_CHAT_GROUP, JSON.stringify(params), {})
            .map((res: Response) => res.json())
    }

    getGroupsList(): Observable<any> {
        return this.http.get(Constants.GROUPS_CHAT_LIST)
            .map((res: Response) => res.json())
    }

    loadMoreGroups(url:string):Observable<any> {
        return this.http.get(url)
            .map((res: Response) => res.json())
    }

    getChatRoom(id: number): Observable<any> {
        let params: any = {
            user_id: id
        };
        return this.http.post(Constants.GET_CHAT_ROOM, JSON.stringify(params), {})
            .map((res: Response) => res.json())
    }

    getMessages(chatRoomId: number): Observable<any> {
        let params: any = {
            chat_room_id: chatRoomId
        };
        return this.http.post(Constants.GET_MESSAGES, JSON.stringify(params), {})
            .map((res: Response) => res.json())
            .catch((err: Response) => {
                let details = err.json();
                return Observable.throw(details);
            });
    }

    sendMessage(chatRoomId: number, message: string): Observable<any> {
        let params: any = {
            chat_room_id: chatRoomId,
            message: message,
        };
        return this.http.post(Constants.SEND_MESSAGE, JSON.stringify(params), {})
            .map((res: Response) => res.json())
            .catch((err: Response) => {
                let details = err.json();
                return Observable.throw(details);
            });
    }
}