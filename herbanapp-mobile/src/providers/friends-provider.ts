import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {Observable} from "rxjs";
import {Constants} from "../shared/constants";

@Injectable()
export class FriendsProvider {
    constructor(public http: Http) {}

    add(id: any): Observable<any> {
        let params: any = {
            user_id: id
        }
        return this.http.post(Constants.ADD_FRIEND_API, JSON.stringify(params), {})
            .map((res: Response) => res.json())
    }

    acceptRequest(id: any): Observable<any> {
        let params: any = {
            user_id: id
        }
        return this.http.post(Constants.ACCEPT_FRIEND_REQUEST_API, JSON.stringify(params), {})
            .map((res: Response) => res.json())
    }

    cancelRequest(id: any): Observable<any> {
        let params: any = {
            user_id: id
        }
        return this.http.post(Constants.CANCEL_FRIEND_REQUEST_API, JSON.stringify(params), {})
            .map((res: Response) => res.json())
    }

    removeFriend(id: any): Observable<any> {
        let params: any = {
            user_id: id
        }
        return this.http.post(Constants.REMOVE_FRIEND_API, JSON.stringify(params), {})
            .map((res: Response) => res.json())
    }

    getFriendsList(): Observable<any> {
        return this.http.get(Constants.FRIENDS_LIST_API)
            .map((res: Response) => res.json())
    }

    getFriendRequests(): Observable<any> {
        return this.http.get(Constants.FRIEND_REQUESTS_API)
            .map((res: Response) => res.json())
    }

    searchFriends(query:string):Observable<any> {
        let params: any = {
            search_key: query
        };
        return this.http.post(Constants.SEARCH_FRIENDS_API, JSON.stringify(params), {})
            .map((res: Response) => res.json())
    }
}
