import { Component, ViewChild } from '@angular/core';
import {NavController, NavParams, Tabs, App} from 'ionic-angular';
import {ClinicsPage} from "../../pages/clinics/clinics";
import {NewsPage} from "../../pages/news/news";
import {DiscoverPage} from "../../pages/discover/discover";
import {ChatListPage} from "../../pages/chat-list/chat-list";
import {ProfilePage} from "../../pages/profile/profile";
import {SearchPage} from "../../pages/search/search";
import {HomeTabsShared} from "../../shared/home-tabs-shared";
import {Constants} from "../../shared/constants";

@Component({
    selector: 'page-tabs',
    templateUrl: 'tabs.html'
})
export class TabsPage {
    private tabClinics: any = ClinicsPage;
    private tabNews: any = NewsPage;
    private tabDiscover: any = DiscoverPage;
    private tabChat: any = ChatListPage;
    @ViewChild('myTabs') tabRef: Tabs;

    constructor(public app: App, public navCtrl: NavController, public navParams: NavParams, public tabsShared: HomeTabsShared) {
    }

    ionViewWillEnter() {
        if(this.tabsShared.getSelectedTab() != undefined) {
            switch (this.tabsShared.getSelectedTab()) {
                case Constants.DISCOVER_TAB_INDEX:
                    this.app.getActiveNav().setRoot(this.tabDiscover);
                    break;
                case Constants.NEWS_TAB_INDEX:
                    this.app.getActiveNav().setRoot(this.tabNews);
                    break;
                case Constants.CLINICS_TAB_INDEX:
                    this.app.getActiveNav().setRoot(this.tabClinics);
                    break;
                case Constants.CHATS_TAB_INDEX:
                    this.app.getActiveNav().setRoot(this.tabChat);
                    break;
            }
        }
    }

    profile():void {
        this.navCtrl.push(ProfilePage);
    }

    search(e:any):void {
        this.navCtrl.push(SearchPage);
    }

}
