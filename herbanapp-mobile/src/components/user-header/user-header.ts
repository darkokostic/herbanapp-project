import {Component} from '@angular/core';
import {Constants} from "../../shared/constants";

@Component({
    selector: 'user-header',
    inputs: ['user: user'],
    templateUrl: 'user-header.html'
})
export class UserHeader {
    private endpoint: string;
    private userAvatar: string;
    private userCover: string;
    constructor() {
        this.endpoint = Constants.ENDPOINT;
        this.userCover = Constants.CLINIC_COVER;
        this.userAvatar = Constants.USER_AVATAR;
    }
}
