import {Component} from '@angular/core';
import {NavController, NavParams, App, Platform} from 'ionic-angular';
import {UserAboutPage} from "../../pages/user-about/user-about";
import {UserFavoritesPage} from "../../pages/user-favorites/user-favorites";
import {UserProfileShared} from "../../shared/user-profile-shared";
import {TabsPage} from "../tabs/tabs";

@Component({
    selector: 'user-profile-tabs',
    templateUrl: 'user-profile-tabs.html'
})
export class UserProfileTabs {
    private tabAbout: any = UserAboutPage;
    private tabFavorites: any = UserFavoritesPage;
    private user: any;
    private isFriendRequest: boolean;

    constructor(public app: App, public platform: Platform, public navCtrl: NavController, public navParams: NavParams, public userProfileShared: UserProfileShared) {
        this.tabAbout = UserAboutPage;
        this.tabFavorites = UserFavoritesPage;
        this.user = this.navParams.get('user');
        this.isFriendRequest = this.navParams.get('friendRequest');
        if(this.isFriendRequest == true) {
            this.userProfileShared.setIsFriendRequest(true);
        } else {
            this.userProfileShared.setIsFriendRequest(false);
        }
        if(this.user.isFriend == true) {
            this.userProfileShared.setIsFriend(true);
        } else {
            this.userProfileShared.setIsFriend(false);
        }

        this.platform.ready().then(() => {
            this.platform.registerBackButtonAction(() => {
                if(this.navCtrl.canGoBack()) {
                    this.navCtrl.pop();
                } else {
                    this.app.getRootNav().setRoot(TabsPage);
                }
            });
        });
    }

    goBack(): void {
        if(this.navCtrl.canGoBack()) {
            this.navCtrl.pop();
        } else {
            this.app.getRootNav().setRoot(TabsPage);
        }
    }
}
