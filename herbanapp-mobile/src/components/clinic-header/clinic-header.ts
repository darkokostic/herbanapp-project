import {Component} from '@angular/core';
import {Constants} from "../../shared/constants";

@Component({
    selector: 'clinic-header',
    inputs: ['clinic: clinic'],
    templateUrl: 'clinic-header.html'
})
export class ClinicHeader {
    private endpoint: string;
    private clinicLogo: string;
    private clinicCover: string;
    constructor() {
        this.endpoint = Constants.ENDPOINT;
        this.clinicLogo = Constants.CLINIC_LOGO;
        this.clinicCover = Constants.CLINIC_COVER;
    }
}
