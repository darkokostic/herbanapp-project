import {Component} from '@angular/core';
import {Constants} from "../../shared/constants";
import {UserProfileTabs} from "../user-profile-tabs/user-profile-tabs";
import {NavController} from "ionic-angular";

@Component({
    selector: 'chat-bubble',
    inputs: ['msg: message'],
    templateUrl: 'chat-bubble.html'
})
export class ChatBubblePage {
    private userAvatar: string;
    private endpoint: string;
    constructor(public navCtrl: NavController) {
        this.endpoint = Constants.ENDPOINT;
        this.userAvatar = Constants.USER_AVATAR;
    }

    goUser(user: any) {
        this.navCtrl.push(UserProfileTabs, {user: user, friendRequest: false});
    }
}
