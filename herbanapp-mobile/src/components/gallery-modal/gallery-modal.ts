import {Component} from '@angular/core';
import {NavController, NavParams, ViewController} from 'ionic-angular';

@Component({
    selector: 'gallery-modal',
    templateUrl: 'gallery-modal.html',
})
export class GalleryModal {
    private photos: any;
    private initialSlide: number;
    constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
        this.photos = this.navParams.data.photos;
        this.initialSlide = this.navParams.data.initialSlide;
    }

    modalDismiss(): void {
        this.viewCtrl.dismiss();
    }
}