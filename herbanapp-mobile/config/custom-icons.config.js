// customIcons config
module.exports = {
    iconSets: [
        {
            src: 'icons/*.svg',
            name: 'MyIcons',
            id: 'mi'
        }
    ]
};