# Ionic HerbanAPP test instructions

## First steps, required for all platforms

$ cd -root

$ npm install

$ ionic state restore

## IOS

Install xCode

$ ionic platform add ios

$ ionic build ios (If we get any error with this command, run npm install for that particular error)

If everything is completed successfully:

$ cd platforms/ios and run xCode file (double clic on it)

after that, build with xCode

## ANDROID

1) Install ANDROID SDK

2) Add Android SDK to Path

$ ionic platform add android

$ ionic run android


## Ionic View

One way to test our app is trough Ionic Team tool app called Ionic View

### Create Account

First, you need to create account at [https://apps.ionic.io/login](https://apps.ionic.io/login)

### Download Ionic View App

[Android](https://play.google.com/store/apps/details?id=com.ionic.viewapp)

[IOS](https://itunes.apple.com/us/app/ionic-view/id849930087?mt=8)

### Sync App in Ionic View

[Screenshot of Ionic View App](https://docs.ionic.io/img/ss-view-app-list.png)

Click on header left button and input APP_ID that I will send. That's it!


## Problems with Ionic View

On Ionic View some of native functionalities cannot work.

For example we are not able to test Nearby Clinics because Ionic View can't use our GPS.

Also that is the same for Social Networks Connection.


## Install APK

The other way is to pull the project where I will put .apk file for direct testing on the device.
